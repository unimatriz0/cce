<?php

/**
 *
 * espacial/lista_geo_sin_carga.php
 *
 * @package     CCE
 * @subpackage  Espacial
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.4.0 (21/08/2018)
 * @copyright   Copyright (c) 2017, INP
 *
 * Procedimiento que recibe como parámetros la clave de un operativo y
 * la clave indec de una jurisdicción, retorna el array con los laboratorios
 * que no han cargado determinaciones y las coordenadas gps de cada uno
 *
*/

// inclusión de archivos
require_once ("../reportes/reportes.class.php");
$cargadas = new Reportes();

// obtenemos la nómina
$nomina = $cargadas->detSinCargarProvincia($_GET["operativo"], $_GET["jurisdiccion"]);

// inicializamos la matriz
$jsondata = array();

// ahora recorremos el vector
foreach($nomina AS $registro){

    // obtenemos los datos
    extract($registro);

    // agregamos el registro
    $jsondata[] = array("Laboratorio" => $laboratorio,
                        "Direccion" => $direccion,
                        "Responsable" => $responsable,
                        "Coordenadas" => $coordenadas);

}

// codificamos y retornamos
echo json_encode($jsondata);

?>