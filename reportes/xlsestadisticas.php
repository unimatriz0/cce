<?php

/**
 *
 * reportes/estadisticas.php
 *
 * @package     CCE
 * @subpackage  Reportes
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.4.0 (05/09/2018)
 * @copyright   Copyright (c) 2018, INP
 *
 * Procedimiento que genera un excel con las estadísticas de
 * la plataforma
 *
*/

// inclusión de archivos
require_once ("reportes.class.php");
require_once ("../clases/phpexcel/PHPExcel.php");

// instancia la clase
$reporte = new Reportes();
$hoja = new PHPExcel();

// fijamos las propiedades
$hoja->getProperties()->setCreator("Lic. Claudio Invernizzi")
					  ->setLastModifiedBy("Lic. Claudio Invernizzi")
					  ->setTitle("Estadísticas de la Plataforma")
					  ->setSubject("Estadísticas del Sistema CCE")
					  ->setDescription("Estadísticas Generales del Sistema CCE")
					  ->setKeywords("Control Calidad")
					  ->setCategory("Reportes");

// leemos la plantilla
$hoja = PHPExcel_IOFactory::load("../clases/phpexcel/plantilla.xls");

// establecemos el ancho de las columnas (luego podemos unir celdas)
$hoja->getActiveSheet()->getColumnDimension('A')->setWidth(15);
$hoja->getActiveSheet()->getColumnDimension('B')->setWidth(15);
$hoja->getActiveSheet()->getColumnDimension('C')->setWidth(15);
$hoja->getActiveSheet()->getColumnDimension('D')->setWidth(15);
$hoja->getActiveSheet()->getColumnDimension('E')->setWidth(15);
$hoja->getActiveSheet()->getColumnDimension('F')->setWidth(15);
$hoja->getActiveSheet()->getColumnDimension('G')->setWidth(15);

// contador de filas
$fila = 10;

// fijamos el estilo del título
$estilo = array(
    'font'  => array(
        'bold'  => true,
        'size'  => 12,
        'name'  => 'Verdana'
    ));

// fijamos el estilo de los encabezados
$encabezado = array(
    'font'  => array(
        'bold'  => true,
        'size'  => 10,
        'name'  => 'Verdana'
    ));

// presenta el título
$hoja->setActiveSheetIndex(0)
     ->setCellValue('C3', 'Sistema de Control de Calidad Externo');
$hoja->setActiveSheetIndex(0)
     ->setCellValue('C5', 'Estadísticas Generales del Sitio');

// unimos las celdas del título
$hoja->getActiveSheet()->mergeCells('C3:G3');
$hoja->getActiveSheet()->mergeCells('C5:G5');

// centramos las celdas de los títulos
$hoja->getActiveSheet()->getStyle('C3')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$hoja->getActiveSheet()->getStyle('C5')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

// fijamos el estilo
$hoja->getActiveSheet()->getStyle('C3')->applyFromArray($estilo);
$hoja->getActiveSheet()->getStyle('C5')->applyFromArray($estilo);

// establecemos la fuente por defecto
$hoja->getDefaultStyle()->getFont()->setName('Arial')
     ->setSize(10);

// obtiene el número de registros
$registros = $reporte->Registros("chag_datos");

// presenta el total de determinaciones
$hoja->setActiveSheetIndex(0)
        ->setCellValue('A' . $fila , "Total de Determinaciones Cargadas:")
        ->setCellValue('D' . $fila , $registros);
$hoja->getActiveSheet()->getStyle('D' . $fila)->getNumberFormat()->setFormatCode("#,###");
$hoja->getActiveSheet()->getStyle('A' . $fila)->applyFromArray($encabezado);
$hoja->getActiveSheet()->getStyle('D' . $fila)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

// unimos las celdas
$hoja->getActiveSheet()->mergeCells('A' . $fila . ':C' . $fila);

// incrementa el contador
$fila++;

// obtiene el número de registros por operativo
$resultado = $reporte->DeterminacionesOperativo();

// presenta el título
$hoja->setActiveSheetIndex(0)->setCellValue('A' . $fila , "Distribución por Operativo");
$hoja->getActiveSheet()->getStyle('A' . $fila)->applyFromArray($encabezado);

// presenta los encabezados y formatea el estilo
$hoja->setActiveSheetIndex(0)->setCellValue('A' . $fila , "Operativo")
                             ->setCellValue('B' . $fila , "Determ.");
$hoja->getActiveSheet()->getStyle('A' . $fila)->applyFromArray($encabezado);
$hoja->getActiveSheet()->getStyle('B' . $fila)->applyFromArray($encabezado);
$hoja->getActiveSheet()->getStyle('A' . $fila)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$hoja->getActiveSheet()->getStyle('B' . $fila)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

// incrementa el contador
$fila++;

// inicia el bucle recorriendo el vector
foreach ($resultado AS $registro){

    // obtiene el registro
    extract($registro);

    // presenta los datos
    $hoja->setActiveSheetIndex(0)->setCellValue('A' . $fila , $operativo)
                                 ->setCellValue('B' . $fila , $registros);
    $hoja->getActiveSheet()->getStyle('A' . $fila)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    $hoja->getActiveSheet()->getStyle('B'. $fila)->getNumberFormat()->setFormatCode("#,###");

    // incrementa el contador
    $fila++;

}

// incrementa el contador
$fila++;

// presenta el título
$hoja->setActiveSheetIndex(0)->setCellValue('A' . $fila , "Distribución de Laboratorios");
$hoja->getActiveSheet()->getStyle('A' . $fila)->applyFromArray($encabezado);
$hoja->getActiveSheet()->mergeCells('A' . $fila . ':C' . $fila);

// incrementa el contador
$fila++;

// determina el número de laboratorios ingresados
$registros = $reporte->Registros("cce.laboratorios");

// presenta el total
$hoja->setActiveSheetIndex(0)->setCellValue('A' . $fila , "Laboratorios Ingresados (Total):")
                             ->setCellValue('D' . $fila , number_format($registros, 0, ",", "."));
$hoja->getActiveSheet()->getStyle('A' . $fila)->applyFromArray($encabezado);
$hoja->getActiveSheet()->mergeCells('A' . $fila . ':C' . $fila);
$hoja->getActiveSheet()->getStyle('D' . $fila)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

// incrementa el contador
$fila++;

// obtenemos la clasificación por financiamiento
$resultado = $reporte->FinanciamientoLaboratorios();

// inicializa el contador
$total_laboratorios = 0;

// presenta el título
$hoja->setActiveSheetIndex(0)->setCellValue('A' . $fila , "Dependencia")
                             ->setCellValue('B' . $fila , "N°");
$hoja->getActiveSheet()->getStyle('A' . $fila)->applyFromArray($encabezado);
$hoja->getActiveSheet()->getStyle('B' . $fila)->applyFromArray($encabezado);

// incrementa el contador
$fila++;

// iniciamos un bucle
foreach ($resultado AS $registro){

    // obtiene el registro
    extract($registro);

    // incrementa el contador de total de laboratorios
    $total_laboratorios += $laboratorios;

    // presenta los laboratorios por fuente de financiamiento
    $hoja->setActiveSheetIndex(0)->setCellValue('A' . $fila , $dependencia)
                                 ->setCellValue('B' . $fila , $laboratorios);

    // incrementa el contador
    $fila++;

}

$hoja->setActiveSheetIndex(0)->setCellValue('A' . $fila , "Total:")
                             ->setCellValue('B' . $fila , $total_laboratorios);
$hoja->getActiveSheet()->getStyle('A' . $fila)->applyFromArray($encabezado);
$hoja->getActiveSheet()->getStyle('B' . $fila)->applyFromArray($encabezado);
$hoja->getActiveSheet()->getStyle('B'. $fila)->getNumberFormat()->setFormatCode("#,###");

// incrementa el contador
$fila = $fila+2;

// presenta el título
$hoja->setActiveSheetIndex(0)->setCellValue('A' . $fila , "Distribución por Jurisdicción");
$hoja->getActiveSheet()->getStyle('A' . $fila)->applyFromArray($encabezado);
$hoja->getActiveSheet()->mergeCells('A' . $fila . ':C' . $fila);

// incrementa el contador
$fila++;

// presenta los encabezados de columna
$hoja->setActiveSheetIndex(0)->setCellValue('A' . $fila , "Jurisdicción")
                             ->setCellValue('C' . $fila , "Laboratorios");
$hoja->getActiveSheet()->getStyle('A' . $fila)->applyFromArray($encabezado);
$hoja->getActiveSheet()->getStyle('C' . $fila)->applyFromArray($encabezado);
$hoja->getActiveSheet()->mergeCells('A' . $fila . ':B' . $fila);

// incrementa el contador
$fila++;

// obtiene la distribución de laboratorios
$resultado = $reporte->JurisdiccionLaboratorios();

// iniciamos un bucle
foreach ($resultado AS $registro){

    // obtenemos el registro
    extract($registro);

    // lo presenta
    $hoja->setActiveSheetIndex(0)->setCellValue('A' . $fila , $jurisdiccion)
                                 ->setCellValue('C' . $fila , $laboratorios);
    $hoja->getActiveSheet()->mergeCells('A' . $fila . ':B' . $fila);

    // incrementa el contador
    $fila++;

}

// incrementa el contador
$fila++;

// determina el número de localidades cargadas
$registros = $reporte->Registros("diccionarios.localidades");

// presenta el título
$hoja->setActiveSheetIndex(0)->setCellValue('A' . $fila , "Localidades Registradas:")
                             ->setCellValue('C' . $fila , $registros);
$hoja->getActiveSheet()->getStyle('A' . $fila)->applyFromArray($encabezado);
$hoja->getActiveSheet()->getStyle('C' . $fila)->applyFromArray($encabezado);
$hoja->getActiveSheet()->mergeCells('A' . $fila . ':B' . $fila);
$hoja->getActiveSheet()->getStyle('C'. $fila)->getNumberFormat()->setFormatCode("#,###");

// incrementa el contador
$fila++;

// determina el número de operativos realizados
$registros = $reporte->Registros("operativos_chagas");

// presenta el número de operativos
$hoja->setActiveSheetIndex(0)->setCellValue('A' . $fila , "Operativos de Calidad:")
                             ->setCellValue('C' . $fila , $registros);
$hoja->getActiveSheet()->getStyle('A' . $fila)->applyFromArray($encabezado);
$hoja->getActiveSheet()->getStyle('C' . $fila)->applyFromArray($encabezado);
$hoja->getActiveSheet()->mergeCells('A' . $fila . ':B' . $fila);

// incrementa la fila
$fila++;

// determina el número de responsables (histórico)
$registros = $reporte->Registros("responsables");

$hoja->setActiveSheetIndex(0)->setCellValue('A' . $fila , "Responsables Registradas:")
                             ->setCellValue('C' . $fila , $registros);
$hoja->getActiveSheet()->getStyle('A' . $fila)->applyFromArray($encabezado);
$hoja->getActiveSheet()->getStyle('C' . $fila)->applyFromArray($encabezado);
$hoja->getActiveSheet()->mergeCells('A' . $fila . ':B' . $fila);

// incrementa el contador
$fila = $fila+2;

// presentamos el título
$hoja->setActiveSheetIndex(0)->setCellValue('A' . $fila , "Estadísticas Interanuales");
$hoja->getActiveSheet()->getStyle('A' . $fila)->applyFromArray($encabezado);
$hoja->getActiveSheet()->mergeCells('A' . $fila . ':B' . $fila);

// incrementa la fila
$fila++;

// presenta el título
$hoja->setActiveSheetIndex(0)->setCellValue('A' . $fila , "Laboratorios participantes por Año");
$hoja->getActiveSheet()->getStyle('A' . $fila)->applyFromArray($encabezado);
$hoja->getActiveSheet()->mergeCells('A' . $fila . ':C' . $fila);

// incrementa la fila
$fila++;

// presenta los encabezados
$hoja->setActiveSheetIndex(0)->setCellValue('A' . $fila , "Año")
                             ->setCellValue('B' . $fila , "Laboratorios");
$hoja->getActiveSheet()->getStyle('A' . $fila)->applyFromArray($encabezado);
$hoja->getActiveSheet()->getStyle('B' . $fila)->applyFromArray($encabezado);
$hoja->getActiveSheet()->getStyle('A' . $fila)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$hoja->getActiveSheet()->getStyle('B' . $fila)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

// incrementa la fila
$fila++;

// obtenemos la tabla de participantes
$resultado = $reporte->getLaboratoriosAnios();

// iniciamos un bucle
foreach ($resultado AS $registro){

    // obtenemos el registro
    extract($registro);

    // presenta el registro
    $hoja->setActiveSheetIndex(0)->setCellValue('A' . $fila , $anio)
                                 ->setCellValue('B' . $fila , $laboratorios);
    $hoja->getActiveSheet()->getStyle('A' . $fila)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    $hoja->getActiveSheet()->getStyle('B' . $fila)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

    // incrementa el contador
    $fila++;

}

// incrementa el contador
$fila++;

$hoja->setActiveSheetIndex(0)->setCellValue('A' . $fila , "Técnicas Utilizadas");
$hoja->getActiveSheet()->getStyle('A' . $fila)->applyFromArray($encabezado);
$hoja->getActiveSheet()->mergeCells('A' . $fila . ':C' . $fila);

// incrementa el contador
$fila++;

// define los encabezados
$hoja->setActiveSheetIndex(0)->setCellValue('A' . $fila , "Año")
                             ->setCellValue('B' . $fila , "1 Técnicas")
                             ->setCellValue('C' . $fila , "2 Técnicas")
                             ->setCellValue('D' . $fila , "3 Técnicas")
                             ->setCellValue('E' . $fila , "4 Técnicas");
$hoja->getActiveSheet()->getStyle('A' . $fila)->applyFromArray($encabezado);
$hoja->getActiveSheet()->getStyle('B' . $fila)->applyFromArray($encabezado);
$hoja->getActiveSheet()->getStyle('C' . $fila)->applyFromArray($encabezado);
$hoja->getActiveSheet()->getStyle('D' . $fila)->applyFromArray($encabezado);
$hoja->getActiveSheet()->getStyle('E' . $fila)->applyFromArray($encabezado);
$hoja->getActiveSheet()->getStyle('A' . $fila)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$hoja->getActiveSheet()->getStyle('B' . $fila)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$hoja->getActiveSheet()->getStyle('C' . $fila)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$hoja->getActiveSheet()->getStyle('D' . $fila)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

// incrementa el contador
$fila++;

// ahora obtenemos la matriz
$tecnicas = $reporte->getTecnicasAnio();

// recorremos el vector
foreach ($tecnicas as $anio => $datos){

    // presenta el año
    $hoja->setActiveSheetIndex(0)->setCellValue('A' . $fila , $anio);

    // si existen con una técnica
    if (array_key_exists("Tecnica1", $datos)){

        // lo presenta
        $hoja->setActiveSheetIndex(0)->setCellValue('B' . $fila , $datos["Tecnica1"]);

    }

    // si existen con dos técnicas
    if (array_key_exists("Tecnica2", $datos)){

        // lo presenta
        $hoja->setActiveSheetIndex(0)->setCellValue('C' . $fila , $datos["Tecnica2"]);

    }

    // si existen con tres técnicas
    if (array_key_exists("Tecnica3", $datos)){

        // lo presenta
        $hoja->setActiveSheetIndex(0)->setCellValue('D' . $fila , $datos["Tecnica3"]);

    }

    // si existen con cuatro técnicas
    if (array_key_exists("Tecnica4", $datos)){

        // lo presenta
        $hoja->setActiveSheetIndex(0)->setCellValue('E' . $fila , $datos["Tecnica4"]);
    }

    // centramos el contenido
    $hoja->getActiveSheet()->getStyle('A' . $fila)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    $hoja->getActiveSheet()->getStyle('B' . $fila)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    $hoja->getActiveSheet()->getStyle('C' . $fila)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    $hoja->getActiveSheet()->getStyle('D' . $fila)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    $hoja->getActiveSheet()->getStyle('E' . $fila)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

    // incrementa el contador
    $fila++;

}

// incrementa el contador
$fila++;

// presenta el título
$hoja->setActiveSheetIndex(0)->setCellValue('A' . $fila , "Determinaciones Realizadas según Técnica");
$hoja->getActiveSheet()->getStyle('A' . $fila)->applyFromArray($encabezado);
$hoja->getActiveSheet()->mergeCells('A' . $fila . ':C' . $fila);

// incrementa el contador
$fila++;

// presenta los títulos
$hoja->setActiveSheetIndex(0)->setCellValue('A' . $fila , "Año")
                             ->setCellValue('B' . $fila , "AP")
                             ->setCellValue('C' . $fila , "HAI")
                             ->setCellValue('D' . $fila , "ELISA")
                             ->setCellValue('E' . $fila , "IFI")
                             ->setCellValue('F' . $fila , "QUIMIO")
                             ->setCellValue('G' . $fila , "OTRAS");
$hoja->getActiveSheet()->getStyle('A' . $fila)->applyFromArray($encabezado);
$hoja->getActiveSheet()->getStyle('B' . $fila)->applyFromArray($encabezado);
$hoja->getActiveSheet()->getStyle('C' . $fila)->applyFromArray($encabezado);
$hoja->getActiveSheet()->getStyle('D' . $fila)->applyFromArray($encabezado);
$hoja->getActiveSheet()->getStyle('E' . $fila)->applyFromArray($encabezado);
$hoja->getActiveSheet()->getStyle('F' . $fila)->applyFromArray($encabezado);
$hoja->getActiveSheet()->getStyle('G' . $fila)->applyFromArray($encabezado);
$hoja->getActiveSheet()->getStyle('A' . $fila)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$hoja->getActiveSheet()->getStyle('B' . $fila)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$hoja->getActiveSheet()->getStyle('C' . $fila)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$hoja->getActiveSheet()->getStyle('E' . $fila)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$hoja->getActiveSheet()->getStyle('D' . $fila)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$hoja->getActiveSheet()->getStyle('F' . $fila)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$hoja->getActiveSheet()->getStyle('G' . $fila)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

// incrementa el contador
$fila++;

// obtenemos la matriz
$tecnicas = $reporte->getMetodosAnio();

// ahora recorremos la matriz
foreach ($tecnicas as $anio => $datos){

    // presenta el año
    $hoja->setActiveSheetIndex(0)->setCellValue('A' . $fila , $anio);

    // si existen determinaciones AP
    if (array_key_exists("AP", $datos)){

        // lo presenta
        $hoja->setActiveSheetIndex(0)->setCellValue('B' . $fila , $datos["AP"]);

    }

    // si existen determinaciones HAI
    if (array_key_exists("HAI", $datos)){

        // lo presenta
        $hoja->setActiveSheetIndex(0)->setCellValue('C' . $fila , $datos["HAI"]);

    }

    // si existen determinaciones ELISA
    if (array_key_exists("ELISA", $datos)){

        // lo presenta
        $hoja->setActiveSheetIndex(0)->setCellValue('D' . $fila , $datos["ELISA"]);

    }

    // si existen determinaciones IFI
    if (array_key_exists("IFI", $datos)){

        // lo presenta
        $hoja->setActiveSheetIndex(0)->setCellValue('E' . $fila , $datos["IFI"]);

    }

    // si existen determinaciones QUIMIO
    if (array_key_exists("QUIMIOLUMINISCENCIA", $datos)){

        // lo presenta
        $hoja->setActiveSheetIndex(0)->setCellValue('F' . $fila , $datos["QUIMIOLUMINISCENCIA"]);

    }

    // si existen OTRAS determinaciones
    if (array_key_exists("OTRA", $datos)){

        // lo presenta
        $hoja->setActiveSheetIndex(0)->setCellValue('G' . $fila , $datos["OTRA"]);

    }

    // centramos el contenido
    $hoja->getActiveSheet()->getStyle('A' . $fila)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    $hoja->getActiveSheet()->getStyle('B' . $fila)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    $hoja->getActiveSheet()->getStyle('C' . $fila)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    $hoja->getActiveSheet()->getStyle('D' . $fila)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    $hoja->getActiveSheet()->getStyle('E' . $fila)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    $hoja->getActiveSheet()->getStyle('F' . $fila)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    $hoja->getActiveSheet()->getStyle('G' . $fila)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

    // fijamos el formato numérico
    $hoja->getActiveSheet()->getStyle('B'. $fila)->getNumberFormat()->setFormatCode("#,###");
    $hoja->getActiveSheet()->getStyle('C'. $fila)->getNumberFormat()->setFormatCode("#,###");
    $hoja->getActiveSheet()->getStyle('D'. $fila)->getNumberFormat()->setFormatCode("#,###");
    $hoja->getActiveSheet()->getStyle('E'. $fila)->getNumberFormat()->setFormatCode("#,###");
    $hoja->getActiveSheet()->getStyle('F'. $fila)->getNumberFormat()->setFormatCode("#,###");
    $hoja->getActiveSheet()->getStyle('G'. $fila)->getNumberFormat()->setFormatCode("#,###");

    // incrementa el contador
    $fila++;

}

// incrementa el contador
$fila++;

// presenta el título
$hoja->setActiveSheetIndex(0)->setCellValue('A' . $fila , "Métodos Utilizados en ELISA");
$hoja->getActiveSheet()->getStyle('A' . $fila)->applyFromArray($encabezado);
$hoja->getActiveSheet()->mergeCells('A' . $fila . ':C' . $fila);

// incrementa el contador
$fila++;

// presenta los encabezados
$hoja->setActiveSheetIndex(0)->setCellValue('A' . $fila , "Año")
                             ->setCellValue('B' . $fila , "Lector")
                             ->setCellValue('C' . $fila , "Visual");
$hoja->getActiveSheet()->getStyle('A' . $fila)->applyFromArray($encabezado);
$hoja->getActiveSheet()->getStyle('B' . $fila)->applyFromArray($encabezado);
$hoja->getActiveSheet()->getStyle('C' . $fila)->applyFromArray($encabezado);
$hoja->getActiveSheet()->getStyle('A' . $fila)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$hoja->getActiveSheet()->getStyle('B' . $fila)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$hoja->getActiveSheet()->getStyle('C' . $fila)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

// incrementa el contador
$fila++;

// obtenemos la matriz
$metodos = $reporte->getLectorVisual();

// ahora recorremos la matriz
foreach ($metodos as $anio => $datos){

    // presenta el año
    $hoja->setActiveSheetIndex(0)->setCellValue('A' . $fila , $anio);

    // si existen determinaciones con lector
    if (array_key_exists("Lector", $datos)){

        // lo presenta
        $hoja->setActiveSheetIndex(0)->setCellValue('B' . $fila , $datos["Lector"]);

    }

    // si existen determinaciones visuales
    if (array_key_exists("Visual", $datos)){

        // lo presenta
        $hoja->setActiveSheetIndex(0)->setCellValue('C' . $fila , $datos["Visual"]);

    }

    // centramos el contenido
    $hoja->getActiveSheet()->getStyle('A' . $fila)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    $hoja->getActiveSheet()->getStyle('B' . $fila)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    $hoja->getActiveSheet()->getStyle('C' . $fila)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

    // fijamos el formato numérico
    $hoja->getActiveSheet()->getStyle('B'. $fila)->getNumberFormat()->setFormatCode("#,###");
    $hoja->getActiveSheet()->getStyle('C'. $fila)->getNumberFormat()->setFormatCode("#,###");

    // incrementa el contador
    $fila++;

}

// incrementa el contador
$fila++;

// presentamos el título de eficiencia
$hoja->setActiveSheetIndex(0)->setCellValue('A' . $fila , "Eficiencia General del Sistema");
$hoja->getActiveSheet()->getStyle('A' . $fila)->applyFromArray($encabezado);
$hoja->getActiveSheet()->mergeCells('A' . $fila . ':C' . $fila);

// incrementa el contador
$fila++;

// presentamos los encabezados
$hoja->setActiveSheetIndex(0)->setCellValue('A' . $fila , "Año")
                             ->setCellValue('B' . $fila , "Determ.")
                             ->setCellValue('C' . $fila , "Correctas")
                             ->setCellValue('D' . $fila , "Eficiencia");
$hoja->getActiveSheet()->getStyle('A' . $fila)->applyFromArray($encabezado);
$hoja->getActiveSheet()->getStyle('B' . $fila)->applyFromArray($encabezado);
$hoja->getActiveSheet()->getStyle('C' . $fila)->applyFromArray($encabezado);
$hoja->getActiveSheet()->getStyle('D' . $fila)->applyFromArray($encabezado);
$hoja->getActiveSheet()->getStyle('A' . $fila)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$hoja->getActiveSheet()->getStyle('B' . $fila)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$hoja->getActiveSheet()->getStyle('C' . $fila)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$hoja->getActiveSheet()->getStyle('D' . $fila)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

// incrementa el contador
$fila++;

// obtenemos la matriz de eficiencia
$eficiencia = $reporte->getEficiencia();

// ahora recorremos la matriz
foreach ($eficiencia as $anio => $datos){

    // presenta el año
    $hoja->setActiveSheetIndex(0)->setCellValue('A' . $fila , $anio);

    // si hay determinaciones
    if (array_key_exists("Determinaciones", $datos)){

        // lo presenta
        $hoja->setActiveSheetIndex(0)->setCellValue('B' . $fila , $datos["Determinaciones"]);

    }

    // si hay correctas
    if (array_key_exists("Correctas", $datos)){

        // lo presenta
        $hoja->setActiveSheetIndex(0)->setCellValue('C' . $fila , $datos["Correctas"]);

    }

    // si hubo determinaciones y también hubo determinaciones correctas
    if (array_key_exists("Correctas", $datos) && array_key_exists("Determinaciones", $datos)){

        // calcula la eficiencia y la formatea
        $correctas = $datos["Correctas"] / $datos["Determinaciones"];
        $correctas = $correctas * 100;
        $correctas = number_format($correctas, 2, ",", ".");

        // lo presenta
        $hoja->setActiveSheetIndex(0)->setCellValue('D' . $fila , $correctas);

    }

    // centramos el contenido
    $hoja->getActiveSheet()->getStyle('A' . $fila)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    $hoja->getActiveSheet()->getStyle('B' . $fila)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    $hoja->getActiveSheet()->getStyle('C' . $fila)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    $hoja->getActiveSheet()->getStyle('D' . $fila)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

    // fijamos el formato numérico
    $hoja->getActiveSheet()->getStyle('B'. $fila)->getNumberFormat()->setFormatCode("#,###");
    $hoja->getActiveSheet()->getStyle('C'. $fila)->getNumberFormat()->setFormatCode("#,###");
    $hoja->getActiveSheet()->getStyle('B'. $fila)->getNumberFormat()->setFormatCode("#,##0,00");

    // incrementa el contador
    $fila++;

}

// incrementamos el contador
$fila++;

// definimos el título
$hoja->setActiveSheetIndex(0)->setCellValue('A' . $fila , "Eficiencia Interanual según Técnica");
$hoja->getActiveSheet()->getStyle('A' . $fila)->applyFromArray($encabezado);

// incrementamos el contador
$fila++;

// definimos los títulos
$hoja->setActiveSheetIndex(0)->setCellValue('A' . $fila , "Año")
                             ->setCellValue('B' . $fila , "AP")
                             ->setCellValue('C' . $fila , "HAI")
                             ->setCellValue('D' . $fila , "ELISA")
                             ->setCellValue('E' . $fila , "IFI")
                             ->setCellValue('F' . $fila , "QUIMIO")
                             ->setCellValue('G' . $fila , "OTRAS");
$hoja->getActiveSheet()->getStyle('A' . $fila)->applyFromArray($encabezado);
$hoja->getActiveSheet()->getStyle('B' . $fila)->applyFromArray($encabezado);
$hoja->getActiveSheet()->getStyle('C' . $fila)->applyFromArray($encabezado);
$hoja->getActiveSheet()->getStyle('D' . $fila)->applyFromArray($encabezado);
$hoja->getActiveSheet()->getStyle('E' . $fila)->applyFromArray($encabezado);
$hoja->getActiveSheet()->getStyle('F' . $fila)->applyFromArray($encabezado);
$hoja->getActiveSheet()->getStyle('G' . $fila)->applyFromArray($encabezado);
$hoja->getActiveSheet()->getStyle('A' . $fila)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$hoja->getActiveSheet()->getStyle('B' . $fila)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$hoja->getActiveSheet()->getStyle('C' . $fila)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$hoja->getActiveSheet()->getStyle('D' . $fila)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$hoja->getActiveSheet()->getStyle('E' . $fila)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$hoja->getActiveSheet()->getStyle('F' . $fila)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$hoja->getActiveSheet()->getStyle('G' . $fila)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

// incrementa el contador
$fila++;

// obtenemos la tabla de eficiencia
$eficiencia = $reporte->getEficienciaTecnicas();

// ahora recorremos la matriz
foreach ($eficiencia as $anio => $datos){

    // agregamos el año
    $hoja->setActiveSheetIndex(0)->setCellValue('A' . $fila , $anio);

    // si hubo determinaciones ap y determinaciones
    // correctas ap
    if (array_key_exists("CorrectasAP", $datos) && array_key_exists("DeterminacionesAP", $datos)){

        // calcula la eficiencia y la formatea
        $correctas = $datos["CorrectasAP"] / $datos["DeterminacionesAP"];
        $correctas = $correctas * 100;
        $correctas = number_format($correctas, 2, ",", ".");

        // lo presenta
        $hoja->setActiveSheetIndex(0)->setCellValue('B' . $fila , $correctas);

    }

    // si hubo determinaciones hai y determinaciones
    // correctas hai
    if (array_key_exists("CorrectasHAI", $datos) && array_key_exists("DeterminacionesHAI", $datos)){

        // calcula la eficiencia y la formatea
        $correctas = $datos["CorrectasHAI"] / $datos["DeterminacionesHAI"];
        $correctas = $correctas * 100;
        $correctas = number_format($correctas, 2, ",", ".");

        // lo presenta
        $hoja->setActiveSheetIndex(0)->setCellValue('C' . $fila , $correctas);

    }

    // si hubo determinaciones elisa y determinaciones
    // correctas elisa
    if (array_key_exists("CorrectasELISA", $datos) && array_key_exists("DeterminacionesELISA", $datos)){

        // calcula la eficiencia y la formatea
        $correctas = $datos["CorrectasELISA"] / $datos["DeterminacionesELISA"];
        $correctas = $correctas * 100;
        $correctas = number_format($correctas, 2, ",", ".");

        // lo presenta
        $hoja->setActiveSheetIndex(0)->setCellValue('D' . $fila , $correctas);

    }

    // si hubo determinaciones ifi y determinaciones
    // correctas ifi
    if (array_key_exists("CorrectasIFI", $datos) && array_key_exists("DeterminacionesIFI", $datos)){

        // calcula la eficiencia y la formatea
        $correctas = $datos["CorrectasIFI"] / $datos["DeterminacionesIFI"];
        $correctas = $correctas * 100;
        $correctas = number_format($correctas, 2, ",", ".");

        // lo presenta
        $hoja->setActiveSheetIndex(0)->setCellValue('E' . $fila , $correctas);

    }

    // si hubo determinaciones quimio y determinaciones
    // correctas quimio
    if (array_key_exists("CorrectasQUIMIO", $datos) && array_key_exists("DeterminacionesQUIMIO", $datos)){

        // calcula la eficiencia y la formatea
        $correctas = $datos["CorrectasQUIMIO"] / $datos["DeterminacionesQUIMIO"];
        $correctas = $correctas * 100;
        $correctas = number_format($correctas, 2, ",", ".");

        // lo presenta
        $hoja->setActiveSheetIndex(0)->setCellValue('F' . $fila , $correctas);

    }

    // si hubo otras determinaciones y determinaciones correctas
    if (array_key_exists("CorrectasOTRA", $datos) && array_key_exists("DeterminacionesOTRA", $datos)){

        // calcula la eficiencia y la formatea
        $correctas = $datos["CorrectasAP"] / $datos["DeterminacionesAP"];
        $correctas = $correctas * 100;
        $correctas = number_format($correctas, 2, ",", ".");

        // lo presenta
        $hoja->setActiveSheetIndex(0)->setCellValue('G' . $fila , $correctas);

    }

    // centramos el contenido
    $hoja->getActiveSheet()->getStyle('A' . $fila)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    $hoja->getActiveSheet()->getStyle('B' . $fila)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    $hoja->getActiveSheet()->getStyle('C' . $fila)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    $hoja->getActiveSheet()->getStyle('D' . $fila)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    $hoja->getActiveSheet()->getStyle('E' . $fila)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    $hoja->getActiveSheet()->getStyle('F' . $fila)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    $hoja->getActiveSheet()->getStyle('G' . $fila)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

    // fijamos el formato numérico
    $hoja->getActiveSheet()->getStyle('B'. $fila)->getNumberFormat()->setFormatCode("#,##0,00");
    $hoja->getActiveSheet()->getStyle('C'. $fila)->getNumberFormat()->setFormatCode("#,##0,00");
    $hoja->getActiveSheet()->getStyle('D'. $fila)->getNumberFormat()->setFormatCode("#,##0,00");
    $hoja->getActiveSheet()->getStyle('E'. $fila)->getNumberFormat()->setFormatCode("#,##0,00");
    $hoja->getActiveSheet()->getStyle('F'. $fila)->getNumberFormat()->setFormatCode("#,##0,00");
    $hoja->getActiveSheet()->getStyle('G'. $fila)->getNumberFormat()->setFormatCode("#,##0,00");

    // incrementa el contador
    $fila++;

}

// renombramos la hoja
$hoja->getActiveSheet()->setTitle('Estadísticas');

// fijamos la primer hoja como activa para abrirla predeterminada
$hoja->setActiveSheetIndex(0);

// creamos el writer y lo dirigimos al navegador en formato 2007
header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment;filename="estadisticas.xlsx"');
header('Cache-Control: max-age=0');
header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // fecha en el pasado
header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // siempre modificado
header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
header ('Pragma: public'); // HTTP/1.0
$Writer = PHPExcel_IOFactory::createWriter($hoja, 'Excel2007');
$Writer->save('php://output');

?>