<?php

/**
 *
 * reportes/det_sin_carga.php
 *
 * @package     CCE
 * @subpackage  Reportes
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.4.0 (18/08/2018)
 * @copyright   Copyright (c) 2018, INP
 *
 * Método que presenta la grilla con los laboratorios que no han cargado
 * determinaciones
 *
*/

// incluimos e instanciamos la clase
require_once ("reportes.class.php");
$reporte = new Reportes();

// obtenemos la nómina de laboratorios
$laboratorios = $reporte->determinacionesSinCargar($_GET["operativo"]);

// si hubo registros
if (count($laboratorios) != 0){

    // definimos la tabla
    echo "<table width='90%' align='center' border='0' id='det_sin_carga'>";

    // definimos el encabezado
    echo "<thead>";
    echo "<tr>";
    echo "<th>Jurisdicción</th>";
    echo "<th>Laboratorio</th>";
    echo "</tr>";
    echo "</thead>";

    // definimos el cuerpo de la tabla
    echo "<tbody>";

    // iniciamos un bucle recorriendo el vector
    foreach ($laboratorios AS $registro){

        // obtenemos el registro
        extract($registro);

        // lo agregamos
        echo "<tr>";
        echo "<td>$jurisdiccion</td>";
        echo "<td>$laboratorio</td>";
        echo "</tr>";

    }

    // cerramos la tabla
    echo "</tbody>";
    echo "</table>";

    // insertamos un separador
    echo "<br>";

    // define el div para el paginador
    echo "<div class='paging'></div>";

    // presentamos el botón exportar
    echo "<p align='center'>";
    echo "<input type='button'
                 name='btnExportar'
                 id='btnExportar'
                 value='Exportar'
                 title='Exporta la tabla a Hoja de Cálculo'
                 class='boton_analitica'
                 onClick='reportes.xlsSinCarga()'>";
    echo "</p>";

// si no hubo registros
} else {

    // presenta el mensaje
    echo "<h2>Todos los laboratorios que recibieron muestras han cargado ";
    echo "determinaciones en el Operativo solicitado</h2>";

}
?>
<script>

    // definimos las propiedades de la tabla
     $('#det_sin_carga').datatable({
        pageSize: 15,
        sort: [true, true],
        filters: ['select', true],
        filterText: 'Buscar ... '
    });

</script>