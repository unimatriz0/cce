<?php

/**
 *
 * Class Reportes | reportes/reportes.class.php
 *
 * @package     CCE
 * @subpackage  Reportes
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.4.0 (20/08/2018)
 * @copyright   Copyright (c) 2018, INP
 *
 * Clase que provee los métodos para generar reportes de participación
 * de los laboratorios
*/

// inclusión de archivos
require_once ("../clases/conexion.class.php");

// convención para la nomenclatura de las propiedades, comienzan con una
// letra mayúscula, de tener mas de una palabra no se utilizan separadores
// y la inicial de cada palabra va en mayúscula
// para las variables recibidas como parámetro el criterio es todas en
// minúscula

// convención para la nomenclatura de los metodos, comienzan con set o get
// según asignen un valor o lo lean y luego el nombre del valor a obtener

/**
 * Definición de la clase
 * @author Claudio Invernizzi <cinvernizzi@gmail.com>
 */
class Reportes {

    // declaración de variables de la clase, las declaramos
    // como protected para que puedan ser heredadas pero
    // para asignarle el valor debemos crear los métodos
    protected $Link;                  // puntero a la base de datos
    protected $NivelCentral;          // indica si el usuario es de nivel central
    protected $Jurisdiccion;          // Jurisdicción del usuario
    protected $CodProv;               // clave indec de la jurisdicción

    /**
     * Constructor de la clase, instanciamos la conexión con la base
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     */
    function __construct (){

        // nos conectamos a la base de datos
        $this->Link = new Conexion();

        // iniciamos sesión
        session_start();

        // si existe la sesión
        if(isset($_SESSION["ID"])){

            // obtenemos los datos del usuario
            $this->NivelCentral = $_SESSION["NivelCentral"];
            $this->Jurisdiccion = $_SESSION["Jurisdiccion"];
            $this->CodProv = $_SESSION["CodProv"];

        }

        // cerramos sesión
        session_write_close();

    }

    /**
     * Destructor de la clase, cerramos la conexión con la base
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     */
    function __destruct(){

        // elimina el enlace a la base
        $this->Link = null;

    }

    /**
     * Método que recibe como parámetro el operativo a informar,
     * según si el usuario es de nivel central o jurisdiccional
     * reporta en un array las determinaciones cargadas por los
     * laboratorios de la jurisdicción o por todo el país
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param string $operativo - cadena con el número de operativo
     * @return array $registros
     */
    public function determinacionesCargadas($operativo){

        // si no es de nivel central
        if ($this->NivelCentral == "No"){

            // obtenemos las determinaciones
            $registros = $this->detCargadasProvincias($operativo);

        // si es de nivel central
        } else {

            // obtenemos las determinaciones
            $registros = $this->detCargadasNivelCentral($operativo);

        }

        // retornamos el vector
        return $registros;

    }

    /**
     * Método protegido que recibe como parámetro el año a reportar de
     * las determinaciones cargadas de todas las provincias
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param string $operativo - operativo a reportar
     * @return array $registros
     */
    protected function detCargadasNivelCentral($operativo){

        // consulta todo el país
        $consulta = "SELECT COUNT(cce.chag_datos.ID) AS determinaciones,
                            diccionarios.provincias.NOM_PROV AS jurisdiccion,
                            cce.laboratorios.NOMBRE AS laboratorio
                     FROM cce.chag_datos INNER JOIN cce.laboratorios ON cce.chag_datos.LABORATORIO = cce.laboratorios.ID
                                         INNER JOIN cce.operativos_chagas ON cce.chag_datos.OPERATIVO = cce.operativos_chagas.ID
                                         INNER JOIN diccionarios.localidades ON cce.laboratorios.LOCALIDAD = diccionarios.localidades.CODLOC
                                         INNER JOIN diccionarios.provincias ON diccionarios.localidades.CODPCIA = diccionarios.provincias.COD_PROV
                     WHERE cce.operativos_chagas.ID = '$operativo'
                     GROUP BY diccionarios.provincias.NOM_PROV,
                              cce.laboratorios.NOMBRE
                     ORDER BY diccionarios.provincias.NOM_PROV,
                              cce.laboratorios.NOMBRE; ";

        // obtenemos el vector
        $resultado = $this->Link->query($consulta);
        $registros = array_change_key_case($resultado->fetchAll(PDO::FETCH_ASSOC), CASE_LOWER);

        // retornamos el vector
        return $registros;

    }

    /**
     * Método protegido que recibe como parámetro el operativo a reportar
     * y retorna los laboratorios con determinaciones de la jurisdicción
     * del usuario identificado, si recibe como parámetro la jurisdicción
     * retorna los datos de esa jurisdicción
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param int $operativo - clave del registro
     * @param string $jurisdiccion - jurisdicción a reportar
     * @return array
     */
    public function detCargadasProvincias($operativo, $jurisdiccion = false){

        // si no recibió la jurisdicción
        if ($jurisdiccion == false){

            // asigna al usuario activo
            $jurisdiccion = $this->CodProv;

        }

        // consulta la jurisdicción del usuario
        $consulta = "SELECT COUNT(cce.chag_datos.ID) AS determinaciones,
                            diccionarios.provincias.NOM_PROV AS jurisdiccion,
                            cce.laboratorios.NOMBRE AS laboratorio,
                            cce.laboratorios.DIRECCION AS direccion,
                            cce.laboratorios.RESPONSABLE AS responsable,
                            cce.laboratorios.COORDENADAS AS coordenadas
                    FROM cce.chag_datos INNER JOIN cce.laboratorios ON cce.chag_datos.LABORATORIO = cce.laboratorios.ID
                                        INNER JOIN cce.operativos_chagas ON cce.chag_datos.OPERATIVO = cce.operativos_chagas.ID
                                        INNER JOIN diccionarios.localidades ON cce.laboratorios.LOCALIDAD = diccionarios.localidades.CODLOC
                                        INNER JOIN diccionarios.provincias ON diccionarios.localidades.CODPCIA = diccionarios.provincias.COD_PROV
                    WHERE cce.operativos_chagas.ID = '$operativo' AND
                          diccionarios.provincias.COD_PROV = '$jurisdiccion'
                    GROUP BY cce.laboratorios.NOMBRE,
                             cce.laboratorios.COORDENADAS
                    ORDER BY diccionarios.provincias.NOM_PROV,
                             cce.laboratorios.NOMBRE; ";

        // obtenemos el vector
        $resultado = $this->Link->query($consulta);
        $registros = array_change_key_case($resultado->fetchAll(PDO::FETCH_ASSOC), CASE_LOWER);

        // retornamos el vector
        return $registros;

    }

    /**
     * Mètodo que según el nivel de acceso del usuario retorna un array con
     * las determinaciones cargadas de todo el país (o la jurisdicción)
     * de los operativos abiertos
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @return array
     */
    public function totalDeterminaciones(){

        // si no es de nivel central
        if ($this->NivelCentral == "No"){

            // obtenemos las determinaciones
            $registros = $this->totalDeterminacionesProvincia();

        // si es de nivel central
        } else {

            // obtenemos las determinaciones
            $registros = $this->totalDeterminacionesNacion();

        }

        // retornamos el vector
        return $registros;

    }

    /**
     * Método llamado cuando el usuario es de nivel central y retorna
     * las determinaciones cargadas de todo el país
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @return array
     */
    protected function totalDeterminacionesNacion(){

        // componemos la consulta
        $consulta = "SELECT COUNT(cce.chag_datos.ID) AS determinaciones,
                            cce.operativos_chagas.operativo_nro AS operativo,
                            diccionarios.provincias.NOM_PROV AS jurisdiccion,
                            cce.laboratorios.NOMBRE AS laboratorio
                     FROM cce.chag_datos INNER JOIN cce.laboratorios ON cce.chag_datos.LABORATORIO = cce.laboratorios.ID
                                         INNER JOIN cce.operativos_chagas ON cce.chag_datos.OPERATIVO = cce.operativos_chagas.ID
                                         INNER JOIN diccionarios.localidades ON cce.laboratorios.LOCALIDAD = diccionarios.localidades.CODLOC
                                         INNER JOIN diccionarios.provincias ON diccionarios.localidades.CODPCIA = diccionarios.provincias.COD_PROV
                     WHERE cce.operativos_chagas.abierto = 'Si'
                     GROUP BY cce.operativos_chagas.id,
                              diccionarios.provincias.NOM_PROV,
                              cce.laboratorios.NOMBRE
                     ORDER BY operativos_chagas.operativo_nro,
                              diccionarios.provincias.NOM_PROV,
                              cce.laboratorios.NOMBRE;";
        $resultado = $this->Link->query($consulta);

        // lo pasamos a minúsculas y retornamos
        $registros = array_change_key_case($resultado->fetchAll(PDO::FETCH_ASSOC), CASE_LOWER);
        return $registros;

    }

    /**
     * Método llamado cuando el usuario no es de nivel central, en ese caso
     * obtiene el vector con las determinaciones cargadas de su provincia
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @return array
     */
    protected function totalDeterminacionesProvincia(){

        // componemos la consulta
        $consulta = "SELECT COUNT(cce.chag_datos.ID) AS determinaciones,
                            cce.operativos_chagas.operativo_nro AS operativo,
                            diccionarios.provincias.NOM_PROV AS jurisdiccion,
                            cce.laboratorios.NOMBRE AS laboratorio
                     FROM cce.chag_datos INNER JOIN cce.laboratorios ON cce.chag_datos.LABORATORIO = cce.laboratorios.ID
                                         INNER JOIN cce.operativos_chagas ON cce.chag_datos.OPERATIVO = cce.operativos_chagas.ID
                                         INNER JOIN diccionarios.localidades ON cce.laboratorios.LOCALIDAD = diccionarios.localidades.CODLOC
                                         INNER JOIN diccionarios.provincias ON diccionarios.localidades.CODPCIA = diccionarios.provincias.COD_PROV
                     WHERE cce.operativos_chagas.abierto = 'Si' AND
                           diccionarios.provincias.COD_PROV = '$this->CodProv'
                     GROUP BY cce.operativos_chagas.id,
                              diccionarios.provincias.NOM_PROV,
                              cce.laboratorios.NOMBRE
                     ORDER BY operativos_chagas.operativo_nro,
                              diccionarios.provincias.NOM_PROV,
                              cce.laboratorios.NOMBRE;";
        $resultado = $this->Link->query($consulta);

        // lo pasamos a minúsculas y retornamos
        $registros = array_change_key_case($resultado->fetchAll(PDO::FETCH_ASSOC), CASE_LOWER);
        return $registros;

    }

    /**
     * Método que recibe como parámetro un número de operativo y retorna
     * un resultset con los laboratorios que no tienen determinaciones
     * cargadas en esa jurisdicción, usado en los reportes de los
     * responsables jurisdiccionales
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param string $operativo - cadena con el número de operativo
     * @return array $registros
     */
    public function determinacionesSinCargar($operativo){

        // si no es de nivel central
        if ($this->NivelCentral == "No"){

            // obtenemos el registro
            $registros = $this->detSinCargarProvincia($operativo);

        // si es de nivel central
        } else {

            // obtenemos el vector
            $registros = $this->detSinCargarNacional($operativo);

        }

        // retornamos el vector
        return $registros;

    }

    /**
     * Método que recibe el número de operativo y retorna un vector con
     * la nómina de los laboratorios con determinaciones sin cargar
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param string $operativo - operativo a reportar
     * @return array $registros
     */
    protected function detSinCargarNacional($operativo){

            // obtenemos los laboratorios de todo el país
            $consulta = "SELECT cce.laboratorios.ID AS id,
                                diccionarios.provincias.NOM_PROV AS jurisdiccion,
                                cce.laboratorios.NOMBRE AS laboratorio
                         FROM cce.laboratorios INNER JOIN diccionarios.localidades ON cce.laboratorios.LOCALIDAD = diccionarios.localidades.CODLOC
                                               INNER JOIN diccionarios.provincias ON diccionarios.localidades.CODPCIA = diccionarios.provincias.COD_PROV
                         WHERE cce.laboratorios.ACTIVO = 'Si' AND
                               cce.laboratorios.ID NOT IN (SELECT existentes.ID AS id_laboratorio
                                                           FROM cce.chag_datos INNER JOIN cce.laboratorios AS existentes ON cce.chag_datos.LABORATORIO = existentes.ID
                                                                               INNER JOIN cce.operativos_chagas ON cce.chag_datos.OPERATIVO = cce.operativos_chagas.ID
                                                                               INNER JOIN diccionarios.localidades ON existentes.LOCALIDAD = diccionarios.localidades.CODLOC
                                                                               INNER JOIN diccionarios.provincias ON diccionarios.localidades.CODPCIA = diccionarios.provincias.COD_PROV
                                                           WHERE operativos_chagas.ID = '$operativo')
                         ORDER BY diccionarios.provincias.NOM_PROV,
                                  cce.laboratorios.NOMBRE;";

        // obtenemos el vector
        $resultado = $this->Link->query($consulta);
        $registros = array_change_key_case($resultado->fetchAll(PDO::FETCH_ASSOC), CASE_LOWER);

        // retornamos el vector
        return $registros;

    }

    /**
     * Mètodo protegido que recibe el número de operativo y retorna un
     * vector con la nómina de laboratorios de la provincia del usuario
     * con determinaciones sin cargar, si no recibe la jurisdicción
     * obtiene para el usuario activo
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param int $operativo - clave del operativo a reportar
     * @param string $jurisdiccion - jurisdicción a reportar
     * @return array $registros
     */
    public function detSinCargarProvincia($operativo, $jurisdiccion = false){

        // si no recibió la jurisdicción
        if (!$jurisdiccion){

            // asigna en la variable de clase
            $jurisdiccion = $this->CodProv;

        }

        // obtenemos los laboratorios de la jurisdicción del usuario
        $consulta = "SELECT cce.laboratorios.ID AS id,
                            diccionarios.provincias.NOM_PROV AS jurisdiccion,
                            cce.laboratorios.NOMBRE AS laboratorio,
                            cce.laboratorios.DIRECCION AS direccion,
                            cce.laboratorios.COORDENADAS AS coordenadas,
                            cce.laboratorios.RESPONSABLE AS responsable
                     FROM cce.laboratorios INNER JOIN diccionarios.localidades ON cce.laboratorios.LOCALIDAD = diccionarios.localidades.CODLOC
                                           INNER JOIN diccionarios.provincias ON diccionarios.localidades.CODPCIA = diccionarios.provincias.COD_PROV
                     WHERE diccionarios.provincias.COD_PROV = '$jurisdiccion' AND
                           cce.laboratorios.ACTIVO = 'Si' AND
                           cce.laboratorios.ID NOT IN (SELECT existentes.ID AS id_laboratorio
                                                       FROM cce.chag_datos INNER JOIN cce.laboratorios AS existentes ON cce.chag_datos.LABORATORIO = existentes.ID
                                                                           INNER JOIN cce.operativos_chagas ON cce.chag_datos.OPERATIVO = cce.operativos_chagas.ID
                                                                           INNER JOIN diccionarios.localidades ON existentes.LOCALIDAD = diccionarios.localidades.CODLOC
                                                                           INNER JOIN diccionarios.provincias ON diccionarios.localidades.CODPCIA = diccionarios.provincias.COD_PROV
                                                       WHERE cce.operativos_chagas.ID = '$operativo' AND
                                                                diccionarios.provincias.COD_PROV = '$jurisdiccion')
                     ORDER BY diccionarios.provincias.NOM_PROV,
                              cce.laboratorios.NOMBRE;";

        // obtenemos el vector
        $resultado = $this->Link->query($consulta);
        $registros = array_change_key_case($resultado->fetchAll(PDO::FETCH_ASSOC), CASE_LOWER);

        // retornamos el vector
        return $registros;

    }

    /**
     * Método público que según la dependencia del usuario retorna el
     * array de laboratorios que no tienen cargadas determinaciones
     * clasificados por operativo
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @return array
     */
    public function totalesSinCargar(){

        // si no es de nivel central
        if ($this->NivelCentral == "No"){

            // obtenemos el registro
            $registros = $this->totalesSinCargarProvincia();

        // si es de nivel central
        } else {

            // obtenemos el vector
            $registros = $this->totalesSinCargarNacional();

        }

        // retornamos el vector
        return $registros;

    }

    /**
     * Método que obtiene la nómina de laboratorios que no han cargado
     * determinaciones clasificados por operativo de la provincia del
     * usuario actual
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @return array
     */
    protected function totalesSinCargarProvincia(){

        // declaración de variables
        $registros = array();

        // obtenemos la nómina de operativos abiertos
        $nomina = $this->operativos();

        // recorremos el vector de operativos
        foreach($nomina AS $fila){

            // obtenemos el registro
            extract($fila);

            // ahora obtenemos el array de laboratorios
            // que no han cargado para ese operativo
            $pendientes = $this->detSinCargarProvincia($id);

            // ahora recorremos el vector
            foreach ($pendientes AS $suspenso){

                // obtenemos el registro
                extract($suspenso);

                // lo agregamos al vector
                $registros[] = array("Operativo"    => $operativo_nro,
                                     "Jurisdiccion" => $jurisdiccion,
                                     "Laboratorio"  => $laboratorio);

            }

        }

        // retornamos el array
        return $registros;

    }

    /**
     * Método que obtiene la nómina de laboratorios que no han cargado
     * determinaciones clasificados por operativo y por jurisdicción
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @return array
     */
    protected function totalesSinCargarNacional(){

        // declaración de variables
        $registros = array();

        // obtenemos la nómina de operativos abiertos
        $nomina = $this->operativos();

        // recorremos el vector de operativos
        foreach($nomina AS $fila){

            // obtenemos el registro
            extract($fila);

            // ahora obtenemos el array de laboratorios
            // que no han cargado para ese operativo
            $pendientes = $this->detSinCargarNacional($id);

            // ahora recorremos el vector
            foreach ($pendientes AS $suspenso){

                // obtenemos el registro
                extract($suspenso);

                // lo agregamos al vector
                $registros[] = array("operativo"    => $operativo_nro,
                                     "jurisdiccion" => $jurisdiccion,
                                     "laboratorio"  => $laboratorio);

            }

        }

        // retornamos el vector
        return $registros;

    }

    /**
     * Método que retorna un array con los operativos abiertos
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @return array
     */
    protected function operativos(){

        // primero obtenemos el vector con los operativos abiertos
        $consulta = "SELECT cce.operativos_chagas.ID AS id,
                            cce.operativos_chagas.OPERATIVO_NRO AS operativo_nro
                     FROM cce.operativos_chagas
                     WHERE cce.operativos_chagas.ABIERTO = 'Si'
                     ORDER BY cce.operativos_chagas.ID;";
        $resultado = $this->Link->query($consulta);

        // obtenemos el array y retornamos
        $registros = array_change_key_case($resultado->fetchAll(PDO::FETCH_ASSOC), CASE_LOWER);
        return $registros;

    }

    /**
     * Método público que retorna un vector con los años que tienen
     * reportes generados
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @return array $anios
     */
    public function aniosReportados(){

        // componemos la consulta y la ejecutamos
        $consulta = "SELECT DISTINCT(RIGHT(cce.reportes.OPERATIVO,4)) AS anio
                     FROM cce.reportes
                     ORDER BY RIGHT(cce.reportes.OPERATIVO,4) DESC;";

        // obtenemos el vector
        $resultado = $this->Link->query($consulta);
        $anios = array_change_key_case($resultado->fetchAll(PDO::FETCH_ASSOC), CASE_LOWER);

        // retornamos el vector
        return $anios;

    }

    /**
     * Método que recibe como parámetro un año y una jurisdicción, este
     * último puede estar en blanco, y retorna una cadena con el reporte
     * correspondiente, si recibe la jurisdicción en blanco, busca un
     * reporte nacional
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param $anio string,  año a obtener
     * @param $jurisdiccion string, clave de la jurisdicción
     */
    public function obtenerReporte($anio, $jurisdiccion = ""){

        // si no recibió la jurisdicción
        if ($jurisdiccion == ""){

            // buscamos un reporte nacional
            $consulta = "SELECT cce.reportes.reporte AS documento
                         FROM cce.reportes
                         WHERE cce.reportes.OPERATIVO = '$anio' AND
                               cce.reportes.NIVEL_CENTRAL = 'Si';";

        // si recibió la jurisdicción
        } else {

            // buscamos un reporte provincial
            $consulta = "SELECT cce.reportes.reporte AS documento
                         FROM cce.reportes
                         WHERE cce.reportes.OPERATIVO = '$anio' AND
                               cce.reportes.provincia = '$jurisdiccion';";

        }

        // ejecutamos la consulta
        $resultado = $this->Link->query($consulta);

        // si no hubo registros
        if ($resultado->rowCount() == 0){

            // asignamos en la variable
            $documento = false;

        // si hubo registros
        } else {

            // obtenemos el registro
            $registro = array_change_key_case($resultado->fetch(PDO::FETCH_ASSOC), CASE_LOWER);
            extract($registro);

        }

        // retornamos la cadena con el documento
        // o el resultado de la consulta
        return $documento;

    }

    /**
     * Método público que es llamado en la georreferenciación de
     * la carga de determinaciones, en caso que el responsable sea
     * de nivel central, retorna la matriz con las jurisdicciones
     * el número de laboratorios que han cargado y el número de
     * laboratorios que no han cargado
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @return array $estadisticas
     */
    public function getAgrupados(){

        // definimos el array
        $estadisticas = array();

        // primero obtenemos las provincias y la población
        $consulta = "SELECT diccionarios.provincias.NOM_PROV AS provincia,
                            diccionarios.provincias.poblacion AS poblacion
                     FROM diccionarios.provincias
                     WHERE diccionarios.provincias.NOM_PROV != 'Indeterminado'
                     ORDER BY diccionarios.provincias.NOM_PROV;";
        $resultado = $this->Link->query($consulta);
        $fila = array_change_key_case($resultado->fetchAll(PDO::FETCH_ASSOC), CASE_LOWER);

        // recorremos el vector y completamos el array
        foreach($fila AS $registro){

            // obtenemos el registro
            extract($registro);

            // agregamos el registro
            $estadisticas[$provincia] = array();
            $estadisticas[$provincia]["poblacion"] = $poblacion;

        }

        // ahora obtenemos el total de determinaciones cargadas
        $consulta = "SELECT diccionarios.provincias.NOM_PROV AS provincia,
                            COUNT(DISTINCT(cce.laboratorios.ID)) AS laboratorios,
                            COUNT(cce.chag_datos.ID) AS determinaciones
                     FROM cce.laboratorios INNER JOIN diccionarios.localidades ON cce.laboratorios.LOCALIDAD = diccionarios.localidades.CODLOC
                                           INNER JOIN diccionarios.provincias ON diccionarios.localidades.CODPCIA = diccionarios.provincias.COD_PROV
                                           INNER JOIN cce.chag_datos ON cce.laboratorios.ID = cce.chag_datos.LABORATORIO
                                           INNER JOIN cce.operativos_chagas ON cce.chag_datos.OPERATIVO = cce.operativos_chagas.ID
                     WHERE cce.operativos_chagas.ABIERTO = 'Si'
                     GROUP BY diccionarios.provincias.NOM_PROV
                     ORDER BY diccionarios.provincias.NOM_PROV;";
        $resultado = $this->Link->query($consulta);
        $fila = array_change_key_case($resultado->fetchAll(PDO::FETCH_ASSOC), CASE_LOWER);

        // recorremos el vector agregando los elementos
        foreach ($fila AS $registro){

            // obtenemos el registro
            extract($registro);

            // lo agregamos al array
            $estadisticas[$provincia]["laboratorios"] = $laboratorios;
            $estadisticas[$provincia]["determinaciones"] = $determinaciones;

        }

        // ahora obtenemos el total de laboratorios que no han cargado
        $consulta = "SELECT diccionarios.provincias.NOM_PROV AS provincia,
                            COUNT(DISTINCT(cce.laboratorios.ID)) AS laboratorio
                     FROM cce.laboratorios INNER JOIN diccionarios.localidades ON cce.laboratorios.LOCALIDAD = diccionarios.localidades.CODLOC
                                           INNER JOIN diccionarios.provincias ON diccionarios.localidades.CODPCIA = diccionarios.provincias.COD_PROV
                     WHERE cce.laboratorios.ID NOT IN (SELECT cce.laboratorios.ID AS id
                                                       FROM cce.laboratorios INNER JOIN cce.chag_datos ON cce.laboratorios.ID = cce.chag_datos.LABORATORIO
                                                                             INNER JOIN cce.operativos_chagas ON cce.chag_datos.OPERATIVO = cce.operativos_chagas.ID
                                                 WHERE operativos_chagas.ABIERTO = 'Si'
                                                 GROUP BY cce.laboratorios.ID) AND
                           cce.laboratorios.ACTIVO = 'Si'
                     GROUP BY diccionarios.provincias.NOM_PROV
                     ORDER BY diccionarios.provincias.NOM_PROV;";
        $resultado = $this->Link->query($consulta);
        $fila = array_change_key_case($resultado->fetchAll(PDO::FETCH_ASSOC), CASE_LOWER);

        // recorremos el vector
        foreach ($fila AS $registro){

            // obtenemos el registro
            extract($registro);

            // lo agregamos a la matriz
            $estadisticas[$provincia]["faltantes"] = $laboratorio;

        }

        // retorna el array de resultados
        return $estadisticas;

    }

    /**
     * Método público que retorna el número de registros de la
     * tabla de datos de determinaciones de chagas
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param string $tabla - cadena con el nombre de la tabla
     * @return int $registros
     */
    public function Registros($tabla){

        // determina el número de determinaciones cargadas
        $consulta = "SELECT COUNT(*) AS registros
                     FROM $tabla;";
        $resultado = $this->Link->query($consulta);
        $fila = array_change_key_case($resultado->fetch(PDO::FETCH_ASSOC), CASE_LOWER);

        // obtenemos el valor
        extract($fila);

        // retorna el valor
        return $registros;

    }

    /**
     * Método público que retorna un array con el número de
     * registros por operativo
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @return array $resultado
     */
    public function DeterminacionesOperativo(){

        // consulta el número de registros por operativo
        $consulta = "SELECT cce.operativos_chagas.OPERATIVO_NRO AS operativo,
                            COUNT(cce.chag_datos.ID) AS registros
                     FROM cce.operativos_chagas LEFT JOIN cce.chag_datos ON cce.chag_datos.OPERATIVO = cce.operativos_chagas.ID
                     GROUP BY cce.operativos_chagas.OPERATIVO_NRO
                     ORDER BY cce.operativos_chagas.FECHA_INICIO;";
        $resultado = $this->Link->query($consulta);
        $fila = array_change_key_case($resultado->fetchAll(PDO::FETCH_ASSOC), CASE_LOWER);

        // retorna el vector
        return $fila;

    }

    /**
     * Método público que retorna el número de laboratorios por fuente
     * de financiamiento
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @return array $resultado
     */
    public function FinanciamientoLaboratorios(){

        // compone la consulta
        $consulta = "SELECT COUNT(cce.laboratorios.ID) AS laboratorios,
                            diccionarios.dependencias.DEPENDENCIA AS dependencia
                     FROM cce.laboratorios INNER JOIN diccionarios.dependencias ON cce.laboratorios.DEPENDENCIA = diccionarios.dependencias.ID_DEPENDENCIA
                     WHERE cce.laboratorios.ACTIVO = 'Si'
                     GROUP BY diccionarios.dependencias.DEPENDENCIA;";
        $resultado = $this->Link->query($consulta);
        $fila = array_change_key_case($resultado->fetchAll(PDO::FETCH_ASSOC), CASE_LOWER);

        // retorna el vector
        return $fila;

    }

    /**
     * Método público que retorna el número de laboratorios por
     * jurisdicción
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @return array $resultado
     */
    public function JurisdiccionLaboratorios(){

        // la distribución de laboratorios participantes por provincia
        $consulta = "SELECT COUNT(cce.laboratorios.ID) AS laboratorios,
                            diccionarios.provincias.NOM_PROV AS jurisdiccion
                     FROM cce.laboratorios INNER JOIN diccionarios.localidades ON cce.laboratorios.LOCALIDAD = diccionarios.localidades.CODLOC
                                           INNER JOIN diccionarios.provincias ON diccionarios.localidades.CODPCIA = diccionarios.provincias.COD_PROV
                     WHERE cce.laboratorios.ACTIVO = 'Si'
                     GROUP BY diccionarios.provincias.NOM_PROV
                     ORDER BY diccionarios.provincias.NOM_PROV;";
        $resultado = $this->Link->query($consulta);
        $fila = array_change_key_case($resultado->fetchAll(PDO::FETCH_ASSOC), CASE_LOWER);

        // retorna el vector
        return $fila;

    }

    /**
     * Método público que consulta la vista de laboratorios por
     * año que remitieron muestras
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @return array $resultado
     */
    public function getLaboratoriosAnios(){

        // ejecuta la consulta
        $consulta = "SELECT * FROM cce.vw_laboratorios_anios;";
        $resultado = $this->Link->query($consulta);
        $fila = array_change_key_case($resultado->fetchAll(PDO::FETCH_ASSOC), CASE_LOWER);

        // retorna el array
        return $fila;

    }

    /**
     * Método público que retorna un array con el número de
     * laboratorios que utilizan 1,2,3 o 4 técnicas agrupadas
     * por año
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @return array $tecnicas
     */
    public function getTecnicasAnio(){

        // inicializamos el array
        $tecnicas = array();

        // obtenemos los años de los operativos
        $resultado = $this->getAnios();

        // recorremos el vector
        foreach($resultado AS $registro){

            // obtenemos el registro
            extract($registro);

            // agregamos el resultado al array
            $tecnicas[$anio] = array();

        }

        // ahora obtenemos los laboratorios clasificados por año del
        // operativo y por el número de técnicas que aplican
        $consulta = "SELECT consulta.anio AS anio,
                            COUNT(consulta.laboratorio) AS laboratorios,
                            consulta.tecnicas AS nro_tecnicas
                     FROM (SELECT RIGHT(cce.operativos_chagas.OPERATIVO_NRO,4) AS anio,
                                  cce.laboratorios.ID AS laboratorio,
                                  COUNT(DISTINCT(cce.chag_datos.TECNICA)) AS tecnicas
                           FROM cce.chag_datos INNER JOIN cce.operativos_chagas ON cce.operativos_chagas.ID = cce.chag_datos.OPERATIVO
                                               INNER JOIN cce.laboratorios ON cce.chag_datos.LABORATORIO = cce.laboratorios.ID
                           GROUP BY RIGHT(cce.operativos_chagas.OPERATIVO_NRO,4),
                                    cce.laboratorios.ID) AS consulta
                     GROUP BY consulta.anio,
                              consulta.tecnicas
                     ORDER BY consulta.anio;";
        $resultado = $this->Link->query($consulta);
        $fila = array_change_key_case($resultado->fetchAll(PDO::FETCH_ASSOC), CASE_LOWER);

        // recorremos el vector
        foreach($fila AS $registro){

            // obtenemos el registro
            extract($registro);

            // según el número de técnicas lo agregamos al array
            if ($nro_tecnicas == 1){

                // lo agregamos como una técnica
                $tecnicas[$anio]["Tecnica1"] = $laboratorios;

            } elseif ($nro_tecnicas == 2){

                // lo agregamos como dos técnicas
                $tecnicas[$anio]["Tecnica2"] = $laboratorios;

            } elseif ($nro_tecnicas == 3){

                // lo agregamos como tres técnicas
                $tecnicas[$anio]["Tecnica3"] = $laboratorios;

            } elseif ($nro_tecnicas == 4){

                // lo agregamos como cuatro técnica
                $tecnicas[$anio]["Tecnica4"] = $laboratorios;

            }

        }

        // retorna el array
        return $tecnicas;

    }

    /**
     * Método pùblico que retorna un array con el número de determinaciones
     * de cada técnica clasificadas por año
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @return array $metodos
     */
    public function getMetodosAnio(){

        // definimos el array
        $metodos = array();

        // obtenemos los años de los operativos
        $resultado = $this->getAnios();

        // recorremos el vector
        foreach($resultado AS $fila){

            // obtenemos el registro
            extract($fila);

            // agregamos el resultado al array
            $metodos[$anio] = array();

        }

        // ahora obtenemos el número de determinaciones de cada técnica
        // clasificados por año
        $consulta = "SELECT RIGHT(cce.operativos_chagas.OPERATIVO_NRO,4) AS anio,
                            cce.tecnicas.TECNICA AS tecnica,
                            COUNT(cce.chag_datos.ID) AS determinaciones
                     FROM cce.operativos_chagas INNER JOIN cce.chag_datos ON cce.chag_datos.OPERATIVO = cce.operativos_chagas.ID
                                                INNER JOIN cce.tecnicas ON cce.chag_datos.TECNICA = cce.tecnicas.ID
                     GROUP BY RIGHT(cce.operativos_chagas.OPERATIVO_NRO,4),
                              cce.chag_datos.TECNICA
                     ORDER BY RIGHT(cce.operativos_chagas.OPERATIVO_NRO,4);";
        $resultado = $this->Link->query($consulta);
        $fila = array_change_key_case($resultado->fetchAll(PDO::FETCH_ASSOC), CASE_LOWER);

        // recorremos el vector
        foreach ($fila AS $registro){

            // obtenemos el registro
            extract($registro);

            // según la técnica
            if ($tecnica == "AP"){

                // lo agrega
                $metodos[$anio]["AP"] = $determinaciones;

            // si es hai
            } elseif ($tecnica == "HAI"){

                // lo agrega
                $metodos[$anio]["HAI"] = $determinaciones;

            // si es elisa
            } elseif ($tecnica == "ELISA"){

                // lo agrega
                $metodos[$anio]["ELISA"] = $determinaciones;

            // si es ifi
            } elseif ($tecnica == "IFI"){

                // lo agrega
                $metodos[$anio]["IFI"] = $determinaciones;

            // si es quimioluminiscencia
            } elseif ($tecnica == "QUIMIOLUMINISCENCIA"){

                // lo agrega
                $metodos[$anio]["QUIMIOLUMINISCENCIA"] = $determinaciones;

            // si es otra
            } elseif ($tecnica == "OTRA"){

                // lo agrega
                $metodos[$anio]["OTRA"] = $determinaciones;

            }

        }

        // retorna el array
        return $metodos;

    }

    /**
     * Método público que retorna el vector con los años
     * de los operativos
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @return array $resultado
     */
    public function getAnios(){

        // primero obtenemos los años de los operativos
        $consulta = "SELECT RIGHT(operativos_chagas.OPERATIVO_NRO,4) AS anio
                     FROM operativos_chagas
                     GROUP BY RIGHT(operativos_chagas.OPERATIVO_NRO,4)
                     ORDER BY RIGHT(operativos_chagas.OPERATIVO_NRO,4);";
        $resultado = $this->Link->query($consulta);
        $fila = array_change_key_case($resultado->fetchAll(PDO::FETCH_ASSOC), CASE_LOWER);

        // retorna el vector
        return $fila;

    }

    /**
     * Método público que retorna un array con los años y el número
     * de determinaciones elisa según lector o visual
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @return array $metodos
     */
    public function getLectorVisual(){

        // definimos el array
        $metodo = array();

        // obtenemos los años
        $resultado = $this->getAnios();

        // recorremos el vector
        foreach ($resultado AS $fila){

            // obtenemos el registro
            extract($fila);

            // agregamos el resultado al array
            $metodos[$anio] = array();

        }

        // obtenemos las determinaciones según método
        $consulta = "SELECT RIGHT(cce.operativos_chagas.OPERATIVO_NRO,4) AS anio,
                            cce.chag_datos.METODO AS tecnica,
                            COUNT(cce.chag_datos.ID) AS determinaciones
                     FROM cce.operativos_chagas INNER JOIN cce.chag_datos ON cce.chag_datos.OPERATIVO = cce.operativos_chagas.ID
                                                INNER JOIN cce.tecnicas ON cce.chag_datos.TECNICA = cce.tecnicas.ID
                     WHERE cce.tecnicas.TECNICA = 'ELISA'
                     GROUP BY RIGHT(cce.operativos_chagas.OPERATIVO_NRO,4),
                              cce.chag_datos.METODO
                     ORDER BY RIGHT(cce.operativos_chagas.OPERATIVO_NRO,4);";
        $resultado = $this->Link->query($consulta);
        $fila = array_change_key_case($resultado->fetchAll(PDO::FETCH_ASSOC), CASE_LOWER);

        // recorremos el vector
        foreach ($fila As $registro){

            // obtenemos el registro
            extract($registro);

            // si el método fue lector
            if ($tecnica == "Lector"){

                // lo agrega a la matriz
                $metodos[$anio]["Lector"] = $determinaciones;

            // si fue visual
            } elseif ($tecnica == "Visual"){

                // lo agrega a la matriz
                $metodos[$anio]["Visual"] = $determinaciones;

            }

        }

        // retorna el array
        return $metodos;

    }

    /**
     * Método público que retorna una matriz con el número de
     * determinaciones por año y la eficiencia correspondiente
     * considera solo los operativos abiertos
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @return array $eficiencia
     */
    public function getEficiencia(){

        // definimos la matriz
        $eficiencia = array();

        // obtenemos los años
        $resultado = $this->getAnios();

        // recorremos el vector
        foreach ($resultado AS $fila){

            // obtenemos el registro
            extract($fila);

            // agregamos el resultado al array
            $eficiencia[$anio] = array();

        }

        // ahora obtenemos el número de determinaciones por año
        $consulta = "SELECT RIGHT(cce.operativos_chagas.OPERATIVO_NRO,4) AS anio,
                            COUNT(cce.chag_datos.ID) AS determinaciones
                     FROM cce.operativos_chagas INNER JOIN cce.chag_datos ON cce.chag_datos.OPERATIVO = cce.operativos_chagas.ID
                     WHERE cce.operativos_chagas.ABIERTO = 'No'
                     GROUP BY RIGHT(cce.operativos_chagas.OPERATIVO_NRO,4)
                     ORDER BY RIGHT(cce.operativos_chagas.OPERATIVO_NRO,4);";
        $resultado = $this->Link->query($consulta);
        $fila = array_change_key_case($resultado->fetchAll(PDO::FETCH_ASSOC), CASE_LOWER);

        // recorremos el vector
        foreach ($fila AS $registro){

            // obtenemos el registro
            extract($registro);

            // lo agregamos a la matriz
            $eficiencia[$anio]["Determinaciones"] = $determinaciones;

        }

        // ahora obtenemos el número de determinaciones correctas
        // clasificadas por año
        $consulta = "SELECT RIGHT(cce.operativos_chagas.OPERATIVO_NRO,4) AS anio,
                            COUNT(cce.chag_datos.ID) AS determinaciones
                     FROM cce.operativos_chagas INNER JOIN cce.chag_datos ON cce.chag_datos.OPERATIVO = cce.operativos_chagas.ID
                     WHERE cce.operativos_chagas.ABIERTO = 'No' AND
                           (cce.chag_datos.DETERMINACION = cce.chag_datos.CORRECTO OR
                           (cce.chag_datos.DETERMINACION = 'Reactivo' AND cce.chag_datos.CORRECTO = 'Indeterminado') OR
                           (cce.chag_datos.DETERMINACION = 'Indeterminado' AND cce.chag_datos.CORRECTO = 'Reactivo'))
                     GROUP BY RIGHT(cce.operativos_chagas.OPERATIVO_NRO,4)
                     ORDER BY RIGHT(cce.operativos_chagas.OPERATIVO_NRO,4);";
        $resultado = $this->Link->query($consulta);
        $fila = array_change_key_case($resultado->fetchAll(PDO::FETCH_ASSOC), CASE_LOWER);

        // recorremos el vector
        foreach ($fila AS $registro){

            // obtenemos el registro
            extract($registro);

            // lo agregamos a la matriz
            $eficiencia[$anio]["Correctas"] = $determinaciones;

        }

        // retorna la matriz
        return $eficiencia;

    }

    /**
     * Método público que retorna la matriz con las determinaciones y
     * la eficiencia por cada técnica según años
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @return array $eficiencia
     */
    public function getEficienciaTecnicas(){

        // definimos el array
        $eficiencia = array();

        // obtenemos los años
        $resultado = $this->getAnios();

        // recorremos el vector
        foreach ($resultado AS $fila){

            // obtenemos el registro
            extract($fila);

            // agregamos el resultado al array
            $eficiencia[$anio] = array();

        }

        // obtenemos las determinaciones por año y técnica
        // de los operativos que están cerrados
        $consulta = "SELECT RIGHT(cce.operativos_chagas.OPERATIVO_NRO,4) AS anio,
                            cce.tecnicas.TECNICA AS tecnica,
                            COUNT(cce.chag_datos.ID) AS determinaciones
                     FROM cce.operativos_chagas INNER JOIN cce.chag_datos ON cce.chag_datos.OPERATIVO = cce.operativos_chagas.ID
                                                INNER JOIN cce.tecnicas ON cce.chag_datos.TECNICA = cce.tecnicas.ID
                     WHERE cce.operativos_chagas.ABIERTO = 'No'
                     GROUP BY RIGHT(cce.operativos_chagas.OPERATIVO_NRO,4),
                              cce.chag_datos.TECNICA
                     ORDER BY RIGHT(cce.operativos_chagas.OPERATIVO_NRO,4);";
        $resultado = $this->Link->query($consulta);
        $fila = array_change_key_case($resultado->fetchAll(PDO::FETCH_ASSOC), CASE_LOWER);

        // recorremos el vector
        foreach ($fila AS $registro){

            // obtenemos el registro
            extract($registro);

            // según la técnica
            // si es ap
            if ($tecnica == "AP"){

                // lo agrega a la matriz
                $eficiencia[$anio]["DeterminacionesAP"] = $determinaciones;

            // si es hai
            } elseif ($tecnica == "HAI"){

                // lo agrega a la matriz
                $eficiencia[$anio]["DeterminacionesHAI"] = $determinaciones;

            // si es elisa
            } elseif ($tecnica == "ELISA"){

                // lo agrega a la matriz
                $eficiencia[$anio]["DeterminacionesELISA"] = $determinaciones;

            // si es ifi
            } elseif ($tecnica == "IFI"){

                // lo agrega a la matriz
                $eficiencia[$anio]["DeterminacionesIFI"] = $determinaciones;

            // si es quimioluminiscencia
            } elseif ($tecnica == "QUIMIOLUMINISCENCIA") {

                // lo agrega a la matriz
                $eficiencia[$anio]["DeterminacionesQUIMIO"] = $determinaciones;

            // si es otra
            } elseif ($tecnica == "OTRA"){

                // lo agrega a la matriz
                $eficiencia[$anio]["DeterminacionesOTRA"] = $determinaciones;

            }

        }

        // ahora obtenemos las determinaciones correctas clasificadas
        // por año y técnica solo para los operativos cerrados
        $consulta = "SELECT RIGHT(cce.operativos_chagas.OPERATIVO_NRO,4) AS anio,
                            cce.tecnicas.TECNICA AS tecnica,
                            COUNT(cce.chag_datos.ID) AS determinaciones
                        FROM cce.operativos_chagas INNER JOIN cce.chag_datos ON cce.chag_datos.OPERATIVO = cce.operativos_chagas.ID
                                                   INNER JOIN cce.tecnicas ON cce.chag_datos.tecnica = cce.tecnicas.ID
                        WHERE cce.operativos_chagas.ABIERTO = 'No' AND
                            (cce.chag_datos.DETERMINACION = cce.chag_datos.CORRECTO OR
                            (cce.chag_datos.DETERMINACION = 'Reactivo' AND cce.chag_datos.CORRECTO = 'Indeterminado') OR
                            (cce.chag_datos.DETERMINACION = 'Indeterminado' AND cce.chag_datos.CORRECTO = 'Reactivo'))
                        GROUP BY RIGHT(cce.operativos_chagas.OPERATIVO_NRO,4),
                                 cce.tecnicas.TECNICA
                        ORDER BY RIGHT(cce.operativos_chagas.OPERATIVO_NRO,4);";
        $resultado = $this->Link->query($consulta);
        $fila = array_change_key_case($resultado->fetchAll(PDO::FETCH_ASSOC), CASE_LOWER);

        // ahora recorremos el vector
        foreach ($fila AS $registro){

            // extraemos el registro
            extract($registro);

            // lo agregamos a la matriz
            // si es ap
            if ($tecnica == "AP"){

                // lo agrega a la matriz
                $eficiencia[$anio]["CorrectasAP"] = $determinaciones;

            // si es hai
            } elseif ($tecnica == "HAI"){

                // lo agrega a la matriz
                $eficiencia[$anio]["CorrectasHAI"] = $determinaciones;

            // si es elisa
            } elseif ($tecnica == "ELISA"){

                // lo agrega a la matriz
                $eficiencia[$anio]["CorrectasELISA"] = $determinaciones;

            // si es ifi
            } elseif ($tecnica == "IFI"){

                // lo agrega a la matriz
                $eficiencia[$anio]["CorrectasIFI"] = $determinaciones;

            // si es quimioluminiscencia
            } elseif ($tecnica == "QUIMIOLUMINISCENCIA"){

                // lo agrega  a la matriz
                $eficiencia[$anio]["CorrectasQUIMIO"] = $determinaciones;

            // si es otra
            } elseif ($tecnica == "OTRA"){

                // lo agrega a la matriz
                $eficiencia[$anio]["CorrectasOTRA"] = $determinaciones;

            }

        }

        // retorna la matriz
        return $eficiencia;

    }

    /**
     * Método público que retorna un array con las determinaciones realizadas
     * por el laboratorio, utilizado en el reporte por laboratorio para
     * calcular la eficiencia interanual
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param int $idlaboratorio - clave del laboratorio a evaluar
     * @return array
     */
    public function concordanciaLaboratorio($idlaboratorio){

        // componemos la consulta
        $consulta = "SELECT SUBSTRING(cce.vw_determinaciones.operativo, -4, 4) AS anio,
                            cce.vw_determinaciones.operativo AS operativo,
                            cce.vw_determinaciones.marca AS marca,
                            cce.vw_determinaciones.lote AS lote,
                            cce.vw_determinaciones.vencimiento AS vencimiento,
                            cce.vw_determinaciones.muestra_nro AS muestra_nro,
                            cce.vw_determinaciones.valor_corte AS valor_corte,
                            cce.vw_determinaciones.valor_lectura AS valor_lectura,
                            cce.vw_determinaciones.metodo AS metodo,
                            cce.vw_determinaciones.tecnica AS tecnica,
                            cce.vw_determinaciones.determinacion AS determinacion,
                            cce.vw_determinaciones.correcto AS correcto,
                            cce.vw_determinaciones.usuario AS usuario
                     FROM cce.vw_determinaciones
                     WHERE cce.vw_determinaciones.idlaboratorio = '$idlaboratorio' AND
                           !ISNULL(cce.vw_determinaciones.correcto)
                     ORDER BY SUBSTRING(cce.vw_determinaciones.operativo, -4, 4),
                              cce.vw_determinaciones.idoperativo;";
        $resultado = $this->Link->query($consulta);

        // obtenemos el vector y retornamos
        $fila = array_change_key_case($resultado->fetchAll(PDO::FETCH_ASSOC), CASE_LOWER);
        return $fila;

    }

}