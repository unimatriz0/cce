<?php

/**
 *
 * reportes/ver_reporte.php
 *
 * @package     CCE
 * @subpackage  Reportes
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.4.0 (18/08/2018)
 * @copyright   Copyright (c) 2018, INP
 *
 * Procedimiento que recibe como parámetros un año y una jurisdicción
 * obtiene el documento con el reporte correspondiente y lo presenta
 * en pantalla
 *
*/

// inclusión de archivos
require_once ("reportes.class.php");
$reporte = new Reportes();

// obtenemos el documento
$documento = $reporte->obtenerReporte($_GET["anio"], $_GET["jurisdiccion"]);

// si obtuvimos el documento
if ($documento != false){

    // abrimos el archivo para escritura
    $puntero = fopen("../temp/Informe.pdf", 'w');
    fwrite($puntero, $documento);
    fclose($puntero);

    // enviamos los headers
    header("Cache-Control: no-store, no-cache, must-revalidate");
    header("Cache-Control: post-check=0, pre-check=0", false);
    header("Pragma: no-cache");

    // ahora lo presentamos
    ?>
    <object data="../CCE4/temp/Informe.pdf"
            type="application/pdf"
            width="850" height="500">
    </object>
    <?php

}
?>