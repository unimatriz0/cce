/*
 * Nombre: reportes.js
 * Autor: Lic. Claudio Invernizzi
 * E-Mail: cinvernizzi@gmail.com
 * Fecha: 16/02/2017
 * Licencia: GPL
 * Comentarios: Serie de funciones y procedimientos utilizados por el
 *              sistema de control de calidad en los reportes
 */

 /**
 *
 * Atención, utilizamos el nuevo estandard definido en ECMAScript 2015 (ES6)
 * este permite definir clases con el mètodo ya utilizado en php y java
 * como creo que es mas claro lo utilizaremos, la compatibilidad está
 * anunciada para firefox y chrome así que no deberìa haber problemas
 *
 */

/* jshint esversion: 6 */

/**
 * @author Claudio Invernizzi <cinvernizzi@gmail.com>
 * @class Clase que presenta los métodos para la presentación de
 * reportes de seguimiento
 */
class Reportes {

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @constructor
     */
    constructor(){

        // inicializamos las variables

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Inicializamos las variables de clase
     */
    initReportes(){

        // inicializamos las variables
        this.layerReportes = "";             // layer emergente

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Presenta el cuadro de diálogo pidiendo el año del reporte
     * de las determinaciones cargadas
     */
    determinacionesCargadas(){

        // reiniciamos el contador
        cce.contador();

        // abrimos el formulario
        this.layerReportes = new jBox('Modal', {
                                animation: 'flip',
                                closeOnEsc: true,
                                closeOnClick: false,
                                closeOnMouseleave: false,
                                closeButton: true,
                                overlay: false,
                                onCloseComplete: function(){
                                this.destroy();
                                },
                                draggable: 'title',
                                repositionOnContent: true,
                                title: 'Determinaciones Cargadas',
                                theme: 'TooltipBorder',
                                width: 300,
                                ajax: {
                                    url: 'reportes/anio_det_cargadas.html',
                                    reload: 'strict'
                                }
                            });
        this.layerReportes.open();

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Verifica se halla seleccionado un operativo a informar y
     * luego carga en el contenido el informe con los laboratorios
     * que han cargado determinaciones
     */
    reporteCargados(){

        // reiniciamos el contador
        cce.contador();

        // obtenemos el valor seleccionado
        var operativo = document.getElementById("operativo_cargado").value;

        // si no seleccionó
        if (operativo == ""){

            // presenta el mensaje y retorna
            mensaje = "Debe seleccionar el operativo a reportar";
            new jBox('Notice', {content: mensaje, color: 'red'});
            document.getElementById("operativo_cargado").focus();
            return false;

        }

        // cargamos en el contenido el reporte
        $("#form_reportes").load("reportes/det_cargadas.php?operativo="+operativo);

        // cerramos el layer
        this.layerReportes.destroy();

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que presenta el cuadro de diálogo pidiendo el año del reporte de
     * los laboratorios con determinaciones sin cargar
     */
    determSinCargar(){

        // reiniciamos el contador
        cce.contador();

        // abrimos el formulario
        this.layerReportes = new jBox('Modal', {
                                    animation: 'flip',
                                    closeOnEsc: true,
                                    closeOnClick: false,
                                    closeOnMouseleave: false,
                                    closeButton: true,
                                    overlay: false,
                                    title: 'Laboratorios sin Carga',
                                    theme: 'TooltipBorder',
                                    draggable: 'title',
                                    onCloseComplete: function(){
                                        this.destroy();
                                    },
                                    repositionOnContent: true,
                                    width: 300,
                                    ajax: {
                                        url: 'reportes/anio_det_sin_carga.html',
                                        reload: 'strict'
                                    }
                                });
        this.layerReportes.open();

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Verifica se halla seleccionado un operativo a informar y
     * luego carga en el contenido el informe con los laboratorios
     * que no han cargado determinaciones
     */
    reporteSinCargar(){

        // reiniciamos el contador
        cce.contador();

        // obtenemos el valor seleccionado
        var operativo = document.getElementById("operativo_cargado").value;

        // si no seleccionó
        if (operativo == ""){

            // presenta el mensaje y retorna
            mensaje = "Debe seleccionar el operativo a reportar";
            new jBox('Notice', {content: mensaje, color: 'red'});
            document.getElementById("operativo_cargado").focus();
            return false;

        }

        // cargamos en el contenido el reporte
        $("#form_reportes").load("reportes/det_sin_carga.php?operativo="+operativo);

        // cerramos el layer
        this.layerReportes.destroy();

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Abre el cuadro de diálogo solicitando el año y la jurisdicción
     * (en caso de ser referente nacional) del reporte a presentar
     */
    pedirReporte(){

        // reiniciamos el contador
        cce.contador();

        // definimos el layer
        this.layerReportes = new jBox('Modal', {
                        animation: 'flip',
                        closeOnEsc: true,
                        closeOnClick: false,
                        closeOnMouseleave: false,
                        closeButton: true,
                        overlay: false,
                        title: 'Año del Reporte',
                        theme: 'TooltipBorder',
                        draggable: 'title',
                        repositionOnContent: true,
                        onCloseComplete: function(){
                        this.destroy();
                        },
                        width: 300,
                        ajax: {
                            url: 'reportes/anios_reporte.html',
                            reload: 'strict'
                        }
                    });

        // abrimos el layer
        this.layerReportes.open();

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método llamado en el formulario de selección de reportes, verifica
     * que se halla seleccionado el año y pasa los valores a la función
     * php que mostrará el reporte en pantalla
     */
    verReporte(){

        // reiniciamos el contador
        cce.contador();

        // declaración de variables
        var mensaje;
        var anio = document.getElementById("anio_reporte").value;
        var jurisdiccion;

        // verifica se halla seleccionado un año
        if (anio == 0){

            // presenta el mensaje y retorna
            mensaje = "Debe seleccionar el año a ver";
            new jBox('Notice', {content: mensaje, color: 'red'});
            document.getElementById("anio_reporte").focus();
            return false;

        }

        // si el usuario es de nivel central
        if (sessionStorage.getItem("NivelCentral") == "Si"){

            // si seleccionó jurisdicción
            if (document.getElementById("lista_jur_reporte").value != ""){

                // asignamos la jurisdicción
                jurisdiccion = document.getElementById("lista_jur_reporte").value;

            // si no seleccionó jurisdicción
            } else {

                // lo asignamos en blanco y presentamos el mensaje
                jurisdiccion = "";
                mensaje = "Se listará un reporte nacional";
                new jBox('Notice', {content: mensaje, color: 'green'});

            }

        // si el usuario no es de nivel central
        } else {

            // asignamos la jurisdicción
            jurisdiccion = sessionStorage.getItem("CodProv");

        }

        // destruimos el layer del año
        this.layerReportes.destroy();

        // abrimos el layer y mostramos el reporte
        // con las opciones target lo fijamos a un div de la página
        // y con las opciones left y top lo fijamos con respecto a
        // ese elemento
        this.layerReportes = new jBox('Modal', {
                        animation: 'flip',
                        closeOnEsc: true,
                        closeOnClick: false,
                        closeOnMouseleave: false,
                        closeButton: true,
                        overlay: false,
                        repositionOnContent: true,
                        draggable: 'title',
                        title: 'Reporte Anual',
                        onCloseComplete: function(){
                            this.destroy();
                        },
                        theme: 'TooltipBorder',
                        target: '#top',
                            position: {
                                x: 'left',
                                y: 'top'
                            },
                        ajax: {
                            url: 'reportes/ver_reporte.php?anio='+anio+'&jurisdiccion='+jurisdiccion,
                            reload: 'strict'
                        }
                    });
        this.layerReportes.open();

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que abre el cuadro de diálogo pidiendo los datos
     * de la etiqueta a buscar
     */
    buscaEtiqueta(){

        // reiniciamos el contador
        cce.contador();

        // definimos el layer
        this.layerReportes = new jBox('Modal', {
                        animation: 'flip',
                        closeOnEsc: true,
                        closeOnClick: false,
                        closeOnMouseleave: false,
                        closeButton: true,
                        overlay: false,
                        repositionOnContent: true,
                        draggable: 'true',
                        title: 'Búsqueda de Etiquetas',
                        theme: 'TooltipBorder',
                        onCloseComplete: function(){
                        this.destroy();
                        },
                        width: 400,
                        ajax: {
                            url: 'reportes/form_busca_etiq.html',
                            reload: 'strict'
                        }
                    });

        // abrimos el layer
        this.layerReportes.open();

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que según el radio pulsado muestra u oculta
     * los campos de búsqueda
     */
    criterioBusqueda(){

        // si pulsó sobre nombre de laboratorio
        if (document.getElementById("criterio_laboratorio").checked){

            // muestra el div
            $("#busca_etiqueta_laboratorio").show();
            $("#busca_etiqueta_nro").hide();
            document.getElementById("nombre_laboratorio").focus();

        // si pulsó sobre número de etiqueta
        } else {

            // muestra el div y setea el foco
            $("#busca_etiqueta_nro").show();
            $("#busca_etiqueta_laboratorio").hide();
            document.getElementById("nro_etiqueta").focus();

        }

    }

    /**
     * Método llamado al pulsar el botón buscar etiqueta,
     * verifica el formulario y luego carga en el contenedor
     * el resultado de la búsqueda
     */
    encuentraEtiqueta(){

        // reiniciamos el contador
        cce.contador();

        var nroEtiqueta = document.getElementById("nro_etiqueta").value;
        var laboratorio = document.getElementById("nombre_laboratorio").value;

        // si ningún radio está marcado
        if (!document.getElementById("criterio_etiqueta").checked && !document.getElementById("criterio_laboratorio").checked){

            // presenta el mensaje de error y retorna
            mensaje = "Debe indicar un criterio de búsqueda";
            new jBox('Notice', {content: mensaje, color: 'red'});
            return false;

        // si busca por número y no ingresó el valor
        } else if (document.getElementById("criterio_etiqueta").checked && nroEtiqueta == "") {

            // presenta el mensaje de error y retorna
            mensaje = "Indique el número de etiqueta a buscar";
            new jBox('Notice', {content: mensaje, color: 'red'});
            document.getElementById("nro_etiqueta").focus();
            return false;

        // si busca por laboratorio y no ingresó el nombre
        } else if (document.getElementById("criterio_laboratorio").checked &&  laboratorio == ""){

            // presenta el mensaje de error y retorna
            mensaje = "Ingrese el laboratorio a buscar";
            new jBox('Notice', {content: mensaje, color: 'red'});
            document.getElementById("nombre_laboratorio").focus();
            return false;

        }

        // ahora destruimos el layer
        this.layerReportes.destroy();

        // llamamos la rutina php pasándole los argumentos
        $("#form_reportes").load("reportes/lista_etiquetas.php?etiqueta="+nroEtiqueta+"&laboratorio="+laboratorio);

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Muestra en el layer de contenido el reporte con la información estadística
     * del sistema
     */
    Estadisticas(){

        // reiniciamos el contador
        cce.contador();

        // llama al formulario de reportes
        $("#form_reportes").load("reportes/estadisticas.php");

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que abre el cuadro de diálogo solicitando el reporte a informar
     * (y la jurisdicción si es de nivel central) y para georreferenciar los
     * laboratorios que han cargado determinaciones
     */
    mapaCarga(){

        // reiniciamos el contador
        cce.contador();

        // definimos el layer
        this.layerReportes = new jBox('Modal', {
                            animation: 'flip',
                            closeOnEsc: true,
                            closeOnClick: false,
                            closeOnMouseleave: false,
                            closeButton: true,
                            overlay: false,
                            title: 'Determinaciones Cargadas',
                            draggable: 'title',
                            repositionOnContent: true,
                            theme: 'TooltipBorder',
                            onCloseComplete: function(){
                                    this.destroy();
                            },
                            width: 400,
                            ajax: {
                                    url: 'reportes/geo_det_cargadas.html',
                                    reload: 'strict'
                            }
                            });

        // abrimos el layer
        this.layerReportes.open();

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param {string} operativo clave del operativo
     * @param {string} jurisdiccion código indec de la jurisdicción
     * Método que recibe como parámetro la clave del operativo y la clave
     * indec de la jurisdicción obtiene los datos de las determinaciones
     * cargadas e instancia el mapa
     */
    mapearCargados(operativo, jurisdiccion){

        // centra las coordenadas del mapa (como presenta todo el país
        // lo centra en la pampa
        var latlng = new google.maps.LatLng(-37.894736,-65.095761);

        // los tipos de mapa son
        // TERRAIN incluye relieve físico
        // ROADMAP mapa normal en 2D
        // SATELLITE fotografías satelitales
        // HYBRID híbrido de roadmap y satélite

        // array con las opciones del mapa
        var myOptions = {zoom: 4,
                            center: latlng,
                            panControl: true,
                            zoomControl: true,
                            mapTypeControl: true,
                            scaleControl: true,
                            streetViewControl: false,
                            overviewMapControl: true,
                            mapTypeId: google.maps.MapTypeId.ROADMAP
                        };

        // destruimos el cuadro de diálogo
        this.layerReportes.destroy();

        // mostramos los layers
        $("#map_cerrar").show();
        $("#map").show();

        // llamamos la rutina de google y centramos el mapa
        // en el medio de argentina
        var map = new google.maps.Map(document.getElementById('map'), myOptions);

        // declara la ventana de información
        var infowindow = new google.maps.InfoWindow(
                                { content: "",
                                    size: new google.maps.Size(50,50)
                                });

        // obtenemos la nómina y las coordenadas de los
        // laboratorios que han enviado muestras
        $.get('reportes/lista_geo_cargadas.php?operativo='+operativo+'&jurisdiccion='+jurisdiccion, function(data){

            // recorre el vector de resultados
            for(var i=0; i < data.length; i++){

                // compone la dirección usando variables locales al bucle
                var texto = data[i].Laboratorio;
                texto += "<br>";
                texto += data[i].Responsable;
                texto += "<br>";
                texto += data[i].Direccion;
                texto += "<br>";
                texto += "Determ." + data[i].Determinaciones;

                // recorta los paréntesis de las coordenadas
                var temp = data[i].Coordenadas;
                var latitud = temp.substring(1,temp.indexOf(","));
                var longitud = temp.substring(temp.indexOf(",") + 1, temp.length -1);
                var coordenadas = new google.maps.LatLng(latitud, longitud);

                // agrega el punto al mapa
                var marca = new google.maps.Marker({
                                position: coordenadas,
                                map: map,
                                icon: "http://www.google.com/mapfiles/dd-start.png"
                            });

                // llama la función de etiquetado
                mapas.agregaMarcador(map, infowindow, marca, coordenadas, texto);

            }

        }, "json");

        // ahora hacemos lo mismo pero para los laboratorios que
        // no han cargado determinaciones
        $.get('reportes/lista_geo_sin_carga.php?operativo='+operativo+'&jurisdiccion='+jurisdiccion, function(data){

            // recorre el vector de resultados
            for(var i=0; i < data.length; i++){

                // compone la dirección usando variables locales al bucle
                var texto = data[i].Laboratorio;
                texto += "<br>";
                texto += data[i].Responsable;
                texto += "<br>";
                texto += data[i].Direccion;

                // recorta los paréntesis de las coordenadas
                var temp = data[i].Coordenadas;
                var latitud = temp.substring(1,temp.indexOf(","));
                var longitud = temp.substring(temp.indexOf(",") + 1, temp.length -1);
                var coordenadas = new google.maps.LatLng(latitud, longitud);

                // agrega el punto al mapa
                var marca = new google.maps.Marker({
                                position: coordenadas,
                                map: map,
                                icon: "http://www.google.com/mapfiles/marker.png"
                            });

                // llama la función de etiquetado
                mapas.agregaMarcador(map, infowindow, marca, coordenadas, texto);

            }

        }, "json");

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método llamado al pulsar el botón exportar, que llama la rutina
     * php para generar un excel con las determinaciones cargadas
     */
    xlsCargadas(){

        // probamos de redirigir el navegador
        window.location = "reportes/xls_det_cargadas.php";

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método llamado al pulsar el botón exportar, que llama la rutina
     * php para generar un excel con los laboratorios sin cargar
     */
    xlsSinCarga(){

        // probamos de redirigir el navegador
        window.location = "reportes/xls_det_sin_cargar.php";

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método llamado al pulsar el botón exportar, que llama la rutina
     * php para generar un excel con las estadísticas de la plataforma
     */
    xlsEstadisticas(){

        // probamos de redirigir el navegador
        window.location = "reportes/xlsestadisticas.php";

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que llama la rutina php que genera un reporte excel de
     * concordancia para el laboratorio actual
     */
    xlsConcordanciaLaboratorio(){

        // obtenemos la id del laboratorio que está en pantalla
        idlaboratorio = document.getElementById("idlaboratorio").value;

        // si está en blanco o vacío
        if (idlaboratorio == "" || idlaboratorio == 0){
            return false;
        }

        // redirigimos el navegador
        window.location = "reportes/xls_concordancia.php?laboratorio="+idlaboratorio;

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que verifica los datos del formulario de laboratorios con
     * carga y luego obtiene los datos por ajax para presentarlos en
     * un mapa
     */
    generaMapaCarga(){

        // reiniciamos el contador
        cce.contador();

        // declaración de variables
        var mensaje;
        var operativo;
        var jurisdiccion = "";

        // verifica el operativo
        operativo = document.getElementById("operativo_cargado").value;
        if (operativo == ""){

            // presenta el mensaje y abandona
            mensaje = "Seleccione un operativo de la lista";
            new jBox('Notice', {content: mensaje, color: 'red'});
            document.getElementById("operativo_cargado").focus();
            return false;

        }

        // si es de nivel central
        if (sessionStorage.getItem("NivelCentral") == "Si"){

            // verifica si seleccionó jurisdicción
            jurisdiccion = document.getElementById("geo_jurisdiccion").value;
            if (jurisdiccion == ""){

                // presenta el mensaje y abandona
                mensaje = "Seleccione una jurisdicción de la lista";
                new jBox('Notice', {content: mensaje, color: 'red'});
                document.getElementById("geo_jurisdiccion").focus();
                return false;

            }

        // si es un responsable de una provincia
        } else {

            // asignamos en la variable
            jurisdiccion = sessionStorage.getItem("CodProv");

        }

        // ahora llamamos la rutina que instancia el mapa
        this.mapearCargados(operativo, jurisdiccion);

    }

}