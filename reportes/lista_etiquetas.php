<?php

/**
 *
 * reportes/lista_etiquetas.php
 *
 * @package     CCE
 * @subpackage  Reportes
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.4.0 (18/08/2018)
 * @copyright   Copyright (c) 2018, INP
 *
 * Procedimiento que recibe por get un número de etiquete o parte del
 * nombre de un laboratorio, y presenta la tabla con los resultados
 * de la búsqueda
*/

// incluimos e instanciamos las clases
require_once ("../etiquetas/etiquetas.class.php");
$etiquetas = new Etiquetas();

// obtenemos los valores recibidos por get
$nroetiqueta = $_GET["etiqueta"];
$laboratorio = $_GET["laboratorio"];

// si recibió el número
if (!empty($nroetiqueta)){

    // buscamos por etiqueta
    $resultado = $etiquetas->buscaEtiqueta($nroetiqueta);

// si recibió el laboratorio
} elseif (!empty($laboratorio)) {

    // buscamos por laboratorio
    $resultado = $etiquetas->buscaLaboratorio($laboratorio);

}

// si hubo resultados
if (count($resultado) != 0){

    // definimos la tabla
    echo "<table id='listaEtiquetas' width='90%' align='center' border='0'>";

    // definimos los encabezados
    echo "<thead>";
    echo "<tr>";
    echo "<th>Jurisdicción</th>";
    echo "<th>Localidad</th>";
    echo "<th>Laboratorio</th>";
    echo "<th>Operativo</th>";
    echo "<th>Etiqueta</th>";
    echo "</tr>";
    echo "</thead>";

    // definimos el cuerpo
    echo "<tbody>";

    // recorremos el vector
    foreach ($resultado AS $registro){

        // obtenemos el registro
        extract($registro);

        // lo mostramos
        echo "<tr>";
        echo "<td>$provincia</td>";
        echo "<td>$localidad</td>";
        echo "<td>$laboratorio</td>";
        echo "<td>$operativo</td>";
        echo "<td>$etiqueta_nro</td>";
        echo "</tr>";

    }

    // cierramos la tabla
    echo "</tbody></table>";

    // inserta un separador
    echo "<br>";

    // definimos el div para el paginador
    echo "<div class='paging'></div>";

// si no hubo resultados
} else {

    // presenta el mensaje
    echo "<h3>Lo siento, no hemos encontrado ninguna etiqueta que cumpla con
              el criterio ingresado</h3>";

}
?>
<SCRIPT>

    // definimos las propiedades de la tabla
    $('#listaEtiquetas').datatable({
        pageSize: 15,
        sort: [true, true, true, true, true],
        filters: ['select', true, true, 'select', true],
        filterText: 'Buscar ... '
    });

</SCRIPT>