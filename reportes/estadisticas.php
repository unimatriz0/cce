<?php

/**
 *
 * reportes/estadisticas.php
 *
 * @package     CCE
 * @subpackage  Reportes
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.4.0 (18/08/2018)
 * @copyright   Copyright (c) 2018, INP
 *
 * Procedimiento que arma la página con las estadísticas del sistema
 *
*/

// inclusión de archivos
require_once ("reportes.class.php");

// instancia la clase
$reporte = new Reportes();

// presenta el título
echo "<h2>Estadísticas  Generales del Sitio</h2>";

// presenta el texto introductorio
echo "<p align='justify'>";
echo "En esta página usted encontrará algunos indicadores de volúmenes de
      producción del sistema, tenga en cuenta que son indicadores generales
      que solo pretenden dar una idea del volumen de datos registrado.</p>";

// define la tabla
echo "<table width='50%' align='center' border='0'>";

// declara el cuerpo
echo "<tbody>";

// obtiene el número de registros
$registros = $reporte->Registros("chag_datos");

echo "<tr>";
echo "<td><b>Total de Determinaciones Cargadas: </b></td>";
echo "<td align='right'><b>". number_format($registros, 0, ",", ".") . "</b></td>";
echo "</tr>";

// abre una fila
echo "<tr>";

// define la columna
echo "<td colspan='2'>";

// abre una tabla dentro
echo "<table width='100%' align='center' border='0'>";

// obtiene el número de registros por operativo
$resultado = $reporte->DeterminacionesOperativo();

// inicia el bucle recorriendo el vector
foreach ($resultado AS $fila){

    // obtiene el registro
    extract($fila);

    // abre la fila y presenta los datos con una viñeta
    echo "<tr>";
    echo "<td align='center'><img src='../imagenes/bullet_azul.png' height='12'></td>";
    echo "<td>Operativo $operativo:</td>";
    echo "<td align='right'>" . number_format($registros, 0, ",", ".") . "</td>";
    echo "</tr>";

}

// cierra la tabla
echo "</table>";

// cierra los tags
echo "</td>";
echo "</tr>";

// cierra la tabla
echo "</table>";

// inserta un separador
echo "<br>";

// define una nueva tabla
echo "<table width='50%' align='center' border='0'>";

// presenta el título
echo "<tr>";
echo "<td colspan='2'>";
echo "<b>Distribución de Laboratorios</b>";
echo "</tr>";

// determina el número de laboratorios ingresados
$registros = $reporte->Registros("cce.laboratorios");

// abre la fila
echo "<tr>";
echo "<td><b>Laboratorios Ingresados (Total): </b></td>";
echo "<td align='right'><b>" . number_format($registros, 0, ",", ".") . "</b></td>";
echo "</tr>";

$resultado = $reporte->FinanciamientoLaboratorios();

// inicializa el contador
$total_laboratorios = 0;

// iniciamos un bucle
foreach ($resultado AS $fila){

    // obtiene el registro
    extract($fila);

    // incrementa el contador de total de laboratorios
    $total_laboratorios += $laboratorios;

    // presenta los laboratorios por fuente de financiamiento
    echo "<tr>";
    echo "<td>$dependencia: </td>";
    echo "<td align='right'>" . number_format($laboratorios, 0, ",", ".") . "</td>";
    echo "</tr>";

}

// el número de laboratorios participantes
echo "<tr>";
echo "<td><b>Total Lab. Participantes: </b></td>";
echo "<td align='right'><b>" . number_format($total_laboratorios, 0, ",", ".") . "</b></td>";
echo "</tr>";

// cierra la tabla
echo "</table>";

// inserta un separador
echo "<br>";

// abre una nueva tabla
echo "<table width='50%' align='center' border='0'>";

// presenta el título
echo "<tr>";
echo "<td colspan='2'>";
echo "<b>Distribución por Jurisdicción (Laboratorios Participantes)</b>";
echo "</td></tr>";

// obtiene la distribución de laboratorios
$resultado = $reporte->JurisdiccionLaboratorios();

// iniciamos un bucle
foreach ($resultado AS $fila){

    // obtenemos el registro
    extract($fila);

    // lo presenta
    echo "<tr>";
    echo "<td>$jurisdiccion</td>";
    echo "<td align='right'>" . number_format($laboratorios, 0, ",", ".") . "</td>";
    echo "</tr>";

}

// cierra la tabla
echo "</table>";

// inserta un separador
echo "<br>";

// abre una nueva tabla
echo "<table width='50%' align='center' border='0'>";

// determina el número de localidades cargadas
$registros = $reporte->Registros("diccionarios.localidades");

// abre la fila
echo "<tr>";
echo "<td><b>Localidades Registradas: </b></td>";
echo "<td align='right'>" . number_format($registros, 0, ",", ".") . "</td>";
echo "</tr>";

// determina el número de operativos realizados
$registros = $reporte->Registros("operativos_chagas");

// abre la fila
echo "<tr>";
echo "<td><b>Operativos de Calidad: </b></td>";
echo "<td align='right'>" . number_format($registros, 0, ",", ".") . "</td>";
echo "</tr>";

// determina el número de responsables (histórico)
$registros = $reporte->Registros("responsables");

// abre la fila
echo "<tr>";
echo "<td><b>Responsables Registrados (histórico): </b></td>";
echo "<td align='right'>" . number_format($registros, 0, ",", ".") . "</td>";
echo "</tr>";

// cierra el cuerpo
echo "</tbody>";

// cierra la tabla
echo "</table>";

// insertamos un separador
echo "<br>";

// presentamos el título
echo "<h2>Estadísticas Interanuales de Chagas</h2>";

// insertamos un separador
echo "<br>";

// definimos la tabla
echo "<table width='50%' align='center' border='0'>";

// abre el cuerpo
echo "<tbody>";

// abre la fila y presenta el título
echo "<tr>";
echo "<td colspan='2'><b>Laboratorios participantes por Año</b></td>";
echo "<tr>";

// presentamos el título
echo "<tr>";
echo "<td><b>Año</b></td>";
echo "<td><b>Laboratorios</td>";

// obtenemos la tabla de participantes
$resultado = $reporte->getLaboratoriosAnios();

// iniciamos un bucle
foreach ($resultado AS $fila){

    // obtenemos el registro
    extract($fila);

    // abrimos la fila y presentamos el registro
    echo "<tr>";
    echo "<td>$anio</td>";
    echo "<td>$laboratorios</td>";
    echo "</tr>";

}

// cierra la tabla
echo "</tbody></table>";

// presenta un separador
echo "<br>";

// abre la tabla
echo "<table width='50%' align='center' border='0'>";

// define el cuerpo
echo "<tbody>";

// define el título
echo "<tr>";
echo "<td colspan='5'><b>";
echo "Número de Laboratorios Según Técnicas Utilizadas por Año";
echo "</b></td>";
echo "</tr>";

// define los encabezados
echo "<tr>";
echo "<td><b>Año</b></td>";
echo "<td align='center'><b>1 Técnica</b></td>";
echo "<td align='center'><b>2 Técnicas</b></td>";
echo "<td align='center'><b>3 Técnicas</b></td>";
echo "<td align='center'><b>4 Técnicas</b></td>";
echo "</tr>";

// ahora obtenemos la matriz
$tecnicas = $reporte->getTecnicasAnio();

// recorremos el vector
foreach ($tecnicas as $anio => $datos){

    // abre la fila
    echo "<tr>";

    // presenta el año
    echo "<td>$anio</td>";

    // si existen con una técnica
    if (array_key_exists("Tecnica1", $datos)){

        // lo presenta
        echo "<td align='center'>" . $datos["Tecnica1"] . "</td>";

    // si está vacío
    } else {

        // presenta la celda en blanco
        echo "<td></td>";

    }

    // si existen con dos técnicas
    if (array_key_exists("Tecnica2", $datos)){

        // lo presenta
        echo "<td align='center'>" . $datos["Tecnica2"] . "</td>";

    // si está vacío
    } else {

        // presenta la celda en blanco
        echo "<td></td>";

    }

    // si existen con tres técnicas
    if (array_key_exists("Tecnica3", $datos)){

        // lo presenta
        echo "<td align='center'>" . $datos["Tecnica3"] . "</td>";

    // si está vacío
    } else {

        // presenta la celda en blanco
        echo "<td></td>";

    }

    // si existen con cuatro técnicas
    if (array_key_exists("Tecnica4", $datos)){

        // lo presenta
        echo "<td align='center'>" . $datos["Tecnica4"] . "</td>";

    // si está vacío
    } else {

        // presenta la celda en blanco
        echo "<td></td>";

    }

    // cierra la fila
    echo "</tr>";

}

// cierra la tabla
echo "</tbody></table>";

// inserta un separador
echo "<br>";

// presenta el título de técnicas que utilizan cada laboratorio
echo "<table width='50%' align='center' border ='0'>";

// abre el cuerpo
echo "<tbody>";

// presenta el título
echo "<tr>";
echo "<td colspan='7'>";
echo "<b>Número de Determinaciones Realizadas según Técnica</b>";
echo "</td></tr>";

// presenta los títulos
echo "<tr>";
echo "<td align='center'><b>Año</b></td>";
echo "<td align='center'><b>AP</b></td>";
echo "<td align='center'><b>HAI</b></td>";
echo "<td align='center'><b>ELISA</b></td>";
echo "<td align='center'><b>IFI</b></td>";
echo "<td align='center'><b>QUIMIO</b></td>";
echo "<td align='center'><b>Otra</b></td>";
echo "</tr>";

// obtenemos la matriz
$tecnicas = $reporte->getMetodosAnio();

// ahora recorremos la matriz
foreach ($tecnicas as $anio => $datos){

    // abre la fila
    echo "<tr>";

    // presenta el año
    echo "<td>$anio</td>";

    // si existen determinaciones AP
    if (array_key_exists("AP", $datos)){

        // lo presenta
        echo "<td align='center'>" . $datos["AP"] . "</td>";

    // si está vacío
    } else {

        // presenta la celda en blanco
        echo "<td></td>";

    }

    // si existen determinaciones HAI
    if (array_key_exists("HAI", $datos)){

        // lo presenta
        echo "<td align='center'>" . $datos["HAI"] . "</td>";

    // si está vacío
    } else {

        // presenta la celda en blanco
        echo "<td></td>";

    }

    // si existen determinaciones ELISA
    if (array_key_exists("ELISA", $datos)){

        // lo presenta
        echo "<td align='center'>" . $datos["ELISA"] . "</td>";

    // si está vacío
    } else {

        // presenta la celda en blanco
        echo "<td></td>";

    }

    // si existen determinaciones IFI
    if (array_key_exists("IFI", $datos)){

        // lo presenta
        echo "<td align='center'>" . $datos["IFI"] . "</td>";

    // si está vacío
    } else {

        // presenta la celda en blanco
        echo "<td></td>";

    }

    // si existen determinaciones QUIMIO
    if (array_key_exists("QUIMIOLUMINISCENCIA", $datos)){

                // lo presenta
                echo "<td align='center'>" . $datos["QUIMIOLUMINISCENCIA"] . "</td>";

            // si está vacío
            } else {

                // presenta la celda en blanco
                echo "<td></td>";

            }

    // si existen OTRAS determinaciones
    if (array_key_exists("OTRA", $datos)){

        // lo presenta
        echo "<td align='center'>" . $datos["OTRA"] . "</td>";

    // si está vacío
    } else {

        // presenta la celda en blanco
        echo "<td></td>";

    }

    // cierra la fila
    echo "</tr>";

}

// cierra la tabla
echo "</tbody></table>";

// inserta un separador
echo "<br>";

// ahora presentamos las determinaciones lector / visual
echo "<table width='40%' align='center' border='0'>";

// abre el cuerpo de la tabla
echo "<tbody>";

// presenta el título
echo "<tr>";
echo "<td colspan='3'>";
echo "<b>Métodos Utilizados en ELISA</b>";
echo "</td></tr>";

// presenta los títulos
echo "<tr>";
echo "<td><b>Año</b></td>";
echo "<td align='center'><b>Lector</b></td>";
echo "<td align='center'><b>Visual</b></td>";

// obtenemos la matriz
$metodos = $reporte->getLectorVisual();

// ahora recorremos la matriz
foreach ($metodos as $anio => $datos){

    // abre la fila
    echo "<tr>";

    // presenta el año
    echo "<td>$anio</td>";

    // si existen determinaciones con lector
    if (array_key_exists("Lector", $datos)){

        // lo presenta
        echo "<td align='center'>" . $datos["Lector"] . "</td>";

    // si está vacío
    } else {

        // presenta la celda en blanco
        echo "<td></td>";

    }

    // si existen determinaciones visuales
    if (array_key_exists("Visual", $datos)){

        // lo presenta
        echo "<td align='center'>" . $datos["Visual"] . "</td>";

    // si está vacío
    } else {

        // presenta la celda en blanco
        echo "<td></td>";

    }

    // cierra la fila
    echo "</tr>";

}

// cierra la tabla
echo "</tbody></table>";

// insertamos un separador
echo "<br>";

// ahora presentamos la eficiencia general
echo "<table width='40%' align='center' border='0'>";

// definimos el cuerpo
echo "<tbody>";

// presentamos el título
echo "<tr>";
echo "<td colspan='4'>";
echo "<b>Eficiencia General Interanual</b>";
echo "</td></tr>";

// presentamos los encabezados
echo "<tr>";
echo "<td><b>Año</b>";
echo "<td align='center'><b>Determinaciones</b></td>";
echo "<td align='center'><b>Correctas</b></td>";
echo "<td align='center'><b>Eficiencia</b></td>";
echo "</tr>";

// obtenemos la matriz de eficiencia
$eficiencia = $reporte->getEficiencia();

// ahora recorremos la matriz
foreach ($eficiencia as $anio => $datos){

    // abre la fila
    echo "<tr>";

    // presenta el año
    echo "<td>$anio</td>";

    // si hay determinaciones
    if (array_key_exists("Determinaciones", $datos)){

        // lo presenta
        echo "<td align='center'>" . $datos["Determinaciones"] . "</td>";

    // si no hubo determinaciones (casi imposible pero para
    // verificar los errores
    } else {

        // deja la celda en blanco
        echo "<td></td>";

    }

    // si hay correctas
    if (array_key_exists("Correctas", $datos)){

        // lo presenta
        echo "<td align='center'>" . $datos["Correctas"] . "</td>";

    // si no hubo correctas
    } else {

        // deja la celda en blanco
        echo "<td></td>";

    }

    // si hubo determinaciones y también hubo determinaciones correctas
    if (array_key_exists("Correctas", $datos) && array_key_exists("Determinaciones", $datos)){

        // calcula la eficiencia y la formatea
        $correctas = $datos["Correctas"] / $datos["Determinaciones"];
        $correctas = $correctas * 100;
        $correctas = number_format($correctas, 2, ",", ".");

        // lo presenta
        echo "<td align='center'>$correctas</td>";

    // si alguno de los valores fue indeterminado
    } else {

        // presenta la celda en blanco
        echo "<td></td>";

    }

    // cierra la fila
    echo "</tr>";

}

// cerramos la tabla
echo "</tbody></table>";

// inserta un separador
echo "<br>";

// ahora presentamos la eficiencia según técnica
echo "<table width='60%' align='center' border='0'>";

// definimos el cuerpo
echo "<tbody>";

// definimos el título
echo "<tr>";
echo "<td colspan='7'>";
echo "<b>Eficiencia Interanual según Técnica</b>";
echo "</td></tr>";

// definimos los títulos
echo "<tr>";
echo "<td><b>Año</b></td>";
echo "<td align='center'><b>AP</b></td>";
echo "<td align='center'><b>HAI</b></td>";
echo "<td align='center'><b>ELISA</b></td>";
echo "<td align='center'><b>IFI</b></td>";
echo "<td align='center'><b>QUIMIO</b></td>";
echo "<td align='center'><b>Otra</b></td>";

// obtenemos la tabla de eficiencia
$eficiencia = $reporte->getEficienciaTecnicas();

// ahora recorremos la matriz
foreach ($eficiencia as $anio => $datos){

    // abrimos la fila
    echo "<tr>";

    // agregamos el año
    echo "<td>$anio</td>";

    // si hubo determinaciones ap y determinaciones
    // correctas ap
    if (array_key_exists("CorrectasAP", $datos) && array_key_exists("DeterminacionesAP", $datos)){

        // calcula la eficiencia y la formatea
        $correctas = $datos["CorrectasAP"] / $datos["DeterminacionesAP"];
        $correctas = $correctas * 100;
        $correctas = number_format($correctas, 2, ",", ".");

        // lo presenta
        echo "<td align='center'>$correctas</td>";

    } else {

        // presenta la celda en blanco
        echo "<td></td>";

    }

    // si hubo determinaciones hai y determinaciones
    // correctas hai
    if (array_key_exists("CorrectasHAI", $datos) && array_key_exists("DeterminacionesHAI", $datos)){

        // calcula la eficiencia y la formatea
        $correctas = $datos["CorrectasHAI"] / $datos["DeterminacionesHAI"];
        $correctas = $correctas * 100;
        $correctas = number_format($correctas, 2, ",", ".");

        // lo presenta
        echo "<td align='center'>$correctas</td>";

    } else {

        // presenta la celda en blanco
        echo "<td></td>";

    }

    // si hubo determinaciones elisa y determinaciones
    // correctas elisa
    if (array_key_exists("CorrectasELISA", $datos) && array_key_exists("DeterminacionesELISA", $datos)){

        // calcula la eficiencia y la formatea
        $correctas = $datos["CorrectasELISA"] / $datos["DeterminacionesELISA"];
        $correctas = $correctas * 100;
        $correctas = number_format($correctas, 2, ",", ".");

        // lo presenta
        echo "<td align='center'>$correctas</td>";

    } else {

        // presenta la celda en blanco
        echo "<td></td>";

    }

    // si hubo determinaciones ifi y determinaciones
    // correctas ifi
    if (array_key_exists("CorrectasIFI", $datos) && array_key_exists("DeterminacionesIFI", $datos)){

        // calcula la eficiencia y la formatea
        $correctas = $datos["CorrectasIFI"] / $datos["DeterminacionesIFI"];
        $correctas = $correctas * 100;
        $correctas = number_format($correctas, 2, ",", ".");

        // lo presenta
        echo "<td align='center'>$correctas</td>";

    } else {

        // presenta la celda en blanco
        echo "<td></td>";

    }

    // si hubo determinaciones quimio y determinaciones
    // correctas quimio
    if (array_key_exists("CorrectasQUIMIO", $datos) && array_key_exists("DeterminacionesQUIMIO", $datos)){

        // calcula la eficiencia y la formatea
        $correctas = $datos["CorrectasQUIMIO"] / $datos["DeterminacionesQUIMIO"];
        $correctas = $correctas * 100;
        $correctas = number_format($correctas, 2, ",", ".");

        // lo presenta
        echo "<td align='center'>$correctas</td>";

    } else {

        // presenta la celda en blanco
        echo "<td></td>";

    }

    // si hubo otras determinaciones y determinaciones correctas
    if (array_key_exists("CorrectasOTRA", $datos) && array_key_exists("DeterminacionesOTRA", $datos)){

        // calcula la eficiencia y la formatea
        $correctas = $datos["CorrectasAP"] / $datos["DeterminacionesAP"];
        $correctas = $correctas * 100;
        $correctas = number_format($correctas, 2, ",", ".");

        // lo presenta
        echo "<td align='center'>$correctas</td>";

    } else {

        // presenta la celda en blanco
        echo "<td></td>";

    }

    // cerramos la fila
    echo "</tr>";

}

// cerramos la tabla
echo "</tbody></table>";

// presentamos el botón exportar
echo "<p align='center'>";
echo "<input type='button'
                name='btnExportar'
                id='btnExportar'
                value='Exportar'
                title='Exporta la tabla a Hoja de Cálculo'
                class='boton_analitica'
                onClick='reportes.xlsEstadisticas()'>";
echo "</p>";

?>