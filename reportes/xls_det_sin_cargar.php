<?php

/**
 *
 * reportes/xls_det_sin_cargar.php
 *
 * @package     CCE
 * @subpackage  Reportes
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.4.0 (03/09/2018)
 * @copyright   Copyright (c) 2018, INP
 *
 * Procedimiento que genera un xls con la nómina de laboratorios
 * que no han cargado determinaciones
 *
*/

// incluimos e instanciamos las clases
require_once ("reportes.class.php");
require_once ("../clases/phpexcel/PHPExcel.php");
$reporte = new Reportes();
$hoja = new PHPExcel();

// fijamos las propiedades
$hoja->getProperties()->setCreator("Lic. Claudio Invernizzi")
					  ->setLastModifiedBy("Lic. Claudio Invernizzi")
					  ->setTitle("Determinaciones Sin Cargar")
					  ->setSubject("Determinaciones Sin Cargar")
					  ->setDescription("Nómina de laboratorios sin cargar determinaciones")
					  ->setKeywords("Control Calidad")
					  ->setCategory("Reportes");

// obtenemos la nómina de laboratorios
$laboratorios = $reporte->totalesSinCargar();

// si hubo registros
if (count($laboratorios) != 0){

    // leemos la plantilla
    $hoja = PHPExcel_IOFactory::load("../clases/phpexcel/plantilla.xls");

    // establecemos el ancho de las columnas
    $hoja->getActiveSheet()->getColumnDimension('A')->setWidth(25);
    $hoja->getActiveSheet()->getColumnDimension('B')->setWidth(70);
    $hoja->getActiveSheet()->getColumnDimension('C')->setWidth(15);

    // contador de filas
    $fila = 8;

    // switch de cambio de operativo
    $operativo_ant = "";

    // fijamos el estilo del título
    $estilo = array(
        'font'  => array(
            'bold'  => true,
            'size'  => 12,
            'name'  => 'Verdana'
        ));

    // fijamos el estilo de los encabezados
    $encabezado = array(
        'font'  => array(
            'bold'  => true,
            'size'  => 10,
            'name'  => 'Verdana'
        ));

    // centramos la columna de las determinaciones
    $hoja->getActiveSheet()->getStyle('C')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

    // presenta el título
    $hoja->setActiveSheetIndex(0)
         ->setCellValue('B3', 'Sistema de Control de Calidad Externo');
    $hoja->setActiveSheetIndex(0)
         ->setCellValue('B5', 'Laboratorios sin Cargar Determinaciones');

    // centramos las celdas de los títulos
    $hoja->getActiveSheet()->getStyle('B3')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    $hoja->getActiveSheet()->getStyle('B5')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

    // fijamos el estilo
    $hoja->getActiveSheet()->getStyle('B3')->applyFromArray($estilo);
    $hoja->getActiveSheet()->getStyle('B5')->applyFromArray($estilo);

    // establecemos la fuente
    $hoja->getDefaultStyle()->getFont()->setName('Arial')
         ->setSize(10);

    // iniciamos un bucle recorriendo el vector
    foreach ($laboratorios AS $registro){

        // obtenemos el registro
        extract($registro);

        // si cambió el operativo
        if ($operativo_ant != $operativo){

            // llamamos los encabezados
            imprimeEncabezado($operativo);

            // asignamos a la variable control
            $operativo_ant = $operativo;

        }

        // presenta el registro
        $hoja->setActiveSheetIndex(0)
             ->setCellValue('A' . $fila , $jurisdiccion)
             ->setCellValue('B' . $fila , $laboratorio);

        // incrementamos el contador
        $fila++;

    }

    // renombramos la hoja
    $hoja->getActiveSheet()->setTitle('Sin Cargar');

    // fijamos la primer hoja como activa para abrirla predeterminada
    $hoja->setActiveSheetIndex(0);

    // creamos el writer y lo dirigimos al navegador en formato 2007
    header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
    header('Content-Disposition: attachment;filename="sincargar.xlsx"');
    header('Cache-Control: max-age=0');
    header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // fecha en el pasado
    header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // siempre modificado
    header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
    header ('Pragma: public'); // HTTP/1.0
    $Writer = PHPExcel_IOFactory::createWriter($hoja, 'Excel2007');
    $Writer->save('php://output');

}

/**
 * Función que recibe como parámetros el objeto con la hoja de
 * cálculo, la fila actual y el operativo, presenta los encabezados
 * y retorna la fila actual
 * @author Claudio Invernizzi <cinvernizzi@gmail.com>
 * @param string $operativo - cadena con el número de operativo
 */
function imprimeEncabezado($operativo){

    // definimos las variables globales
    global $hoja, $encabezado, $fila;

    // incrementamos la fila
    $fila = $fila + 2;

    // presenta el operativo
    $hoja->setActiveSheetIndex(0)
          ->setCellValue('A' . $fila, 'Operativo N°: ' . $operativo);

    // establecemos la fuente de los encabezados
    $hoja->getActiveSheet()->getStyle('A' . $fila)->applyFromArray($encabezado);

    // incrementamos la fila
    $fila++;

    // presenta los encabezados
    $hoja->setActiveSheetIndex(0)
         ->setCellValue('A' . $fila, 'Jurisdicción')
         ->setCellValue('B' . $fila, 'Laboratorio');

    // establecemos la fuente de los encabezados
    $hoja->getActiveSheet()->getStyle('A' . $fila)->applyFromArray($encabezado);
    $hoja->getActiveSheet()->getStyle('B' . $fila)->applyFromArray($encabezado);

    // volvemos a incrementar la fila
    $fila++;

}

?>