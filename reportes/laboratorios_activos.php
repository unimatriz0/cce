<?php

/**
 *
 * reportes/laboratorios_activos.php
 *
 * @package     CCE
 * @subpackage  Reportes
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.4.0 (18/08/2018)
 * @copyright   Copyright (c) 2018, INP
 *
 * Procedimiento que presenta la grilla con los laboratorios activos de
 * la jurisdicción del usuario y presenta la información de quien
 * recibe las muestras
 *
*/

// incluimos e instanciamos las clases
require_once ("laboratorios.class.php");
$laboratorios = new Laboratorios();

// obtenemos la jurisdiccion del usuario
session_start();
if (isset($_SESSION["Jurisdiccion"])){
    $jurisdiccion = $_SESSION["Jurisdiccion"];
}
session_write_close();

// obtenemos la nomina de laboratorios
$nominaLaboratorios = $laboratorios->laboratoriosParticipantes($jurisdiccion);

// presentamos el titulo
echo "<p align='justify'> ";
echo "Esta es la nomina de laboratorios activos en la Jurisdicción de ";
echo "$jurisdiccion, indicando tambien a quien se enviaran las muestras ";
echo "verifique se encuentren todos y en caso de ser necesario puede ";
echo "modificar el registro a través de la pestaña correspondiente. ";
echo "&nbsp;&nbsp;";

// presentamos el botón
echo "<input type='button' name='btnNota'
       id='btnNota'
       value='Generar'
       class='boton_confirmar'
       onClick='generarNota()'>";

// cerramos el párrafo
echo "</p>";

// definimos la tabla
echo "<table width='90%' align='center' border='0'
       id='laboratorios_activos'>";

// definimos los encabezados
echo "<thead>";
echo "<tr>";
echo "<th>Laboratorio</th>";
echo "<th>Localidad</th>";
echo "<th>Dirección</th>";
echo "<th>Dependencia</th>";
echo "<th>Recibe</th>";
echo "<th></th>";
echo "</tr>";
echo "</thead>";

// definimos el cuerpo de la tabla
echo "<tbody>";

// recorremos el vector
foreach($nominaLaboratorios AS $registro){

    // obtenemos el registro y lo presentamos
    extract($registro);

    // lo presentamos
    echo "<tr>";
    echo "<td>$laboratorio</td>";
    echo "<td>$localidad</td>";
    echo "<td>$direccion</td>";
    echo "<td>$dependencia</td>";

    // si el laboratorio recibe directamente las muestras
    if ($recibe == "Si"){
        echo "<td>Laboratorio</td>";
    } else {
        echo "<td>$recibe_muestras</td>";
    }

    // presentamos el enlace para mostrar el registro
    // en el tab de laboratorios entonces el usuario
    // puede editar el laboratorio y volver al listado
    echo "<td>";
    echo "<input type='button'
           name='btnVer'
           class='botoneditar'
           onClick='muestraLaboratorio($id_laboratorio)'>";
    echo "</td>";
    echo "</td>";

    // cerramos la fila
    echo "</tr>";

}

// cerramos la tabla
echo "</tbody></table>";

// define el div para el paginador
echo "<div class='paging'></div>";

?>
<SCRIPT>

    // definimos las propiedades de la tabla
     $('#laboratorios_activos').datatable({
        pageSize: 15,
        sort:    [true, true, true, true,     true,     false],
        filters: [true, true, true, 'select', 'select', false],
        filterText: 'Buscar ... '
    });

</SCRIPT>
