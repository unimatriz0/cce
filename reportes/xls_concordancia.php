<?php

/**
 *
 * reportes/xls_concordancia.php
 *
 * @package     CCE
 * @subpackage  Reportes
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.4.0 (10/09/2018)
 * @copyright   Copyright (c) 2018, INP
 *
 * Procedimiento que recibe por get la clave de un laboratorio y
 * genera la hoja de cálculo con la concordancia histórica
 *
*/

// incluimos e instanciamos las clases
require_once ("reportes.class.php");
require_once ("../clases/phpexcel/PHPExcel.php");
require_once ("../laboratorios/laboratorios.class.php");
$reporte = new Reportes();
$hoja = new PHPExcel();
$laboratorio = new Laboratorios();

// obtenemos el nombre del laboratorio
$laboratorio->getDatosLaboratorio($_GET["laboratorio"]);
$nombre = $laboratorio->getNombre();

// obtenemos la nómina de determinaciones realizadas
$determinaciones = $reporte->concordanciaLaboratorio($_GET["laboratorio"]);

// fijamos las propiedades
$hoja->getProperties()->setCreator("Lic. Claudio Invernizzi")
					  ->setLastModifiedBy("Lic. Claudio Invernizzi")
					  ->setTitle("Concordancia Histórica")
					  ->setSubject("Concordancia Histórica")
					  ->setDescription("Informe histórico de la concordancia")
					  ->setKeywords("Control Calidad")
					  ->setCategory("Reportes");

// contador de filas
$fila = 10;

// switch de cambio de año
$anio_actual = "";

// contadores de determinaciones
$total_determinaciones = 0;
$total_correctas = 0;

// fijamos el estilo del título
$estilo = array(
    'font'  => array(
        'bold'  => true,
        'size'  => 12,
        'name'  => 'Verdana'
    ));

// fijamos el estilo de los encabezados
$encabezado = array(
    'font'  => array(
        'bold'  => true,
        'size'  => 10,
        'name'  => 'Verdana'
    ));

// si hay determinaciones
if (count($determinaciones) != 0){

    // leemos la plantilla
    $hoja = PHPExcel_IOFactory::load("../clases/phpexcel/plantilla.xls");

    // establecemos el ancho de las columnas
    $hoja->getActiveSheet()->getColumnDimension('A')->setWidth(15);
    $hoja->getActiveSheet()->getColumnDimension('B')->setWidth(25);
    $hoja->getActiveSheet()->getColumnDimension('C')->setWidth(15);
    $hoja->getActiveSheet()->getColumnDimension('D')->setWidth(10);
    $hoja->getActiveSheet()->getColumnDimension('E')->setWidth(10);
    $hoja->getActiveSheet()->getColumnDimension('F')->setWidth(10);
    $hoja->getActiveSheet()->getColumnDimension('G')->setWidth(10);
    $hoja->getActiveSheet()->getColumnDimension('H')->setWidth(15);
    $hoja->getActiveSheet()->getColumnDimension('I')->setWidth(15);
    $hoja->getActiveSheet()->getColumnDimension('J')->setWidth(15);
    $hoja->getActiveSheet()->getColumnDimension('K')->setWidth(15);

    // presenta el título y el laboratorio
    $hoja->setActiveSheetIndex(0)
        ->setCellValue('B3', 'Sistema de Control de Calidad Externo');
    $hoja->setActiveSheetIndex(0)
        ->setCellValue('B5', 'Análisis de Concordancia');
    $hoja->setActiveSheetIndex(0)
         ->setCellValue('B7', $nombre);

    // centramos las celdas de los títulos
    $hoja->getActiveSheet()->getStyle('B3')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    $hoja->getActiveSheet()->getStyle('B5')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    $hoja->getActiveSheet()->getStyle('B7')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

    // fijamos el estilo
    $hoja->getActiveSheet()->getStyle('B3')->applyFromArray($estilo);
    $hoja->getActiveSheet()->getStyle('B5')->applyFromArray($estilo);
    $hoja->getActiveSheet()->getStyle('B7')->applyFromArray($estilo);

    // unimos las celdas
    $hoja->getActiveSheet()->mergeCells('B3:F3');
    $hoja->getActiveSheet()->mergeCells('B5:F5');
    $hoja->getActiveSheet()->mergeCells('B7:F7');

    // establecemos la fuente
    $hoja->getDefaultStyle()->getFont()->setName('Arial')
         ->setSize(10);

    // renombramos la hoja
    $hoja->getActiveSheet()->setTitle('Determinaciones');

    // presentamos el total de determinaciones realizadas
    $hoja->setActiveSheetIndex(0)
         ->setCellValue('A' . $fila, 'Total Determinaciones Realizadas:');
    $hoja->setActiveSheetIndex(0)
         ->setCellValue('C' . $fila, count($determinaciones));

    // fijamos el estilo
    $hoja->getActiveSheet()->getStyle('A' . $fila)->applyFromArray($encabezado);
    $hoja->getActiveSheet()->getStyle('C' . $fila)->applyFromArray($encabezado);

    // incrementamos el contador
    $fila++;

    // recorremos el vector
    foreach($determinaciones AS $registro){

        // obtenemos el registro
        extract($registro);

        // si cambió el año
        if ($anio != $anio_actual){

            // si no es el primero
            if ($anio_actual != ""){

                // cerramos el ejercicio
                cierraAnio();

                // inicializamos los contadores
                $total_determinaciones = 0;
                $total_correctas = 0;

            }

            // presenta los encabezados
            titulosColumna($anio);

            // asignamos el nuevo valor
            $anio_actual = $anio;

        }

        // presentamos el registro
        $hoja->setActiveSheetIndex(0)
              ->setCellValue('A' . $fila, $operativo);
        $hoja->setActiveSheetIndex(0)
             ->setCellValue('B' . $fila, $marca);
        $hoja->setActiveSheetIndex(0)
             ->setCellValue('C' . $fila, $lote);
        $hoja->setActiveSheetIndex(0)
             ->setCellValue('D' . $fila, $vencimiento);
        $hoja->setActiveSheetIndex(0)
             ->setCellValue('E' . $fila, $muestra_nro);

        // si declaró valor de corte
        if ($valor_corte != 0){
            $hoja->setActiveSheetIndex(0)
                ->setCellValue('F' . $fila, $valor_corte);
        }

        // si declaró valor de lectura
        if ($valor_lectura != 0){
            $hoja->setActiveSheetIndex(0)
                ->setCellValue('G' . $fila, $valor_lectura);
        }

        // seguimos cargando
        $hoja->setActiveSheetIndex(0)
             ->setCellValue('H' . $fila, $tecnica);

        // si la técnica es elisa
        if ($tecnica == "ELISA"){
            $hoja->setActiveSheetIndex(0)
                 ->setCellValue('I' . $fila, $metodo);
        }

        // seguimos cargando
        $hoja->setActiveSheetIndex(0)
             ->setCellValue('J' . $fila, $determinacion);
        $hoja->setActiveSheetIndex(0)
             ->setCellValue('K' . $fila, $correcto);
        $hoja->setActiveSheetIndex(0)
             ->setCellValue('L' . $fila, $usuario);

        // centramos las columnas correspondientes
        $hoja->getActiveSheet()->getStyle('A' . $fila)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $hoja->getActiveSheet()->getStyle('C' . $fila)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $hoja->getActiveSheet()->getStyle('D' . $fila)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $hoja->getActiveSheet()->getStyle('E' . $fila)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $hoja->getActiveSheet()->getStyle('F' . $fila)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $hoja->getActiveSheet()->getStyle('G' . $fila)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

        // incrementa la fila
        $fila++;

        // incrementamos el contador de determinaciones
        $total_determinaciones++;

        // si coincide el resultado con el correcto o si
        // es indeterminado lo sumamos
        if ($determinacion == $correcto){
            $total_correctas++;
        } elseif ($determinacion == "Reactivo" && $correcto == "Indeterminado"){
            $total_correctas++;
        } elseif ($determinacion == "Indeterminado" && $correcto == "Reactivo"){
            $total_correctas++;
        }

    }

    // cerramos el último ejercicio
    cierraAnio();

    // fijamos la primer hoja como activa para abrirla predeterminada
    $hoja->setActiveSheetIndex(0);

    // creamos el writer y lo dirigimos al navegador en formato 2007
    header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
    header('Content-Disposition: attachment;filename="concordancia.xlsx"');
    header('Cache-Control: max-age=0');
    header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // fecha en el pasado
    header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // siempre modificado
    header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
    header ('Pragma: public'); // HTTP/1.0
    $Writer = PHPExcel_IOFactory::createWriter($hoja, 'Excel2007');
    $Writer->save('php://output');

}

/**
 * Función que presenta los títulos de columna
 * @author Claudio Invernizzi <cinvernizzi@gmail.com>
 * @param string $anio - el año del encabezado
 */
function titulosColumna($anio){

    // declaración de variables globales
    global $hoja, $fila, $encabezado;

    // incrementamos la fila
    $fila++;

    // presentamos el año del operativo
    $hoja->setActiveSheetIndex(0)
          ->setCellValue('A' . $fila, 'Año: ' . $anio);
    $hoja->getActiveSheet()->getStyle('A' . $fila)->applyFromArray($encabezado);

    // incrementamos el contador
    $fila++;

    // presentamos los encabezados
    $hoja->setActiveSheetIndex(0)
          ->setCellValue('A' . $fila, 'Operativo');
    $hoja->setActiveSheetIndex(0)
          ->setCellValue('B' . $fila, 'Marca');
    $hoja->setActiveSheetIndex(0)
          ->setCellValue('C' . $fila, 'Lote N°');
    $hoja->setActiveSheetIndex(0)
          ->setCellValue('D' . $fila, 'Venc.');
    $hoja->setActiveSheetIndex(0)
          ->setCellValue('E' . $fila, 'Muestra');
    $hoja->setActiveSheetIndex(0)
          ->setCellValue('F' . $fila, 'Corte');
    $hoja->setActiveSheetIndex(0)
          ->setCellValue('G' . $fila, 'Lectura');
    $hoja->setActiveSheetIndex(0)
          ->setCellValue('H' . $fila, 'Técnica');
    $hoja->setActiveSheetIndex(0)
          ->setCellValue('I' . $fila, 'Método');
    $hoja->setActiveSheetIndex(0)
          ->setCellValue('J' . $fila, 'Resultado');
    $hoja->setActiveSheetIndex(0)
          ->setCellValue('K' . $fila, 'Correcto');
    $hoja->setActiveSheetIndex(0)
          ->setCellValue('L' . $fila, 'Usuario');

    // fijamos el formato
    $hoja->getActiveSheet()->getStyle('A' . $fila)->applyFromArray($encabezado);
    $hoja->getActiveSheet()->getStyle('B' . $fila)->applyFromArray($encabezado);
    $hoja->getActiveSheet()->getStyle('C' . $fila)->applyFromArray($encabezado);
    $hoja->getActiveSheet()->getStyle('D' . $fila)->applyFromArray($encabezado);
    $hoja->getActiveSheet()->getStyle('E' . $fila)->applyFromArray($encabezado);
    $hoja->getActiveSheet()->getStyle('F' . $fila)->applyFromArray($encabezado);
    $hoja->getActiveSheet()->getStyle('G' . $fila)->applyFromArray($encabezado);
    $hoja->getActiveSheet()->getStyle('H' . $fila)->applyFromArray($encabezado);
    $hoja->getActiveSheet()->getStyle('I' . $fila)->applyFromArray($encabezado);
    $hoja->getActiveSheet()->getStyle('J' . $fila)->applyFromArray($encabezado);
    $hoja->getActiveSheet()->getStyle('K' . $fila)->applyFromArray($encabezado);
    $hoja->getActiveSheet()->getStyle('L' . $fila)->applyFromArray($encabezado);

    // centramos las columnas correspondientes
    $hoja->getActiveSheet()->getStyle('A' . $fila)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    $hoja->getActiveSheet()->getStyle('C' . $fila)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    $hoja->getActiveSheet()->getStyle('D' . $fila)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    $hoja->getActiveSheet()->getStyle('E' . $fila)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    $hoja->getActiveSheet()->getStyle('F' . $fila)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    $hoja->getActiveSheet()->getStyle('G' . $fila)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

    // incrementa la fila
    $fila++;

}

/**
 * Método llamado al cambiar de año o al finalizar el reporte
 * que presenta los totales de eficiencia por período
 * @author Claudio Invernizzi <cinvernizzi@gmail.com>
 */
function cierraAnio(){

    // declaración de variables globales
    global $hoja, $encabezado, $total_determinaciones, $total_correctas, $fila;

    // incrementamos la fila
    $fila++;

    // presentamos el total de determinaciones
    $hoja->setActiveSheetIndex(0)
          ->setCellValue('A' . $fila, 'Total Determinaciones:' . $total_determinaciones);
    $hoja->getActiveSheet()->getStyle('A' . $fila)->applyFromArray($encabezado);

    // incrementamos la fila
    $fila++;

    // presentamos el total de correctas
    $hoja->setActiveSheetIndex(0)
          ->setCellValue('A' . $fila, 'Total Correctas: ' . $total_correctas);
    $hoja->getActiveSheet()->getStyle('A' . $fila)->applyFromArray($encabezado);

    // incrementamos la fila
    $fila++;

    // presentamos la concordancia
    $concordancia = $total_correctas / $total_determinaciones;
    $hoja->setActiveSheetIndex(0)
          ->setCellValue('A' . $fila, 'Concordancia: ' . round($concordancia,2));
    $hoja->getActiveSheet()->getStyle('A' . $fila)->applyFromArray($encabezado);
    $hoja->getActiveSheet()->getStyle('A' . $fila)->getNumberFormat()->setFormatCode("#,###.##");

    // incrementamos la fila
    $fila++;

}
?>