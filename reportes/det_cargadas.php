<?php

/**
 *
 * reportes/det_cargadas.php
 *
 * @package     CCE
 * @subpackage  Reportes
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.4.0 (18/08/2018)
 * @copyright   Copyright (c) 2018, INP
 *
 * Procedimiento que presenta la grilla con los laboratorios con determinaciones
 * cargadas
 *
*/

// incluimos e instanciamos la clase
require_once ("reportes.class.php");
$reporte = new Reportes();

// obtenemos la nómina de laboratorios
$laboratorios = $reporte->determinacionesCargadas($_GET["operativo"]);

// si hubo registros
if (count($laboratorios) != 0){

    // definimos la tabla
    echo "<table width='90%' align='center' border='0' id='det_cargadas'>";

    // definimos el encabezado
    echo "<thead>";
    echo "<tr>";
    echo "<th align='left'>Jurisdicción</th>";
    echo "<th align='left'>Laboratorio</th>";
    echo "<th align='left'>Det.</th>";
    echo "</tr>";
    echo "</thead>";

    // definimos el cuerpo de la tabla
    echo "<tbody>";

    // iniciamos un bucle recorriendo el vector
    foreach ($laboratorios AS $registro){

        // obtenemos el registro
        extract($registro);

        // lo agregamos
        echo "<tr>";
        echo "<td>$jurisdiccion</td>";
        echo "<td>$laboratorio</td>";
        echo "<td align='center'>$determinaciones</td>";
        echo "</tr>";

    }

    // cerramos la tabla
    echo "</tbody>";
    echo "</table>";

    // insertamos un separador
    echo "<br>";

    // define el div para el paginador
    echo "<div class='paging'></div>";

    // presentamos el botón exportar
    echo "<p align='center'>";
    echo "<input type='button'
                 name='btnExportar'
                 id='btnExportar'
                 value='Exportar'
                 title='Exporta la tabla a Hoja de Cálculo'
                 class='boton_analitica'
                 onClick='reportes.xlsCargadas()'>";
    echo "</p>";

// si no hubo registros
} else {

    // presentamos el mensaje
    echo "<h2>No hay determinaciones cargadas en el Operativo indicado";

}
?>
<script>

    // definimos las propiedades de la tabla
    $('#det_cargadas').datatable({
        pageSize: 15,
        sort: [true, true, true],
        filters: ['select', true, false],
        filterText: 'Buscar ... '
    });

</script>
