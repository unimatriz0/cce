<?php

/**
 *
 * jurisdicciones/verifica_jurisdiccion.php
 *
 * @package     CCE
 * @subpackage  Jurisdicciones
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.4.0 (09/08/2018)
 * @copyright   Copyright (c) 2018, INP
 *
 * Método que recibe por get el nombre de una jurisdicción y la
 * clave del país, y verifica si existe, retorna el número de
 * registros encontrados
*/

// incluimos e instanciamos las clases
require_once ("jurisdicciones.class.php");
$jurisdicciones = new Jurisdicciones();

// verificamos si existe
$registros = $jurisdicciones->validaJurisdiccion($_GET["provincia"], $_GET["idpais"]);

// retornamos el resultado de la operación
echo json_encode(array("Registros" => $registros));

?>