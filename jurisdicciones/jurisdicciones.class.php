<?php

/**
 *
 * Class Jurisdicciones | jurisdicciones/jurisdicciones.class.php
 *
 * @package     CCE
 * @subpackage  Jurisdicciones
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.4.0 (09/08/2018)
 * @copyright   Copyright (c) 2018, INP
 *
 * Cláse que contiene las operaciones sobre la base de datos
 * de jurisdicciones
 *
*/

// inclusión de archivos
require_once ("../clases/conexion.class.php");

// convención para la nomenclatura de las propiedades, comienzan con una
// letra mayúscula, de tener mas de una palabra no se utilizan separadores
// y la inicial de cada palabra va en mayúscula
// para las variables recibidas como parámetro el criterio es todas en
// minúscula

// convención para la nomenclatura de los metodos, comienzan con set o get
// según asignen un valor o lo lean y luego el nombre del valor a obtener

/**
 * Definición de la clase
 * @author Claudio Invernizzi <cinvernizzi@gmail.com>
 */
class Jurisdicciones{

    // declaración de variables de la clase, las declaramos
    // como protected para que puedan ser heredadas pero
    // para asignarle el valor debemos crear los métodos
    protected $IdUsuario;             // clave del usuario
    protected $IdPais;                // clave del país
    protected $IdProvincia;           // clave indec de la provincia
    protected $NombreProvincia;       // nombre de la provincia
    protected $PoblacionProvincia;    // población de la provincia
    protected $FechaAlta;             // fecha de alta del registro
    protected $Link;                  // puntero a la base de datos

    /**
     * Constructor de la clase, establece la conexión con la base
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     */
    function __construct (){

        // inicializamos las variables
        $this->IdPais = 0;
        $this->IdProvincia = "";
        $this->NombreProvincia = "";
        $this->PoblacionProvincia = 0;

        // inicializamos la fecha de alta en formato mysql
        $this->FechaAlta = date('Y/m/d');

        // abrimos la sesión y obtenemos el usuario
        session_start();
        if (!empty($_SESSION["ID"])){
            $this->IdUsuario = $_SESSION["ID"];
        }
        session_write_close();

        // nos conectamos a la base de datos
        $this->Link = new Conexion();

    }

    /**
     * Destructor de la clase, cierra la conexión con la base
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     */
    function __destruct(){

        // elimina el enlace a la base
        $this->Link = null;

    }

    // métodos de asignación de variables
    public function setIdPais($idpais){
        $this->IdPais = $idpais;
    }
    public function setNombreProvincia($provincia){
        $this->NombreProvincia = $provincia;
    }
    public function setIdProvincia($idprovincia){
        $this->IdProvincia = $idprovincia;
    }
    public function setPoblacionProvincia($poblacion){

        // verifica si es un número
        if (!is_numeric($poblacion)){

            // abandona por error
            echo "La población de la Jurisdicción debe ser un número";
            exit;

        // si es correcto
        } else {

            // lo asigna
            $this->PoblacionProvincia = $poblacion;

        }

    }

    /**
     * Método que retorna un vector con la nómina de provincias y sus
     * claves, recibe como parámetro la clave del país
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param int $idpais - clave del país
     * @return array
     */
    public function listaProvincias($idpais){

        // componemos la consulta
        $consulta = "SELECT diccionarios.v_provincias.cod_prov AS idprovincia,
                            diccionarios.v_provincias.provincia AS nombreprovincia,
                            diccionarios.v_provincias.poblacion AS poblacion,
                            diccionarios.v_provincias.usuario AS usuario,
                            diccionarios.v_provincias.fecha_alta AS fecha_alta
                     FROM diccionarios.v_provincias
                     WHERE diccionarios.v_provincias.provincia != 'Indeterminado' AND
                           diccionarios.v_provincias.idpais = '$idpais'
                     ORDER BY diccionarios.v_provincias.provincia;";

        // ejecutamos la consulta
        $resultado = $this->Link->query($consulta);

        // lo pasamos a minúsculas porque según la versión de
        // pdo lo devuelve en mayúsculas o minúsculas
        $nomina = array_change_key_case($resultado->fetchAll(PDO::FETCH_ASSOC), CASE_LOWER);

        // retornamos el vector
        return $nomina;

    }

    /**
     * Método que recibe como parámetro el nombre de una jurisdicción y
     * la clave del país, la busca en la base, retorna el número de
     * registros encontrados, usada para evitar registros duplicados en el abm
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param string $provincia - nombre de la provincia
     * @param int $idpais - clave del país
     * @return int
     */
    public function validaJurisdiccion($provincia, $idpais){

        // inicializamos las variables
        $registros = 0;

        // compone y ejecuta la consulta
        $consulta = "SELECT COUNT(diccionarios.provincias.COD_PROV) AS registros
                     FROM diccionarios.provincias
                     WHERE diccionarios.provincias.NOM_PROV = '$provincia' AND
                           diccionarios.provincias.pais = '$idpais';";
        $resultado = $this->Link->query($consulta);

        // obtenemos el registro y retornamos
        $fila = array_change_key_case($resultado->fetch(PDO::FETCH_ASSOC), CASE_LOWER);
        extract($fila);
        return $registros;

    }

    /**
     * Método que ejecuta la consulta de actualización de las jurisdicciones
     * y retorna la id del registro afectado
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param string con la acción
     * @return $int resultado de la operación
     */
    public function grabaProvincia($evento){

        // si está insertando
        if ($evento == "insertar"){

            // insertamos el registro
            $resultado = $this->nuevaProvincia();

        // si está editando
        } else {

            // editamos el registro
            $resultado = $this->editaProvincia();

        }

        // retorna el resultado de la operación
        return $resultado;

    }

    /**
     * Método protegido que graba un nuevo registro de provincia
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @return bool
     */
    protected function nuevaProvincia(){

        // crea la consulta de inserción
        $consulta = "INSERT INTO diccionarios.provincias
                            (PAIS,
                             NOM_PROV,
                             COD_PROV,
                             POBLACION,
                             USUARIO,
                             FECHA_ALTA)
                            VALUES
                            (:idpais,
                             :provincia,
                             :cod_prov,
                             :poblacion,
                             :usuario,
                             :fecha_alta);";

        // asignamos la consulta
        $psInsertar = $this->Link->prepare($consulta);

        // asigna los valores
        $psInsertar->bindParam(":idpais", $this->IdPais);
        $psInsertar->bindParam(":provincia", $this->NombreProvincia);
        $psInsertar->bindParam(":cod_prov", $this->IdProvincia);
        $psInsertar->bindParam(":poblacion", $this->PoblacionProvincia);
        $psInsertar->bindParam(":usuario", $this->IdUsuario);
        $psInsertar->bindParam(":fecha_alta", $this->FechaAlta);

        // ejecuta la consulta
        $resultado = $psInsertar->execute();

        // si hubo un error
        if ($resultado){
            return $resultado;
        } else {
            echo $resultado;
        }

    }

    /**
     * Método protegido que edita el registro de la provincia
     * retorna resultado de la operación
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @return bool
     */
    protected function editaProvincia(){

        // crea la consulta de edición
        $consulta = "UPDATE diccionarios.provincias SET
                            PAIS = :idpais,
                            NOM_PROV = :provincia,
                            POBLACION = :poblacion,
                            USUARIO = :usuario,
                            FECHA_ALTA = :fecha_alta
                     WHERE diccionarios.provincias.COD_PROV = :cod_prov;";

        // asignamos la consulta
        $psInsertar = $this->Link->prepare($consulta);

        // asigna los valores
        $psInsertar->bindParam(":idpais", $this->IdPais);
        $psInsertar->bindParam(":provincia", $this->NombreProvincia);
        $psInsertar->bindParam(":poblacion", $this->PoblacionProvincia);
        $psInsertar->bindParam(":usuario", $this->IdUsuario);
        $psInsertar->bindParam(":fecha_alta", $this->FechaAlta);
        $psInsertar->bindParam(":cod_prov", $this->IdProvincia);

        // ejecuta la consulta
        $resultado = $psInsertar->execute();

        // si salió todo bien
        if($resultado){
            return $resultado;
        } else {
            echo $resultado;
        }

    }

}
?>
