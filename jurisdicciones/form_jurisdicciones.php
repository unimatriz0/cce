<?php

/**
 *
 * jurisdicciones/form:jurisdicciones.php
 *
 * @package     CCE
 * @subpackage  Jurisdicciones
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.4.0 (09/08/2018)
 * @copyright   Copyright (c) 2018, INP
 *
 * Método que arma el formulario de edición de jurisdicciones, recibe
 * por get la clave del país
*/

// incluimos e instanciamos las clases
require_once ("jurisdicciones.class.php");
$jurisdicciones = new Jurisdicciones();

// presentamos el título
echo "<h2>Nómina de Jurisdicciones</h2>";

// obtenemos la nómina
$lista_provincias = $jurisdicciones->listaProvincias($_GET["idpais"]);

// definimos la tabla
echo "<table width='70%' align='center' border='0'>";

// definimos el encabezado
echo "<thead>";
echo "<tr>";
echo "<th>COD.</th>";
echo "<th>Jurisdiccion</th>";
echo "<th>Población</th>";
echo "<th>Usuario</th>";
echo "<th>Alta</th>";
echo "<th></th>";
echo "</tr>";
echo "</thead>";

// definimos el cuerpo
echo "<tbody>";

// recorremos el vector
foreach($lista_provincias AS $registro){

    // obtenemos el registro
    extract($registro);

    // abrimos la fila
    echo "<tr>";

    // presentamos el registro

    // la clave indec
    echo "<td align='center'>";
    echo "<span class='tooltip'
                title='Clave de la Jurisdicción'>";
    echo "<input type='text'
           name='codprov'
           id='codprov_$idprovincia'
           size='5'
           value='$idprovincia'>";
    echo "</span>";
    echo "</td>";

    // el nombre de la jurisdicción
    echo "<td>";
    echo "<span class='tooltip'
                title='Nombre de la Jurisdicción'>";
    echo "<input type='text'
           name='provincia'
           id='provincia_$idprovincia'
           size='25'
           value='$nombreprovincia'>";
    echo "</span>";
    echo "</td>";

    // la población
    echo "<td align='center'>";
    echo "<span class='tooltip'
                title='Número de habitantes'>";
    echo "<input type='number'
           name='poblacion'
           id='poblacion_$idprovincia'
           size='10'
           class='numerogrande'
           value='$poblacion'>";
    echo "</span>";
    echo "</td>";

    // el usuario y la contraseña
    echo "<td align='center'>$usuario</td>";
    echo "<td align='center'>$fecha_alta</td>";

    // el enlace de edición
    echo "<td align='center'>";
    echo "<input type='button'
           name='btnGrabaProvincia'
           id='btnGrabaProvincia'
           title='Actualiza el registro en la base'
           onClick='jurisdicciones.verificaProvincia(" . chr(34) . $idprovincia . chr(34) . ")'
           class='botongrabar'>";
    echo "</td>";

    // cerramos la fila
    echo "</tr>";

}

// abrimos la fila para el nuevo registro
echo "<tr>";

// presentamos el registro

// la clave indec
echo "<td align='center'>";
echo "<span class='tooltip'
            title='Clave de la Jurisdicción'>";
echo "<input type='text'
       name='codprov'
       id='codprov_nueva'
       size='5'>";
echo "</span>";
echo "</td>";

// el nombre de la provincia
echo "<td>";
echo "<span class='tooltip'
            title='Nombre de la Jurisdicción'>";
echo "<input type='text'
       name='provincia'
       id='provincia_nueva'
       size='25'>";
echo "</span>";
echo "</td>";

// la población
echo "<td align='center'>";
echo "<span class='tooltip'
            title='Número de habitantes'>";
echo "<input type='number'
       name='poblacion'
       id='poblacion_nueva'
       class='numerogrande'
       size='10'>";
echo "</span>";
echo "</td>";

// la fecha de alta y el usuario
echo "<td align='center'>";
echo "<input type='text'
             name='usuario_provincia'
             id='usuario_provincia'
             size='12'
             readonly>";
echo "</td>";
echo "<td align='center'>";
echo "<input type='text'
             name='alta_provincia'
             id='alta_provincia'
             size='12'
             readonly>";
echo "</td>";

// el enlace de edición
echo "<td align='center'>";
echo "<input type='button'
       name='btnGrabaProvincia'
       id='btnGrabaProvincia'
       onClick='jurisdicciones.verificaProvincia()'
       title='Graba el registro en la base'
       class='botonagregar'>";
echo "</td>";

// cerramos la fila
echo "</tr>";

// cerramos la tabla
echo "</tbody>";
echo "</table>";

?>

<SCRIPT>

    // instanciamos los tooltips
    new jBox('Tooltip', {
      attach: '.tooltip',
      theme: 'TooltipBorder'
    });

    // fijamos la fecha de alta y el usuario
    document.getElementById("usuario_provincia").value = sessionStorage.getItem("Usuario");
    document.getElementById("alta_provincia").value = fechaActual();

</SCRIPT>