<?php

/**
 *
 * jurisdicciones/lista_jurisdicciones.class.php
 *
 * @package     CCE
 * @subpackage  Jurisdicciones
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.1.0 (14/11/2017)
 * @copyright   Copyright (c) 2017, INP
 *
 * Método que recibe por get la clave de un país y retorna
 * un array json con las jurisdicciones de ese país
 *
*/

// incluímos e instanciamos las clases
require_once ("jurisdicciones.class.php");
$jurisdicciones = new Jurisdicciones();

// obtenemos la nómina
$nomina = $jurisdicciones->listaProvincias($_GET["Pais"]);

// declaramos la matriz
$jsondata = array();

// recorremos el vector
foreach($nomina AS $registro){

    // obtenemos el registro
    extract($registro);

    // lo agregamos a la matriz
    $jsondata[] = array("Id" => $idprovincia,
                        "Provincia" => $nombreprovincia);

}

// retornamos el vector
echo json_encode($jsondata);

?>