<?php

/**
 *
 * jurisdicciones/graba_jurisdiccion.php
 *
 * @package     CCE
 * @subpackage  Jurisdicciones
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.4.0 (09/08/2018)
 * @copyright   Copyright (c) 2018, INP
 *
 * Método que recibe por post los datos del formulario y ejecuta
 * la consulta en el servidor, retorna el resultado de la
 * operación
 *
*/

// incluimos e instanciamos las clases
require_once ("jurisdicciones.class.php");
$jurisdicciones = new Jurisdicciones();

// asigna los valores
$jurisdicciones->setIdPais($_POST["Pais"]);
$jurisdicciones->setIdProvincia($_POST["CodProv"]);
$jurisdicciones->setNombreProvincia($_POST["Provincia"]);
$jurisdicciones->setPoblacionProvincia($_POST["Poblacion"]);

// graba el registro y obtiene la clave
$resultado = $jurisdicciones->grabaProvincia($_POST["Evento"]);

// retorna el resultado
echo json_encode(array("Resultado" => $resultado));

?>