/*
 * Nombre: jurisdicciones.js
 * Autor: Lic. Claudio Invernizzi
 * E-Mail: cinvernizzi@gmail.com
 * Fecha: 10/01/2017
 * Licencia: GPL
 * Comentarios: Serie de funciones y procedimientos utilizados por el
 *              sistema de control de calidad en los formularios de
 *              abm de jurisdicciones
 */

 /**
 *
 * Atención, utilizamos el nuevo estandard definido en ECMAScript 2015 (ES6)
 * este permite definir clases con el mètodo ya utilizado en php y java
 * como creo que es mas claro lo utilizaremos, la compatibilidad está
 * anunciada para firefox y chrome así que no deberìa haber problemas
 *
 */

/* jshint esversion: 6 */

/**
 * @author Claudio Invernizzi <cinvernizzi@gmail.com>
 * @class Clase que controla las operaciones del formulario de marcas
 * Definición de la clase
 */
class Jurisdicciones {

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @constructor
     */
    constructor(){

        // inicializamos las variables de clase
        this.initJurisdicciones();

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que inicializa las variables de clase
     */
    initJurisdicciones(){

        // inicializamos las variables
        this.IdProvincia = 0;            // clave del registro
        this.IdPais = 0;                 // clave del país
        this.NomProv = "";               // nombre de la provincia
        this.CodProv = "";               // clave indec de la provincia
        this.Poblacion = 0;              // población de la jurisdicción
        this.Evento = "";                // evento, edición o inserción
        this.Correcto = true;            // switch de provincia repetida

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que carga en el contenedor el formulario de jurisdicciones
     */
    formJurisdicciones(){

        // reiniciamos el contador
        cce.contador();

        // carga el formulario
        $("#form_administracion").load("jurisdicciones/jurisdicciones.html");

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método llamado en el evento onchange del select de país del abm
     * de jurisdicciones, obtiene el valor del país seleccionado y
     * carga la grilla php en el div correspondiente
     */
    cargaJurisdicciones(){

        // obtenemos el país seleccionado
        var idpais = document.getElementById("pais_jurisdiccion").value;

        // si no hay ninguno seleccionado
        if (idpais == 0){
            return false;
        }

        // cargamos la grilla
        $("#grilla_jurisdicciones").load("jurisdicciones/form_jurisdicciones.php?idpais="+idpais);

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método llamado por callback que verifica no esté repetida la
     * jurisdicción
     */
    validaProvincia(provinciaCorrecta){

        // inicializamos las variables
        this.Correcto = false;

        // llamamos la rutina php
        $.ajax({
            url: 'jurisdicciones/verifica_jurisdiccion.php?provincia='+this.NomProv+'&idpais='+this.IdPais,
            type: "GET",
            cahe: false,
            contentType: false,
            processData: false,
            dataType: 'json',
            async: false,
            success: function(data) {

                // si retornó verdadero
                if (data.Registros == 0){
                    jurisdicciones.Correcto = true;
                } else {
                    jurisdicciones.Correcto = false;
                }

            }});

        // retornamos
        provinciaCorrecta(this.Correcto);

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que verifica los datos del formulario, recibe
     * la clave del registro y si no recibe nada asume que
     * se trata de un alta
     */
    verificaProvincia(id){

        // reiniciamos el contador
        cce.contador();

        // declaración de variables
        var mensaje;

        // obtenemos la clave del país
        this.IdPais = document.getElementById("pais_jurisdiccion").value;

        // si no recibió id
        if (typeof(id) == "undefined"){

            // está dando un alta
            id = 'nueva';
            this.Evento = "insertar";

        // si recibió
        } else {

            // asigna en la variable de clase
            this.Evento = "Editar";

        }

        // si no ingresó el código de localidad
        if (document.getElementById("codprov_" + id) == ""){

            // presenta el mensaje y retorna
            mensaje = "Debe ingresar el Código de Georreferenciación";
            new jBox('Notice', {content: mensaje, color: 'red'});
            document.getElementById("codprov_" + id).focus();
            return false;

        // si ingresó
        } else {

            // asignamos en la variable de clase
            this.CodProv = document.getElementById("codprov_" + id).value;


        }

        // verificamos si ingresó provincia
        if (document.getElementById("provincia_" + id).value == ""){

            // presenta el mensaje y retorna
            mensaje = "Debe ingresar el nombre de la Jurisdicción";
            new jBox('Notice', {content: mensaje, color: 'red'});
            document.getElementById("provincia_" + id).focus();
            return false;

        // si ingresó
        } else {

            // asigna en la variable de clase
            this.NomProv = document.getElementById("provincia_" + id).value;

        }

        // verificamos si ingresó la población
        if (document.getElementById("poblacion_" + id).value == ""){

            // presenta el mensaje y retorna
            mensaje = "Debe ingresar el número de habitantes";
            new jBox('Notice', {content: mensaje, color: 'red'});
            document.getElementById("poblacion_" + id).focus();
            return false;

        // si ingresó
        } else {

            // asigna en la variable de clase
            this.Poblacion = document.getElementById("poblacion_" + id).value;

        }

        // si está insertando
        if (id == "nueva"){

            // verificamos por callback
            this.validaProvincia(function(provinciaCorrecta){

                // si está repetido
                if (!jurisdicciones.Correcto){

                    // presenta el mensaje
                    mensaje = "Esa Jurisdicción ya se encuentra declarada !!!";
                    new jBox('Notice', {content: mensaje, color: 'red'});

                }

            });

        }

        // grabamos el registro
        this.grabaProvincia();

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que ejecuta la consulta en el servidor
     */
    grabaProvincia(){

        // si hubo un registro repetido
        if (!this.Correcto){
            return false;
        }

        // obtenemos la clave de pais y jurisdicción del
        // formulario de responsables y de instituciones
        var paisresponsable = document.getElementById("pais").value;
        var jurresponsable = document.getElementById("jurisdiccion").value;
        var paislaboratorio = document.getElementById("paislaboratorio").value;
        var jurlaboratorio = document.getElementById("jurisdiccionlaboratorio").value;

        // implementa el formulario usando objetos html5
        var formulario = new FormData();

        // agrega la marca y la técnica
        formulario.append("Evento", this.Evento);
        formulario.append("Pais", this.IdPais);
        formulario.append("CodProv", this.CodProv);
        formulario.append("Provincia", this.NomProv);
        formulario.append("Poblacion", this.Poblacion);

        // serializa el formulario y lo envía, recibe
        // como respuesta el resultado de la operación
        $.ajax({
            url: "jurisdicciones/graba_jurisdiccion.php",
            type: "POST",
            data: formulario,
            cahe: false,
            contentType: false,
            processData: false,
            success: function(data) {

                // si salío todo bien
                if (data.error !== false){

                    // presenta el mensaje
                    new jBox('Notice', {content: "Registro grabado ... ", color: 'green'});

                    // reiniciamos las variables de clase
                    jurisdicciones.initJurisdicciones();

                    // recarga el formulario para reflejar los cambios
                    jurisdicciones.cargaJurisdicciones();

                    // si el país que estamos editando es el mismo del responsable
                    if (paisresponsable == document.getElementById("pais_jurisdiccion")){

                        // actualizamos el select
                        jurisdiciones.listaJurisdicciones("jurisdiccion", paisresponsable, jurresponsable);

                    }

                    // si el país del laboratorio es el mismo que estamos editando
                    if (paislaboratorio == document.getElementById("pais_jurisdiccion")){

                        // actualizamos el select
                        jurisdicciones.listaJurisdicciones("jurisdiccionlaboratorio", paislaboratorio, jurisdiccionlaboratorio);

                    }

                // si hubo algún error
                } else {

                    // presenta la alerta
                    new jBox('Notice', {content: "Ha ocurrido un error, intente nuevamente", color: 'red'});

                }

            },

        });

    }

    /**
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param {string} idelemento - id de un objeto html
     * @param {int} idpais - clave del valor preseleccionado
     * @param {string} idjurisdiccion (puede ser nulo)
     * Método que recibe como parámetro la id de un elemento del formulario,
     * la clave del país y (optativo) la clave de la jurisdicción preseleccionada
     * carga en el select recibido como parámetro la nómina de jurisdicciones
     * de ese país y en todo caso preselecciona uno
     */
    listaJurisdicciones(idelemento, idpais, idjurisdiccion){

        // limpia el combo
        $("#" + idelemento).html('');

        // agrega el texto
        $("#" + idelemento).append("<option value=0>Seleccione</option>");

        // si no recibió el país
        if (typeof(idpais) == "undefined"){

            // asume que es argentina
            idpais = 1;

        }

        // si hay algún país seleccionado
        if (idpais != 0){

            // lo llamamos asincrónico
            $.ajax({
                url: "jurisdicciones/lista_jurisdicciones.php?Pais="+idpais,
                type: "GET",
                cahe: false,
                contentType: false,
                processData: false,
                dataType: 'json',
                success: function(data) {

                    // recorre el vector de resultados
                    for(var i=0; i < data.length; i++){

                        // si recibió la clave de la jurisdicción
                        if (typeof(idjurisdiccion) != "undefined"){

                            // si coincide
                            if (data[i].Id == idjurisdiccion){

                                // lo agrega seleccionado
                                $("#" + idelemento).append("<option selected value=" + data[i].Id + ">" + data[i].Provincia + "</option>");

                            // si no coinciden
                            } else {

                                // simplemente lo agrega
                                $("#" + idelemento).append("<option value=" + data[i].Id + ">" + data[i].Provincia + "</option>");

                            }

                        // si no recibió jurisdicción
                        } else {
                            $("#" + idelemento).append("<option value=" + data[i].Id + ">" + data[i].Provincia + "</option>");
                        }

                    }

            }});

        }

    }

}