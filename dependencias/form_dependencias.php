<?php

/**
 *
 * dependencias/form_dependencias.class.php
 *
 * @package     CCE
 * @subpackage  Dependencias
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.4.0 (01/03/2018)
 * @copyright   Copyright (c) 2018, INP
 *
 * Método que arma la grilla con las dependencias declaradas
 *
*/

// incluimos e instanciamos la clase
require_once ("dependencias.class.php");
$dependencias = new Dependencias();

// presentamos el título
echo "<h2>Nómina de Dependencias de los Laboratorios</h2>";

// definimos la tabla
echo "<table width='25%' align='center' border='0'>";

// definimos los encabezados
echo "<thead>";
echo "<tr>";
echo "<th>Dependencia</th>";
echo "<th></th>";
echo "</tr>";
echo "</thead>";

// obtenemos la nómina de dependencias
$nomina = $dependencias->listaDependencias();

// definimos el cuerpo
echo "<tbody>";

// recorremos el vector
foreach($nomina AS $registro){

    // obtenemos el registro
    extract($registro);

    // abrimos la fila
    echo "<tr>";

    // presentamos el registro
    echo "<td>";
    echo "<span class='tooltip'
           title='Descripción de la dependencia'>";
    echo "<input type='text'
           name='dependencia'
           id='$iddependencia'
           size='15'
           value='$dependencia'>";
    echo "</td>";

    // presentamos el botón grabar
    echo "<td align='center'>";
    echo "<input type='button' name='btnGrabaDependencia'
           id='btnGrabaDependencia'
           onClick='dependencias.verificaDependencia($iddependencia)'
           class='botongrabar'>";
    echo "</td>";

    // cerramos la fila
    echo "<tr>";

}

// abrimos la fila para insertar una nueva dependencia
echo "<tr>";

// presentamos el registro
echo "<td>";
echo "<span class='tooltip'
       title='Descripción de la dependencia'>";
echo "<input type='text' name='dependencia'
       id='nueva'
       size='15'>";
echo "</td>";

// presentamos el botón grabar
echo "<td align='center'>";
echo "<input type='button' name='btnNuevaDependencia'
       id='btnNuevaDependencia'
       onClick='dependencias.verificaDependencia()'
       class='botonagregar'>";
echo "</td>";

// cerramos la fila
echo "<tr>";

// cerramos la tabla
echo "</tbody>";
echo "</table>";

?>
<SCRIPT>

    // instanciamos los tooltips
    new jBox('Tooltip', {
      attach: '.tooltip',
      theme: 'TooltipBorder'
    });

</SCRIPT>