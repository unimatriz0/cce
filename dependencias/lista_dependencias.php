<?php

/**
 *
 * dependencias/lista_dependencias.php
 *
 * @package     CCE
 * @subpackage  Dependencias
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.4.0 (07/01/2018)
 * @copyright   Copyright (c) 2017, INP
 *
 * Método que retorna un array json con la nómina completa
 * de dependencias de las instituciones
*/

// incluimos e instanciamos las clases
require_once("dependencias.class.php");
$dependencias = new Dependencias();

// obtenemos la nómina
$nomina = $dependencias->listaDependencias();

// declaramos la matriz
$jsondata = array();

// recorremos el vector
foreach($nomina AS $registro){

    // obtenemos el registro
    extract($registro);

    // lo agregamos a la matriz
    $jsondata[] = array("Id" => $iddependencia,
                        "Dependencia" => $dependencia);

}

// retornamos el vector
echo json_encode($jsondata);

?>