<?php

/**
 *
 * dependencias/verifica_dependencia.php
 *
 * @package     CCE
 * @subpackage  Dependencias
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.4.0 (08/03/2018)
 * @copyright   Copyright (c) 2018, INP
 *
 * Método que recibe como parámetro el nombre de una dependencia
 * y verifica que no se encuentre repetido
 *
*/

// incluimos e instanciamos la clase
require_once ("dependencias.class.php");
$dependencias = new Dependencias();

// verificamos si existe
$registros = $dependencias->existeDependencia($_GET["dependencia"]);

// retornamos el resultado de la operación
echo json_encode(array("Registros" => $registros));

?>