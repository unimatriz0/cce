/*
 * Nombre: dependencias.js
 * Autor: Lic. Claudio Invernizzi
 * E-Mail: cinvernizzi@gmail.com
 * Fecha: 09/01/2017
 * Licencia: GPL
 * Comentarios: Serie de funciones y procedimientos utilizados por el
 *              sistema de control de calidad en los formularios de
 *              abm de dependencias
 */

/**
 *
 * Atención, utilizamos el nuevo estandard definido en ECMAScript 2015 (ES6)
 * este permite definir clases con el mètodo ya utilizado en php y java
 * como creo que es mas claro lo utilizaremos, la compatibilidad está
 * anunciada para firefox y chrome así que no deberìa haber problemas
 *
 */

/* jshint esversion: 6 */

/**
 * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @class Clase que controla las operaciones de dependencias
 */
class Dependencias {

    /**
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * @constructor
     */
    constructor(){

        // inicializamos las variables
        this.initDependencias();

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que inicializa las variables de clase
     */
    initDependencias(){

        // inicializamos las variables
        this.IdDependencia = 0;         // clave de la dependencia
        this.Dependencia = "";          // nombre de la dependencia
        this.Correcto = true;           // switch de dependencia repetida

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que carga en el contenido el formulario de abm de dependencias
     */
    formDependencias(){

        // reiniciamos el contador
        cce.contador();

        // carga el formulario
        $("#form_administracion").load("dependencias/form_dependencias.php");

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Nombre: validaDependencia
     * Método llamado por callback que verifica no se encuentre
     * repetida una dependencia
     */
    validaDependencia(dependenciaCorrecta){

        // inicializamos el switch
        this.Correcto = false;

        // llamamos la rutina php
        $.ajax({
            url: 'dependencias/verifica_dependencia.php?dependencia='+dependencias.Dependencia,
            type: "GET",
            cahe: false,
            contentType: false,
            processData: true,
            dataType: 'json',
            async: false,
            success: function(data) {

                // si retornó verdadero
                if (data.Registros == 0){
                    dependencias.Correcto = true;
                } else {
                    dependencias.Correcto = false;
                }

            }});

        // retornamos
        dependenciaCorrecta(this.Correcto);

    }

    /**
     * @param {int} iddependencia - clave del registro
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que recibe como parámetro la clave del registro y verifica
     * se hallan completado los campos, si no recibe la clave asume que
     * se trata de un alta
     */
    verificaDependencia(iddependencia){

        // reiniciamos la sesión
        cce.contador();

        // definición de variables
        var mensaje;

        // si está insertando
        if (typeof(iddependencia) == "undefined"){

            // asignamos
            iddependencia = 'nueva';

        // si está editando
        } else {

            // asigna en la variable de clase
            this.IdDependencia = iddependencia;

        }

        // si no declaró el nombre
        if (document.getElementById(iddependencia).value == ""){

            // presenta el mensaje y retorna
            mensaje = "Debe ingresar el nombre de la dependencia";
            new jBox('Notice', {content: mensaje, color: 'red'});
            document.getElementById(iddependencia).focus();
            return false;

        // si declaró
        } else {

            // asignamos en la variable de clase
            this.Dependencia = document.getElementById(iddependencia).value;

        }

        // si está dando un alta
        if (this.IdDependencia == 0){

            // verificamos por callback
            this.validaDependencia(function(dependenciaCorrecta){

                // si está repetido
                if (!dependencias.Correcto){

                    // presenta el mensaje
                    mensaje = "Esa dependencia ya se encuentra declarada !!!";
                    new jBox('Notice', {content: mensaje, color: 'red'});

                }

            });

        }

        // grabamos el registro
        this.grabaDependencia();

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que ejecuta la consulta de actualización del registro
     */
    grabaDependencia(){

        // reiniciamos el contador
        cce.contador();

        // si está repetido
        if (!this.Correcto){
            return false;
        }

        // implementa el formulario usando objetos html5
        var formulario = new FormData();

        // si está editando
        if (this.IdDependencia != 0){
            formulario.append("Id", this.IdDependencia);
        }

        // agrega la marca y la técnica
        formulario.append("Dependencia", this.Dependencia);

        // obtenemos el valor del select de laboratorios
        var iddependencia = document.getElementById("dependencia").value;

        // serializa el formulario y lo envía, recibe
        // como respuesta el resultado de la operación
        $.ajax({
            url: "dependencias/graba_dependencia.php",
            type: "POST",
            data: formulario,
            cahe: false,
            contentType: false,
            processData: false,
            success: function(data) {

                // si salío todo bien
                if (data.Id != 0){

                    // presenta el mensaje
                    new jBox('Notice', {content: "Registro grabado ... ", color: 'green'});

                    // recargamos el select de dependencias de los laboratorios
                    dependencias.listaDependencias("dependencia", iddependencia);

                    // reiniciamos las variables de clase
                    dependencias.initDependencias();

                    // recarga el formulario para reflejar los cambios
                    dependencias.formDependencias();

                // si hubo algún error
                } else {

                    // presenta la alerta
                    new jBox('Notice', {content: "Ha ocurrido un error, intente nuevamente", color: 'red'});

                }

            },

        });

    }

    /**
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param {string} idelemento - id de un objeto html
     * @param {int} iddependencia - clave a predeterminar
     * Método que recibe como parámetro la id de un select de un formulario
     * carga en ese elemento la nómina de dependencias, si además recibe
     * la clave de un elemento, lo predetermina
     */
    listaDependencias(idelemento, iddependencia){

        // limpiamos el select
        $("#" + idelemento).html('');

        // agrega el texto
        $("#" + idelemento).append("<option value=0>Seleccione</option>");

        // obtenemos la nómina
        $.ajax({
            url: 'dependencias/lista_dependencias.php',
            type: "GET",
            cahe: false,
            contentType: false,
            processData: false,
            dataType: 'json',
            success: function(data) {

                // recorre el vector de resultados
                for(var i=0; i < data.length; i++){

                    // si recibió la clave
                    if (typeof(iddependencia) != "undefined"){

                        // si coindicen
                        if (iddependencia == data[i].Id){

                            // lo agrega preseleccionado
                            $("#" + idelemento).append("<option selected value='" + data[i].Id + "'>" + data[i].Dependencia + "</option>");

                        // si no coincide
                        } else {

                            // simplemente lo agrega
                            $("#" + idelemento).append("<option value='" + data[i].Id + "'>" + data[i].Dependencia + "</option>");

                        }

                    // si no recibió
                    } else {

                        // simplemente lo agrega
                        $("#" + idelemento).append("<option value='" + data[i].Id + "'>" + data[i].Dependencia + "</option>");

                    }

                }

            }});

    }

}