<?php

/**
 *
 * dependencias/graba_dependencia.php
 *
 * @package     CCE
 * @subpackage  Dependencias
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.4.0 (08/08/2018)
 * @copyright   Copyright (c) 2018, INP
 *
 * Método que recibe por post los datos de una dependencia y
 * ejecuta la consulta de grabación
 *
*/

// incluimos e instanciamos la clase
require_once ("dependencias.class.php");
$dependencias = new Dependencias();

// si recibió la clave
if (!empty($_POST["Id"])){
    $dependencias->setIdDependencia($_POST["Id"]);
}

// asigna el nombre
$dependencias->setDependencia($_POST["Dependencia"]);

// grabamos el registro
$id = $dependencias->grabaDependencia();

// retornamos el resultado de la operación
echo json_encode(array("Id" => $id));

?>