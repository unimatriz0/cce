<?php

/**
 *
 * Class Dependencias | dependencias/dependencias.class.php
 *
 * @package     CCE
 * @subpackage  Dependencias
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.4.0 (01/03/2018)
 * @copyright   Copyright (c) 2018, INP
 *
 * Cláse que contiene las operaciones sobre la base de datos
 * de dependencias
 *
*/

// inclusión de archivos
require_once ("../clases/conexion.class.php");

// convención para la nomenclatura de las propiedades, comienzan con una
// letra mayúscula, de tener mas de una palabra no se utilizan separadores
// y la inicial de cada palabra va en mayúscula
// para las variables recibidas como parámetro el criterio es todas en
// minúscula

// convención para la nomenclatura de los metodos, comienzan con set o get
// según asignen un valor o lo lean y luego el nombre del valor a obtener

/**
 * Definición de la clase
 * @author Claudio Invernizzi <cinvernizzi@gmail.com>
 **/
class Dependencias{

    // declaración de variables de la clase, las declaramos
    // como protected para que puedan ser heredadas pero
    // para asignarle el valor debemos crear los métodos
    protected $IdDependencia;         // clave de la dependencia
    protected $Dependencia;           // nombre de la dependencia
    protected $IdUsuario;             // clave del usuario
    protected $FechaAlta;             // fecha de alta del registro
    protected $Link;                  // puntero a la base de datos

    /**
     * Constructor de la clase, instancia la conexión con la
     * base de datos e inicializa las variables
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     */
    function __construct (){

        // inicializamos las variables
        $this->IdDependencia = 0;
        $this->Dependencia = "";
        $this->FechaAlta = "";

        // abrimos la sesión y obtenemos el usuario
        session_start();
        if (!empty($_SESSION["ID"])){
            $this->IdUsuario = $_SESSION["ID"];
        }
        session_write_close();

        // nos conectamos a la base de datos
        $this->Link = new Conexion();

    }

    /**
     * Destructor de la clase, cierra la conexión con la base
     * de datos
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     */
    function __destruct(){

        // elimina el enlace a la base
        $this->Link = null;

    }

    // métodos de asignación de variables
    public function setIdDependencia($iddependencia){

        // verificamos que sea un número
        if (!is_numeric($iddependencia)){

            // abandona por error
            echo "La clave de la dependencia debe ser un número";
            exit;

        // si es correcto
        } else {

            // lo asigna
            $this->IdDependencia = $iddependencia;

        }

    }
    public function setDependencia($dependencia){
        $this->Dependencia = $dependencia;
    }

    /**
     * Método que retorna la nómina de dependencias de las instituciones
     * @return arrray
     */
    public function listaDependencias(){

        // componemos la consulta
        $consulta = "SELECT diccionarios.dependencias.dependencia AS dependencia,
                            diccionarios.dependencias.id_dependencia AS iddependencia
                     FROM diccionarios.dependencias;";
        $resultado = $this->Link->query($consulta);

        // lo pasamos a minúsculas porque según la versión de
        // pdo lo devuelve en mayúsculas o minúsculas
        $nomina = array_change_key_case($resultado->fetchAll(PDO::FETCH_ASSOC), CASE_LOWER);

        // retornamos el vector
        return $nomina;

    }

    /**
     * Método que recibe como parámetro el nombre de una dependencia,
     * retorna el número de registros coincidentes, utilizado en el abm
     * de dependencia para evitar registros duplicados
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param string $dependencia - dependencia a buscar
     * @return int
     */
    public function existeDependencia($dependencia){

        // inicializamos las variables
        $registros = 0;

        // componemos la consulta
        $consulta = "SELECT COUNT(diccionarios.dependencias.id_dependencia) AS registros
                     FROM diccionarios.dependencias
                     WHERE diccionarios.dependencias.dependencia = '$dependencia';";
        $resultado = $this->Link->query($consulta);

        // obtenemos el registro y retornamos
        $registro = array_change_key_case($resultado->fetch(PDO::FETCH_ASSOC), CASE_LOWER);
        extract($registro);
        return $registros;

    }

    /**
     * Método que ejecuta la consulta de actualización o inserción en la
     * tabla de dependencias, retorna la clave del registro afectado
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @return int
     */
    public function grabaDependencia(){

        // si está dando altas
        if ($this->IdDependencia == 0){
            $this->nuevaDependencia();
        } else {
            $this->editaDependencia();
        }

        // retornamos la id del registro afectado
        return $this->IdDependencia;

    }

    /**
     * Método que inserta una nueva dependencia en la tabla
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     */
    protected function nuevaDependencia(){

        // componemos la consulta de inserción
        $consulta = "INSERT INTO diccionarios.dependencias
                            (dependencia,
                             id_usuario)
                            VALUES
                            (:dependencia,
                             :id_usuario);";

        // asignamos la consulta
        $psInsertar = $this->Link->prepare($consulta);

        // asignamos los valores
        $psInsertar->bindParam(":dependencia", $this->Dependencia);
        $psInsertar->bindParam(":id_usuario", $this->IdUsuario);

        // ejecutamos la consulta
        $resultado = $psInsertar->execute();

        // si salió todo bien
        if ($resultado){

            // obtenemos la clave del registro
            $this->IdDependencia = $this->Link->lastInsertId();

        // si hubo un error
        } else {

            // inicializamos la clave y mostramos el error
            $this->IdDependencia = 0;
            echo $resultado;

        }

    }

    /**
     * Método protegido que edita el registro de dependencias
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     */
    protected function editaDependencia(){

        // componemos la consulta de edición
        $consulta = "UPDATE diccionarios.dependencias SET
                            dependencia = :dependencia,
                            id_usuario = :id_usuario
                     WHERE diccionarios.dependencias.id_dependencia = :id";

        // asignamos la consulta
        $psInsertar = $this->Link->prepare($consulta);

        // asignamos los valores
        $psInsertar->bindParam(":dependencia", $this->Dependencia);
        $psInsertar->bindParam(":id_usuario", $this->IdUsuario);
        $psInsertar->bindParam(":id", $this->IdDependencia);

        // ejecutamos la consulta
        $resultado = $psInsertar->execute();

        // si hubo un error
        if (!$resultado){

            // inicializamos la variable y presentamos el error
            $this->IdDependencia = 0;
            echo $resultado;

        }

    }

}
?>
