<?php

/**
 *
 * determinaciones/sin_resultados.php
 *
 * @package     CCE
 * @subpackage  Determinaciones
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.4.0 (10/09/2018)
 * @copyright   Copyright (c) 2017, INP
 *
 * Método que retorna un array json con la nómina de operativos que no
 * tienen asignado el resultado correcto
*/

// incluimos e instanciamos las clases
require_once("determinaciones.class.php");
$determinaciones = new Determinaciones();

// obtenemos la nómina
$nomina = $determinaciones->sinResultados();

// declaramos la matriz
$jsondata = array();

// recorremos el vector
foreach($nomina AS $registro){

    // obtenemos el registro
    extract($registro);

    // lo agregamos a la matriz
    $jsondata[] = array("Id" => $idoperativo,
                        "Operativo" => $operativo);

}

// retornamos el vector
echo json_encode($jsondata);

?>