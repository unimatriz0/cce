<?php

/**
 *
 * determinaciones/borra_determinacion.php
 *
 * @package     CCE
 * @subpackage  Determinaciones
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.4.0 (29/08/2018)
 * @copyright   Copyright (c) 2018, INP
 *
 * Procedimiento que recibe por get la clave de una determinación
 * y ejecuta la consulta de eliminación
 *
*/

// incluimos e instanciamos la clase
require_once ("determinaciones.class.php");
$determinacion = new Determinaciones();

// ejecutamos la consulta
$determinacion->borraDeterminacion($_GET["id"]);

// retornamos siempre verdadero
echo json_encode(array("Error" => 1));

?>