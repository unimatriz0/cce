<?php

/**
 *
 * determinaciones/graba_resultado.php
 *
 * @package     CCE
 * @subpackage  Determinaciones
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.4.0 (10/09/2018)
 * @copyright   Copyright (c) 2018, INP
 *
 * Procedimiento que recibe por get la clave de un operativo y el
 * resultado correcto del mismo y se los pasa al servidor para
 * actualizar la base
 *
*/

// incluimos e instanciamos la clase
require_once ("determinaciones.class.php");
$determinacion = new Determinaciones();

// actualizamos la base
$registros = $determinacion->resultadoCorrecto($_GET["operativo"], $_GET["resultado"]);

// retornamos los registros afectados
echo json_encode(array("Registros" => $registros));

?>