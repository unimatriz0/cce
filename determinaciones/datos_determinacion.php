<?php

/**
 *
 * determinaciones/datos_determinacion.php
 *
 * @package     CCE
 * @subpackage  Determinaciones
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.4.0 (16/08/2018)
 * @copyright   Copyright (c) 2018, INP
 *
 * Procedimiento que recibe por get la clave de una determinación
 * y retorna los datos de la misma en formato json
 *
*/

// incluimos e instanciamos la clase
require_once ("determinaciones.class.php");
$determinacion = new Determinaciones();

// obtenemos el registro
$determinacion->getDatosDeterminacion($_GET["id"]);

// armamos la matriz y retornamos
echo json_encode(array("Id" =>                $determinacion->getIdDatos(),
                       "Laboratorio" =>       $determinacion->getIdLaboratorio(),
                       "Operativo" =>         $determinacion->getOperativo(),
                       "IdOperativo" =>       $determinacion->getIdOperativo(),
                       "Abierto" =>           $determinacion->getAbierto(),
                       "Marca" =>             $determinacion->getIdMarca(),
                       "Tecnica" =>           $determinacion->getIdTecnica(),
                       "Lote" =>              $determinacion->getLote(),
                       "Vencimiento" =>       $determinacion->getVencimiento(),
                       "Muestra" =>           $determinacion->getMuestra(),
                       "Corte" =>             $determinacion->getValorCorte(),
                       "Lectura" =>           $determinacion->getValorLectura(),
                       "Metodo" =>            $determinacion->getMetodo(),
                       "Determinacion" =>     $determinacion->getDeterminacion(),
                       "Correcto" =>          $determinacion->getCorrecto(),
                       "Comentarios" =>       $determinacion->getComentarios(),
                       "FechaAlta" =>         $determinacion->getFechaAlta(),
                       "Usuario" =>           $determinacion->getUsuario(),
                       "IdOtra" =>            $determinacion->getIdOtra(),
                       "OtraPrueba" =>        $determinacion->getPruebaOtra(),
                       "OtraMarca" =>         $determinacion->getMarcaOtra()));

?>