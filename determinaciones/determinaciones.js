/*

 Nombre: determinaciones.js
 Fecha: 23/12/2016
 Autor: Lic. Claudio Invernizzi
 E-Mail: cinvernizzi@gmail.com
 Licencia: GPL
 Producido en: INP - Dr. Mario Fatala Chaben
 Buenos Aires - Argentina
 Comentarios: serie de funciones utilizadas en la presentación y carga de
              determinaciones

 */

/**
 *
 * Atención, utilizamos el nuevo estandard definido en ECMAScript 2015 (ES6)
 * este permite definir clases con el mètodo ya utilizado en php y java
 * como creo que es mas claro lo utilizaremos, la compatibilidad está
 * anunciada para firefox y chrome así que no deberìa haber problemas
 *
 */

/* jshint esversion: 6 */

/**
 * @author Claudio Invernizzi <cinvernizzi@gmail.com>
 * @class Determinaciones, clase que controla el comportamiento del
 * formulario de carga de nuevas determinaciones
 */
class Determinaciones{

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @constructor
     */
    constructor(){

        // inicializamos las variables de clase
        this.initDeterminaciones();

        // inicializamos el layer
        this.layerDeterminacion = "";

        // cargamos el layer
        this.cargaFormulario();

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que inicializa las variables de clase
     */
    initDeterminaciones(){

        // variables de la tabla chag_datos
        this.IdDeterminacion = 0;           // clave del registro
        this.Operativo = "";                // número de operativo
        this.IdOperativo = 0;               // clave del operativo
        this.Laboratorio = 0;               // clave del laboratorio
        this.Marca = 0;                     // clave de la marca de reactivo
        this.Lote = "";                     // número de lote del reactivo
        this.Vencimiento = "";              // fecha de vencimiento del reactivo
        this.Muestra = 0;                   // número de muestra
        this.ValorCorte = "";               // valor de corte
        this.ValorLectura = "";             // valor obtenido
        this.Metodo = "";                   // método utilizado en eliza (lector/visual)
        this.Tecnica = 0;                   // clave de la técnica utilizada
        this.Determinacion = "";            // resultado de la determinación
        this.Correcto = "";                 // resultado correcto
        this.Comentarios = "";              // comentarios del usuario
        this.FechaAlta = "";                // fecha de alta del registro
        this.Usuario = "";                  // nombre del usuario

        // de la tabla chag_otras
        this.IdOtra = 0;                    // clave del registro
        this.IdDatos = 0;                   // clave de la tabla chag_datos
        this.PruebaOtra = "";               // nombre de la prueba utilizada
        this.MarcaOtra = "";                // nombre de la marca de reactivo

        // propias de la clase
        this.Abierto = "";                  // indica si el operativo está abierto

    }

    // métodos de inicialización de valores
    setIdDeterminacion(id){
        this.IdDeterminacion = id;
    }
    setOperativo(operativo){
        this.Operativo = operativo;
    }
    setIdOperativo(idoperativo){
        this.IdOperativo = idoperativo;
    }
    setLaboratorio(laboratorio){
        this.Laboratorio = laboratorio;
    }
    setMarca(marca){
        this.Marca = marca;
    }
    setLote(lote){
        this.Lote = lote;
    }
    setVencimiento(vencimiento){
        this.Vencimiento = vencimiento;
    }
    setMuestra(muestra){
        this.Muestra = muestra;
    }
    setValorCorte(corte){
        this.ValorCorte = corte;
    }
    setValorLectura(lectura){
        this.ValorLectura = lectura;
    }
    setMetodo(metodo){
        this.Metodo = metodo;
    }
    setTecnica(tecnica){
        this.Tecnica = tecnica;
    }
    setDeterminacion(determinacion){
        this.Determinacion = determinacion;
    }
    setCorrecto(correcto){
        this.Correcto = correcto;
    }
    setComentarios(comentarios){
        this.Comentarios = comentarios;
    }
    setFechaAlta(fecha){
        this.FechaAlta = fecha;
    }
    setUsuario(usuario){
        this.Usuario = usuario;
    }
    setIdOtra(id){
        this.IdOtra = id;
    }
    setIdDatos(id){
        this.IdDatos = id;
    }
    setPruebaOtra(prueba){
        this.PruebaOtra = prueba;
    }
    setMarcaOtra(marca){
        this.MarcaOtra = marca;
    }
    setAbierto(abierto){
        this.Abierto = abierto;
    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param {int} id  - clave del laboratorio
     * Método que recibe como parámetro la id de un laboratorio y
     * carga mediante ajax la nómina de determinaciones realizadas
     * por ese laboratorio
     */
    muestraDeterminaciones(id){

        // reiniciamos el contador
        cce.contador();

        // cargamos las determinaciones
        $("#determinaciones").load("determinaciones/lista_determinaciones.php?id="+id);

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método llamado en el constructor que carga el layer y lo
     * mantiene disponible para mostrarlo u ocultarlo
     */
    cargaFormulario(){

        // cargamos el layer de determinaciones y sobrecargamos
        // el evento del botón cerrar para que solo oculte
        // el layer y no lo destruya
        this.layerDeterminacion = new jBox('Modal', {
                    animation: 'flip',
                    closeOnEsc: false,
                    closeOnClick: false,
                    closeOnMouseleave: false,
                    closeButton: true,
                    overlay: false,
                    theme: 'TooltipBorder',
                    title: 'Datos de la Determinación',
                    draggable: 'title',
                    onCreated : function () {
                        this.wrapper.find('.jBox-closeButton')
                          .off('click touchend')
                          .on('click touchend', function () {
                              determinaciones.layerDeterminacion.hide();
                        });
                    },
                    width: 950,
                    heigth: 600,
                    ajax: {
                        url: 'determinaciones/form_determinaciones.html',
                        reload: 'strict'
                    }
                });
        this.layerDeterminacion.open();

        // ahora lo ocultamos pero así lo tenemos disponible
        this.layerDeterminacion.hide();

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método llamado al ingresar una nueva determinación
     * limpia el formulario e inicializa las variables de
     * clase
     */
    nuevaDeterminacion(){

        // inicializamos las variables de clase
        this.initDeterminaciones();

        // mostramos el formulario
        this.layerDeterminacion.show();

        // reiniciamos el formulario
        document.getElementById("iddeterminacion").value = "";
        document.getElementById("idlabdatos").value = document.getElementById("idlaboratorio").value;
        document.getElementById("operativo").value = 0;
        document.getElementById("texto_operativo").value = "";
        document.getElementById("tecnica").value = 0;
        document.getElementById("metodo").value = 0;
        document.getElementById("idotra").value = "";
        document.getElementById("nombre_prueba").value = "";
        document.getElementById("marca").value = 0;
        document.getElementById("texto_marca").value = "";
        document.getElementById("lote").value = "";
        document.getElementById("vencimiento").value = "";
        document.getElementById("corte").value = 0;
        document.getElementById("texto_corte").value = "";
        document.getElementById("muestra").value = "";
        document.getElementById("lectura").value = 0;
        document.getElementById("texto_lectura").value = "";
        document.getElementById("determinacion").value = "";
        document.getElementById("resultado").value = "";
        document.getElementById("observacionesdeterminacion").value = "";
        CKEDITOR.instances['observacionesdeterminacion'].setData();
        document.getElementById("alta_determinacion").value = fechaActual();
        document.getElementById("usuario_determinacion").value = sessionStorage.getItem("Usuario");

        // mostramos el select de operativos
        $("#operativo").show();

        // por defecto oculta los combos
        $("#combo_metodo").hide();
        $("#marca").hide();
        $("#corte").hide();
        $("#lectura").hide();

        // ocultamos también los textos
        $("#texto_operativo").hide();
        $("#texto_marca").hide();
        $("#texto_corte").hide();
        $("#texto_lectura").hide();

        // oculta el div de la otra prueba
        $("#otra_prueba").hide();

        // ocultamos el resultado
        $("#correcto").hide();

        // setea las máscaras
        $("#muestra").mask("9999");

        // oculta el botón borrar
        $("#btnBorrar").hide();

        // fijamos la id del laboratorio
        document.getElementById("idlabdatos").value = document.getElementById("idlaboratorio").value;

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Metodo llamado al cambiar el combo del metodo, actualiza segun el
     * estado oculta o muestra los valores de corte y lectura
     */
    cambiaMetodo(){

        // reiniciamos el contador
        cce.contador();

        // si seleccionó lector
        if ($("#metodo option:selected").text() == "Lector"){

            // nos aseguramos de mostrar corte y lectura
            $("#texto_corte").show();
            $("#texto_lectura").show();

        // si en cambio es visual
        } else {

            // nos aseguramos de ocultarlos
            $("#texto_corte").hide();
            $("#texto_lectura").hide();

        }

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que cierra el formulario
     */
    cancelaDeterminacion(){

        // oculta el formulario
        this.layerDeterminacion.hide();

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método llamado al cambiar el combo de técnica o al cargar
     * el formulario recibe el valor predeterminado y llama al método
     * de la clase de marcas
     */
    cambiaTecnica(){

        // reiniciamos el contador
        cce.contador();

        // obtenemos el valor seleccionado
        var idtecnica = document.getElementById("tecnica").value;

        // verificamos que no esté en blanco
        if (idtecnica.value == 0){

            // limpia el combo
            $("#marca").html('');

            // oculta todos los combos y textos
            $("#combo_metodo").hide();
            $("#marca").hide();
            $("#corte").hide();
            $("#lectura").hide();
            $("#texto_operativo").hide();
            $("#texto_marca").hide();
            $("#texto_corte").hide();
            $("#texto_lectura").hide();

        // si la técnica es otra
        } else if ($("#tecnica option:selected").text() == "OTRA"){

            // oculta los select de corte marca y lectura
            // y deja en formato libre los campos de texto
            $("#combo_metodo").hide();
            $("#marca").hide();
            $("#corte").hide();
            $("#lectura").hide();

            // removemos las máscaras
            $("#texto_corte").mask("9999.99");
            $("#texto_lectura").mask("9999.99");

            // mostramos los textos
            $("#otra_prueba").show();
            $("#texto_marca").show();
            $("#texto_corte").show();
            $("#texto_lectura").show();

            // si está editando

            // seteamos el foco
            document.getElementById("nombre_prueba").focus();

        // si es cualquier técnica registrada
        } else {

            // nos aseguramos de ocultar texto de marca
            // y mostrar el combo
            $("#otra_prueba").hide();
            $("#texto_marca").hide();
            $("#marca").show();

        }

        // si la técnica no es elisa
        if ($("#tecnica option:selected").text() != "ELISA"){

            // oculta el combo de método
            $("#combo_metodo").hide();

            // setea el foco
            document.getElementById("marca").focus();

        // si es elisa
        } else {

            // muestra el combo de método
            $("#combo_metodo").show();

            // setea el foco
            document.getElementById("combo_metodo").focus();

        }

        // si la técnica es distinta de otra actualizamos
        // el select de corte y marcas según corresponda
        if ($("#tecnica option:selected").text() != "OTRA"){

            // actualizamos los valores de corte

            // si está editando
            if (determinaciones.ValorCorte != 0 && determinaciones.ValorLectura != 0){
                this.listaCortes(idtecnica,this.ValorCorte, this.ValorLectura);
            } else {
                this.listaCortes(idtecnica);
            }

            // actualizamos el select de las marcas

            // si está editando llamamos  actualizando el select
            if (determinaciones.Marca != 0){
                marcas.nominaMarcas("marca", idtecnica, determinaciones.Marca);
            } else {
                marcas.nominaMarcas("marca", idtecnica);
            }

        }

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param {id} id - clave de la técnica
     * @param {string} corte - valor predeterminado
     * @param {string} lectura - valor predeterminado
     * Método que recibe como parámetro la id de una técnica y actualiza
     * los combos o las máscaras de acuerdo a esa técnica
     */
    listaCortes(tecnica, corte, lectura){

        // reiniciamos el contador
        cce.contador();

        // obtenemos los valores de corte aceptados
        $.ajax({
            url: 'valores/lista_valores.php?tecnica='+tecnica,
            type: "GET",
            cahe: false,
            contentType: false,
            processData: true,
            dataType: 'json',
            async: false,
            success: function(data) {

                // si recibió un solo elemento
                if (data.length == 1){

                    // oculta los combos
                    $("#corte").hide();
                    $("#lectura").hide();

                    // obtenemos el patrón de la máscara
                    var mascara = data[0].Valor;

                    // setea las máscaras
                    $("#texto_corte").mask(mascara);
                    $("#texto_lectura").mask(mascara);

                    // si recibió el corte y la lectura
                    if (typeof("corte") != "undefined" && typeof("lectura") != "undefined"){

                        // fijamos los valores
                        document.getElementById("texto_corte").value = corte;
                        document.getElementById("texto_lectura").value = lectura;

                    }

                    // mostramos los textos
                    $("#texto_corte").show();
                    $("#texto_lectura").show();

                // si es una matriz
                } else {

                    // nos aseguramos de ocultar los textos
                    $("#texto_corte").hide();
                    $("#texto_lectura").hide();

                    // limpia el combo de corte y lectura
                    $("#corte").html('');
                    $("#lectura").html('');

                    // nos aseguramos que estén visibles
                    $("#corte").show();
                    $("#lectura").show();

                    // agrega el primer elemento en blanco
                    $("#corte").append("<option value='0'>Seleccione</option>");
                    $("#lectura").append("<option value='0'>Seleccione</option>");

                    // recorre el vector de resultados
                    for(var i=0; i < data.length; i++){

                        // si recibió el corte y la lectura
                        if (typeof("corte") != "undefined" && typeof("lectura") != "undefined"){

                            // si coincide el de corte
                            if (corte == data[i].Valor){

                                // lo agrega seleccionándolo
                                $("#corte").append("<option selected>" + data[i].Valor + "</option>");

                            // si no coincide
                            } else {

                                // simplemente lo agrega
                                $("#corte").append("<option>" + data[i].Valor + "</option>");

                            }

                            // si coincide el de lectura
                            if (lectura == data[i].Valor){

                                // lo agrega seleccionándolo
                                $("#lectura").append("<option selected>" + data[i].Valor + "</option>");

                            // si no coincide
                            } else {

                                // simplemente lo agrega
                                $("#lectura").append("<option>" + data[i].Valor + "</option>");

                            }

                        // si no los recibió
                        } else {

                            // agregamos el elemento al select
                            $("#corte").append("<option>" + data[i].Valor + "</option>");
                            $("#lectura").append("<option>" + data[i].Valor + "</option>");

                        }

                    }

                }

            }});

        // retornamos
        return;

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param {int} id - clave del registro
     * Método que recibe como parámetro la clave del registro
     * y asigna en las variables de clase los valores de la base
     */
    getDatosDeterminacion(id){

        // reiniciamos el contador
        cce.contador();

        // obtiene los datos de la determinación
        $.get('determinaciones/datos_determinacion.php','id='+id,
            function(data){

                // asignamos los valores en las variables de clase
                determinaciones.setIdDeterminacion(data.Id);
                determinaciones.setLaboratorio(data.Laboratorio);
                determinaciones.setAbierto(data.Abierto);
                determinaciones.setIdOperativo(data.IdOperativo);
                determinaciones.setOperativo(data.Operativo);
                determinaciones.setTecnica(data.Tecnica);
                determinaciones.setValorCorte(data.Corte);
                determinaciones.setValorLectura(data.Lectura);
                determinaciones.setMetodo(data.Metodo);
                determinaciones.setMarca(data.Marca);
                determinaciones.setLote(data.Lote);
                determinaciones.setVencimiento(data.Vencimiento);
                determinaciones.setMuestra(data.Muestra);
                determinaciones.setDeterminacion(data.Determinacion);
                determinaciones.setCorrecto(data.Correcto);
                determinaciones.setComentarios(data.Comentarios);
                determinaciones.setFechaAlta(data.FechaAlta);
                determinaciones.setUsuario(data.Usuario);
                determinaciones.setIdOtra(data.IdOtra);
                determinaciones.setPruebaOtra(data.OtraPrueba);
                determinaciones.setMarcaOtra(data.OtraMarca);

                // llamamos la rutina de carga
                determinaciones.muestraDeterminacion();

            }, "json");

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que a partir de las variables de clase carga los
     * datos en el formulario
     */
    muestraDeterminacion(){

        // reiniciamos el contador
        cce.contador();

        // mostramos el formulario
        this.layerDeterminacion.show();

        // asignamos los valores en el formulario
        document.getElementById("iddeterminacion").value = this.IdDeterminacion;
        document.getElementById("idlabdatos").value = this.Laboratorio;

        // si el operativo está abierto
        if (this.Abierto == "Si"){

            // setea el valor del combo y oculta el texto
            document.getElementById("operativo").value = this.IdOperativo;
            $("#operativo").show();
            $("#texto_operativo").hide();

            // se asegura de ocultar el resultado
            $("#correcto").hide();

            // se asegura de mostrar los botones
            $("#btnGrabar").show();
            $("#btnCancelar").show();
            $("#btnBorrar").show();

        // si está cerrado
        } else {

            // oculta el combo y muestra el texto
            $("#operativo").hide();
            $("#texto_operativo").show();
            document.getElementById("texto_operativo").value = this.Operativo;

            // muestra el resultado
            $("#correcto").show();
            document.getElementById("resultado").value = this.Correcto;

            // oculta los botones de acción
            $("#btnGrabar").hide();
            $("#btnCancelar").hide();
            $("#btnBorrar").hide();

        }

        // continúa cargando el formulario
        document.getElementById("tecnica").value = this.Tecnica;

        // llamamos al evento onchange de la técnica
        this.cambiaTecnica();

        // seguimos cargando
        document.getElementById("metodo").value = this.Metodo;

        // si el método es visual
        if (this.Metodo == "Visual"){

            // oculta el texto de corte y lectura
            $("#texto_corte").hide();
            $("#texto_lectura").hide();

        }

        // seguimos cargando
        document.getElementById("lote").value = this.Lote;
        document.getElementById("vencimiento").value = this.Vencimiento;
        document.getElementById("muestra").value = this.Muestra;
        document.getElementById("determinacion").value = this.Determinacion;
        document.getElementById("observacionesdeterminacion").value = this.Comentarios;
        CKEDITOR.instances['observacionesdeterminacion'].setData(this.Comentarios);
        document.getElementById("alta_determinacion").value = this.FechaAlta;
        document.getElementById("usuario_determinacion").value = this.Usuario;

        // si es otra técnica cargamos los textos
        if (this.IdOtra != 0){

            // carga los textos
            document.getElementById("idotra").value = this.IdOtra;
            document.getElementById("nombre_prueba").this = this.PruebaOtra;
            document.getElementById("texto_marca").this = this.MarcaOtra;

        }

        // si hay un resultado correcto
        if(this.Correcto != ""){
            document.getElementById("resultado").value = this.Correcto;
        }

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que verifica los valores del formulario antes de
     * enviarlo al servidor
     */
    verificaDeterminacion(){

        // reiniciamos el contador
        cce.contador();

        // reiniciamos las variables de clase para estar
        // seguros de no arrastrar basura
        this.initDeterminaciones();

        // declaración de variables
        var mensaje;

        // si está editando
        if (document.getElementById("iddeterminacion").value != ""){
            this.IdDeterminacion = document.getElementById("iddeterminacion").value;
        }

        // agregamos la id del laboratorio
        this.Laboratorio = document.getElementById("idlabdatos").value;

        // verifica se halla seleccionado el operativo
        if (document.getElementById("operativo").value == 0){

            // presenta el mensaje y retorna
            mensaje = "Debe seleccionar el operativo";
            new jBox('Notice', {content: mensaje, color: 'red'});
            document.getElementById("operativo").focus();
            return false;

        // si seleccionó
        } else {

            // asigna en la variable de clase
            this.IdOperativo = document.getElementById("operativo").value;

        }

        // verifica se halla seleccionado la técnica
        if (document.getElementById("tecnica").value == 0){

            // presenta el mensaje y retorna
            mensaje = "Debe seleccionar la técnica utilizada";
            new jBox('Notice', {content: mensaje, color: 'red'});
            document.getElementById("tecnica").focus();
            return false;

        // si seleccionó
        } else {

            // asigna en la variable de clase
            this.Tecnica = document.getElementById("tecnica").value;

        }

        // si la técnica es elisa, verifica se seleccione el método
        if ($("#tecnica option:selected").text() == "ELISA"){

            // verifica el método
            if (document.getElementById("metodo").value == 0){

                // presenta el mensaje y retorna
                mensaje = "Indique el método utilizado";
                new jBox('Notice', {content: mensaje, color: 'red'});
                document.getElementById("metodo").focus();
                return false;

            // si indicó
            } else {

                // asigna en la variable de clase
                this.Metodo = document.getElementById("metodo").value;

            }

        }

        // si la técnica es otra
        if ($("#tecnica option:selected").text() == "OTRA"){

            // verifica se halla ingresado el nombre
            if (document.getElementById("nombre_prueba").value == ""){

                // presenta el mensaje y retorna
                mensaje = "Debe indicar la técnica utilizada";
                new jBox('Notice', {content: mensaje, color: 'red'});
                document.getElementById("nombre_prueba").focus();
                return false;

            // si ingresó
            } else {

                // agrega el nombre de la prueba
                this.PruebaOtra = document.getElementById("nombre_prueba").value;

                // si está editando
                if (document.getElementById("idotra").value != ""){
                    this.IdOtra = document.getElementById("idotra").value;
                }

            }

            // verifica se halla ingresado la marca
            if (document.getElementById("texto_marca").value == ""){

                // presenta el mensaje y retorna
                mensaje = "Ingrese la marca de reactivo utilizada";
                new jBox('Notice', {content: mensaje, color: 'red'});
                document.getElementById("texto_marca").focus();
                return false;

            // si seleccionó
            } else {

                // asigna en la variable de clase
                this.MarcaOtra = document.getElementById("texto_marca").value;

            }

        // si es cualquier otra técnica registrada en el sistema
        } else {

            // verifica se halla seleccionado la marca
            if (document.getElementById("marca").value == 0){

                // presenta el mensaje y retorna
                mensaje = "Seleccione la marca de reactivo utilizada";
                new jBox('Notice', {content: mensaje, color: 'red'});
                document.getElementById("marca").focus();
                return false;

            // si la seleccionó
            } else {

                // asigna en la variable de clase
                this.Marca = document.getElementById("marca").value;

            }

        }

        // verifica se ingrese el lote
        if (document.getElementById("lote").value == ""){

            // presenta el mensaje y retorna
            mensaje = "Indique el número de lote";
            new jBox('Notice', {content: mensaje, color: 'red'});
            document.getElementById("lote").focus();
            return false;

        // si ingresó
        } else {

            // asigna en la variable de clase
            this.Lote = document.getElementById("lote").value;

        }

        // verifica se ingrese la fecha de vencimiento
        if (document.getElementById("vencimiento").value == ""){

            // presenta el mensaje y retorna
            mensaje = "Indique la fecha de vencimiento del reactivo";
            new jBox('Notice', {content: mensaje, color: 'red'});
            document.getElementById("vencimiento").focus();
            return false;

        // si ingresó
        } else {

            // asigna en la variable de clase
            this.Vencimiento = document.getElementById("vencimiento").value;

        }

        // verifica se halla seleccionado un valor de lectura o ingresado
        if (document.getElementById("corte").selectedIndex === 0 && document.getElementById("texto_corte").value === ""){

            // presenta el mensaje y retorna
            mensaje = "Indique el valor de corte utilizado";
            new jBox('Notice', {content: mensaje, color: 'red'});
            return false;

        // si seleccionó valor de corte
        } else if (document.getElementById("corte").value != ""){

            // asigna en la variable de clase
            this.ValorCorte = document.getElementById("corte").value;

        // si ingresó un texto
        } else if (document.getElementById("texto_corte").value != ""){

            // asigna en la variable de clase
            this.ValorCorte = document.getElementById("texto_corte").value;

        }

        // verifica se halla ingresado la etiqueta
        if (document.getElementById("muestra").value == ""){

            // presenta el mensaje y retorna
            mensaje = "Ingrese el número de muestra";
            new jBox('Notice', {content: mensaje, color: 'red'});
            document.getElementById("muestra").focus();
            return false;

        // si ingresó
        } else {

            // asigna en la variable de clase
            this.Etiqueta = document.getElementById("muestra").value;
        }

        // verifica se halla seleccionado un resultado
        if (document.getElementById("determinacion").selectedIndex == 0){

            // presenta el mensaje y retorna
            mensaje = "Seleccione el resultado de la prueba";
            new jBox('Notice', {content: mensaje, color: 'red'});
            document.getElementById("determinacion").focus();
            return false;

        // si seleccionó
        } else {

            // asigna en la variable de clase
            this.Determinacion = document.getElementById("determinacion").value;

        }

        // verifica se halla seleccionado un valor de corte o ingresado
        // si el resultado es reactivo
        if ($("#resultado option:selected").text() == "Reactivo"){

            // si no declaró lectura o seleccionó el combo
            if (document.getElementById("lectura").value == 0 && document.getElementById("texto_lectura").value == ""){

                // presenta el mensaje y retorna
                mensaje = "Indique el valor de lectura obtenido";
                new jBox('Notice', {content: mensaje, color: 'red'});
                return false;

            }

        }

        // si indicó algún valor de lectura lo agrega
        if (document.getElementById("lectura").selectedIndex !== 0){
            this.ValorLectura = document.getElementById("lectura").value;
        } else if (document.getElementById("texto_lectura").value != ""){
            this.ValorLectura = document.getElementById("texto_lectura").value;
        }

        // actualiza los comentarios
        this.Comentarios = CKEDITOR.instances['observacionesdeterminacion'].getData();

        // grabamos el registro
        this.grabaDeterminacion();
    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método llamado al pulsar sobre el botón borrar, pide confirmación y
     * luego llama la consulta de eliminación
     */
    borraDeterminacion(){

        // obtenemos la clave del registro
        var id = document.getElementById("iddeterminacion").value;
        var laboratorio = document.getElementById("idlabdatos").value;

        // si no hay un registro activo
        if (id == ""){
            return false;
        }

        // definimos el cuadro de diálogo de confirmación
        var Confirmacion = new jBox('Confirm', {
            animation: 'flip',
            title: 'Eliminar Determinación',
            closeOnEsc: true,
            closeOnClick: false,
            closeOnMouseleave: false,
            closeButton: true,
            onCloseComplete: function(){
                this.destroy();
            },
            overlay: false,
            content: 'Está seguro que desea eliminar el registro?',
            theme: 'TooltipBorder',
            confirm: function() {

                // llama la rutina php
                $.get('determinaciones/borra_determinacion.php?id='+id,
                    function(data){

                        // si retornó correcto
                        if (data.Error != 0){

                            // recargamos la grilla para reflejar los cambios
                            determinaciones.muestraDeterminaciones(laboratorio);

                            // presenta el mensaje
                            new jBox('Notice', {content: "Registro eliminado", color: 'green'});

                            // cerramos el layer
                            determinaciones.layerDeterminacion.hide();

                        // si no pudo eliminar
                        } else {

                            // presenta el mensaje
                            new jBox('Notice', {content: "Ha ocurrido un error", color: 'red'});

                        }

                    }, "json");

            },
            cancel: function(){
                Confirmacion.destroy();
            },
            confirmButton: 'Aceptar',
            cancelButton: 'Cancelar'

        });

        // mostramos el diálogo
        Confirmacion.open();

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que arma el formulario de datos y lo envía al servidor
     */
    grabaDeterminacion(){

        // reiniciamos el contador
        cce.contador();

        // creamos un objeto html5
        var datosDeterminacion = new FormData();

        // si está editando
        if (this.IdDeterminacion != 0){
            datosDeterminacion.append("IdDeterminacion", this.IdDeterminacion);
        }

        // agregamos la id del laboratorio y luego los datos del formualrio
        datosDeterminacion.append("Laboratorio", this.Laboratorio);
        datosDeterminacion.append("Operativo", this.IdOperativo);
        datosDeterminacion.append("Tecnica", this.Tecnica);

        // si la técnica es elisa e indicó método
        if (this.Metodo != ""){

            // agrega el método
            datosDeterminacion.append("Metodo", this.Metodo);

        }

        // si la técnica es otra
        if (this.PruebaOtra != ""){

            // agrega el nombre de la prueba
            datosDeterminacion.append("Prueba", this.PruebaOtra);

            // si está editando
            if (this.IdOtra != 0){
                datosDeterminacion.append("IdOtra", this.IdOtra);
            }

            // agrega la marca
            datosDeterminacion.append("OtraMarca", this.MarcaOtra);

        // si es cualquier otra técnica registrada en el sistema
        } else {

            // agrega la marca
            datosDeterminacion.append("Marca", this.Marca);
        }

        // seguimos agregando
        datosDeterminacion.append("Lote", this.Lote);
        datosDeterminacion.append("Vencimiento", this.Vencimiento);
        datosDeterminacion.append("Corte", this.ValorCorte);
        datosDeterminacion.append("Etiqueta", this.Etiqueta);
        datosDeterminacion.append("Resultado", this.Determinacion);
        datosDeterminacion.append("Lectura", this.ValorLectura);
        datosDeterminacion.append("Comentarios", this.Comentarios);

        // serializa el formulario y lo envía a la rutina php en forma asincrónica
        $.ajax({
            type: "POST",
            url: "determinaciones/graba_determinacion.php",
            data: datosDeterminacion,
            cahe: false,
            contentType: false,
            processData: false,
            dataType: 'json',
            success: function(data) {

                // actualiza la id
                document.getElementById("iddeterminacion").value = data.Id;

                // presenta el mensaje y retorna
                new jBox('Notice', {content: "Registro Grabado ... ", color: 'green'});

                // recarga la grilla para mostrar el nuevo registro
                determinaciones.muestraDeterminaciones(document.getElementById("idlabdatos").value);

                // reiniciamos las variables de clase
                determinaciones.initDeterminaciones();

                // ocultamos el layer
                determinaciones.layerDeterminacion.hide();

                },

            });

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que presenta la nómina de operativos cerrados que
     * no tienen los resultados establecidos aún
     */
    sinResultados(){

        // cargamos en el panel de administracion
        $("#form_administracion").load("determinaciones/sin_resultados.html");

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que recibe como parámetro la clave del operativo
     * y establece el resultado correcto del mismo de acuerdo
     * al valor indicado por el usuario
     */
    establecerResultado(){

        // declaración de variables
        var mensaje;
        var operativo = document.getElementById("sel_operativo").value;
        var resultado = document.getElementById("resultado_correcto").value;

        // verificamos se halla seleccionado un operativo
        if (operativo == 0){

            mensaje = "Debe seleccionar el operativo";
            new jBox('Notice', {content: mensaje, color: 'red'});
            document.getElementById("sel_operativo").focus();
            return false;

        }

        // verificamos se halla seleccionado un resultado
        if (resultado == ""){

            mensaje = "Debe indicar el resultado correcto";
            new jBox('Notice', {content: mensaje, color: 'red'});
            document.getElementById("resultado_correcto").focus();
            return false;

        }

        // lo enviamos al servidor
        $.get('determinaciones/graba_resultado.php?operativo='+operativo+'&resultado='+resultado,
            function(data){

                // si pudo grabar
                if (data.Registros != 0){

                    // presenta el mensaje
                    new jBox('Notice', {content: "Se han actualizado: " + data.Registros + " registros.", color: 'green'});

                    // recargamos el select
                    determinaciones.nominaSinResultados("sel_operativo");

                    // reiniciamos el select de resultados
                    document.getElementById("resultado_correcto").value = "";

                // si hubo un error
                } else {

                    // presenta el mensaje
                    new jBox('Notice', {content: "Ha ocurrido un error", color: 'red'});

                }

            }, "json");

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param {string} idelemento - id de un elemento html
     * Método que recibe como parámetro la id de un elemento de un
     * formulario (normalmente un select) y carga en el la nómina
     * de operativos que no tienen asignadas resultados correctos
     */
    nominaSinResultados(idelemento){

        // limpia el combo
        $("#" + idelemento).html('');

        // obtenemos la nómina
        $.ajax({
            url: 'determinaciones/sin_resultados.php',
            type: "GET",
            cahe: false,
            contentType: false,
            processData: false,
            dataType: 'json',
            success: function(data) {

                // agrega el primer elemento en blanco
                $("#" + idelemento).append("<option value='0'>Seleccione</option>");

                // recorre el vector de resultados
                for(var i=0; i < data.length; i++){

                    // lo agrega al combo
                    $("#" + idelemento).append("<option value='" + data[i].Id + "'>" + data[i].Operativo + "</option>");

                }

            }});

    }

}