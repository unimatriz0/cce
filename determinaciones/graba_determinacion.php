<?php

/**
 *
 * determinaciones/graba_determinacion.php
 *
 * @package     CCE
 * @subpackage  Determinaciones
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.4.0 (16/08/2018)
 * @copyright   Copyright (c) 2018, INP
 *
 * Procedimiento que recibe por post los datos de una determinación
 * y ejecuta la consulta en la base
 *
*/

// incluimos e instanciamos la clase
require_once ("determinaciones.class.php");
$determinacion = new Determinaciones();

// si recibió la id
if (!empty($_POST["IdDeterminacion"])){
    $determinacion->setIdDatos($_POST["IdDeterminacion"]);
}

// vamos seteando los valores
$determinacion->setIdLaboratorio($_POST["Laboratorio"]);
$determinacion->setIdOperativo($_POST["Operativo"]);

// si recibió la marca (puede ser otra prueba)
// si no la recibió fijamos la clave de otra marca
if(!empty($_POST["Marca"])){
    $determinacion->setIdMarca($_POST["Marca"]);
} else {
    $determinacion->setIdMarca('29');
}

// seguimos asignando
$determinacion->setIdTecnica($_POST["Tecnica"]);
$determinacion->setLote($_POST["Lote"]);
$determinacion->setVencimiento($_POST["Vencimiento"]);
$determinacion->setMuestra($_POST["Etiqueta"]);
$determinacion->setDeterminacion($_POST["Resultado"]);
$determinacion->setComentarios($_POST["Comentarios"]);

// si es elisa
if (!empty($_POST["Metodo"])){
    $determinacion->setMetodo($_POST["Metodo"]);
}

// si envió el valor de corte
if (!empty($_POST["Corte"])){
    $determinacion->setValorCorte($_POST["Corte"]);
}

// si se envió valor de lectura
if (!empty($_POST["Lectura"])){
    $determinacion->setValorLectura($_POST["Lectura"]);
}

// si seleccionó otra prueba
if (!empty($_POST["Prueba"])){

    // si está editando
    if (!empty($_POST["IdOtra"])){
        $determinacion->setIdOtra($_POST["IdOtra"]);
    }

    // asignamos
    $determinacion->setPruebaOtra($_POST["Prueba"]);
    $determinacion->setMarcaOtra($_POST["OtraMarca"]);

}

// ejecutamos la consulta
$id = $determinacion->grabaDeterminacion();

// retornamos la id
echo json_encode(array("Id" => $id));

?>