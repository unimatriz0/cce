<?php

/**
 *
 * determinaciones/lista_determinaciones.php
 *
 * @package     CCE
 * @subpackage  Determinaciones
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.4.0 (16/08/2018)
 * @copyright   Copyright (c) 2018, INP
 *
 * Procedimniento que recibe como parámetro la id de un laboratorio
 * y presenta la grilla con todas las determinaciones realizadas, permitiendo
 * editar y dar de alta nuevas determinaciones
 *
*/

// incluimos la clase y la instanciamos
require_once ("determinaciones.class.php");
$determinacion = new Determinaciones();

// obtenemos la nómina de determinaciones
$resultado = $determinacion->nominaDeterminaciones($_GET["id"]);

// define la tabla
echo "<table width='90%' align='center' border='0' id='nomina_determinaciones'>";

// define los encabezados
echo "<thead>";
echo "<tr>";
echo "<th>Operativo</th>";
echo "<th>Inicio</th>";
echo "<th>Fin</th>";
echo "<th>Abierto</th>";
echo "<th>Tecnica</th>";
echo "<th>Marca</th>";
echo "<th>Determinación</th>";
echo "<th></th>";
echo "<th></th>";
echo "<th>";
echo "<input type='button'
       name='btnNuevaDeterminacion'
       id='btnNuevaDeterminacion'
       title='Pulse para cargar una nueva determinación'
       class='botonagregar'
       onClick='determinaciones.nuevaDeterminacion()'>";
echo "</th>";
echo "</tr>";
echo "</thead>";

// define el cuerpo
echo "<tbody>";

// recorremos el vector
foreach ($resultado AS $registro){

    // obtenemos el registro
    extract($registro);

    // abrimos la fila
    echo "<tr>";

    // agregamos el resto de los campos
    echo "<td align='center'><small>$operativo</small></td>";
    echo "<td align='center'><small>$fecha_inicio</small></td>";
    echo "<td align='center'><small>$fecha_fin</small></td>";
    echo "<td align='center'><small>$abierto</small></td>";
    echo "<td><small>$tecnica</small></td>";
    echo "<td><small>$marca</small></td>";
    echo "<td><small>$determinacion</small></td>";

    // presenta el botón editar
    echo "<td align='center'>";
    echo "<span class='tooltip'
            title='Pulse para ver el registro'>";
    echo "<input type='button'
                name='btnVerDeterminacion'
                class='botoneditar'
                onClick='determinaciones.getDatosDeterminacion($id_determinacion)'>";
    echo "</span>";
    echo "</td>";

    // si el operativo está cerrado y hay diferencias
    // entre el resultado y la determinación
    if ($abierto == "No" && ($determinacion != $correcto)){

        // presenta el ícono de observaciones
        echo "<td align='center'>";
        echo "<span class='tooltip'
               title='La determinación tiene errores'>";
        echo "<input type='button'
                     name='btnErrores'
                     class='botonborrar'>";
        echo "</td>";

    // de otra forma
    } else {

        // presentamos la celda vacía
        echo "<td></td>";

    }

    // si tiene comentarios
    if (strlen($comentarios) != 0){

        // armamos el enlace
        echo "<td>";
        echo "<span class='tooltip'
                title='La Determinación tiene comentarios'>";
        echo "<input type='button'
                name='btnComentarios'
                class='botoninformacion'>";
        echo "</span>";
        echo "</td>";

    // de otra forma
    } else {

        // presentamos la celda vacía
        echo "<td></td>";

    }

    // cerramos la fila
    echo "</tr>";

}

// cierra la tabla
echo "</tbody>";
echo "</table>";

// define el div para el paginador
echo "<div class='paging'></div>";

?>
<SCRIPT>

    // definimos las propiedades de la tabla
     $('#nomina_determinaciones').datatable({
        pageSize: 12,
        sort:    [true, true, true, true,     true,     true,     true,     false,    false, false],
        filters: [true, true, true, 'select', 'select', 'select', 'select', false, false, false],
        filterText: 'Buscar ... '
    });

    // instanciamos los tooltips
    new jBox('Tooltip', {
      attach: '.tooltip',
      theme: 'TooltipBorder'
    });

</SCRIPT>