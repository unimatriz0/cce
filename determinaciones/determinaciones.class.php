<?php

/**
 *
 * Class Determinaciones | determinaciones/determinaciones.class.php
 *
 * @package     CCE
 * @subpackage  Determinaciones
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.4.0 (16/08/2018)
 * @copyright   Copyright (c) 2018, INP
 *
 * Clase que controla las operaciones sobre la tabla de determinaciones
 *
*/

// inclusión de archivos
require_once ("../clases/conexion.class.php");

// convención para la nomenclatura de las propiedades, comienzan con una
// letra mayúscula, de tener mas de una palabra no se utilizan separadores
// y la inicial de cada palabra va en mayúscula
// para las variables recibidas como parámetro el criterio es todas en
// minúscula

// convención para la nomenclatura de los metodos, comienzan con set o get
// según asignen un valor o lo lean y luego el nombre del valor a obtener

/**
 * Definición de la clase
 * @author Claudio Invernizzi <cinvernizzi@gmail.com>
 */
// definición de la clase
class Determinaciones {

    // declaración de variables de la clase, las declaramos
    // como protected para que puedan ser heredadas pero
    // para asignarle el valor debemos crear los métodos
    protected $Link;                  // puntero a la base de datos

    // variables de la base de datos
    protected $Laboratorio;             // nombre del laboratorio
    protected $IdLaboratorio;           // clave del laboratorio
    protected $IdDatos;                 // clave de la determinación
    protected $Operativo;               // número de operativo
    protected $IdOperativo;             // clave del operativo
    protected $Abierto;                 // indica si el operativo está abierto
    protected $Marca;                   // marca del reactivo
    protected $IdMarca;                 // clave de la marca de reactivo
    protected $Tecnica;                 // nombre de la técnica
    protected $IdTecnica;               // clave de la técnica
    protected $Lote;                    // número de lote del reactivo
    protected $Vencimiento;             // fecha de vencimiento
    protected $Muestra;                 // número de muestra (etiqueta)
    protected $ValorCorte;              // valor de corte utilizado
    protected $ValorLectura;            // lectura obtenida
    protected $Metodo;                  // lector o visual para elisa
    protected $Determinacion;           // resultado de la determinación
    protected $Correcto;                // valor correcto de la determinación
    protected $Comentarios;             // comentarios del usuario
    protected $FechaAlta;               // fecha de alta del registro
    protected $Usuario;                 // nombre del usuario
    protected $IdUsuario;               // clave del usuario
    protected $IdOtra;                  // clave del registro si fue otra prueba
    protected $PruebaOtra;              // nombre de la otra prueba
    protected $MarcaOtra;               // nombre de la otra marca

    /**
     * Constructor de la clase, establece la conexión con la base
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     */
    function __construct (){

        // nos conectamos a la base de datos
        $this->Link = new Conexion();

        // inicializamos las variables
        $this->Laboratorio = "";
        $this->IdLaboratorio = 0;
        $this->IdDatos = 0;
        $this->Operativo = "";
        $this->IdOperativo = 0;
        $this->Abierto = "";
        $this->Marca = "";
        $this->IdMarca = 0;
        $this->Tecnica = "";
        $this->IdTecnica = 0;
        $this->Lote = "";
        $this->Vencimiento = "";
        $this->Muestra = 0;
        $this->ValorCorte = "";
        $this->ValorLectura = "";
        $this->Metodo = NULL;
        $this->Determinacion = "";
        $this->Correcto = "";
        $this->Comentarios = "";
        $this->FechaAlta = "";
        $this->Usuario = "";
        $this->IdOtra = 0;
        $this->PruebaOtra = "";
        $this->MarcaOtra = "";

        // iniciamos sesión
        session_start();

        // si existe la sesión
        if(isset($_SESSION["ID"])){

            // obtenemos la id del usuario
            $this->IdUsuario = $_SESSION["ID"];

        }

        // cerramos sesión
        session_write_close();

    }

    /**
     * Destructor de la clase, cierra la conexión con la base
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     */
    function __destruct(){

        // elimina el enlace a la base
        $this->Link = null;

    }

    // métodos de asignación de valores
    public function setIdLaboratorio($idlaboratorio){

        // verifica que sea un número
        if (!is_numeric($idlaboratorio)){

            // abandona por error
            echo "La clave del laboratorio debe ser un número";
            exit;

        // si está correcto
        } else {

            // lo asigna
            $this->IdLaboratorio = $idlaboratorio;

        }

    }
    public function setIdDatos($iddatos){

        // verifica que sea un número
        if (!is_numeric($iddatos)){

            // abandona por error
            echo "La clave de la determinación debe ser un número";
            exit;

        // si está correcto
        } else {

            // lo asigna
            $this->IdDatos = $iddatos;

        }

    }
    public function setIdOperativo($idoperativo){

        // verifica que sea un número
        if (!is_numeric($idoperativo)){

            // abandona por error
            echo "La clave del operativo debe ser un número";
            exit;

        // si está correcto
        } else {

            // lo asigna
            $this->IdOperativo = $idoperativo;

        }

    }
    public function setIdMarca($idmarca){

        // verifica que sea un número
        if (!is_numeric($idmarca)){

            // abandona por error
            echo "La clave de la marca debe ser un número";
            exit;

        // si está correcto
        } else {

            // lo asigna
            $this->IdMarca = $idmarca;

        }

    }
    public function setIdTecnica($idtecnica){

        // verifica que sea un número
        if (!is_numeric($idtecnica)){

            // abandona por error
            echo "La clave de la técnica debe ser un número";
            exit;

        // si está correcto
        } else {

            // lo asigna
            $this->IdTecnica = $idtecnica;

        }
    }
    public function setLote($lote){
        $this->Lote = $lote;
    }

    /**
     * Con el vencimiento recibimos la fecha en formato mm/aaaa
     * pero como usamos STR_TO_DATE en mysql tenemos que
     * agregarle el día
     */
    public function setVencimiento($vencimiento){
        $this->Vencimiento = "01/" . $vencimiento;
    }
    public function setMuestra($muestra){
        $this->Muestra = $muestra;
    }
    public function setValorCorte($valorcorte){
        $this->ValorCorte = $valorcorte;
    }
    public function setValorLectura($valorlectura){
        $this->ValorLectura = $valorlectura;
    }
    public function setMetodo($metodo){
        $this->Metodo = $metodo;
    }
    public function setDeterminacion($determinacion){
        $this->Determinacion = $determinacion;
    }
    public function setCorrecto($correcto){
        $this->Correcto = $correcto;
    }
    public function setComentarios($comentarios){
        $this->Comentarios = $comentarios;
    }
    public function setIdOtra($idotra){

        // verifica que sea un número
        if (!is_numeric($idotra)){

            // abandona por error
            echo "La clave de la otra determinación debe ser un número";
            exit;

        // si está correcto
        } else {

            // lo asigna
            $this->IdOtra = $idotra;

        }

    }
    public function setPruebaOtra($prueba){
        $this->PruebaOtra = $prueba;
    }
    public function setMarcaOtra($marca){
        $this->MarcaOtra = $marca;
    }

    // métodos de retorno de datos
    public function getIdLaboratorio(){
        return $this->IdLaboratorio;
    }
    public function getLaboratorio(){
        return $this->Laboratorio;
    }
    public function getIdDatos(){
        return $this->IdDatos;
    }
    public function getOperativo(){
        return $this->Operativo;
    }
    public function getIdOperativo(){
        return $this->IdOperativo;
    }
    public function getAbierto(){
        return $this->Abierto;
    }
    public function getMarca(){
        return $this->Marca;
    }
    public function getIdMarca(){
        return $this->IdMarca;
    }
    public function getTecnica(){
        return $this->Tecnica;
    }
    public function getIdTecnica(){
        return $this->IdTecnica;
    }
    public function getLote(){
        return $this->Lote;
    }
    public function getVencimiento(){
        return $this->Vencimiento;
    }
    public function getMuestra(){
        return $this->Muestra;
    }
    public function getValorCorte(){
        return $this->ValorCorte;
    }
    public function getValorLectura(){
        return $this->ValorLectura;
    }
    public function getMetodo(){
        return $this->Metodo;
    }
    public function getDeterminacion(){
        return $this->Determinacion;
    }
    public function getCorrecto(){
        return $this->Correcto;
    }
    public function getComentarios(){
        return $this->Comentarios;
    }
    public function getFechaAlta(){
        return $this->FechaAlta;
    }
    public function getUsuario(){
        return $this->Usuario;
    }
    public function getIdOtra(){
        return $this->IdOtra;
    }
    public function getPruebaOtra(){
        return $this->PruebaOtra;
    }
    public function getMarcaOtra(){
        return $this->MarcaOtra;
    }

    /**
     * Método que compone la consulta y asigna a las variables de clase
     * los valores del registro
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param int $iddeterminacion - clave del registro
     */
    public function getDatosDeterminacion($iddeterminacion){

        // inicializamos las variables
        $laboratorio = "";
        $id_laboratorio = 0;
        $id_datos = 0;
        $id_operativo = 0;
        $operativo = "";
        $abierto = "";
        $marca = "";
        $id_marca = 0;
        $tecnica = "";
        $id_tecnica = 0;
        $lote = "";
        $vencimiento = "";
        $muestra_nro = "";
        $valor_corte = "";
        $valor_lectura = "";
        $metodo = "";
        $determinacion = "";
        $correcto = "";
        $comentarios = "";
        $fecha_alta = "";
        $usuario = "";
        $id_otra = 0;
        $prueba_otra = "";
        $marca_otra = "";

        // componemos la consulta
        $consulta = "SELECT cce.laboratorios.NOMBRE AS laboratorio,
                            cce.laboratorios.ID AS id_laboratorio,
                            cce.chag_datos.ID AS id_datos,
                            cce.operativos_chagas.ID AS id_operativo,
                            cce.operativos_chagas.OPERATIVO_NRO AS operativo,
                            cce.operativos_chagas.ABIERTO AS abierto,
                            cce.marcas_chagas.MARCA AS marca,
                            cce.marcas_chagas.ID AS id_marca,
                            cce.tecnicas.TECNICA AS tecnica,
                            cce.tecnicas.ID AS id_tecnica,
                            cce.chag_datos.LOTE AS lote,
                            DATE_FORMAT(cce.chag_datos.VENCIMIENTO, '%m/%Y') AS vencimiento,
                            cce.chag_datos.MUESTRA_NRO AS muestra_nro,
                            cce.chag_datos.VALOR_CORTE AS valor_corte,
                            cce.chag_datos.VALOR_LECTURA AS valor_lectura,
                            cce.chag_datos.METODO AS metodo,
                            cce.chag_datos.DETERMINACION AS determinacion,
                            cce.chag_datos.CORRECTO AS correcto,
                            cce.chag_datos.COMENTARIOS AS comentarios,
                            DATE_FORMAT(cce.chag_datos.FECHA_ALTA, '%d/%m/%Y') AS fecha_alta,
                            cce.responsables.USUARIO AS usuario,
                            cce.chag_otras.ID AS id_otra,
                            cce.chag_otras.PRUEBA AS prueba_otra,
                            cce.chag_otras.MARCA AS marca_otra
                     FROM cce.chag_datos INNER JOIN cce.laboratorios ON cce.chag_datos.LABORATORIO = cce.laboratorios.ID
                                         INNER JOIN cce.operativos_chagas ON cce.chag_datos.OPERATIVO = cce.operativos_chagas.ID
                                         INNER JOIN cce.marcas_chagas ON cce.chag_datos.MARCA = cce.marcas_chagas.ID
                                         INNER JOIN cce.tecnicas ON cce.chag_datos.TECNICA = cce.tecnicas.ID
                                         INNER JOIN cce.responsables ON cce.chag_datos.USUARIO = cce.responsables.ID
                                         LEFT JOIN cce.chag_otras ON cce.chag_datos.ID = cce.chag_otras.ID_DATOS
                     WHERE cce.chag_datos.ID = '$iddeterminacion';";

        // obtenemos el registro
        $resultado = $this->Link->query($consulta);
        $registro = array_change_key_case($resultado->fetch(PDO::FETCH_ASSOC), CASE_LOWER);
        extract($registro);

        // asignamos a las variables de clase
        $this->Laboratorio = $laboratorio;
        $this->IdLaboratorio = $id_laboratorio;
        $this->IdDatos = $id_datos;
        $this->Operativo = $operativo;
        $this->IdOperativo = $id_operativo;
        $this->Abierto = $abierto;
        $this->Marca = $marca;
        $this->IdMarca = $id_marca;
        $this->Tecnica = $tecnica;
        $this->IdTecnica = $id_tecnica;
        $this->Lote = $lote;
        $this->Vencimiento = $vencimiento;
        $this->Muestra = $muestra_nro;
        $this->ValorCorte = $valor_corte;
        $this->ValorLectura = $valor_lectura;
        $this->Metodo = $metodo;
        $this->Determinacion = $determinacion;
        $this->Correcto = $correcto;
        $this->Comentarios = $comentarios;
        $this->FechaAlta = $fecha_alta;
        $this->Usuario = $usuario;
        $this->IdOtra = $id_otra;
        $this->PruebaOtra = $prueba_otra;
        $this->MarcaOtra = $marca_otra;

    }

    /**
     * Método que recibe como parámetro la clave de un laboratorio y
     * retorna el array con las determinaciones realizadas por ese
     * laboratorio
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param int - $idlaboratorio - la clave de un laboratorio
     * @return array
     */
    public function nominaDeterminaciones($idlaboratorio){

        // componemos la consulta
        $consulta = "SELECT cce.chag_datos.ID AS id_determinacion,
                            cce.operativos_chagas.OPERATIVO_NRO AS operativo,
                            cce.operativos_chagas.ABIERTO AS abierto,
                            DATE_FORMAT(cce.operativos_chagas.FECHA_INICIO, '%d/%m/%Y') AS fecha_inicio,
                            DATE_FORMAT(cce.operativos_chagas.FECHA_FIN, '%d/%m/%Y') AS fecha_fin,
                            cce.marcas_chagas.MARCA AS marca,
                            cce.tecnicas.TECNICA AS tecnica,
                            cce.chag_datos.DETERMINACION AS determinacion,
                            cce.chag_datos.VALOR_CORTE AS corte,
                            cce.chag_datos.VALOR_LECTURA AS lectura,
                            cce.chag_datos.CORRECTO AS correcto,
                            DATE_FORMAT(cce.chag_datos.FECHA_ALTA, '%%d/%%m/%%Y') AS fecha_alta,
                            cce.chag_datos.COMENTARIOS AS comentarios
                     FROM cce.chag_datos INNER JOIN cce.operativos_chagas ON cce.chag_datos.OPERATIVO = operativos_chagas.ID
                                         INNER JOIN cce.marcas_chagas ON cce.chag_datos.MARCA = cce.marcas_chagas.ID
                                         INNER JOIN cce.tecnicas ON cce.chag_datos.TECNICA = cce.tecnicas.ID
                     WHERE cce.chag_datos.LABORATORIO = '$idlaboratorio'
                     ORDER BY cce.chag_datos.FECHA_ALTA DESC;";

        // obtenemos el registro
        $resultado = $this->Link->query($consulta);
        $fila = array_change_key_case($resultado->fetchAll(PDO::FETCH_ASSOC), CASE_LOWER);

        // retornamos el vector
        return $fila;

    }

    /**
     * Método que graba en la base el registro de la determinación
     * y retorna la id del registro
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @return int
     */
    public function grabaDeterminacion(){

        // si está insertando
        if ($this->IdDatos == 0){
            $this->nuevaDeterminacion();
        } else {
            $this->editaDeterminacion();
        }

        // si es otra técnica
        if ($this->PruebaOtra != ""){

            // si está dando un alta
            if ($this->IdOtra == 0){
                $this->nuevaOtraDeterminacion();
            } else {
                $this->editaOtraDeterminacion();

            }

        }

        // retorna la id
        return $this->IdDatos;

    }

    /**
     * Método protegido que graba los datos de una nueva determinación
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     */
    protected function nuevaDeterminacion(){

        // compone la consulta de inserción
        $consulta = "INSERT INTO cce.chag_datos
                            (OPERATIVO,
                             LABORATORIO,
                             MARCA,
                             LOTE,
                             VENCIMIENTO,
                             MUESTRA_NRO,
                             VALOR_CORTE,
                             VALOR_LECTURA,
                             METODO,
                             TECNICA,
                             DETERMINACION,
                             COMENTARIOS,
                             USUARIO)
                            VALUES
                            (:idoperativo,
                             :idlaboratorio,
                             :idmarca,
                             :lote,
                             STR_TO_DATE(:vencimiento, '%d/%m/%Y'),
                             :muestra,
                             :valorcorte,
                             :valorlectura,
                             :metodo,
                             :idtecnica,
                             :determinacion,
                             :comentarios,
                             :idusuario);";

        // asignamos la consulta
        $psInsertar = $this->Link->prepare($consulta);

        // asignamos los parámetros de la consulta
        $psInsertar->bindParam(":idoperativo", $this->IdOperativo);
        $psInsertar->bindParam(":idlaboratorio", $this->IdLaboratorio);
        $psInsertar->bindParam(":idmarca", $this->IdMarca);
        $psInsertar->bindParam(":lote", $this->Lote);
        $psInsertar->bindParam(":vencimiento", $this->Vencimiento);
        $psInsertar->bindParam(":muestra", $this->Muestra);
        $psInsertar->bindParam(":valorcorte", $this->ValorCorte);
        $psInsertar->bindParam(":valorlectura", $this->ValorLectura);
        $psInsertar->bindParam(":metodo", $this->Metodo);
        $psInsertar->bindParam(":idtecnica", $this->IdTecnica);
        $psInsertar->bindParam(":determinacion", $this->Determinacion);
        $psInsertar->bindParam(":comentarios", $this->Comentarios);
        $psInsertar->bindParam(":idusuario", $this->IdUsuario);

        // ejecutamos la edición
        $resultado = $psInsertar->execute();

        // si salió todo bien
        if ($resultado){

            // obtiene la id del registro insertado
            $this->IdDatos = $this->Link->lastInsertId();

        // si hubo un error
        } else {

            // inicializa la clave y muestra el error
            $this->IdDatos = 0;
            echo $resultado;

        }

    }

    /**
     * Mètodo protegido que edita el registro actual de la determinación
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     */
    protected function editaDeterminacion(){

        // compone la consulta de edición
        $consulta = "UPDATE cce.chag_datos SET
                            OPERATIVO = :idoperativo,
                            LABORATORIO = :idlaboratorio,
                            MARCA = :idmarca,
                            LOTE = :lote,
                            VENCIMIENTO = STR_TO_DATE(:vencimiento, '%d/%m/%Y'),
                            MUESTRA_NRO = :muestra,
                            VALOR_CORTE = :valorcorte,
                            VALOR_LECTURA = :valorlectura,
                            METODO = :metodo,
                            TECNICA = :idtecnica,
                            DETERMINACION = :determinacion,
                            COMENTARIOS = :comentarios,
                            USUARIO = :idusuario
                    WHERE chag_datos.ID = :iddatos;";

        // asignamos la consulta
        $psInsertar = $this->Link->prepare($consulta);

        // asignamos los parámetros de la consulta
        $psInsertar->bindParam(":idoperativo", $this->IdOperativo);
        $psInsertar->bindParam(":idlaboratorio", $this->IdLaboratorio);
        $psInsertar->bindParam(":idmarca", $this->IdMarca);
        $psInsertar->bindParam(":lote", $this->Lote);
        $psInsertar->bindParam(":vencimiento", $this->Vencimiento);
        $psInsertar->bindParam(":muestra", $this->Muestra);
        $psInsertar->bindParam(":valorcorte", $this->ValorCorte);
        $psInsertar->bindParam(":valorlectura", $this->ValorLectura);
        $psInsertar->bindParam(":metodo", $this->Metodo);
        $psInsertar->bindParam(":idtecnica", $this->IdTecnica);
        $psInsertar->bindParam(":determinacion", $this->Determinacion);
        $psInsertar->bindParam(":comentarios", $this->Comentarios);
        $psInsertar->bindParam(":idusuario", $this->IdUsuario);
        $psInsertar->bindParam(":iddatos", $this->IdDatos);

        // ejecutamos la edición
        $resultado = $psInsertar->execute();

        // si hubo un error
        if (!$resultado){

            // inicializa ls clave y muestra el error
            $this->Iddatos = 0;
            echo $resultado;

        }

    }

    /**
     * Mètodo protegido que inserta un nuevo registro en la tabla
     * de otras determinaciones
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     */
    protected function nuevaOtraDeterminacion(){

        // compone la consulta de inserción
        $consulta = "INSERT INTO cce.chag_otras
                            (ID_DATOS,
                             PRUEBA,
                             MARCA)
                            VALUES
                            (:iddatos,
                             :pruebaotra,
                             :marcaotra); ";

        // asignamos la consulta
        $psInsertar = $this->Link->prepare($consulta);

        // asignamos los parámetros de la consulta
        $psInsertar->bindParam(":iddatos", $this->IdDatos);
        $psInsertar->bindParam(":pruebaotra", $this->PruebaOtra);
        $psInsertar->bindParam(":marcaotra", $this->MarcaOtra);

        // ejecutamos la edición
        $resultado = $psInsertar->execute();

        // si hubo un error
        if (!$resultado){

            // mostramos el error
            echo $resultado;

        }

    }

    /**
     * Método protegido que edita el registro en la tabla de
     * otras determinaciones
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     */
    protected function editaOtraDeterminacion(){

        // compone la consulta de edición
        $consulta = "UPDATE cce.chag_otras SET
                            ID_DATOS = :iddatos,
                            PRUEBA = :pruebaotra,
                            MARCA = :marcaotra
                     WHERE cce.chag_otras.ID = :idotra; ";

        // asignamos la consulta
        $psInsertar = $this->Link->prepare($consulta);

        // asignamos los parámetros de la consulta
        $psInsertar->bindParam(":iddatos", $this->IdDatos);
        $psInsertar->bindParam(":pruebaotra", $this->PruebaOtra);
        $psInsertar->bindParam(":marcaotra", $this->MarcaOtra);
        $psInsertar->bindParam(":idotra", $this->IdOtra);

        // ejecutamos la edición
        $resultado = $psInsertar->execute();

        // si hubo un error
        if (!$resultado){

            // presentamos el error
            echo $resultado;

        }

    }

    /**
     * Método que recibe la clave de una determinación y la elimina
     * de la tabla
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param int $idDeterminacion
     */
    public function borraDeterminacion($idDeterminacion){

        // compone la consulta
        $consulta = "DELETE FROM cce.chag_datos WHERE ID = '$idDeterminacion';";
        $this->Link->exec($consulta);

        // eliminamos si existe en la tabla de otras determinaciones
        $consulta = "DELETE FROM cce.chag_otras WHERE ID_DATOS = '$idDeterminacion';";
        $this->Link->exec($consulta);

    }

    /**
     * Método que retorna el array de operativos cerrados en los cuales
     * las determinaciones no tienen indicados los resultados correctos
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @return array
     */
    public function sinResultados(){

        // componemos la consulta
        $consulta = "SELECT DISTINCT(chag_datos.operativo) AS idoperativo,
                            operativos_chagas.operativo_nro AS operativo
                     FROM chag_datos INNER JOIN operativos_chagas ON chag_datos.operativo = operativos_chagas.id
                     WHERE isnull(chag_datos.correcto) AND
                           operativos_chagas.abierto = 'No'
                           ORDER BY chag_datos.operativo;";
        $resultado = $this->Link->query($consulta);

        // obtenemos la matriz y retornamos
        $fila = array_change_key_case($resultado->fetchAll(PDO::FETCH_ASSOC), CASE_LOWER);
        return $fila;

    }

    /**
     * Método que recibe como parámetros la clave de un operativo y el
     * resultado correcto y ejecuta la consulta de actualización en
     * la base, retorna el número de registros afectados
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param int $idoperativo - clave del operativo
     * @param string $resultado - resultado correcto de la muestra
     * @return int
     */
    public function resultadoCorrecto($idoperativo, $resultado){

        // componemos la consulta
        $consulta = "UPDATE cce.chag_datos SET
                            correcto = '$resultado'
                     WHERE cce.chag_datos.operativo = '$idoperativo';";
        $registros = $this->Link->exec($consulta);

        // retornamos
        return $registros;

    }

}
