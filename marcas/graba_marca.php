<?php

/**
 *
 * marcas/graba_marca.php
 *
 * @package     CCE
 * @subpackage  Marcas
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.4.0 (09/08/2018)
 * @copyright   Copyright (c) 2018, INP
 *
 * Método que recibe por post los datos del formulario y ejecuta
 * la consulta en el servidor
 *
*/

// incluimos e instanciamos la clase
require_once ("marcas.class.php");
$marca = new Marcas();

// asignamos los valores recibidos por post
if (!empty($_POST["Id"])){
    $marca->setIdMarca($_POST["Id"]);
}

// fijamos el resto de las variables
$marca->setMarca($_POST["Marca"]);
$marca->setIdTecnica($_POST["Tecnica"]);

// actualizamos el registro
$id = $marca->grabaMarca();

// retornamos el resultado de la operación
echo json_encode(array("Id" => $id));

?>