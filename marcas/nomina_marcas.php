<?php

/**
 *
 * marcas/marcas.class.php
 *
 * @package     CCE
 * @subpackage  Marcas
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.4.0 (09/08/2018)
 * @copyright   Copyright (c) 2018, INP
 *
 * Método que recibe por get la id de una técnica y arma el formulario
 * de abm de marcas correspondientes a esa técnica
 *
*/

// incluimos e instanciamos las clases
require_once ("marcas.class.php");
$marcas = new Marcas();

// obtenemos la nómina
$nomina = $marcas->nominaMarcas($_GET["tecnica"]);

// definimos la tabla
echo "<table width='90%' align='center' border='0'>";

// definimos el encabezado
echo "<thead>";
echo "<tr>";
echo "<th>Marca</th>";
echo "<th>Alta</th>";
echo "<th>Usuario</th>";
echo "<th></th>";
echo "<thead>";

// definimos el cuerpo
echo "<tbody>";

// recorremos el vector
foreach($nomina AS $registro){

    // obtenemos el registro
    extract($registro);

    // presentamos el registro
    echo "<tr>";
    echo "<td>";
    echo "<span class='tooltip'
           title='Nombre de la Marca'>";
    echo "<input type='text'
           name='marca'
           id='marca_$id_marca'
           size='25'
           value='$marca'>";
    echo "</span>";
    echo "</td>";

    // presentamos oculta la clave de la técnica
    echo "<input type='hidden'
           name='tecnica'
           id='tecnica_$id_marca'
           value='$id_tecnica'>";

    // presentamos la fecha y el usuario
    echo "<td align='center'>$fecha_alta</td>";
    echo "<td align='center'>$usuario</td>";

    // agregamos el enlace para grabar
    echo "<td align='center'>";
    echo "<input type='button' name='btnGrabaTecnica'
           id='btnGrabaTecnica'
           onClick='marcas.verificaMarca($id_marca)'
           class='botongrabar'>";
    echo "</td>";

    // cerramos la fila
    echo "</tr>";

}

// agregamos una fila para insertar un nuevo registro
echo "<tr>";

    // presentamos el registro
    echo "<td>";
    echo "<span class='tooltip'
           title='Nombre de la Marca'>";
    echo "<input type='text'
           name='marca_nueva'
           id='marca_nueva'
           size='25'>";
    echo "</span>";
    echo "</td>";

    // presentamos oculta la clave de la técnica
    echo "<input type='hidden'
           name='tecnica'
           id='tecnica_nueva'>";

    // fecha de alta y usuario
    echo "<td align='center'>";
    echo "<input type='text'
                 name='alta_marca'
                 id='alta_marca'
                 size='10'
                 readonly>";
    echo "</td>";
    echo "<td align='center'>";
    echo "<input type='text'
                 name='usuario_marca'
                 id='usuario_marca'
                 size='10'
                 readonly>";
    echo "</td>";

    // agregamos el enlace para grabar
    echo "<td align='center'>";
    echo "<input type='button' name='btnNuevaMarca'
           id='btnNuevaMarca'
           onClick='marcas.verificaMarca()'
           class='botonagregar'>";
    echo "</td>";

// cerramos la fila
echo "</tr>";

// cerramos la tabla
echo "</tbody>";
echo "</table>";

?>
<script>

    // instanciamos los tooltips
    new jBox('Tooltip', {
      attach: '.tooltip',
      theme: 'TooltipBorder'
    });

    // fijamos la fecha de alta y el usuario
    document.getElementById("alta_marca").value = fechaActual();
    document.getElementById("usuario_marca").value = sessionStorage.getItem("Usuario");

</script>
