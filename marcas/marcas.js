/*
 * Nombre: marcas.js
 * Autor: Lic. Claudio Invernizzi
 * E-Mail: cinvernizzi@gmail.com
 * Fecha: 25/12/2016
 * Licencia: GPL
 * Comentarios: Serie de funciones y procedimientos utilizados por el
 *              sistema de control de calidad en los formularios de
 *              abm de marcas de reactivos
 */

 /**
 *
 * Atención, utilizamos el nuevo estandard definido en ECMAScript 2015 (ES6)
 * este permite definir clases con el mètodo ya utilizado en php y java
 * como creo que es mas claro lo utilizaremos, la compatibilidad está
 * anunciada para firefox y chrome así que no deberìa haber problemas
 *
 */

/* jshint esversion: 6 */

/**
 * @author Claudio Invernizzi <cinvernizzi@gmail.com>
 * @class Clase que controla las operaciones del formulario de marcas
 * Definición de la clase
 */
class Marcas {

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @constructor
     */
    constructor(){

        // inicializamos las variables
        this.initMarcas();

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que inicializa las variables de clase
     */
    initMarcas(){

        // ionicializamos las variables
        this.IdMarca = 0;             // clave del registro
        this.Marca = "";              // descripción de la marca
        this.Tecnica = 0;             // clave de la técnica
        this.Correcto = true;         // switch de marca repetids

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que carga el formulario a dos columnas en el contendor
     */
    formMarcas(){

        // reiniciamos el contador
        cce.contador();

        // cargamos el formulario de marcas
        $("#form_administracion").load("marcas/form_marcas.html");

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método llamado en el evento onchange del combo de técnica
     * obtiene el valor seleccionado y se lo pasa por ajax a la
     * rutina php que carga la nómina en la segunda columna
     */
    listaMarcas(){

        // obtenemos el valor
        var idtecnica = document.nomina_tecnicas.lista_tecnicas.value;

        // si está seleccionado el primer elemento
        if (idtecnica == 0){

            // simplemente retorna
            return false;

        }

        // cargamos la nómina
        $("#columna2").load("marcas/nomina_marcas.php?tecnica="+idtecnica);

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método llamado por callback que verifica no se esté ingresando
     * una técnica repetida
     */
    validaMarca(marcaCorrecta){

        // inicializamos las variables
        this.Correcto = false;

        // llamamos sincrónicamente
        $.ajax({
            url: 'marcas/verifica_marca.php?marca='+this.Marca+'&tecnica='+this.Tecnica,
            type: "GET",
            cahe: false,
            contentType: false,
            processData: true,
            dataType: 'json',
            async: false,
            success: function(data) {

                // si retornó verdadero
                if (data.Registros == 0){
                    marcas.Correcto = true;
                } else {
                    marcas.Correcto = false;
                }

            }});

        // retornamos
        marcaCorrecta(this.Correcto);

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param {int} idmarca - clave del registro
     * Método que verifica el formulario antes de enviarlo a grabar
     * recibe como parámetro la id del registro en caso de edición
     * y si no recibe nada asume que es un alta
     */
    verificaMarca(idmarca){

        // declaración de variables
        var mensaje = "";

        // reiniciamos el contador
        cce.contador();

        // si recibió la id
        if (typeof(idmarca) != "undefined"){

            // asigna en la variable de clase
            this.IdMarca = idmarca;

        // si no recibió
        } else {

            // asigna en la variable local
            idmarca = "nueva";

        }

        // obtenemos la clave de la técnica del combo de técnicas
        this.Tecnica = document.getElementById("lista_tecnicas").value;

        // verifica se halla declarado nombre
        if (document.getElementById("marca_" + idmarca).value == ""){

            // presenta el mensaje y retorna
            mensaje = "Debe ingresar el nombre de la marca";
            new jBox('Notice', {content: mensaje, color: 'red'});
            document.getElementById("marca_" + idmarca).focus();
            return false;

        // si recibió
        } else {

            // asigna en la variable de clase
            this.Marca = document.getElementById("marca_" + idmarca).value;

        }

        // si está dando un alta
        if (this.IdMarca == 0){

            // verificamos por callback
            this.validaMarca(function(marcaCorrecta){

                // si está repetido
                if (!marcas.Correcto){

                    // presenta el mensaje
                    mensaje = "Esa marca ya se encuentra declarada !!!";
                    new jBox('Notice', {content: mensaje, color: 'red'});

                }

            });

        }

        // grabamos el registro
        this.grabaMarca();

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que verifica se halla ingresado la marca y luego la
     * envía por ajax al servidor
     */
    grabaMarca(){

        // si hubo un error
        if (!this.Correcto){
            return false;
        }

        // implementa el formulario usando objetos html5
        var formulario = new FormData();

        // si está editando
        if (this.IdMarca != 0){
            formulario.append("Id", this.IdMarca);
        }

        // agrega la marca y la técnica
        formulario.append("Marca", this.Marca);
        formulario.append("Tecnica", this.Tecnica);

        // serializa el formulario y lo envía, recibe
        // como respuesta el resultado de la operación
        $.ajax({
            url: "marcas/graba_marca.php",
            type: "POST",
            data: formulario,
            cahe: false,
            contentType: false,
            processData: false,
            success: function(data) {

                // si salío todo bien
                if (data.Id != 0){

                    // presentamos el resultado
                    new jBox('Notice', {content: "Registro grabado ... ", color: 'green'});

                    // reiniciamos las variables de clase
                    marcas.initMarcas();

                    // recarga el formulario para reflejar los cambios
                    marcas.listaMarcas();

                // si hubo algún error
                } else {

                    // presenta la alerta
                    new jBox('Notice', {content: "Ha ocurrido un error, intente nuevamente", color: 'red'});

                }
            },

        });

    }

    /**
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param {string} idelemento - id de un objeto html
     * @param {int} idtecnica - la clave de la técnica
     * @param {int} idmarca - clave a predeterminar
     * Método que recibe como parámetro la id de un select de un formulario
     * carga en ese elemento la nómina de tecnicas, si además recibe
     * la clave de un elemento, lo predetermina
     */
    nominaMarcas(idelemento, idtecnica, idmarca){

        // limpia el combo
        $("#" + idelemento).html('');

        // obtenemos la nómina
        $.ajax({
            url: 'marcas/lista_marcas.php?tecnica='+idtecnica,
            type: "GET",
            cahe: false,
            contentType: false,
            processData: false,
            dataType: 'json',
            success: function(data) {

                // agrega el primer elemento en blanco
                $("#" + idelemento).append("<option value='0'>Seleccione</option>");

                // recorre el vector de resultados
                for(var i=0; i < data.length; i++){

                    // si recibió la clave
                    if (typeof(idmarca) != "undefined"){

                        // si coindicen
                        if (idmarca == data[i].Id){

                            // lo agrega preseleccionado
                            $("#" + idelemento).append("<option selected value='" + data[i].Id + "'>" + data[i].Marca + "</option>");

                        // si no coincide
                        } else {

                            // simplemente lo agrega
                            $("#" + idelemento).append("<option value='" + data[i].Id + "'>" + data[i].Marca + "</option>");

                        }

                    // si no recibió
                    } else {

                        // simplemente lo agrega
                        $("#" + idelemento).append("<option value='" + data[i].Id + "'>" + data[i].Marca + "</option>");

                    }

                }

            }});

    }

}