<?php

/**
 *
 * marcas/lista_marcas.php
 *
 * @package     CCE
 * @subpackage  Marcas
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.4.0 (09/08/2018)
 * @copyright   Copyright (c) 2017, INP
 *
 * Método que recibe la clave de una técnica y retorna un
 * array json con la nómina completa de marcas declaradas
*/

// incluimos e instanciamos las clases
require_once("marcas.class.php");
$marcas = new Marcas();

// obtenemos la nómina
$nomina = $marcas->nominaMarcas($_GET["tecnica"]);

// declaramos la matriz
$jsondata = array();

// recorremos el vector
foreach($nomina AS $registro){

    // obtenemos el registro
    extract($registro);

    // lo agregamos a la matriz
    $jsondata[] = array("Id" => $id_marca,
                        "Marca" => $marca);

}

// retornamos el vector
echo json_encode($jsondata);

?>