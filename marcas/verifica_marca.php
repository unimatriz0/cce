<?php

/**
 *
 * marcas/verifica_marca.php
 *
 * @package     CCE
 * @subpackage  Marcas
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.4.0 (09/08/2018)
 * @copyright   Copyright (c) 2018, INP
 *
 * Método que recibe por get el nombre de una marca y la clave
 * de la técnica, verifica que no se encuentre repetido y
 * retorna el número de registros encontrados
 *
*/

// incluimos e instanciamos la clase
require_once ("marcas.class.php");
$marca = new Marcas();

// verificamos si la marca existe
$registros = $marca->getClaveMarca($_GET["tecnica"], $_GET["marca"]);

// retornamos el resultado de la operación
echo json_encode(array("Registros" => $registros));

?>