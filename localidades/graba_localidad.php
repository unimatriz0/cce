<?php

/**
 *
 * localidades/graba_localidad.php
 *
 * @package     CCE
 * @subpackage  Localidades
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.4.0 (10/08/2018)
 * @copyright   Copyright (c) 2018, INP
 *
 * Método que recibe por post los datos del formulario y ejecuta la
 * consulta en el servidor
 *
*/

// incluimos e instanciamos las clases
require_once ("localidades.class.php");
$localidades = new Localidades();

// asignamos los valores en la clase
$localidades->setCodPcia($_POST["Provincia"]);
$localidades->setIdLocalidad($_POST["CodLoc"]);
$localidades->setNombreLocalidad($_POST["Localidad"]);
$localidades->setPoblacionLocalidad($_POST["Poblacion"]);

// ejecutamos la consulta pasándole como argumento la acción
$resultado = $localidades->grabaLocalidad($_POST["Evento"]);

// retornamos el resultado de la operación
echo json_encode(array("Resultado" => $resultado));

?>