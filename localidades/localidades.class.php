<?php

/**
 *
 * Class Localidades | localidades/localidades.class.php
 *
 * @package     CCE
 * @subpackage  Localidades
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.4.0 (10/08/2018)
 * @copyright   Copyright (c) 2018, INP
 *
 * Cláse que contiene las operaciones sobre la base de datos
 * de localidades
 *
*/

// inclusión de archivos
require_once ("../clases/conexion.class.php");

// convención para la nomenclatura de las propiedades, comienzan con una
// letra mayúscula, de tener mas de una palabra no se utilizan separadores
// y la inicial de cada palabra va en mayúscula
// para las variables recibidas como parámetro el criterio es todas en
// minúscula

// convención para la nomenclatura de los metodos, comienzan con set o get
// según asignen un valor o lo lean y luego el nombre del valor a obtener

/**
 * Definición de la clase
 * @author Claudio Invernizzi <cinvernizzi@gmail.com>
 */
class Localidades {

    // declaración de variables de la clase, las declaramos
    // como protected para que puedan ser heredadas pero
    // para asignarle el valor debemos crear los métodos
    protected $IdUsuario;             // clave del usuario
    protected $FechaAlta;             // fecha de alta del registro
    protected $NombreLocalidad;       // nombre de la localidad
    protected $IdLocalidad;           // clave indec de la lodiccionarios
    protected $PoblacionLocalidad;    // poblacion de la localidad
    protected $CodPcia;               // código indec de la jurisdicción
    protected $Link;                  // puntero a la base de datos

    /**
     * Constructor de la clase, establece la conexión con la base
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     */
    function __construct (){

        // inicializamos las variables
        $this->IdLocalidad = "";
        $this->NombreLocalidad = "";
        $this->PoblacionLocalidad = 0;
        $this->CodPcia = "";

        // inicializamos la fecha de alta en formato mysql
        $this->FechaAlta = date('Y/m/d');

        // abrimos la sesión y obtenemos el usuario
        session_start();
        if (!empty($_SESSION["ID"])){
            $this->IdUsuario = $_SESSION["ID"];
        }
        session_write_close();

        // nos conectamos a la base de datos
        $this->Link = new Conexion();

    }

    /**
     * Destructor de la clase, cierra la conexión con la base
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     */
    function __destruct(){

        // elimina el enlace a la base
        $this->Link = null;

    }

    // métodos de asignación
    public function setCodPcia($codpcia){
        $this->CodPcia = $codpcia;
    }
    public function setNombreLocalidad($localidad){
        $this->NombreLocalidad = $localidad;
    }
    public function setIdLocalidad($idlocalidad){
        $this->IdLocalidad = $idlocalidad;
    }
    public function setPoblacionLocalidad($poblacion){

        // verifica que sea un número
        if (!is_numeric($poblacion)){

            // abandona por error
            echo "La poblacion de la localidad debe ser un número";
            exit;

        // si está correcto
        } else {

            // lo asigna
            $this->PoblacionLocalidad = $poblacion;

        }

    }

    /**
     * Método que recibe como parámetro la id de una provincia o el nombre
     * y retorna la nómina de los diccionarios en esa provincia junto con su clave
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param string $provincia - clave indec de la provincia
     * @return array
     */
    public function listaLocalidades($provincia){

        // componemos la consulta
        $consulta = "SELECT diccionarios.v_localidades.codloc AS idlocalidad,
                            UPPER(diccionarios.v_localidades.localidad) AS nombre_localidad,
                            diccionarios.v_localidades.poblacion AS poblacion,
                            v_localidades.usuario AS usuario,
                            diccionarios.v_localidades.fecha_alta AS fecha_alta
                     FROM diccionarios.v_localidades
                     WHERE diccionarios.v_localidades.codpcia = '$provincia' OR
                           diccionarios.v_localidades.provincia = '$provincia'
                     ORDER BY diccionarios.v_localidades.localidad; ";

        // ejecutamos la consulta
        $resultado = $this->Link->query($consulta);

        // lo pasamos a minúsculas porque según la versión de
        // pdo lo devuelve en mayúsculas o minúsculas
        $nomina = array_change_key_case($resultado->fetchAll(PDO::FETCH_ASSOC), CASE_LOWER);

        // retornamos el vector
        return $nomina;

    }

    /**
     * Método que recibe como parámetros la clave indec de la provincia y
     * el nombre de la localidad, retorna el número de registros
     * encontrados
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param string $provincia - clave indec de la provincia
     * @param string $localidad - nombre de la localidad
     * @return int
     */
    public function validaLocalidad($provincia, $localidad){

        // inicializamos las variables
        $registros = 0;

        // componemos la consulta
        $consulta = "SELECT COUNT(diccionarios.v_localidades.id) AS registros
                     FROM diccionarios.v_localidades
                     WHERE diccionarios.v_localidades.codpcia = '$provincia' AND
                           diccionarios.v_localidades.localidad = '$localidad';";

        // obtenemos el registro y retornamos
        $resultado = $this->Link->query($consulta);
        $fila = array_change_key_case($resultado->fetch(PDO::FETCH_ASSOC), CASE_LOWER);
        extract($fila);
        return $registros;

    }

    /**
     * Método que recibe como parámetro la acción a realizar (insertar o
     * editar) compone la consulta y ejecuta sobre la tabla de los diccionarios
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param string con la acción
     * @return bool
     */
    public function grabaLocalidad($evento){

        // si está insertando
        if ($evento == "insertar"){
            $resultado = $this->nuevaLocalidad();
        } else {
            $resultado = $this->editaLocalidad();
        }

        // retornamos el estado de la operación
        return $resultado;

    }

    /**
     * Método protegido que inserta un nuevo registro en la tabla
     * de localidades, retorna el resultado de la operación
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @return $resultado
     */
    protected function nuevaLocalidad(){

        // compone la consulta de inserción
        $consulta = "INSERT INTO diccionarios.localidades
                            (CODPCIA,
                             NOMLOC,
                             CODLOC,
                             POBLACION,
                             USUARIO,
                             FECHA_ALTA)
                            VALUES
                            (:codpcia,
                             :localidad,
                             :codloc,
                             :poblacion,
                             :usuario,
                             :fecha_alta);";

        // asignamos la consulta
        $psInsertar = $this->Link->prepare($consulta);

        // asignamos los valores
        $psInsertar->bindParam(":codpcia", $this->CodPcia);
        $psInsertar->bindParam(":localidad", $this->NombreLocalidad);
        $psInsertar->bindParam(":codloc", $this->IdLocalidad);
        $psInsertar->bindParam(":poblacion", $this->PoblacionLocalidad);
        $psInsertar->bindParam(":usuario", $this->IdUsuario);
        $psInsertar->bindParam(":fecha_alta", $this->FechaAlta);

        // ejecutamos la consulta
        $resultado = $psInsertar->execute();

        // si no hubo error
        if ($resultado){
            return $resultado;
        } else {
            echo $resultado;
        }

    }

    /**
     * Método protegido que edita el registro de localidades
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @return $resultado
     */
    protected function editaLocalidad(){

        // compone la consulta de edición
        $consulta = "UPDATE diccionarios.localidades SET
                            CODPCIA = :codpcia,
                            NOMLOC = :localidad,
                            POBLACION = :poblacion,
                            USUARIO = :usuario,
                            FECHA_ALTA = :fecha_alta
                     WHERE diccionarios.localidades.CODLOC = :codloc;";

        // asignamos la consulta
        $psInsertar = $this->Link->prepare($consulta);

        // asignamos los valores
        $psInsertar->bindParam(":codpcia", $this->CodPcia);
        $psInsertar->bindParam(":localidad", $this->NombreLocalidad);
        $psInsertar->bindParam(":poblacion", $this->PoblacionLocalidad);
        $psInsertar->bindParam(":usuario", $this->IdUsuario);
        $psInsertar->bindParam(":fecha_alta", $this->FechaAlta);
        $psInsertar->bindParam(":codloc", $this->IdLocalidad);

        // ejecutamos la consulta
        $resultado = $psInsertar->execute();

        // si salió todo bien
        if ($resultado){
            return $resultado;
        } else {
            echo $resultado;
        }

    }

}
?>
