<?php

/**
 *
 * localidades/lista_localidad.php
 *
 * @package     CCE
 * @subpackage  Localidades
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.4.0 (10/08/2018)
 * @copyright   Copyright (c) 2018, INP
 *
 * Método que recibe como parámetr la clave de una jurisdicción
 * y retorna un array json con los datos de las localidades de
 * esa jurisdicción
 *
*/

// inclusión de archivos
require_once ("localidades.class.php");
$localidades = new Localidades();

// obtiene la nómina de localidades
$nomina = $localidades->listaLocalidades($_GET["provincia"]);

// inicializa las variables
$jsondata = array();

// inicia un bucle recorriendo el vector
foreach ($nomina AS $registro){

    // obtiene el registro
    extract($registro);

    // lo agrega a la matriz
    $jsondata[] = array("Id" => $idlocalidad,
                        "Localidad" => $nombre_localidad);

}

// devuelve la cadena
echo json_encode($jsondata);

?>
