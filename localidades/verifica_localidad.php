<?php

/**
 *
 * localidades/verifica_localidad.php
 *
 * @package     CCE
 * @subpackage  Localidades
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.4.0 (10/08/2018)
 * @copyright   Copyright (c) 2018, INP
 *
 * Método que recibe por get la clave de la provincia y el nombre
 * de una localidad, y verifica que no se encuentre repetida
 *
*/

// incluimos e instanciamos las clases
require_once ("localidades.class.php");
$localidades = new Localidades();

// verificamos si existe
$registros = $localidades->validaLocalidad($_GET["provincia"], $_GET["localidad"]);

// retornamos el resultado de la operación
echo json_encode(array("Registros" => $registros));

?>
