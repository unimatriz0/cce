/*
 * Nombre: localidades.js
 * Autor: Lic. Claudio Invernizzi
 * E-Mail: cinvernizzi@gmail.com
 * Fecha: 10/01/2017
 * Licencia: GPL
 * Comentarios: Serie de funciones y procedimientos utilizados por el
 *              sistema de control de calidad en los formularios de
 *              abm de localidades
 */

 /**
 *
 * Atención, utilizamos el nuevo estandard definido en ECMAScript 2015 (ES6)
 * este permite definir clases con el mètodo ya utilizado en php y java
 * como creo que es mas claro lo utilizaremos, la compatibilidad está
 * anunciada para firefox y chrome así que no deberìa haber problemas
 *
 */

/* jshint esversion: 6 */

/**
 * @author Claudio Invernizzi <cinvernizzi@gmail.com>
 * @class Clase que controla las operaciones sobre la tabla de
 * localidades
 */
class Localidades{

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @constructor
     */
    constructor(){

        // inicializamos las variables de clase

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que inicializa las variables de clase
     */
    initLocalidades(){

        // inicializamos las variables
        this.CodPcia = "";               // clave indec de la provincia
        this.NomLoc = "";                // nombre de la localidad
        this.CodLoc = "";                // clave indec de la localidad
        this.Poblacion = 0;              // población de la localidad
        this.Correcto = true;            // switch de localidad repetida

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que carga en el contenido el formulario con el
     * abm de localidades
     */
    formLocalidades(){

        // reiniciamos el contador
        cce.contador();

        // carga el formulario
        $("#form_administracion").load("localidades/form_localidades.html");

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método llamado en el evento onchange del combo de jurisdicciones
     * obtiene la id de la jurisdicción y llama por ajax la rutina
     * php para cargar en el div la nómina de localidades de esa
     * jurisdicción
     */
    listaLocalidades(){

        // reiniciamos el contador
        cce.contador();

        // obtenemos el valor seleccionado
        var provincia = document.getElementById("jurisdicciones_localidad").value;

        // si está seleccionado el primer elemento
        if (provincia == 0){
            return false;
        }

        // cargamos el formulario de localidades
        $("#grilla_localidades").load("localidades/nomina_localidades.php?provincia="+provincia);

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método llamado en el onchange del combo de país, llama la clase de
     * jurisdicciones para cargar los valores del país seleccionado
     */
    listaJurisdicciones(){

        // obtenemos el país
        var pais = document.getElementById("pais_localidad").value;

        // si no seleccionó
        if (pais == 0){
            return false;
        }

        // cargamos en el combo de jurisdicciones la nómina
        jurisdicciones.listaJurisdicciones("jurisdicciones_localidad", pais);

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método llamado por callback que verifica no se encuentre
     * repetida la localidad
     */
    validaLocalidad(localidadCorrecta){

        // inicializamos las variables
        this.Correcto = false;

        // llamamos sincrónicamente
        $.ajax({
            url: 'localidades/verifica_localidad.php?provincia='+this.CodLoc+'&localidad='+this.NomLoc,
            type: "GET",
            cahe: false,
            contentType: false,
            processData: false,
            dataType: 'json',
            async: false,
            success: function(data) {

                // si retornó verdadero
                if (data.Registros == 0){
                    localidades.Correcto = true;
                } else {
                    localidades.Correcto = false;
                }

            }});

        // retornamos
        localidadCorrecta(this.Correcto);

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que verifica el formulario de datos antes de
     * enviarlo al servidor
     */
    verificaLocalidad(id){

        // reiniciamos el contador
        cce.contador();

        // si está dando un alta
        if (typeof(id) == "undefined"){
            id = "nueva";
            this.Evento = "insertar";
        } else {
            this.Evento = "editar";
        }

        // obtenemos los valores del formulario
        this.CodPcia = document.form_localidades.jurisdicciones_localidad.value;

        // verifica el código
        if (document.getElementById("codloc_" + id).value == ""){

            // presenta el mensaje y retorna
            mensaje = "Debe ingresar el Código Indec de la localidad";
            new jBox('Notice', {content: mensaje, color: 'red'});
            document.getElementById("codloc_" + id).focus();
            return false;

        // si lo ingresó
        } else {

            // asigna en la variable de clase
            this.CodLoc = document.getElementById("codloc_" + id).value;

        }

        // verifica el nombre
        if (document.getElementById("localidad_" + id).value == ""){

            // presenta el mensaje y retorna
            mensaje = "Debe ingresar el nombre de la localidad";
            new jBox('Notice', {content: mensaje, color: 'red'});
            document.getElementById("localidad_" + id).focus();
            return false;

        // si asignó
        } else {

            // asigna en la variable de clase
            this.NomLoc = document.getElementById("localidad_" + id).value;

        }

        // verifica la población
        if (document.getElementById("poblacion_" + id).value == ""){

            // presenta el mensaje y retorna
            mensaje = "Debe ingresar el Código Indec de la localidad";
            new jBox('Notice', {content: mensaje, color: 'red'});
            document.getElementById("poblacion_" + id).focus();
            return false;

        // si asignó
        } else {

            // asignamos en la variable de clase
            this.Poblacion = document.getElementById("poblacion_" + id).value;

        }

        // si está dando un alta verifica que no esté repetida
        if (this.Evento == "insertar"){

            // verificamos por callback
            this.validaLocalidad(function(localidadCorrecta){

                // si está repetido
                if (!localidades.Correcto){

                    // presenta el mensaje
                    mensaje = "Esa localidad ya se encuentra declarada !!!";
                    new jBox('Notice', {content: mensaje, color: 'red'});

                }

            });

        }

        // grabamos el registro
        this.grabaLocalidad();

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que ejecuta la consulta en el servidor
     */
    grabaLocalidad(){

        // si el registro está repetido
        if (!this.Correcto){
            return false;
        }

        // obtenemos los valores de jurisdicción y localidad
        // del formulario de responsables y de laboratorios
        var jurresponsable = document.getElementById("jurisdiccion").value;
        var locresponsable = document.getElementById("localidad").value;
        var jurlaboratorio = document.getElementById("jurisdiccionlaboratorio").value;
        var loclaboratorio = document.getElementById("localidadlaboratorio").value;

        // implementa el formulario usando objetos html5
        var formulario = new FormData();

        // agrega los valores
        formulario.append("CodLoc", this.CodLoc);
        formulario.append("Provincia", this.CodPcia);
        formulario.append("Localidad", this.NomLoc);
        formulario.append("Poblacion", this.Poblacion);
        formulario.append("Evento", this.Evento);

        // serializa el formulario y lo envía, recibe
        // como respuesta el resultado de la operación
        $.ajax({
            url: "localidades/graba_localidad.php",
            type: "POST",
            data: formulario,
            cahe: false,
            contentType: false,
            processData: false,
            success: function(data) {

                // si salío todo bien
                if (data.Resultado != 0){

                    // presenta el mensaje
                    new jBox('Notice', {content: "Registro grabado ... ", color: 'green'});

                    // reiniciamos las variables de clase
                    localidades.initLocalidades();

                    // recarga el formulario para reflejar los cambios
                    localidades.listaLocalidades();

                    // si es la misma jurisdicción que la del responsable
                    if (document.getElementById("jurisdicciones_localidad").value == jurresponsable){

                        // actualizamos el select de responsables
                        localidades.nominaLocalidades("localidad", jurresponsable, locresponsable);

                    }

                    // si es la misma jurisdicción que la del laboratorio
                    if (document.getElementById("jurisdicciones_localidad").value == jurlaboratorio){

                        // actualizamos el select de laboratorios
                        localidades.nominaLocalidades("localidadlaboratorio", jurlaboratorio, loclaboratorio);

                    }

                // si hubo algún error
                } else {

                    // presenta la alerta
                    new jBox('Notice', {content: "Ha ocurrido un error, intente nuevamente", color: 'red'});

                }

            },

        });

    }

    /**
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param {string} idelemento - select del formulario
     * @param {string} idprovincia - clave de la jurisdicción
     * @param {string} idlocalidad - clave de la localidad (puede ser nulo)
     * Método que recibe como parámetro la id del select de un formulario, la clave
     * indec de la jurisdicción y (optativo) la clave de la jurisdicción
     * preseleccionada, carga en el select recibido como argumento la nómina
     * de localidades correspondientes a la jurisdicción y en todo caso
     * preselecciona la recibida
     */
    nominaLocalidades(idelemento, idprovincia, idlocalidad){

        // limpia el combo
        $("#" + idelemento).html('');

        // agrega el texto al combo
        $("#" + idelemento).append("<option value=0>Seleccione</option>");

        // si hay alguna jurisdicción seleccionada
        if (idprovincia != 0){

            // lo llamamos sincrónico
            $.ajax({
                url: "localidades/lista_localidad.php?provincia="+idprovincia,
                type: "GET",
                cahe: false,
                contentType: false,
                processData: false,
                dataType: 'json',
                success: function(data) {

                    // recorre el vector de resultados
                    for(var i=0; i < data.length; i++){

                        // si recibió la clave de la localidad
                        if (typeof(idlocalidad) != "undefined"){

                            // verifica y si es igual preselecciona
                            if (data[i].Id == idlocalidad){
                                $("#" + idelemento).append("<option selected value=" + data[i].Id + ">" + data[i].Localidad + "</option>");
                            } else {
                                $("#" + idelemento).append("<option value=" + data[i].Id + ">" + data[i].Localidad + "</option>");
                            }

                        // si no recibió nada
                        } else {
                            $("#" + idelemento).append("<option value=" + data[i].Id + ">" + data[i].Localidad + "</option>");
                        }
                    }

            }});

        }

    }

}