<?php

/**
 *
 * localidades/nomina_localidades.php
 *
 * @package     CCE
 * @subpackage  Localidades
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.4.0 (10/08/2018)
 * @copyright   Copyright (c) 2018, INP
 *
 * Método que recibe como parámetro la id de una jurisdicción y
 * arma el formulario de abm de localidades de esa provincia
 *
*/

// incluimos e instanciamos las clases
require_once ("localidades.class.php");
$localidades = new Localidades();

// obtenemos la nómina
$nomina = $localidades->listaLocalidades($_GET["provincia"]);

// definimos la tabla
echo "<table width='90%' align='center' border='0'>";

// definimos los encabezados a dos columnas
echo "<thead>";
echo "<tr>";

// la primer columna
echo "<th>COD</th>";
echo "<th>Localidad</th>";
echo "<th>Población</th>";
echo "<th></th>";

// la segunda columna
echo "<th>COD</th>";
echo "<th>Localidad</th>";
echo "<th>Población</th>";
echo "<th></th>";

// cerramos la fila y el encabezado
echo "</tr>";
echo "</thead>";

// definimos el cuerpo
echo "<tbody>";

// iniciamos el contador de columnas
$columna = 1;

// recorremos el vector
foreach($nomina AS $registro){

    // obtenemos el registro
    extract($registro);

    // si estamos en la primer columna
    if ($columna == 1){

        // abrimos la fila
        echo "<tr>";

    }

    // presentamos el código de localidad como solo
    // lectura porque es la clave del registro y
    // no se puede modificar
    echo "<td align='center'>";
    echo "<span class='tooltip'
                title='Código de la Localidad'>";
    echo "<input type='text'
           name='codloc'
           id='codloc_$idlocalidad'
           value='$idlocalidad'
           size='8'
           readonly>";
    echo "</span>";
    echo "</td>";

    // presentamos el nombre
    echo "<td align='center'>";
    echo "<span class='tooltip'
                title='Nombre de la Localidad'>";
    echo "<input type='text'
           name='localidad'
           id='localidad_$idlocalidad'
           value='$nombre_localidad'
           size='25'>";
    echo "</span>";
    echo "</td>";

    // la población
    echo "<td align='center'>";
    echo "<span class='tooltip'
               title='Número de habitantes'>";
    echo "<input type='number'
           name='poblacion'
           id='poblacion_$idlocalidad'
           class='numerogrande'
           value='$poblacion'
           size='10'>";
    echo "</span>";
    echo "</td>";

    // presenta el botón grabar
    echo "<td align='center'>";
    echo "<input type='button' name='btnGrabaLocalidad'
           id='btnGrabaLocalidad'
           onClick='localidades.verificaLocalidad(" . chr(34) . $idlocalidad . chr(34) . ")'
           title='Pulse para actualizar la localidad'
           class='botongrabar'>";
    echo "</td>";

    // incrementamos el contador de columnas
    $columna++;

    // si ya presentamos la segunda columna
    if ($columna > 2){

        // cerramos la fila y reiniciamos el contador
        echo "</tr>";
        $columna = 1;

    }

}

// agregamos el formulario para insertar una nueva localidad

// si estamos en la primer columna
if ($columna == 1){

    // abrimos la fila
    echo "<tr>";

}

// presentamos el código de localidad
echo "<td align='center'>";
echo "<span class='tooltip'
            title='Código de la Localidad'>";
echo "<input type='text'
       name='codloc'
       id='codloc_nueva'
       size='8'>";
echo "</span>";
echo "</td>";

// presentamos el nombre
echo "<td align='center'>";
echo "<span class='tooltip'
            title='Nombre de la Localidad'>";
echo "<input type='text'
       name='localidad'
       id='localidad_nueva'
       size='25'>";
echo "</span>";
echo "</td>";

// la población
echo "<td align='center'>";
echo "<span class='tooltip'
            title='Número de Habitantes'>";
echo "<input type='number'
       name='poblacion'
       id='poblacion_nueva'
       class='numerogrande'
       size='10'>";
echo "</span>";
echo "</td>";

// presenta el botón agregar
echo "<td align='center'>";
echo "<input type='button' name='btnNuevaLocalidad'
       id='btnNuevaLocalidad'
       onClick='localidades.verificaLocalidad()'
       title='Pulse para agregar una localidad'
       class='botonagregar'>";
echo "</td>";

// siempre cerramos la fila
echo "</tr>";

// cerramos la tabla
echo "</tbody>";
echo "</table>";

?>
<script>

    // instanciamos los tooltips
    new jBox('Tooltip', {
      attach: '.tooltip',
      theme: 'TooltipBorder'
    });

</script>
