<?php

    /*

    Archivo: generar_nota.php
    Proyecto: CCE
    Autor: Claudio Invernizzi
    E-Mail: cinvernizzi@gmail.com
    Institución:INP - Dr. Mario Fatala Chaben
    Fecha: 23-02-2017
    Version: 1.0
    Licencia: GPL
    Comentarios: Procedimiento que instancia la clase y luego genera el documento
                 pdf con las notas del responsable jurisdiccional al referente
                 nacional

    */

// incluimos e instanciamos la clase
require_once ("../clases/notas.class.php");
require_once ("../clases/laboratorios.class.php");
$notas = new Notas();
$laboratorios = new Laboratorios();

// obtenemos la jurisdiccion del usuario
session_start();
if (isset($_SESSION["Jurisdiccion"])){
    $jurisdiccion = $_SESSION["Jurisdiccion"];
}
session_write_close();

// obtenemos la nomina de laboratorios y la asignamos al objeto
$nominaLaboratorios = $laboratorios->laboratoriosParticipantes($jurisdiccion);
$notas->setNominaLaboratorios($nominaLaboratorios);

// generamos la nota y obtenemos el documento
$notas_enviadas = $notas->imprimirNota();

// grabamos el archivo a disco
$file = fopen("documento.pdf", "w");
fwrite($file, $notas_enviadas);
fclose($file);

// enviamos los headers
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);
header("Pragma: no-cache");

// ahora lo presentamos
?>
<object data="reportes/documento.pdf"
        type="application/pdf"
        width="850" height="500">
</object>
?>
