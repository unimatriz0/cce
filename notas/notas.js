/*
 * Nombre: notas.js
 * Autor: Lic. Claudio Invernizzi
 * E-Mail: cinvernizzi@gmail.com
 * Fecha: 25/12/2016
 * Licencia: GPL
 * Comentarios: Serie de funciones y procedimientos utilizados por el
 *              sistema de control de calidad en el envío de notas de
 *              los responsables jurisdiccionales al nivel central
 */

/**
 * Nombre: enviarNota
 * Parámetros: Ninguno
 * Retorna: Nada
 * Propósito: carga en el contenido la nómina de laboratorios que reciben
 *            muestras del responsable actual
 */
function enviarNota(){

    // reiniciamos el contador
    contador();

    // cargamos la grilla en el contendido
    $("#form_reportes").load("reportes/laboratorios_activos.php");

}

/**
 * Nombre: generarNota
 * Parámetros: Ninguno
 * Retorna: Nada
 * Propósito: evento llamado al pulsar el botón, llama la rutina php
 *            que genera el documento pdf y luego lo presenta en el
 *            layer emergente
 */
function generarNota(){

    // reiniciamos el contador
    contador();

    // definimos el cuadro de diálogo
    ver_notas = new jBox('Modal', {
                           animation: 'flip',
                           closeOnEsc: true,
                           closeOnClick: false,
                           closeOnMouseleave: false,
                           closeButton: true,
                           overlay: false,
                           onCloseComplete: function(){
                                this.destroy();
                           },
                           title: 'Notas Enviadas',
                           repositionOnContent: true,
                           draggable: 'title',
                           theme: 'TooltipBorder',
                           target: '#top',
                                position: {
                                    x: 'left',
                                    y: 'top'
                                },
                           ajax: {
                              url: 'reportes/generar_nota.php',
                           reload: 'strict'
                           }
                        });
    ver_notas.open();

}
