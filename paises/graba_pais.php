<?php

/**
 *
 * paises/graba_pais.class.php
 *
 * @package     CCE
 * @subpackage  Paises
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.4.0 (10/08/2018)
 * @copyright   Copyright (c) 2018, INP
 *
 * Método que ejecuta la consulta de grabación en el servidor,
 * retorna la clave del registro afectado
 *
*/

// incluimos e instanciamos la clase
require_once ("paises.class.php");
$pais = new Paises();

// si recibió la id
if (!empty($_POST["Id"])){
    $pais->setIdPais($_POST["Id"]);
}

// asigna el nombre
$pais->setNombrePais($_POST["Pais"]);

// grabamos
$id = $pais->grabaPais();

// retorna el resultado
echo json_encode(array("Id" => $id));

?>