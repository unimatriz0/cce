<?php

/**
 *
 * paises/form_paises.php
 *
 * @package     CCE
 * @subpackage  Paises
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.4.0 (10/08/2018)
 * @copyright   Copyright (c) 2018, INP
 *
 * Método que arma el formulario para el abm de
 * países
 *
*/

// incluimos e instanciamos la clase
require_once ("paises.class.php");
$paises = new Paises();

// obtenemos la nómina de países
$nomina = $paises->listaPaises();

// presentamos el título
echo "<h2>Nómina de Paises registrados</h2>";

// definimos la tabla con un formato a dos columnas
echo "<table width='95%' align='center' border='0'>";

// definimos los encabezados
echo "<thead>";
echo "<tr>";

// la primer columna
echo "<th align='left'>País</th>";
echo "<th>Usuario</th>";
echo "<th>Alta</th>";
echo "<th></th>";

// la segunda columna
echo "<th align='left'>País</th>";
echo "<th>Usuario</th>";
echo "<th>Alta</th>";
echo "<th></th>";

// cerramos el encabezado
echo "</tr>";
echo "</thead>";

// abrimos el cuerpo
echo "<tbody>";

// iniciamos el contador de columnas
$columna = 1;

// recorremos el vector
foreach ($nomina AS $registro){

    // obtenemos el registro
    extract($registro);

    // si estamos en la primer columna
    if ($columna == 1){

        // abrimos la fila
        echo "<tr>";

    }

    // presentamos el país
    echo "<td>";
    echo "<input type='text' name='nombrepais'
                 id='nombrepais_$idpais'
                 value='$pais'
                 size='20'>";
    echo "</td>";

    // presentamos el resto del registro
    echo "<td align='center'>$usuario</td>";
    echo "<td align='center'>$fecha_alta</td>";

    // presenta el botón grabar
    echo "<td align='center'>";
    echo "<span class='tooltip'
           title='Graba el país en la base'>";
    echo "<input type='button' name='btnGrabaPais'
           id='btnGrabaPais'
           onClick='paises.verificaPais($idpais)'
           class='botongrabar'>";
    echo "</span>";
    echo "</td>";

    // incrementamos el contador de columnas
    $columna++;

    // si ya presentamos la segunda columna
    if ($columna > 2){

        // cerramos la fila y reiniciamos el contador
        echo "</tr>";
        $columna = 1;

    }

}

// agregamos el nuevo registro

// si estamos en la primer columna
if ($columna == 1){

    // abrimos la fila
    echo "<tr>";

}

// presentamos el país
echo "<td>";
echo "<input type='text' name='nombrepais'
             id='nombrepais_nuevo'
             size='20'>";
echo "</td>";

// obtenemos la fecha actual
$fecha_alta = date('d/m/Y');

// presentamos el resto del registro
echo "<td>";
echo "<input type='text'
             name='usuario_nuevo'
             id='usuario_nuevo'
             size='10'
             readonly>";
echo "</td>";
echo "<td>";
echo "<input type='text'
             name='alta_nuevo'
             id='alta_nuevo'
             size='10'
             readonly>";
echo "</td>";

// presenta el botón grabar
echo "<td align='center'>";
echo "<input type='button' name='btnGrabaPais'
       id='btnGrabaPais'
       onClick='paises.verificaPais()'
       class='botonagregar'>";
echo "</td>";

// siempre cerramos
echo "</tr>";

// cerramos la tabla
echo "</tbody>";
echo "</table>";

?>
<SCRIPT>

    // instanciamos los tooltips
    new jBox('Tooltip', {
      attach: '.tooltip',
      theme: 'TooltipBorder'
    });

    // fijamos la fecha de alta y el usuario
    document.getElementById("alta_nuevo").value = fechaActual();
    document.getElementById("usuario_nuevo").value = sessionStorage.getItem("Usuario");

</SCRIPT>
