<?php

/**
 *
 * paises/lista_paises.php
 *
 * @package     CCE
 * @subpackage  Paises
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.1.0 (22/10/2017)
 * @copyright   Copyright (c) 2017, INP
 *
 * Método que retorna un array en formato json con la nómina completa
 * de países
 *
*/

// inclusión de archivos
require_once ("paises.class.php");
$paises = new Paises();

// obtenemos el listado
$nomina = $paises->listaPaises();

// declaramos el array
$jsondata = array();

// recorremos el vector
foreach($nomina AS $registro){

    // obtenemos el registro
    extract($registro);

    // lo agregamos al vector
    $jsondata[] = array("Id" => $idpais,
                        "Pais" => $pais);

}

// retornamos el vector
echo json_encode($jsondata);

?>