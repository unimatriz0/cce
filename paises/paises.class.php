<?php

/**
 *
 * Class Paises | paises/paises.class.php
 *
 * @package     CCE
 * @subpackage  Paises
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.4.0 (10/08/2018)
 * @copyright   Copyright (c) 2018, INP
 *
 * Cláse que contiene las operaciones sobre la base de datos
 * de países
 *
*/

// inclusión de archivos
require_once ("../clases/conexion.class.php");

// convención para la nomenclatura de las propiedades, comienzan con una
// letra mayúscula, de tener mas de una palabra no se utilizan separadores
// y la inicial de cada palabra va en mayúscula
// para las variables recibidas como parámetro el criterio es todas en
// minúscula

// convención para la nomenclatura de los metodos, comienzan con set o get
// según asignen un valor o lo lean y luego el nombre del valor a obtener

/**
 * Definición de la clase
 * @author Claudio Invernizzi <cinvernizzi@gmail.com>
 */
// definición de la clase
class Paises{

    // declaración de variables de la clase, las declaramos
    // como protected para que puedan ser heredadas pero
    // para asignarle el valor debemos crear los métodos
    protected $IdUsuario;             // clave del usuario
    protected $IdPais;                // clave del país
    protected $NombrePais;            // nombre del país
    protected $FechaAlta;             // fecha de alta del registro
    protected $Link;                  // puntero a la base de datos

    /**
     * Constructor de la clase, establece el enlace con la base
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     */
    function __construct (){

        // inicializamos las variables
        $this->IdPais = 0;
        $this->NombrePais = "";
        $this->FechaAlta = date('Y/m/d');

        // abrimos la sesión y obtenemos el usuario
        session_start();
        if (!empty($_SESSION["ID"])){
            $this->IdUsuario = $_SESSION["ID"];
        }
        session_write_close();

        // nos conectamos a la base de datos
        $this->Link = new Conexion();

    }

    /**
     * Destructor de la clase, cierra la conexión con la base
     */
    function __destruct(){

        // elimina el enlace a la base
        $this->Link = null;

    }

    // métodos de asignación
    public function setIdPais($idpais){

        // verifica que sea un número
        if (!is_numeric($idpais)){

            // abandona por error
            echo "La clave del país debe ser un número";
            exit;

        // si está correcto
        } else {

            // lo asigna
            $this->IdPais = $idpais;

        }

    }
    public function setNombrePais($nombrepais){
        $this->NombrePais = $nombrepais;

    }

    /**
     * Método que retorna un vector con la nómina de países de la base
     * de datos de diccionarios
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @return array
     */
    public function listaPaises(){

        // componemos la consulta
        $consulta = "SELECT diccionarios.v_paises.pais AS pais,
                            diccionarios.v_paises.id AS idpais,
                            diccionarios.v_paises.usuario AS usuario,
                            diccionarios.v_paises.fecha_alta AS fecha_alta
                     FROM diccionarios.v_paises
                     WHERE diccionarios.v_paises.pais != 'NO DECLARADA'
                     ORDER BY diccionarios.v_paises.pais;";

        // ejecutamos la consulta
        $resultado = $this->Link->query($consulta);

        // lo pasamos a minúsculas porque según la versión de
        // pdo lo devuelve en mayúsculas o minúsculas
        $nomina = array_change_key_case($resultado->fetchAll(PDO::FETCH_ASSOC), CASE_LOWER);

        // retornamos el vector
        return $nomina;

    }

    /**
     * Método que recibe como parámetro el nombre de un país y retorna
     * el número de registros encontrados
     * @param string $pais - nombre de un país
     * @return int
     */
    public function validaPais($pais){

        // inicializamos las variables
        $registros = 0;

        // componemos y ejecutamos la consulta
        $consulta = "SELECT COUNT(diccionarios.v_paises.id) AS registros
                     FROM diccionarios.v_paises
                     WHERE diccionarios.v_paises.pais = '$pais';";
        $resultado = $this->Link->query($consulta);

        // obtenemos el registro y retornamos
        $fila = array_change_key_case($resultado->fetch(PDO::FETCH_ASSOC), CASE_LOWER);
        extract($fila);
        return $registros;

    }

    /**
     * Método que según el caso realiza la consulta de inserción o edición
     * en la tabla de paises, retorna la id del registro o el resultado
     * de la operación
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @return int
     */
    public function grabaPais(){

        // si está insertando
        if ($this->IdPais == 0){
            $this->nuevoPais();
        } else {
            $this->editaPais();
        }

        // retornamos la id del registro
        return $this->IdPais;

    }

    /**
     * Método protegido que inserta un nuevo registro en la tabla de países
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     */
    protected function nuevoPais(){

        // compone la consulta de inserción
        $consulta = "INSERT INTO diccionarios.paises
                                 (NOMBRE,
                                  USUARIO,
                                  FECHA_ALTA)
                                 VALUES
                                 (:pais,
                                  :usuario,
                                  :fecha_alta);";

        // asignamos la consulta
        $psInsertar = $this->Link->prepare($consulta);

        // asignamos los valores
        $psInsertar->bindParam(":pais", $this->NombrePais);
        $psInsertar->bindParam(":usuario", $this->IdUsuario);
        $psInsertar->bindParam(":fecha_alta", $this->FechaAlta);

        // ejecutamos la consulta
        $resultado = $psInsertar->execute();

        // si no hubo error
        if ($resultado){

            // obtenemos la clave del registro
            $this->IdPais = $this->Link->lastInsertId();

        // si hubo un error
        } else {

            // inicializamos la variable y mostramos el error
            $this->IdPais = 0;
            echo $resultado;

        }

    }

    /**
     * Método protegido que edita el registro de la tabla de países
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     */
    protected function editaPais(){

        // compone la consulta de actualización
        $consulta = "UPDATE diccionarios.paises SET
                            NOMBRE = :pais,
                            USUARIO = :usuario,
                            FECHA_ALTA = :fecha_alta
                     WHERE diccionarios.paises.ID = :id;";

        // asignamos la consulta
        $psInsertar = $this->Link->prepare($consulta);

        // asignamos los valores
        $psInsertar->bindParam(":pais", $this->NombrePais);
        $psInsertar->bindParam(":usuario", $this->IdUsuario);
        $psInsertar->bindParam(":fecha_alta", $this->FechaAlta);
        $psInsertar->bindParam(":id", $this->IdPais);

        // ejecutamos la consulta
        $resultado = $psInsertar->execute();

        // si hubo un error
        if (!$resultado){

            // inicializamos la variable y presentamos el error
            $this->IdPais = 0;
            echo $resultado;

        }

    }

}
?>
