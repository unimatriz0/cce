<?php

/**
 *
 * paises/verifica_pais.php
 *
 * @package     CCE
 * @subpackage  Paises
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.4.0 (10/08/2018)
 * @copyright   Copyright (c) 2018, INP
 *
 * Método que recibe por get el nombre de un país y verifica que
 * no esté repetido, retorna el número de registros encontrados
 *
*/

// incluimos e instanciamos la clase
require_once ("paises.class.php");
$paises = new Paises();

// verificamos si existe
$registros = $paises->validaPais($_GET["pais"]);

// retornamos el estado de la operación
echo json_encode(array("Registros" => $registros));

?>