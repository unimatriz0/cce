/*
 * Nombre: paises.js
 * Autor: Lic. Claudio Invernizzi
 * E-Mail: cinvernizzi@gmail.com
 * Fecha: 13/01/2017
 * Licencia: GPL
 * Comentarios: Serie de funciones y procedimientos utilizados por el
 *              sistema de control de calidad en los formularios de
 *              abm de paises
 */

 /**
 *
 * Atención, utilizamos el nuevo estandard definido en ECMAScript 2015 (ES6)
 * este permite definir clases con el mètodo ya utilizado en php y java
 * como creo que es mas claro lo utilizaremos, la compatibilidad está
 * anunciada para firefox y chrome así que no deberìa haber problemas
 *
 */

/* jshint esversion: 6 */

/**
 * @author Claudio Invernizzi <cinvernizzi@gmail.com>
 * @class Clase que contiene los métodos para el abm de países
 */
class Paises {

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @constructor
     */
    constructor(){

        // inicializamos las variables
        this.initPaises();

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que inicializa las variables de clase
     */
    initPaises(){

        // inicializamos las variables
        this.Pais = "";             // nombre del país
        this.IdPais = 0;            // clave del registro
        this.Correcto = true;       // switch de país repetido

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que carga en el contenido el formulario de países
     */
    formPaises(){

        // reiniciamos el contador
        cce.contador();

        // carga el formulario
        $("#form_administracion").load("paises/form_paises.php");

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método llamado por callback que verifica el país no
     * se encuentre repetido
     */
    validaPais(paisCorrecto){

        // inicializamos las variables
        this.Correcto = false;

        // llama sincrónicamente
        $.ajax({
            url: 'paises/verifica_pais.php?pais='+pais,
            type: "GET",
            cahe: false,
            contentType: false,
            processData: false,
            dataType: 'json',
            async: false,
            success: function(data) {

                // si retornó verdadero
                if (data.Registros == 0){
                    paises.Correcto = true;
                } else {
                    paises.Correcto = false;
                }

            }});

        // retornamos
        paisCorrecto(this.Correcto);

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param {int} id - clave del registro
     * Método llamado antes de grabar el registro que verifica
     * el formulario
     */
    verificaPais(id){

        // reiniciamos el contador
        cce.contador();

        // si está dando un alta
        if (typeof(id) == "undefined"){
            id = "nuevo";
        } else {
            this.IdPais = id;
        }

        // si no lo ingresó
        if (document.getElementById("nombrepais_" + id).value == ""){

            // presenta el mensaje y retorna
            mensaje = "Debe ingresar el nombre del País";
            new jBox('Notice', {content: mensaje, color: 'red'});
            document.getElementById("nombrepais_" + id).focus();
            return false;

        // si ingresó
        } else {

            // asigna en la variable de clase
            this.Pais = document.getElementById("nombrepais_" + id).value;

        }

        // si está dando un alta
        if (this.IdPais != 0){

            // verificamos por callback
            this.validaPais(function(paisCorrecto){

                // si está repetido
                if (!paises.Correcto){

                    // presenta el mensaje
                    mensaje = "Ese país ya se encuentra declarada !!!";
                    new jBox('Notice', {content: mensaje, color: 'red'});

                }

            });

        }

        // grabamos el registro
        this.grabaPais();

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que envía los datos del formulario al servidor
     */
    grabaPais(){

        // si el registro está repetido
        if (!this.Correcto){
            return false;
        }

        // obtenemos los valores actuales de los select en
        // responsables y laboratorios
        var paisresponsable = document.getElementById("pais").value;
        var paislaboratorio = document.getElementById("paislaboratorio").value;

        // implementa el formulario usando objetos html5
        var formulario = new FormData();

        // si está editando
        if (this.IdPais != 0){
            formulario.append("Id", this.IdPais);
        }

        // agrega los valores
        formulario.append("Pais", this.Pais);

        // serializa el formulario y lo envía, recibe
        // como respuesta el resultado de la operación
        $.ajax({
            url: "paises/graba_pais.php",
            type: "POST",
            data: formulario,
            cahe: false,
            contentType: false,
            processData: false,
            success: function(data) {

                // si salío todo bien
                if (data.Id != 0){

                    // presenta el mensaje
                    new jBox('Notice', {content: "Registro grabado ... ", color: 'green'});

                    // actualizamos el select de responsables y laboratorios revisando el
                    // valor actual del select
                    paises.listaPaises("pais", paisresponsable);
                    paises.listaPaises("paislaboratorio", paislaboratorio);

                    // reiniciamos las variables de clase
                    paises.initPaises();

                    // recarga el formulario para reflejar los cambios
                    paises.formPaises();

                // si hubo algún error
                } else {

                    // presenta la alerta
                    new jBox('Notice', {content: "Ha ocurrido un error, intente nuevamente", color: 'red'});

                }

            },

        });

    }

    /**
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param {string} idelemento id del select de un formulario
     * @param {int} idpais clave del país preseleccionado (optativo)
     * Método que recibe como parámetro la id del elemento de un
     * formulario y (optativo) la clave de un país, carga en el
     * select la nómina de países y si recibió la clave de uno
     * de ellos lo preselecciona
     */
    listaPaises(idelemento, idpais){

        // limpia el combo
        $("#" + idelemento).html('');

        // lo llamamos asincrónico
        $.ajax({
            url: "paises/lista_paises.php",
            type: "GET",
            cahe: false,
            contentType: false,
            processData: false,
            dataType: 'json',
            success: function(data) {

                // agrega el primer elemento en blanco
                $("#" + idelemento).append("<option value=0>Seleccione</option>");

                // recorre el vector de resultados
                for(var i=0; i < data.length; i++){

                    // si existe la clave y es igual
                    if (typeof(idpais) != "undefined"){

                        // si es la misma
                        if (idpais == data[i].Id){

                            // lo agrega seleccionado
                            $("#" + idelemento).append("<option selected value=" + data[i].Id + ">" + data[i].Pais + "</option>");

                        // si son distintos
                        } else {

                            // simplemente lo agrega
                            $("#" + idelemento).append("<option value=" + data[i].Id + ">" + data[i].Pais + "</option>");

                        }

                    // si no recibió la clave del país recorre el
                    // bucle agregando
                    } else {
                        $("#" + idelemento).append("<option value=" + data[i].Id + ">" + data[i].Pais + "</option>");
                    }

                }

        }});

    }

}
