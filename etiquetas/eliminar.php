<?php

/**
 *
 * etiquetas/lista_etiquetas.php
 *
 * @package     CCE
 * @subpackage  Etiquetas
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.4.0 (31/08/2018)
 * @copyright   Copyright (c) 2018, INP
 *
 * Método que recibe como parámetro los datos de una ediqueta y
 * ejecuta la consulta de eliminación
 *
*/

// incluimos e instanciamos las clases
require_once ("etiquetas.class.php");
$etiqueta = new Etiquetas();

// ejecutamos la consulta
$etiqueta->borrarEtiqueta($_POST["IdLaboratorio"], $_POST["Etiqueta"], $_POST["IdOperativo"]);

?>