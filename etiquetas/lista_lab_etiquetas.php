<?php

/**
 *
 * etiquetas/lista_lab_etiquetas.php
 *
 * @package     CCE
 * @subpackage  Etiquetas
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.4.0 (31/08/2018)
 * @copyright   Copyright (c) 2018, INP
 *
 * Método que recibe por get parte del nombre de un laboratorio y la
 * clave de un operativo, retorna un array json con los laboratorios
 * que han recibido etiquetas en ese período
 *
*/

// incluimos e instanciamos la clase
require_once ("etiquetas.class.php");
$etiquetas = new Etiquetas();

// obtenemos la nómina de laboratorios que han recibido etiquetas
$laboratorios = $etiquetas->buscaLabOperativo($_GET["laboratorio"], $_GET["operativo"]);

// si no hubo registros
if (count($laboratorios) == 0){

    // presenta el mensaje
    echo "<h3>No se han encontrado laboratorios que contengan el
              termino indicado que hallan recibido etiquetas</h3>";

// si recibió
} else {

    // definimos la tabla
    echo "<table width='90%' align='center' border='0'>";

    // definimos los encabezados
    echo "<thead>";
    echo "<th align='left'>Jurisdicción</th>";
    echo "<th align='left'>Laboratorio</th>";
    echo "<th></th>";
    echo "</thead>";

    // definimos el cuerpo
    echo "<tbody>";

    // recorremos el vector
    foreach($laboratorios AS $registro){

        // obtenemos el registro
        extract($registro);

        // agregamos la fila
        echo "<tr>";
        echo "<td>$provincia</td>";
        echo "<td>$laboratorio</td>";

        // agregamos el enlace
        echo "<td align='center'>";
        echo "<input type='button'
                     name='btnSelLabEtiqueta'
                     id='btnSelLabEtiqueta'
                     title='Pulse para seleccionar el laboratorio'
                     class='botoneditar'
                     onClick='etiquetas.seleccionaLabEtiquetas(" . chr(34) . $idlaboratorio . chr(34) . "," . chr(34) . $laboratorio . chr(34) . ")'";
        echo "</td>";

        // cerramos la fila
        echo "</tr>";

    }

    // cerramos la tabla
    echo "</body></table>";

}

?>