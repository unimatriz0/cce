<?php

/**
 *
 * Class Etiquetas etiquetas/etiquetas.class.php
 *
 * @package     CCE
 * @subpackage  Etiquetas
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.4.0 (31/08/2018)
 * @copyright   Copyright (c) 2018, INP
 *
 * Clase que controla las operaciones sobre la tabla de etiquetas
 *
*/

// inclusión de archivos
require_once ("../clases/conexion.class.php");

// convención para la nomenclatura de las propiedades, comienzan con una
// letra mayúscula, de tener mas de una palabra no se utilizan separadores
// y la inicial de cada palabra va en mayúscula
// para las variables recibidas como parámetro el criterio es todas en
// minúscula

// convención para la nomenclatura de los metodos, comienzan con set o get
// según asignen un valor o lo lean y luego el nombre del valor a obtener

/**
 * Definición de la clase
 * @author Claudio Invernizzi <cinvernizzi@gmail.com>
 */
class Etiquetas {

    // declaración de variables de la clase, las declaramos
    // como protected para que puedan ser heredadas pero
    // para asignarle el valor debemos crear los métodos
    protected $Link;                  // puntero a la base de datos

    /**
     * Constructor de la clase, establece la conexión con la base
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     */
    function __construct (){

        // nos conectamos a la base de datos
        $this->Link = new Conexion();

    }

    /**
     * Destructor de la clase, cierra la conexion con la base
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     */
    function __destruct(){

        // elimina el enlace a la base
        $this->Link = null;

    }

    // intentamos que en las consultas buscando por laboratorio o
    // por etiqueta de tener una matriz de datos parecida

    /**
     * Método que recibe como parámetro una cadena con parte del nombre
     * de un laboratorio y retorna las etiquetas que ha recibido ese
     * laboratorio
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param string $laboratorio - nombre del laboratorio
     * @return array
     */
    public function buscaLaboratorio($laboratorio){

        // componemos la consulta
        $consulta = "SELECT cce.laboratorios.NOMBRE AS laboratorio,
                            cce.etiquetas.ETIQUETA_NRO AS etiqueta_nro,
                            diccionarios.localidades.NOMLOC AS localidad,
                            diccionarios.provincias.NOM_PROV AS provincia,
                            cce.etiquetas.OPERATIVO AS operativo,
                            DATE_FORMAT(cce.etiquetas.FECHA_ALTA, '%d/%m/%Y') AS fecha_alta
                     FROM cce.laboratorios INNER JOIN cce.etiquetas ON cce.laboratorios.ID = cce.etiquetas.ID_LABORATORIO
                                           INNER JOIN diccionarios.localidades ON cce.laboratorios.LOCALIDAD = diccionarios.localidades.CODLOC
                                           INNER JOIN diccionarios.provincias ON diccionarios.localidades.CODPCIA = provincias.COD_PROV
                     WHERE cce.laboratorios.NOMBRE LIKE '%$laboratorio%'
                     ORDER BY cce.etiquetas.FECHA_ALTA DESC;";

        // ejecutamos la consulta
        $resultado = $this->Link->query($consulta);

        // lo pasamos a minúsculas porque según la versión de
        // pdo lo devuelve en mayúsculas o minúsculas
        $nomina = array_change_key_case($resultado->fetchAll(PDO::FETCH_ASSOC), CASE_LOWER);

        // retornamos el vector
        return $nomina;

    }

    /**
     * Método que busca en la base de datos los laboratorios que han
     * recibido un determinado número de etiqueta
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param string $nroetiqueta - el un número de etiqueta
     * @return array
     */
    public function buscaEtiqueta($nroetiqueta){

        // componemos la consulta
        $consulta = "SELECT cce.laboratorios.NOMBRE AS laboratorio,
                            cce.etiquetas.ETIQUETA_NRO AS etiqueta_nro,
                            diccionarios.localidades.NOMLOC AS localidad,
                            diccionarios.provincias.NOM_PROV AS provincia,
                            cce.etiquetas.OPERATIVO AS operativo,
                            DATE_FORMAT(cce.etiquetas.FECHA_ALTA, '%d/%m/%Y') AS fecha_alta
                     FROM cce.laboratorios INNER JOIN cce.etiquetas ON cce.laboratorios.ID = cce.etiquetas.ID_LABORATORIO
                                           INNER JOIN diccionarios.localidades ON cce.laboratorios.LOCALIDAD = diccionarios.localidades.CODLOC
                                           INNER JOIN diccionarios.provincias ON diccionarios.localidades.CODPCIA = diccionarios.provincias.COD_PROV
                     WHERE etiquetas.ETIQUETA_NRO = '$nroetiqueta'
                     ORDER BY laboratorios.NOMBRE;";

        // ejecutamos la consulta
        $resultado = $this->Link->query($consulta);

        // lo pasamos a minúsculas porque según la versión de
        // pdo lo devuelve en mayúsculas o minúsculas
        $listaEtiquetas = array_change_key_case($resultado->fetchAll(PDO::FETCH_ASSOC), CASE_LOWER);

        // retornamos el vector
        return $listaEtiquetas;

    }

    /**
     * Método que recibe como parámetro la clave del laboratorio, el
     * número de etiqueta y la clave del operativo ejecuta la consulta de eliminación
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param int $idlaboratorio - clave del registro
     * @param string $etiqueta - número de etiqueta
     * @param int $idoperativo - clave del operativo
     */
    public function borrarEtiqueta($idlaboratorio, $etiqueta, $idoperativo){

        // componemos la consulta de eliminación
        $consulta = "DELETE FROM cce.etiquetas
                     WHERE id_laboratorio = '$idlaboratorio' AND
                           etiqueta_nro = '$etiqueta' AND
                           id_operativo = '$idoperativo';";

        // ejecutamos la consulta
        $this->Link->exec($consulta);

        // retornamos
        return;

    }

    /**
     * Método que recibe como parámetros la clave de un operativo y
     * la clave de un laboratorio, retorna el array de etiquetas
     * enviadas a ese laboratorio (usado en la eliminación de
     * etiquetas)
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param int $operativo - clave del operativo
     * @param int $laboratorio - clave del laboratorio
     * @return array
     */
    public function nominaEtiquetas($operativo, $laboratorio){

        // componemos la consulta
        $consulta = "SELECT cce.etiquetas.etiqueta_nro AS etiqueta
                     FROM cce.etiquetas
                     WHERE cce.etiquetas.id_laboratorio = '$laboratorio' AND
                           cce.etiquetas.id_operativo = '$operativo'
                     ORDER BY cce.etiquetas.etiqueta_nro;";

        // ejecutamos la consulta
        $resultado = $this->Link->query($consulta);

        // lo pasamos a minúsculas porque según la versión de
        // pdo lo devuelve en mayúsculas o minúsculas
        $listaEtiquetas = array_change_key_case($resultado->fetchAll(PDO::FETCH_ASSOC), CASE_LOWER);

        // retornamos el vector
        return $listaEtiquetas;

    }

    /**
     * Método que recibe como parámetro la clave e un operativo y parte
     * del nombre del laboratorio, retorna el array con los laboratorios
     * que contienen ese nombre y que han recibido etiquetas para el
     * operativo indicado
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param string $laboratorio - clave del laboratorio
     * @param int $operativo - clave del operativo
     * @return array
     */
    public function buscaLabOperativo($laboratorio, $operativo){

        // componemos la consulta
        $consulta = "SELECT cce.laboratorios.id AS idlaboratorio,
                            cce.laboratorios.nombre AS laboratorio,
                            diccionarios.provincias.NOM_PROV AS provincia
                     FROM cce.laboratorios INNER JOIN etiquetas ON cce.laboratorios.id = cce.etiquetas.id_laboratorio
                                           INNER JOIN diccionarios.localidades ON cce.laboratorios.localidad = diccionarios.localidades.codloc
                                           INNER JOIN diccionarios.provincias ON diccionarios.localidades.codpcia = diccionarios.provincias.cod_prov
                     WHERE cce.laboratorios.nombre LIKE '%$laboratorio%' AND
                           cce.etiquetas.id_operativo = '$operativo'
                     ORDER BY cce.laboratorios.nombre;";

        // ejecutamos la consulta
        $resultado = $this->Link->query($consulta);

        // lo pasamos a minúsculas porque según la versión de
        // pdo lo devuelve en mayúsculas o minúsculas
        $listaLaboratorios = array_change_key_case($resultado->fetchAll(PDO::FETCH_ASSOC), CASE_LOWER);

        // retornamos el vector
        return $listaLaboratorios;

    }

}