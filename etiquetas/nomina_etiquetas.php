<?php

/**
 *
 * etiquetas/nomina_etiquetas.php
 *
 * @package     CCE
 * @subpackage  Etiquetas
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.4.0 (31/08/2018)
 * @copyright   Copyright (c) 2018, INP
 *
 * Método que recibe por get la clave del operativo y la clave del
 * laboratorio y retorna el array con las etiquetas encontradas
*/

// incluimos e instanciamos la clase
require_once ("etiquetas.class.php");
$etiquetas = new Etiquetas();

// inicializamos la matriz
$jsondata = array();

// obtenemos la lista de etiquetas
$nomina = $etiquetas->nominaEtiquetas($_GET["Operativo"], $_GET["Laboratorio"]);

// recorremos el vector
foreach ($nomina AS $registro){

    // obtenemos el registro
    extract($registro);

    // lo agregamos
    $jsondata[] = array("EtiquetaNro" => $etiqueta);

}

// retornamos en formato json
echo json_encode($jsondata);

?>