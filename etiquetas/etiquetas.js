/*
 * Nombre: etiquetas.js
 * Autor: Lic. Claudio Invernizzi
 * E-Mail: cinvernizzi@gmail.com
 * Fecha: 08/11/2017
 * Licencia: GPL
 * Comentarios: Serie de funciones y procedimientos utilizados por el
 *              sistema de control de calidad en los formularios de
 *              etiquetas
 */

 /**
 *
 * Atención, utilizamos el nuevo estandard definido en ECMAScript 2015 (ES6)
 * este permite definir clases con el mètodo ya utilizado en php y java
 * como creo que es mas claro lo utilizaremos, la compatibilidad está
 * anunciada para firefox y chrome así que no deberìa haber problemas
 *
 */

/* jshint esversion: 6 */

/**
 * @author Claudio Invernizzi <cinvernizzi@gmail.com>
 * @class Clase que controla las operaciones del formulario de marcas
 * Definición de la clase
 */
class Etiquetas {

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @constructor
     */
    constructor(){

        // inicializamos el layer
        this.formLabEtiquetas = "";

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que carga en el contenedor el formulario de eliminación
     * de etiquetas
     */
    borraEtiquetas(){

        // reiniciamos el contador
        cce.contador();

        // cargamos el formulario
        $("#form_administracion").load("etiquetas/lista_etiquetas.html");

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método llamado al pulsar sobre aceptar en la grilla de laboratorios
     * o en el evento onchange del select de operativos
     * carga en el formulario principal la id del laboratorio seleccionado
     */
    seleccionaEtiquetas(){

        // reiniciamos el contador
        cce.contador();

        // si no hay un valor en el combo de operativos
        if (document.getElementById("anios_etiquetas").value == ""){
            return;
        }

        // si no hay cargada una clave de laboratorio
        if (document.getElementById("id_laboratorio_etiqueta").value == ""){
            return;
        }

        // obtenemos los valores
        var IdOperativo = document.getElementById("anios_etiquetas").value;
        var IdLaboratorio = document.getElementById("id_laboratorio_etiqueta").value;

        // limpia el combo
        $("#nomina_etiquetas").html('');

        // llama la rutina php en forma síncronica
        $.ajax({
            url: 'etiquetas/nomina_etiquetas.php?Operativo='+IdOperativo+'&Laboratorio='+IdLaboratorio,
            type: "GET",
            cahe: false,
            contentType: false,
            processData: false,
            dataType: 'json',
            success: function(data) {

                // agrega el primer elemento en blanco
                $("#nomina_etiquetas").append("<option></option>");

                // recorre el vector de resultados
                for(var i=0; i < data.length; i++){

                    // lo agrega seleccionado
                    $("#nomina_etiquetas").append("<option>" + data[i].EtiquetaNro + "</option>");

                }

            }});

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método llamado luego de verificar el formulario de eliminación de
     * etiquetas, lee los datos del formulario y los envía por post
     * al servidor
     */
    eliminaEtiqueta(){

        // reiniciamos el contador
        cce.contador();

        // declaración de variables
        var mensaje;

        // obtenemos los valores
        var idLaboratorio = document.getElementById("id_laboratorio_etiqueta").value;
        var etiqueta = document.getElementById("nomina_etiquetas").value;
        var idOperativo = document.getElementById("anios_etiquetas").value;

        // definimos el formulario y agregamos los elementos
        var formEtiquetas = new FormData();
        formEtiquetas.append("IdLaboratorio", idLaboratorio);
        formEtiquetas.append("Etiqueta", etiqueta);
        formEtiquetas.append("IdOperativo", idOperativo);

        // envía el formulario por ajax en forma asincrónica
        $.ajax({
            type: "POST",
            url: "etiquetas/eliminar.php",
            data: formEtiquetas,
            cahe: false,
            contentType: false,
            processData: false,
            dataType: 'json',
        });

        // presenta el mensaje
        mensaje = "Etiqueta eliminada";
        new jBox('Notice', {content: mensaje, color: 'green'});

        // limpiamos el formulario
        this.limpiaEtiquetas();

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método llamado al pulsar sobre el botón borrar etiqueta, obtiene la id
     * de la etiqueta a borrar y verifica el formulario
     */
    verificaEliminaEtiqueta(){

        // reiniciamos el contador
        cce.contador();

        // declaración de variables
        var mensaje;

        // si no seleccionó el año
        if (document.getElementById("anios_etiquetas").value == ""){

            // presenta el mensaje y retorna
            mensaje = "Debe seleccionar el Año del Operativo";
            new jBox('Notice', {content: mensaje, color: 'red'});
            document.getElementById("anios_etiquetas").focus();
            return false;

        }

        // si no ingresó el texto del laboratorio
        if (document.getElementById("laboratorio_etiqueta").value == ""){

            // presenta el mensaje y retorna
            mensaje = "Debe ingresar el Laboratorio cuya etiqueta<br>";
            mensaje += "desea eliminar";
            new jBox('Notice', {content: mensaje, color: 'red'});
            document.getElementById("laboratorio_etiqueta").focus();
            return false;

        }

        // si no seleccionó correctamente la clave del laboratorio
        if (document.getElementById("id_laboratorio_etiqueta").value == ""){

            // presenta el mensaje y retorna
            mensaje = "Ingrese parte del nombre del laboratorio y luego<br>";
            mensaje += "pulse el botón <b>Buscar<b>";
            new jBox('Notice', {content: mensaje, color: 'red'});
            document.getElementById("id_laboratorio_etiqueta").focus();
            return false;

        }

        // si no seleccionó la etiqueta
        if (document.getElementById("nomina_etiquetas").value == ""){

            // presenta el mensaje y retorna
            mensaje = "Seleccione de la lista la etiqueta a eliminar";
            new jBox('Notice', {content: mensaje, color: 'red'});
            document.getElementById("nomina_etiquetas").focus();
            return false;

        }

        // llamamos la rutina de eliminación
        this.eliminaEtiqueta();

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método llamado al pulsar el botón buscar laboratorio, abre el cuadro
     * de diálogo mostrando los laboratorios coincidentes
     */
    buscaLaboratorioEtiqueta(){

        // reiniciamos el contador
        cce.contador();

        // declaración de variables
        var mensaje;
        var laboratorio = document.getElementById("laboratorio_etiqueta").value;
        var operativo = document.getElementById("anios_etiquetas").value;

        // verificamos que halla ingresado el texto
        if (laboratorio == ""){

            // presenta el mensae y retorna
            mensaje = "Debe ingresar parte del nombre del laboratorio";
            new jBox('Notice', {content: mensaje, color: 'red'});
            document.getElementById("laboratorio_etiqueta").focus();
            return false;

        }

        // si no seleccionó operativo
        if (operativo == ""){

            // presenta el mensae y retorna
            mensaje = "Debe ingresar parte del nombre del laboratorio";
            new jBox('Notice', {content: mensaje, color: 'red'});
            document.getElementById("anios_etiquetas").focus();
            return false;

        }

        // abre el cuadro de diálogo pasándole el nombre del laboratorio
        this.formLabEtiquetas = new jBox('Modal', {
                        animation: 'flip',
                        closeOnEsc: false,
                        closeOnClick: false,
                        closeOnMouseleave: false,
                        closeButton: true,
                        overlay: false,
                        repositionOnContent: true,
                        theme: 'TooltipBorder',
                        onCloseComplete: function(){
                            this.destroy();
                        },
                        ajax: {
                            url: 'etiquetas/lista_lab_etiquetas.php?laboratorio='+laboratorio+'&operativo='+operativo,
                            reload: 'strict'
                        }
                        });

        // mostramos el formulario
        this.formLabEtiquetas.open();

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param {int} idlaboratorio - clave del laboratorio
     * @param {string} laboratorio - nombre del laboratorio
     * Método llamado al pulsar aceptar en la grilla de laboratorios
     * carga en los valores de clave y texto del laboratorio y
     * luego busca las etiquetas
     */
    seleccionaLabEtiquetas(idlaboratorio, laboratorio){

        // reiniciamos el contador
        cce.contador();

        // asigna en el formulario
        document.getElementById("id_laboratorio_etiqueta").value = idlaboratorio;
        document.getElementById("laboratorio_etiqueta").value = laboratorio;

        // destruimos el layer
        this.formLabEtiquetas.destroy();

        // llama la rutina que recarga el select
        this.seleccionaEtiquetas();

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método llamado luego de eliminar la etiqueta, simplemente limpia
     * el formulario de etiquetas
     */
    limpiaEtiquetas(){

        // reiniciamos el contador
        cce.contador();

        // reiniciamos el formulario
        document.getElementById("form_elimina_etiquetas").reset();

    }

}
