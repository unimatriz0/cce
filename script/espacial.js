/*
 * Nombre: espacial.js
 * Autor: Lic. Claudio Invernizzi
 * E-Mail: cinvernizzi@gmail.com
 * Fecha: 21/08/2018
 * Licencia: GPL
 * Comentarios: Clase con los métodos para la georreferenciación
 */

 /**
 *
 * Atención, utilizamos el nuevo estandard definido en ECMAScript 2015 (ES6)
 * este permite definir clases con el mètodo ya utilizado en php y java
 * como creo que es mas claro lo utilizaremos, la compatibilidad está
 * anunciada para firefox y chrome así que no deberìa haber problemas
 *
 */

/* jshint esversion: 6 */

/**
 * @author Claudio Invernizzi
 * @class Clase con los métodos para la georrerenciación del sistema
 */
class Espacial{

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @constructor
     */
    constructor(){

    }

    /**
     * Método que simplemente oculta el layer de los mapas
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     */
    cerrarMapa(){

        // reiniciamos el contador
        cce.contador();

        // ocultamos el mapa y el botón cerrar
        $("#map").hide();
        $("#map_cerrar").hide();

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param {map} map objeto mapa
     * @param {infowindow} infowindow objeto infowindow
     * @param {marker} marcador objeto marcador
     * @param {string} coordenadas
     * @param {string} texto el texto del marcador
     * Método que recibe como parámetros el objeto infowindow, el objeto
     * marcador, las coordenadas a presentar y el texto, añade el
     * marcador al mapa y el evento para mostrar el texto, el objeto
     * mapa lo suponemos público
     */
    agregaMarcador(map, infowindow, marcador, coordenadas, texto){

        // agrega el evento
        google.maps.event.addListener(marcador, 'click', function() {

            // posiciona las coordenadas del la ventana de información
            infowindow.setPosition(coordenadas);

            // setea el texto
            infowindow.setContent(texto);

            // abre la ventana
            infowindow.open(map, marcador);

        });

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param {string} leyenda
     * @param {double} latitud
     * @param {double} longitud
     * Método que recibe como parámetros las coordenadas a presentar
     * muestra los layers y llama la rutina google, recibe la cadena
     * con la leyenda a presentar en el marcador
     */
    initMap(leyenda, latitud, longitud) {

        // mostramos los layers
        $("#map_cerrar").show();
        $("#map").show();

        // llamamos la rutina de google
        var map = new google.maps.Map(document.getElementById('map'), {
                center: {lat: latitud,
                        lng: longitud},
                zoom: 16
            });

        // obtenemos el punto de las coordenadas
        var coordenadas = new google.maps.LatLng(latitud, longitud);

        // declara la ventana de información
        var infowindow = new google.maps.InfoWindow(
                    { content: "",
                    size: new google.maps.Size(50,50)
                    });

        // agrega el punto al mapa
        var marca = new google.maps.Marker({
                        position: coordenadas,
                        map: map
                    });

        // agregamos el marcador
        this.agregaMarcador(map, infowindow, marca, coordenadas, leyenda);

    }

}