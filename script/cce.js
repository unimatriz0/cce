/*
 * Nombre: cce.js
 * Autor: Lic. Claudio Invernizzi
 * E-Mail: cinvernizzi@gmail.com
 * Fecha: 13/12/2016
 * Licencia: GPL
 * Comentarios: Clase con métodos utilizados por varios componentes
 *              del sistema
 */

 /**
 *
 * Atención, utilizamos el nuevo estandard definido en ECMAScript 2015 (ES6)
 * este permite definir clases con el mètodo ya utilizado en php y java
 * como creo que es mas claro lo utilizaremos, la compatibilidad está
 * anunciada para firefox y chrome así que no deberìa haber problemas
 *
 */

/* jshint esversion: 6 */

/**
 * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @class Clase con métodos comunes a todo el sistema
 */
class CCE {

    /**
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * @constructor
     */
    constructor(){

        // iniciamos el contador
        this.contador();

    }

    /**
     * Método llamado por el contador que presenta la información de que
     * se ha cerrado sesión y debe acreditarse nuevamente
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     */
    Abandona(){

        // definición de variables
        var contenido = "Ha pasado mas de 15 minutos sin actividad, <br>";
        contenido += "se cerrará la sesión y deberá autenticarse<br>";
        contenido += "nuevamente.";

        // definimos el cuadro de diálogo de confirmación
        var Confirmacion = new jBox('Confirm', {
                            animation: 'flip',
                            title: 'Sesión Finalizada',
                            closeOnEsc: false,
                            closeOnClick: false,
                            closeOnMouseleave: false,
                            closeButton: false,
                            onCloseComplete: function(){
                                this.destroy();
                            },
                            overlay: false,
                            content: contenido,
                            theme: 'TooltipBorder',
                            confirm: function() {
                                seguridad.Salir();
                            },
                            closeOnConfirm: true,
                            confirmButton: 'Aceptar',
                            cancelButton:''
                        });

        // mostramos el diálogo
        Confirmacion.open();

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que reinicia el contador de la sesión
     */
    contador(){

        // si existe el contador
        if(typeof(this.Temporizador) != "undefined"){

            // primero lo reseteamos
            window.clearTimeout(this.Temporizador);

        }

        // ahora lo iniciamos
        this.Temporizador = window.setTimeout(this.Abandona, 900000);

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que pide confirmación y luego elimina los logs de
     * auditoría de mas de dos años de antiguedad
     */
    limpiaLogs(){

        // definimos el contenido
        var contenido = "Se aliminarán los registros de auditoría ";
        contenido += "que tengan mas de dos años de antiguedad y ";
        contenido += "se recrearán los índices de todas las tablas";

        // definimos el cuadro de diálogo de confirmación
        var Confirmacion = new jBox('Confirm', {
            animation: 'flip',
            title: 'Depurar Base de Datos',
            closeOnEsc: true,
            closeOnClick: false,
            closeOnMouseleave: false,
            closeButton: true,
            onCloseComplete: function(){
                this.destroy();
            },
            overlay: false,
            content: contenido,
            theme: 'TooltipBorder',
            confirm: function() {

                // llama la rutina php
                $.get('clases/limpialog.php',
                    function(data){

                        // si retornó correcto
                        if (data.Error != 0){

                            // presenta el mensaje
                            new jBox('Notice', {content: "Base depurada", color: 'green'});

                        // si no pudo eliminar
                        } else {

                            // presenta el mensaje
                            new jBox('Notice', {content: "Ha ocurrido un error", color: 'red'});

                        }

                    }, "json");

            },
            cancel: function(){
                Confirmacion.destroy();
            },
            confirmButton: 'Aceptar',
            cancelButton: 'Cancelar'

        });

        // mostramos el diálogo
        Confirmacion.open();

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param {string} pagina - la página de ayudas
     * Método llamado en las ayudas, recibe como parámetro la página a
     * mostrar y la carga en el div correspondiente
     */
    Ayuda(pagina){

        // reiniciamos el contador
        cce.contador();

        // cargamos la página de ayuda
        pagina = "ayudas/" + pagina + ".html";
        $('#form_ayudas').load(pagina);

        // retorna
        return false;

    }

    /**
     * Método que abre un layer emergente y muestra en el el
     * instructivo para la carga
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     */
    verInstructivo(){

        // reiniciamos el contador
        cce.contador();

        // generamos el certificado y lo mostramos en un layer
        // con las opciones target lo fijamos a un div de la página
        // y con las opciones left y top lo fijamos con respecto a
        // ese elemento
        var instructivo = new jBox('Modal', {
                            animation: 'flip',
                            closeOnEsc: true,
                            closeOnClick: false,
                            closeOnMouseleave: false,
                            closeButton: true,
                            overlay: false,
                            repositionOnContent: true,
                            onCloseComplete: function(){
                                this.destroy();
                            },
                            title: 'Instructivo',
                            draggable: 'title',
                            theme: 'TooltipBorder',
                            target: '#top',
                                position: {
                                    x: 'left',
                                    y: 'top'
                                },
                            ajax: {
                                url: 'ayudas/ver_instructivo.html',
                                reload: 'strict'
                            }
                        });
        instructivo.open();

    }

    /**
     * Método que abre un layer emergente y muestra en el
     * la planilla para la carga manual
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     */
    verPlanilla(){

        // reiniciamos el contador
        cce.contador();

        // generamos el certificado y lo mostramos en un layer
        // con las opciones target lo fijamos a un div de la página
        // y con las opciones left y top lo fijamos con respecto a
        // ese elemento
        var planilla = new jBox('Modal', {
                            animation: 'flip',
                            closeOnEsc: true,
                            closeOnClick: false,
                            closeOnMouseleave: false,
                            closeButton: true,
                            overlay: false,
                            repositionOnContent: true,
                            onCloseComplete: function(){
                                this.destroy();
                            },
                            theme: 'TooltipBorder',
                            title: 'Planilla de Carga Manual',
                            draggable: 'tittle',
                            target: '#top',
                                position: {
                                    x: 'left',
                                    y: 'top'
                                },
                            ajax: {
                                url: 'ayudas/ver_planilla.html',
                                reload: 'strict'
                            }
                        });
        planilla.open();

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que abre un layer emergente y muestra en el
     * el manual para la toma de muestras
     */
    tomaMuestras(){

        // reiniciamos el contador
        cce.contador();

        // leemos el archivo  lo mostramos en un layer
        // con las opciones target lo fijamos a un div de la página
        // y con las opciones left y top lo fijamos con respecto a
        // ese elemento
        var muestras = new jBox('Modal', {
                                animation: 'flip',
                                closeOnEsc: true,
                                closeOnClick: false,
                                closeOnMouseleave: false,
                                closeButton: true,
                                overlay: false,
                                onCloseComplete: function(){
                                        this.destroy();
                                },
                                title: 'Toma de Muestras',
                                draggable: 'title',
                                repositionOnContent: true,
                                theme: 'TooltipBorder',
                                target: '#top',
                                        position: {
                                            x: 'left',
                                            y: 'top'
                                        },
                                ajax: {
                                    url: 'ayudas/toma_muestras.html',
                                    reload: 'strict'
                                }
                            });
        muestras.open();

    }

    /**
     * Método que abre un layer emergente y muestra en el
     * el manual para la técnica HAI
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     */
    manualHAI(){

        // reiniciamos el contador
        cce.contador();

        // leemos el archivo y lo mostramos en un layer
        // con las opciones target lo fijamos a un div de la página
        // y con las opciones left y top lo fijamos con respecto a
        // ese elemento
        var manual = new jBox('Modal', {
                            animation: 'flip',
                            closeOnEsc: true,
                            closeOnClick: false,
                            closeOnMouseleave: false,
                            closeButton: true,
                            overlay: false,
                            onCloseComplete: function(){
                                    this.destroy();
                            },
                            title: 'Manual HAI',
                            draggable: 'title',
                            theme: 'TooltipBorder',
                            target: '#top',
                                    position: {
                                        x: 'left',
                                        y: 'top'
                                    },
                            ajax: {
                                url: 'ayudas/manual_hai.html',
                            reload: 'strict'
                            }
                            });
        manual.open();

    }

    /**
     * Método que abre un layer emergente y muestra en el
     * el manual para la técnica IFI
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     */
    manualIFI(){

        // reiniciamos el contador
        cce.contador();

        // leemos el archivo y lo mostramos en un layer
        // con las opciones target lo fijamos a un div de la página
        // y con las opciones left y top lo fijamos con respecto a
        // ese elemento
        var manual = new jBox('Modal', {
                                animation: 'flip',
                                closeOnEsc: true,
                                closeOnClick: false,
                                closeOnMouseleave: false,
                                closeButton: true,
                                overlay: false,
                                onCloseComplete: function(){
                                        this.destroy();
                                },
                                title: 'Manual IFI',
                                draggable: 'title',
                                theme: 'TooltipBorder',
                                target: '#top',
                                        position: {
                                            x: 'left',
                                            y: 'top'
                                        },
                                ajax: {
                                    url: 'ayudas/manual_ifi.html',
                                    reload: 'strict'
                                }
                            });
        manual.open();

    }

}