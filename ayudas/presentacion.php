<?php

// Nombre: presentacion.php
// Autor: Lic. Claudio Invernizzi
// E-Mail: cinvernizzi@gmail.com
// Fecha: 18/11/2015
// Licencia: GPL
// Comentarios: Procedimiento que arma la pantalla inicial del sistema de
//              ayudas de la plataforma

?>

<!-- presenta el texto -->
<h2>Sistema de Control de Calidad</h2>
<p align='justify'>
    En estas páginas usted encontrará una guía sobre los procedimientos
    que puede realizar en el <b>Sistema de Control de Calidad.</b></p>
<p align='justify'>
    Basta con seleccionar en el menú a la izquierda de la pantalla
    aquel apartado en el cual desee obtener mas información, o sobre
    el cual tenga alguna duda, la información correspondiente se
    presentará en esta área de la pantalla.</p>
<p align='justify'>
    Tenga presente que en cualquier momento puede volver a este menú
    o a cualquiera de las pantallas del sistema simplemente
    presionando la opción correspondiente del menú superior.
</p>
<p align='justify'>
    Podrá consultar además la medotología recomendada para cada una de las
    técnicas en los siguientes manuales elaborados por el Departamento de
    Diagnóstico del Instituto.
</p>
<ul>
    <li><a href='#'
           onClick='tomaMuestras()'
           title='Pulse para ver el Documento'>
           Manual de Toma de Muestras</a></li>
    <li><a href='#'
           onClick='manualHAI()'
           title='Pulse para ver el Documento'>
           Manual de Procedimientos HAI</a></li>
    <li><a href='#'
           onClick='manualIFI()'
           title='Pulse para ver el Documento'>
           Manual de Procedimientos IFI</a></li>
</ul>
<p align='justify'>
    También tenemos disponible una serie de videos sobre el procedimiento a
    emplear en cada técnica.
</p>
<p align='center'>
    <b>Toma de muestras</b><br>
    <iframe width="420" height="315"
     src="http://www.youtube.com/embed/oyQitd-_sII"
     frameborder="0"
     allowfullscreen>
    </iframe>
</p>
<p align='center'>
    <b>HAI</b><br>
    <iframe width="420" height="315"
     src="http://www.youtube.com/embed/FSB2E7kRE1s"
     frameborder="0"
     allowfullscreen>
    </iframe>
</p>
<p align='center'>
    <b>IFI</b><br>
    <iframe width="420" height="315"
     src="http://www.youtube.com/embed/GKKtiNE-lfI"
     frameborder="0"
     allowfullscreen>
    </iframe>
</p>
<p align='center'>
    <b>Micrométodo</b><br>
    <iframe width="420" height="315"
     src="http://www.youtube.com/embed/qiaYyQCmjMU"
     frameborder="0"
     allowfullscreen>
    </iframe>
</p>