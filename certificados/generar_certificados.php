<?php

/**
 *
 * certificados/generar.class.php
 *
 * @package     CCE
 * @subpackage  Certificados
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.4.0 (16/08/2018)
 * @copyright   Copyright (c) 2018, INP
 *
 * Método que recibe por get un año, obtiene la nómina de laboratorios
 * con determinaciones y luego genera los pdf con los certificados
 * de participación
 *
*/

// incluimos e instanciamos las clases
require_once ("certificados.class.php");
$certificados = new Certificados($_GET["anio"]);

// enviamos los headers
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);
header("Pragma: no-cache");

// ahora lo presentamos
?>
<object data="../temp/Certificado.pdf"
        type="application/pdf"
        width="850" height="500">
</object>
