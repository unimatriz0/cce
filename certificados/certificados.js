/*
 * Nombre: certificados.js
 * Autor: Lic. Claudio Invernizzi
 * E-Mail: cinvernizzi@gmail.com
 * Fecha: 17/08/2018
 * Licencia: GPL
 * Comentarios: Clase con los métodos de impresión de certificados
 */

 /**
 *
 * Atención, utilizamos el nuevo estandard definido en ECMAScript 2015 (ES6)
 * este permite definir clases con el mètodo ya utilizado en php y java
 * como creo que es mas claro lo utilizaremos, la compatibilidad está
 * anunciada para firefox y chrome así que no deberìa haber problemas
 *
 */

/* jshint esversion: 6 */

/**
 * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @class Clase con los métodos de impresión de certificados
 */
class Certificados {

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @constructor
     */
    constructor(){

        // inicializamos las variables de clase
        this.initCertificados();

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que inicializa las variables de clase
     */
    initCertificados(){

        // el layer emergente
        this.layerCertificados = "";

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Script que abre el layer y presenta la nómina a años con
     * operativos para generar los certificados de participación
     */
    pedirCertificado(){

        // reiniciamos el contador
        cce.contador();

        // abrimos el formulario
        this.layerCertificados = new jBox('Modal', {
                            animation: 'flip',
                            closeOnEsc: true,
                            closeOnClick: false,
                            closeOnMouseleave: false,
                            closeButton: true,
                            overlay: false,
                            title: 'Certificados',
                            repositionOnContent: true,
                            draggable: 'title',
                            theme: 'TooltipBorder',
                            width: 300,
                            ajax: {
                                url: 'certificados/form_certificados.html',
                            reload: 'strict'
                        }
                    });
        this.layerCertificados.open();

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Verifica se halla seleccionado el año del certificado a
     * imprimir y envía por ajax para generar los certificados
     */
    generaCertificados(){

        // declaración de variables
        var mensaje;
        var anio = document.getElementById("anio_certificado").value;

        // verifica se halla seleccionado un año
        if (anio == 0){

            // presenta el mensaje y retorna
            mensaje = "Seleccione el año de los certificados";
            new jBox('Notice', {content: mensaje, color: 'red'});
            document.getElementById("anio").focus();
            return false;

        }

        // cerramos el layer
        this.layerCertificados.destroy();

        // generamos el certificado y lo mostramos en un layer
        // con las opciones target lo fijamos a un div de la página
        // y con las opciones left y top lo fijamos con respecto a
        // ese elemento
        this.layerCertificados = new jBox('Modal', {
                                animation: 'flip',
                                closeOnEsc: true,
                                closeOnClick: false,
                                closeOnMouseleave: false,
                                closeButton: true,
                                overlay: false,
                                onCloseComplete: function(){
                                        this.destroy();
                                },
                                title: 'Certificados',
                                draggable: 'title',
                                repositionOnContent: true,
                                theme: 'TooltipBorder',
                                target: '#top',
                                        position: {
                                            x: 'left',
                                            y: 'top'
                                        },
                                ajax: {
                                    url: 'certificados/generar_certificados.php?anio='+anio,
                                    reload: 'strict'
                                }
                            });
        this.layerCertificados.open();

    }

}
