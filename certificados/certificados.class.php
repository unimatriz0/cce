<?php

/**
 *
 * Class Certificados | certificados/certificados.class.php
 *
 * @package     CCE
 * @subpackage  Certificados
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.4.0 (16/08/2018)
 * @copyright   Copyright (c) 2018, INP
 *
 * Clase que genera los certificados de participación de los
 * laboratorios
 *
*/

/*

    Atención, esta clase extiende la clase tfpdf que a su vez extiende
    la clase fpdf, la diferencia es que permite definir y generar documentos
    pdf con página de códigos UTF8, el funcionamiento es el mismo que en
    la clase original.

    Sin embargo, la clase para ahorrar espacio regenera las fuentes que
    son utilizadas en el documento, si migramos el sistema y cambia el
    path de la aplicación arroja un error señalando que no encuentra las
    fuentes.

    Para obligar a que regenere las fuentes basta con eliminar todos los
    archivos dat y aquellos php que tengan nombres de fuentes del directorio
    /font/unifont/ dejando solamente los archivos ttf y el archivo ttfonts.php
    que es el que se encarga de generar las fuentes

    Si se desean incluir otras fuentes en el documento, bastaría con
    copiar los archivos ttf en este directorio y luego al incluirlos en
    el documento el sistema se encarga automáticamente de generar los dat

*/

// el puntero de php queda parado en el directorio reportes luego
// de la llamada, entonces para incluir las fuentes

// define la ruta a las fuentes pdf
define('FPDF_FONTPATH', '../clases/fpdf/font');

// inclusión de archivos
require_once ("../clases/fpdf/tfpdf.php");
require_once ("../clases/conexion.class.php");

// convención para la nomenclatura de las propiedades, comienzan con una
// letra mayúscula, de tener mas de una palabra no se utilizan separadores
// y la inicial de cada palabra va en mayúscula
// para las variables recibidas como parámetro el criterio es todas en
// minúscula

// convención para la nomenclatura de los metodos, comienzan con set o get
// según asignen un valor o lo lean y luego el nombre del valor a obtener

/**
 * Clase que genera los documentos pdf con los certificados de participación
 * @author Claudio Invernizzi <cinvernizzi@gmail.com>
 */
class Certificados extends tFPDF{

    // determinamos el nivel del usuario
    protected $NivelCentral;                  // indica si el usuario es de nivel central
    protected $RespJurisdiccional;            // indica si el usuario es responsable
    protected $Anio;                          // año del certificado
    protected $Link;                          // puntero a la base de datos

    /**
     * Constructor de la clase, instancia el documento pdf y llama
     * los procedimientos para la generación del documento el
     * que genera en el directorio temporal
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param int $anio - año del reporte
     */
    function __construct ($anio){

        // inicializamos las variables
        $Archivo = "";

        // asignamos a la variable de clase
        $this->Anio = $anio;

        // instanciamos la conexión
        $this->Link = new Conexion();

        // iniciamos sesión
        session_start();

        // si existe la sesión
        if(isset($_SESSION["ID"])){

            // obtenemos los valores de la sesión
            $this->IdUsuario = $_SESSION["ID"];
            $this->NivelCentral = $_SESSION["NivelCentral"];
            $this->RespJurisdiccional = $_SESSION["Responsable"];
            $this->RespLaboratorio = $_SESSION["Laboratorio"];
            $this->Jurisdiccion = $_SESSION["Jurisdiccion"];

        }

        // cerramos sesión
        session_write_close();

        // llamamos al constructor de la clase padre
        parent::__construct();

        // establecemos las propiedades
        $this->SetAuthor("Claudio Invernizzi");
        $this->SetCreator("Sistema CCE");
        $this->SetSubject("Certificados de Participación", true);
        $this->SetTitle("Certificado de Participación", true);
        $this->SetAutoPageBreak(true, 10);

        // setea los márgenes
        $this->SetMargins(0.1,0.3,0.3);

        // agrega una fuente unicode
        $this->AddFont('DejaVu','','DejaVuSansCondensed.ttf',true);
        $this->AddFont('DejaVu','B','DejaVuSans-Bold.ttf', true);

        // obtenemos el array de instituciones
        $nomina = $this->nominaCertificados($anio);

        // imprimimos los certificados
        $this->imprimirCertificados($nomina);

    }

    /**
     * Método protegido que recibe un vector y luego lo recorre generando los
     * certificados correspondientes
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param array nominaLaboratorios
     */
    protected function imprimirCertificados($nominaLaboratorios){

        // inicializamos las variables
        $nombre_laboratorio = "";
        $jurisdiccion_laboratorio = "";
        $muestras_laboratorio = "";

        // inicia un bucle recorriendo el vector
        foreach($nominaLaboratorios As $fila) {

            // obtiene el registro
            extract($fila);

            // imprime el certificado
            $this->ImprimirRegistro($nombre_laboratorio, $jurisdiccion_laboratorio, $muestras_laboratorio);

        }

        // grabamos el documento en un archivo
        $this->Output("../temp/Certificado.pdf","F");

    }

    /**
     * Método protegido que recibe como parámetros la provincia y el nombre
     * del laboratorio e imprime el certificado
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param string $laboratorio - nombre del laboratorio
     * @param string $jurisdiccion - nombre de la jurisdicción
     * @param int $determinaciones - determinaciones realizadas
     */
    protected function ImprimirRegistro($laboratorio, $jurisdiccion, $determinaciones){

        // agrega la página apaisada
        $this->AddPage('L','A4');

        // inserta el logo del fatala
        $this->Image("../imagenes/fondo_certificado.jpg",5,5);

        // posiciona la impresión
        $this->SetXY(40,50);

        // seteamos la fuente
        $this->SetFont("DejaVu", "B", 20);

        // presentamos el título
        $texto = "Instituto Nacional de Parasitología\n";
        $texto .= "Dr. Mario Fatala Chaben\n";
        $texto .= "Programa de Control de Calidad Externo";
        $this->MultiCell(210, 7, $texto, 0, "C");

        // inserta un separador
        $this->Ln(20);

        // posiciona la impresión
        $this->SetX(40);

        // setea la fuente
        $this->SetFont("DejaVu", "B", 14);

        // presenta el texto
        $texto = "Por intermedio del presente se certifica que: $laboratorio, ";
        $texto .= "de la Jurisdiccion de: $jurisdiccion, ";
        $texto .= "ha participado durante el ejercicio $this->Anio en el ";
        $texto .= "Programa de Control de Calidad Externo coordinado ";
        $texto .= "por este Instituto, habiendo procesado $determinaciones ";
        $texto .= "muestras.";

        // presenta el texto
        $this->MultiCell(210, 7, $texto, 0, "J");

        // setea la fuente
        $this->SetFont("DejaVu", "", 12);

        // presenta la firma de Andrés
        $this->Image("../imagenes/firma_andres.jpg",55,135,30,30);

        // posiciona el cursor
        $this->SetXY(25, 160);

        // inserta el cargo del director
        $texto = "Dr. Andres Ruiz\n";
        $texto .= "Director\n";
        $texto .= "INP - Dr. Mario Fatala Chaben";
        $this->MultiCell(90, 5, $texto, 0, "C");

        // presenta la firma de karen
        $this->Image("../imagenes/firma_karen.gif",130,135,20,40);

        // posiciona el cursor
        $this->SetXY(95, 160);

        // inserta el cargo del jefe de diagnóstico
        $texto = "Dr. Karenina Scollo\n";
        $texto .= "Jefa Departamento de Diagnóstico\n";
        $texto .= "INP - Dr. Mario Fatala Chaben";
        $this->MultiCell(90, 5, $texto, 0, "C");

        // presenta la firma de claudio
        $this->Image("../imagenes/firma_claudio.jpeg",210,135,40,40);

        // posiciona el cursor
        $this->SetXY(180, 160);

        // inserta el cargo de claudio
        $texto = "Lic. Claudio Invernizzi\n";
        $texto .= "Coordinador Plataforma CCE\n";
        $texto .= "INP - Dr. Mario Fatala Chaben";
        $this->MultiCell(100, 5, $texto, 0, "C");

    }

    /**
     * Método que recibe como parámetro un año y retorna un array con los
     * laboratorios que remitieron determinaciones ese año, verificando
     * si es un responsable nacional, uno jurisdiccional o de un laboratorio
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param int $anio - año a imprimir
     * @return array
     */
    protected function nominaCertificados($anio){

        // si es un responsable de nivel central
        if ($this->NivelCentral == "Si"){

            // ejecutamos la consulta
            $registros = $this->certificadosNacionales($anio);

        // si es responsable de una jurisdicción
        } elseif ($this->NivelCentral == "No" && $this->RespJurisdiccional == "Si") {

            // ejecutamos la consulta
            $registros = $this->certificadosProvinciales($anio);

        // de otra forma es responsable de un laboratorio
        } else {

            // ejecutamos la consulta
            $registros = $this->certificadosLaboratorio($anio);

        }

        // retornamos el vector
        return $registros;

    }

    /**
     * Método que retorna la nómina de laboratorios participantes cuando el
     * usuario es de nivel central
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param string $anio - el año a reportar
     * @return array
     */
    protected function certificadosNacionales($anio){

        // consulta todos los laboratorios del país
        $consulta = "SELECT cce.laboratorios.ID AS id_laboratorio,
                            cce.laboratorios.NOMBRE AS nombre_laboratorio,
                            diccionarios.provincias.NOM_PROV AS jurisdiccion_laboratorio,
                            COUNT(cce.chag_datos.ID) AS muestras_laboratorio
                     FROM cce.laboratorios INNER JOIN diccionarios.localidades ON cce.laboratorios.LOCALIDAD = diccionarios.localidades.CODLOC
                                           INNER JOIN diccionarios.provincias ON diccionarios.localidades.CODPCIA = diccionarios.provincias.COD_PROV
                                           INNER JOIN cce.chag_datos ON cce.laboratorios.ID = cce.chag_datos.LABORATORIO
                                           INNER JOIN cce.operativos_chagas ON cce.chag_datos.OPERATIVO = cce.operativos_chagas.ID
                     WHERE RIGHT(cce.operativos_chagas.OPERATIVO_NRO, 4) = '$anio'
                     GROUP BY cce.laboratorios.ID,
                              diccionarios.provincias.NOM_PROV
                     ORDER BY diccionarios.provincias.NOM_PROV,
                              cce.laboratorios.NOMBRE;";

        // ejecutamos la consulta
        $resultado = $this->Link->query($consulta);

        // lo pasamos a minúsculas porque según la versión de
        // pdo lo devuelve en mayúsculas o minúsculas
        $registros = array_change_key_case($resultado->fetchAll(PDO::FETCH_ASSOC), CASE_LOWER);

        // retornamos el array
        return $registros;

    }

    /**
     * Método que retorna la nómina de laboratorios cuando el usuario es el
     * responsable de la jurisdicción
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param string $anio - el año a reportar
     * @return array
     */
    protected function certificadosProvinciales($anio){

        // obtiene los datos de la jurisdicción
        $consulta = "SELECT cce.laboratorios.NOMBRE AS nombre_laboratorio,
                            diccionarios.provincias.NOM_PROV AS jurisdiccion_laboratorio,
                            COUNT(cce.chag_datos.ID) AS muestras_laboratorio
                     FROM cce.laboratorios INNER JOIN diccionarios.localidades ON cce.laboratorios.LOCALIDAD = diccionarios.localidades.CODLOC
                                           INNER JOIN diccionarios.provincias ON diccionarios.localidades.CODPCIA = diccionarios.provincias.COD_PROV
                                           INNER JOIN cce.chag_datos ON cce.laboratorios.ID = cce.chag_datos.LABORATORIO
                                           INNER JOIN cce.operativos_chagas ON cce.chag_datos.OPERATIVO = cce.operativos_chagas.ID
                     WHERE RIGHT(cce.operativos_chagas.OPERATIVO_NRO, 4) = '$anio' AND
                           diccionarios.provincias.NOM_PROV = '$this->Jurisdiccion'
                     GROUP BY cce.laboratorios.NOMBRE
                     ORDER BY cce.laboratorios.NOMBRE;";

        // ejecutamos la consulta
        $resultado = $this->Link->query($consulta);

        // lo pasamos a minúsculas porque según la versión de
        // pdo lo devuelve en mayúsculas o minúsculas
        $registros = array_change_key_case($resultado->fetchAll(PDO::FETCH_ASSOC), CASE_LOWER);

        // retornamos el array
        return $registros;

    }

    /**
     * Método que retorna los datos del laboratorio para imprimir el certificado
     * cuando el usuario es el responsable de un laboratorio
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param string $anio - el año a reportar
     * @return array
     */
    protected function certificadosLaboratorio($anio){

        // obtiene los datos del laboratorio
        $consulta = "SELECT cce.laboratorios.NOMBRE AS nombre_laboratorio,
                            diccionarios.provincias.NOM_PROV AS jurisdiccion_laboratorio,
                            COUNT(cce.chag_datos.ID) AS muestras_laboratorio
                     FROM cce.laboratorios INNER JOIN diccionarios.localidades ON cce.laboratorios.LOCALIDAD = diccionarios.localidades.CODLOC
                                           INNER JOIN diccionarios.provincias ON diccionarios.localidades.CODPCIA = diccionarios.provincias.COD_PROV
                                           INNER JOIN cce.chag_datos ON cce.laboratorios.ID = cce.chag_datos.LABORATORIO
                                           INNER JOIN cce.operativos_chagas ON cce.chag_datos.OPERATIVO = cce.operativos_chagas.ID
                     WHERE RIGHT(cce.operativos_chagas.OPERATIVO_NRO, 4) = '$anio' AND
                           cce.laboratorios.ID = '$this->RespLaboratorio'
                     GROUP BY cce.laboratorios.NOMBRE
                     ORDER BY cce.laboratorios.NOMBRE;";

        // ejecutamos la consulta
        $resultado = $this->Link->query($consulta);

        // lo pasamos a minúsculas porque según la versión de
        // pdo lo devuelve en mayúsculas o minúsculas
        $registros = array_change_key_case($resultado->fetchAll(PDO::FETCH_ASSOC), CASE_LOWER);

        // retornamos el array
        return $registros;

    }

}
