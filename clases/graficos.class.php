<?php

/**
 *
 * Class Graficos | clases/graficos.class.php
 *
 * @package     CCE
 * @subpackage  Reportes
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.4.0 (21/11/2018)
 * @copyright   Copyright (c) 2018, INP
 *
 * Clase que ofrece los métodos para graficar distintos valores
 *
*/

// inclusion de archivos (como lo llamamos desde distintos
// lugares usamos el document root)
require_once ($_SERVER['DOCUMENT_ROOT'] .  "/clases/libchart/classes/libchart.php");

// convención para la nomenclatura de las propiedades, comienzan con una
// letra mayúscula, de tener mas de una palabra no se utilizan separadores
// y la inicial de cada palabra va en mayúscula
// para las variables recibidas como parámetro el criterio es todas en
// minúscula

// convención para la nomenclatura de los metodos, comienzan con set o get
// según asignen un valor o lo lean y luego el nombre del valor a obtener

// definición de la clase
class Graficos {

    // declaración de variables
    protected $Datos;             // matriz con los datos a graficar
    protected $Titulo;            // título del gráfico
    protected $Tamanio;           // array con el tamaño
    protected $Archivo;           // nombre del archivo

    // métodos de asignación de valores
    public function setDatos($datos){
        $this->Datos = $datos;
    }
    public function setTitulo($titulo){
        $this->Titulo = $titulo;
    }
    public function setTamanio($tamanio){
        $this->Tamanio = $tamanio;
    }
    public function setArchivo($archivo){
        $this->Archivo = $archivo;
    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @return imagen
     * Método que a partir de las variables de clase crea un
     * gráfico circular
     */
    public function Torta(){

        // definimos el gráfico
        $chart = new PieChart($this->Tamanio["Ancho"], $this->Tamanio["Alto"]);

        // definimos el dataset
        $dataSet = new XYDataSet();

        // ahora recorremos el array de datos
        for ($i=0; $i < count($this->Datos); $i++){

            // agregamos el valor (asume que la primera columna
            // es el título y la segunda el valor a graficar
            $dataSet->addPoint(new Point($this->Datos[$i][0], $this->Datos[$i][1]));

        }

        // agregamos el dataset a la gráfica
        $chart->setDataSet($dataSet);

        // si existe un título
        if (!empty($this->Titulo)){
            $chart->setTitle($this->Titulo);
        }

        // renderizamos la gráfica y retornamos
        $archivo = "../../temp/" . $this->Archivo;
        $chart->render($archivo);

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que a partir de las variables de clase crea un gráfico
     * de barras horizontales
     */
    public function barrasHorizontales(){

        // creamos el objeto
        $chart = new HorizontalBarChart($this->Tamanio["Ancho"], $this->Tamanio["Alto"]);

        // definimos el conjunto de datos
        $dataSet = new XYDataSet();

        // ahora recorremos el array de datos
        for ($i=0; $i < count($this->Datos); $i++){

            // agregamos el valor (asume que la primera columna
            // es el título y la segunda el valor a graficar
            $dataSet->addPoint(new Point($this->Datos[$i][0], $this->Datos[$i][1]));

        }

        // fijamos el dataset
        $chart->setDataSet($dataSet);

        // si definió un título
        if (!empty($this->Titulo)){
            $chart->setTitle($this->Titulo);
        }

        // renderizamos la gráfica y retornamos
        $archivo = "../../temp/" . $this->Archivo;
        $chart->render($archivo);

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que a partir de las variables de clase crea un gráfico
     * de barras verticales
     */
    public function barrasVerticales(){

        // creamos el objeto
        $chart = new VerticalBarChart($this->Tamanio["Ancho"], $this->Tamanio["Alto"]);

        // definimos el conjunto de datos
        $dataSet = new XYDataSet();

        // ahora recorremos el array de datos
        for ($i=0; $i < count($this->Datos); $i++){

            // agregamos el valor (asume que la primera columna
            // es el título y la segunda el valor a graficar
            $dataSet->addPoint(new Point($this->Datos[$i][0], $this->Datos[$i][1]));

        }

        // fijamos el dataset
        $chart->setDataSet($dataSet);

        // si definió un título
        if (!empty($this->Titulo)){
            $chart->setTitle($this->Titulo);
        }

        // renderizamos la gráfica y retornamos
        $archivo = "../../temp/" . $this->Archivo;
        $chart->render($archivo);

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que a partir de las variables de clase crea
     * un gráfico de líneas
     */
    public function Lineas(){

        // creamos el objeto
        $chart = new LineChart($this->Tamanio["Ancho"], $this->Tamanio["Alto"]);

        // definimos el conjunto de datos
        $dataSet = new XYDataSet();

        // ahora recorremos el array de datos
        for ($i=0; $i < count($this->Datos); $i++){

            // agregamos el valor (asume que la primera columna
            // es el título y la segunda el valor a graficar
            $dataSet->addPoint(new Point($this->Datos[$i][0], $this->Datos[$i][1]));

        }

        // fijamos el dataset
        $chart->setDataSet($dataSet);

        // si definió un título
        if (!empty($this->Titulo)){
            $chart->setTitle($this->Titulo);
        }

        // renderizamos la gráfica y retornamos
        $archivo = "../../temp/" . $this->Archivo;
        $chart->render($archivo);

    }

}
?>