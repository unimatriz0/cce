<?php

/**
 *
 * clases/limpialog.php
 *
 * @package     CCE
 * @subpackage  Administración
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.4.0 (06/09/2018)
 * @copyright   Copyright (c) 2018, INP
 *
 * Procedimiento que elimina los logs de auditoría con mas de dos años
 * de antiguedad y luego reconstruye los índices
 *
*/

// incluimos e instanciamos las clases
require_once("conexion.class.php");
$Link = new Conexion();

// de la tabla auditoria_chag_datos
$consulta = "DELETE FROM cce.auditoria_chag_datos
             WHERE DATEDIFF(NOW(), cce.auditoria_chag_datos.fecha_operacion) > 700;";
$Link->exec($consulta);

// de la tabla auditoria_etiquetas
$consulta = "DELETE FROM cce.auditoria_etiquetas
             WHERE DATEDIFF(NOW(), cce.auditoria_etiquetas.fecha_operacion) > 700;";
$Link->exec($consulta);

// de la tabla auditoria_laboratorios
$consulta = "DELETE FROM cce.auditoria_laboratorios
             WHERE DATEDIFF(NOW(), cce.auditoria_laboratorios.fecha_operacion) > 700;";
$Link->exec($consulta);

// de la tabla auditoria_operativos_chagas
$consulta = "DELETE FROM cce.auditoria_operativos_chagas
             WHERE DATEDIFF(NOW(), cce.auditoria_operativos_chagas.fecha_alta) > 700;";
$Link->exec($consulta);

// de la tabla auditoria_responsables
$consulta = "DELETE FROM cce.auditoria_responsables
             WHERE DATEDIFF(NOW(), cce.auditoria_responsables.fecha_operacion) > 700;";
$Link->exec($consulta);

// retornamos siempre verdadero
echo json_encode(array("Error" => 1));

?>