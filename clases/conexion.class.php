<?php

/**
 *
 * Class Conexion | clases/conexion.class.php
 *
 * @package     CCE
 * @subpackage  Clases
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.4.0 (16/08/2018)
 * @copyright   Copyright (c) 2018, INP
 *
 * clase inspirada en codigoprogramacion.com que lo que hace es heredar
 * sobrecargar el constructor de pdo para establecer una conexión con la base
 *
*/

/**
 * Definición de la clase
 * @author Claudio Invernizzi <cinvernizzi@gmail.com>
 */
class Conexion extends PDO {

    // definimos el estado del servidor
    private $estado = "Desarrollo";

    // definimos las variables de clase
    private $tipo_de_base = 'mysql';
    private $host = 'localhost';
    private $nombre_de_base = 'cce';
    private $usuario;
    private $contraseña;

    /**
     * Constructor de la clase, sobrecargamos el constructor del padre
     * y establecemos la conexión con la base retornando el puntero
     * a la base
     */
    public function __construct() {

        // según el estado del servidor
        if ($this->estado == "Desarrollo"){

            // activamos la depuración
            ini_set('display_errors', 'On');
            error_reporting(E_ALL);

            // conexión para el servidor de desarrollo
            $this->usuario = 'claude';
            $this->contrasena = 'gamaeco';

        // si está en producción
        } else {

            // desactivamos la depuración
            ini_set('display_errors', 'Off');
            error_reporting(E_ALL & ~E_DEPRECATED & ~E_STRICT);

            // conexión para el servidor de producción
            $this->usuario = 'claudefatala';
            $this->contrasena = 'pickard47alfatango';

        }

        // Sobreescribo el método constructor de la clase PDO
        // y utilizamos la captura de errores
        try {

            // llamamos al constructor del padre
            parent::__construct($this->tipo_de_base . ':host=' . $this->host . ';dbname=' . $this->nombre_de_base, $this->usuario, $this->contrasena);

            // seteamos la página de códigos
            parent::exec("SET CHARACTER_SET_CLIENT=utf8;");
            parent::exec("SET CHARACTER_SET_RESULTS=utf8;");
            parent::exec("SET COLLATION_CONNECTION=utf8_unicode_ci;");

            // fijamos el nivel de pdo para que envìe un warning
            parent::setAttribute(PDO::ERRMODE_WARNING, PDO::ERRMODE_EXCEPTION);

        // si hubo algún error lo presentamos
        } catch(PDOException $e){

            // presenta el error
            echo 'Ha surgido un error y no se puede conectar a la base de datos. Detalle: ' . $e->getMessage();
            exit;

        }

    }

 }
?>
