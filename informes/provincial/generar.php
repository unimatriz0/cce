<?php

/**
 *
 * generar.php
 *
 * @package     CCE
 * @subpackage  Reportes
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.4.0 (29/11/2019)
 * @copyright   Copyright (c) 2019, INP
 *
 * Método que recibe por get los datos del reporte provincial a
 * generar
 *
*/

// incluimos e instanciamos las clases
require_once ("reporte.class.php");
$reporte = new Reporte();

// fijamos las propiedades
$reporte->setAnio($_POST["Anio"]);
$reporte->setEncabezado($_POST["Encabezado"]);
$reporte->setJurisdiccion($_POST["Jurisdiccion"]);

// generamos el reporte
$reporte->initReporte();

// retornamos verdadero
echo json_encode(array("Error" => true));

?>