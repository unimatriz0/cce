<?php

/**
 *
 * Class Reporte | clases/reporte.class.php
 *
 * @package     CCE
 * @subpackage  Reportes
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.4.0 (15/11/2018)
 * @copyright   Copyright (c) 2018, INP
 *
 * Clase que genera el documento con el reporte nacional
 *
*/


// el puntero de php queda parado en el directorio informes/nacional luego
// de la llamada, entonces para incluir las fuentes

// define la ruta a las fuentes pdf, usamos el server root
// porque lo llamamos desde distintos lugares
define('FPDF_FONTPATH', $_SERVER['DOCUMENT_ROOT'] . '/clases/fpdf/font');

// la clase pdf
require_once ($_SERVER['DOCUMENT_ROOT'] . "/clases/fpdf/tfpdf.php");

// inclusión de archivos
require_once ("../../clases/herramientas.class.php");
require_once ("../../clases/graficos.class.php");
require_once ("../../clases/estadistica.class.php");
require_once ("informeprovincial.class.php");

// convención para la nomenclatura de las propiedades, comienzan con una
// letra mayúscula, de tener mas de una palabra no se utilizan separadores
// y la inicial de cada palabra va en mayúscula
// para las variables recibidas como parámetro el criterio es todas en
// minúscula

// convención para la nomenclatura de los metodos, comienzan con set o get
// según asignen un valor o lo lean y luego el nombre del valor a obtener

/*

    Atención, esta clase extiende la clase tfpdf que a su vez extiende
    la clase fpdf, la diferencia es que permite definir y generar documentos
    pdf con página de códigos UTF8, el funcionamiento es el mismo que en
    la clase original.

    Sin embargo, la clase para ahorrar espacio regenera las fuentes que
    son utilizadas en el documento, si migramos el sistema y cambia el
    path de la aplicación arroja un error señalando que no encuentra las
    fuentes.

    Para obligar a que regenere las fuentes basta con eliminar todos los
    archivos dat y aquellos php que tengan nombres de fuentes del directorio
    /font/unifont/ dejando solamente los archivos ttf y el archivo ttfonts.php
    que es el que se encarga de generar las fuentes

    Si se desean incluir otras fuentes en el documento, bastaría con
    copiar los archivos ttf en este directorio y luego al incluirlos en
    el documento el sistema se encarga automáticamente de generar los dat

*/

/**
 * @author Claudio Invernizzi <cinvernizzi@gmail.com>
 * Definición de la clase
 */
class Reporte extends tFPDF{

    // definición de las variables
    protected $Anio;                    // año del reporte
    protected $Alto;                    // alto de línea
    protected $Encabezado;              // encabezado de las páginas
    protected $CodProv;                 // clave indec de la jurisdicción
    protected $Jurisdiccion;            // nombre de la jurisdicción
    protected $Utilidades;              // clase de herramientas
    protected $Imagenes;                // clase de gráficos
    protected $Informe;                 // clase de la base de datos

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Constructor de la clase
     */
    public function __construct(){

        // inicializamos las variables
        $this->Alto = 8;

        // instanciamos las clases
        $this->Utilidades = new Herramientas();
        $this->Informe = new InformeProvincial();
        $this->Imagenes = new Graficos();
        $this->Analisis = new Estadistica();

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Desctructor de la clase
     */
    public function __destruct(){

        // cerramos los punteros de las clases
        $this->Utilidades = null;
        $this->Informe = null;

    }

    // métodos de asignación de valores
    public function setAnio($anio){
        $this->Anio = $anio;
    }
    public function setEncabezado($encabezado){
        $this->Encabezado = $encabezado;
    }
    public function setJurisdiccion($jurisdiccion){

        // asignamos en la variable de clase
        $this->CodProv = $jurisdiccion;

        // obtenemos el nombre de la provincia
        $this->Informe->setCodProv($this->CodProv);
        $this->Jurisdiccion = $this->Informe->getJurisdiccion();

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que inicializa las variables de clase y las propiedades
     * del documento
     */
    public function initReporte(){

        // llamamos al constructor de la clase padre
        parent::__construct();

        // establecemos las propiedades
        $this->SetMargins(4,6,1.5);
        $this->SetAuthor("Claudio Invernizzi");
        $this->SetCreator("Sistema CCE");
        $this->SetSubject("Informes de Concordancia", true);
        $this->SetTitle("Informes de Concordancia", true);
        $this->SetAutoPageBreak(true, 10);

        // agrega una fuente unicode
        $this->AddFont('DejaVu','','DejaVuSansCondensed.ttf',true);
        $this->AddFont('DejaVu','B','DejaVuSans-Bold.ttf', true);

        // establecemos el alias para el número total de páginas
        $this->AliasNbPages();

        // presentamos la caratula
        $this->Caratula();

        // presentamos la descripcion de la red de chagas
        $this->Descripcion();

        // presentamos el porcentaje de respuesta
        $this->panelesEnviados();

        // presentamos las determinaciones fuera de termino
        $this->fueraTermino();

        // presentamos la metodologia
        $this->metodologiaUtilizada();

        // presentamos la distribucion de muestras (nº de determinaciones
        // por cada tecnica)
        $this->recepcionMuestras();

        // presentamos la distribucion lector - visual
        $this->lectorVisual();

        // presentamos el numero de tecnicas utilizadas por cada
        // jurisdiccion
        $this->tecnicasUtilizadas();

        // agrega el analisis general de concordancia
        $this->analisisConcordancia();

        // concordancia segun tecnica empleada en cada jurisdiccion
        $this->concordanciaTecnica();

        // analisis de falsos positivos segun tecnica
        $this->falsosPositivos();

        // analisis de falsos negativos segun tecnica
        $this->falsosNegativos();

        // concordancia segun reactivo utilizado
        $this->concordanciaReactivos();

        // valores de corte mal informados
        $this->valoresInformados();

        // analisis de muestras huerfanas
        $this->muestrasHuerfanas();

        // presenta el anàlisis de discordantes
        $this->discordantes();

        // encabezado del analisis estadistico
        $this->analisisEstadistico();

        // analisis historico
        $this->analisisHistorico();

        // observaciones
        $this->observaciones();

        // presenta el anexo i con la nómina de laboratorios
        $this->anexoI();

        // agregamos el glosario
        $this->glosario();

        // grabamos el documento en un archivo temporal
        $this->Output("../../temp/Informe.pdf","F");

        // grabamos el archivo en la base
        $this->grabaReporte();

        // luego que grabamos eliminamos los archivos temporales
        unlink("../../temp/atermino.png");
        unlink("../../temp/concordancia.png");
        unlink("../../temp/dependencia.png");
        unlink("../../temp/eficiencia.png");
        unlink("../../temp/frectecnicas.png");
        unlink("../../temp/historia.png");
        unlink("../../temp/lector.png");
        unlink("../../temp/tecnicas.png");

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Sobrecargamos el método header del padre para presentar
     * el encabezado de las páginas
     */
    public function Header(){

        // incluimos el logo
        $this->Image('../../imagenes/logo_fatala.jpg', 5, 5,40,30);

        // fijamos la posición de impresión
        $this->SetXY(100,10);

        // establecemos la fuente
        $this->SetFont("DejaVu", "", 10);

        // imprime el encabezado
        $this->Cell(0, $this->Alto, $this->Encabezado, 0, 1, "R");

        // fija las dos líneas separadoras
        $this->Image('../../imagenes/separador.png', 5, 35, 210, 3);

        // fijamos la posición del cabezal
        $this->setY(40);

        // establecemos la fuente normal
        $this->SetFont("DejaVu", "", 12);

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Sobrecargamos el mètodo footer para presentar el
     * piè de página
     */
    public function Footer(){

        // vamos a 1 centímetro del fin de página
        $this->SetY(-10);

        // fijamos la fuente
        $this->SetFont("DejaVu", "", 10);

        // componemos el texto
        $texto = "Programa Nacional de Control de Calidad Externo ";
        $texto .= "- Página Nº: " . $this->PageNo() . " de " . '{nb}';

        // imprimimos el pié y el número de página
        $this->Cell(0, $this->Alto, $texto, "T", 0, 'C');

        // establecemos la fuente
        $this->SetFont("DejaVu", "", 12);

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Metodo que arma la carátula del documento
     */
    protected function Caratula(){

        // agregamos la página
        $this->AddPage('P');

        // presenta el logo del instituto con 300 pixeles
        $this->Image('../../imagenes/logo_fatala.jpg', 60, 40, 80,60);

        // fija la fuente
        $this->SetFont("DejaVu", "B", 18);

        // posicionamos el cursor
        $this->SetY(150);

        // presenta el título
        $texto = "Instituto Nacional de Parasitologìa";
        $this->MultiCell(0, $this->Alto, $texto, 0, 'C');
        $texto = "Dr. Mario Fatala Chaben";
        $this->MultiCell(0, $this->Alto, $texto, 0, 'C');
        $texto = "Departamento de Diagnóstico";
        $this->MultiCell(0, $this->Alto, $texto, 0, 'C');

        // reducimos la fuente
        $this->SetFont("DejaVu", "B", 16);

        // presenta la descripción
        $texto = "Programa Nacional de Control de Calidad Externo";
        $this->MultiCell(0, $this->Alto + 4, $texto, 0, 'C');

        // reducimos la fuente
        $this->SetFont("DejaVu", "B", 14);

        // presenta el año y tipo del informe
        $texto = "Reporte de Concordancia de la Jurisdicción de $this->Jurisdiccion";
        $this->MultiCell(0, $this->Alto + 4, $texto, 0, 'C');
        $this->MultiCell(0, $this->Alto + 4, "Año: " . $this->Anio, 0, 'C');

        // reducimos la fuente
        $this->SetFont("DejaVu", "B", 14);

        // presenta la fecha de impresión
        $this->MultiCell(0, $this->Alto + 4, "Buenos Aires " . $this->Utilidades->fechaLetras(date('d/m/Y')), 0, 'C');

        // fija la fuente normal
        $this->SetFont("DejaVu", "", 12);

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que genera la descripción de la muestra
     */
    protected function Descripcion(){

        // agregamos la página
        $this->AddPage('P');

        // fijamos la fuente
        $this->SetFont("DejaVu", "B", 14);

        // presenta el título
        $this->MultiCell(0, $this->Alto, "Introducción", 0, 'L');

        // fijamos la fuente normal
        $this->SetFont("DejaVu", "", 12);

        // la presentación
        $texto = "Se efectuó el Control de Calidad Externo de la Red de ";
        $texto .= "Laboratorios de Chagas de la Jurisdicción de ";
        $texto .= "$this->Jurisdiccion correspondiente al Año $this->Anio ";
        $texto .= "con el objetivo de evaluar el desempeño de los laboratorios ";
        $texto .= "participantes, como base para decidir eventuales medidas ";
        $texto .= "correctivas o de mejoramiento en cada provincia participante.";
        $this->MultiCell(0, $this->Alto, $texto, 0, 'J');

        // inserta un separador
        $this->Ln($this->Alto);

        // continuamos
        $texto = "Se detalla a continuación, en la Tabla I y en el Gráfico I la ";
        $texto .= "distribución  en la Jurisdicción Informada de Laboratorios relevada por este ";
        $texto .= "programa, independientemente de su participación y clasificados según ";
        $texto .= "fuente de financiamiento.";
        $this->MultiCell(0, $this->Alto, $texto, 0, 'J');

        // insertamos un separador
        $this->Ln($this->Alto);

        // presenta el tí­tulo
        $texto = "Tabla I: Distribución de Laboratorios de Acuerdo a su Financiamiento";
        $this->MultiCell(0, $this->Alto, $texto, 0, 'J');

        // obtenemos la distribución de laboratorios
        $nomina = $this->Informe->laboratoriosParticipantes();

        // reducimos la fuente
        $this->SetFont("DejaVu", "B", 10);

        // presentamos los encabezados
        $this->Cell(70,$this->Alto - 2,"Jurisdicción",1,0);
        $this->Cell(18,$this->Alto - 2,"Nac.",1,0,"C");
        $this->Cell(18,$this->Alto - 2,"Prov.",1,0,"C");
        $this->Cell(18,$this->Alto - 2,"Mun.",1,0,"C");
        $this->Cell(18,$this->Alto - 2,"Univ.",1,0,"C");
        $this->Cell(18,$this->Alto - 2,"Priv.",1,0,"C");
        $this->Cell(18,$this->Alto - 2,"O.Soc.",1,0,"C");
        $this->Cell(18,$this->Alto - 2,"Total",1,1,"C");

        // setea la fuente
        $this->SetFont("DejaVu", "", 10);

        // ahora recorremos el vector que siempre tendrá
        // una sola fila que es la provincia informada
        foreach($nomina AS $registro){

            // obtenemos el registro
            extract($registro);

            // inicializa el contador
            $totalprov = 0;

            // presentamos la provincia
            $this->Cell(70,$this->Alto - 3, $provincia,1,0);

            // si hay nacionales
            $this->Cell(18,$this->Alto - 3, $nacional,1,0,"C");
            $totalprov += $nacional;

            // si hay provinciales
            $this->Cell(18,$this->Alto - 3, $provincial,1,0,"C");
            $totalprov += $provincial;

            // si hay municipales
            $this->Cell(18,$this->Alto - 3, $municipal,1,0,"C");
            $totalprov += $municipal;

            // si hay universitario
            $this->Cell(18,$this->Alto - 3, $universitario,1,0,"C");
            $totalprov += $universitario;

            // si hay privados
            $this->Cell(18,$this->Alto - 3, $privado,1,0,"C");
            $totalprov += $privado;

            // si hay obra social
            $this->Cell(18,$this->Alto - 3, $obrasocial,1,0,"C");
            $totalprov += $obrasocial;

            // presenta el total
            $this->Cell(18,$this->Alto - 3, $totalprov, 1,1,"C");

            // creamos el vector de datos como un array multidimensional
            $datos = array(array("Nacional", $nacional),
                           array("Provincial", $provincial),
                           array("Municipal", $municipal),
                           array("Universitario", $universitario),
                           array("Privado", $privado),
                           array("O.Social", $obrasocial));

        }

        // agrega un salto de página
        $this->AddPage('P');

        // setea la fuente
        $this->SetFont("DejaVu", "B", 12);

        // presenta el título
        $this->MultiCell(0, $this->Alto, "Distribución según Dependencia", 0, 'L');

        // establecemos el tamaño
        $tamanio = array("Ancho" => 1000,
                         "Alto" => 600);

        // fijamos los valores en la clase
        $this->Imagenes->setDatos($datos);
        $this->Imagenes->setTitulo("Distribución según Dependencia");
        $this->Imagenes->setTamanio($tamanio);
        $this->Imagenes->setArchivo("dependencia.png");

        // graficamos los totales
        $this->Imagenes->Torta();

        // lo agregamos al documento
        $this->Image("../../temp/dependencia.png", 10, 50);

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Mètodo que presenta la grilla con la nómina de paneles
     * enviados
     */
    protected function panelesEnviados(){

        // agregamos la página
        $this->AddPage('P');

        // fijamos la fuente
        $this->SetFont("DejaVu", "B", 14);

        // presenta el título
        $this->MultiCell(0, $this->Alto, "Paneles Enviados / Procesados", 0, 'L');

        // fijamos la fuente normal
        $this->SetFont("DejaVu", "", 12);

        // obtenemos el número de paneles enviados
        $paneles = $this->Informe->panelesEnviados($this->Anio);
        $respuesta = $this->Informe->panelesProcesados($this->Anio);

        // presenta la distribución de muestras
        $texto = "Se enviaron un total de $paneles paneles conteniendo ";
        $texto .= "seis muestras glicerinadas al 50% cada una, los cuales fueron ";
        $texto .= "distribuidos según de acuerdo solicitado por el Coordinador ";
        $texto .= "de la Jurisdiccion obteniendo una respuesta de $respuesta.";
        $this->MultiCell(0, $this->Alto, $texto, 0, 'J');

        // inserta un separador
        $this->Ln($this->Alto);

        // informa del listado
        $texto = "En Anexo I se detalla la nómina de Laboratorios participantes ";
        $texto .= "y el grado de respuesta de cada uno de ellos.";
        $this->MultiCell(0, $this->Alto, $texto, 0, 'J');

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que presenta el anexo i con la nómina de laboratorios
     * participantes
     */
    protected function anexoI(){

        // agregamos la página
        $this->AddPage('P');

        // fijamos la fuente
        $this->SetFont("DejaVu", "B", 14);

        // presenta el título
        $this->MultiCell(0, $this->Alto, "Anexo I", 0, 'L');

        // fijamos la fuente normal
        $this->SetFont("DejaVu", "", 12);

        // presenta el texto
        $texto = "Se presenta a continuación la nómina de laboratorios ";
        $texto .= "participantes que han recibido muestras en la Jurisdicción ";
        $texto .= "informada.";
        $this->MultiCell(0, $this->Alto, $texto, 0, 'J');

        // inserta un separador
        $this->Ln($this->Alto);

        // reducimos la fuente
        $this->SetFont("DejaVu", "B", 10);

        // presentamos los encabezados
        $this->Cell(130,$this->Alto - 2,"Laboratorio",1, 0, "L");
        $this->Cell(70,$this->Alto - 2,"Localidad",1, 1, "L");

        // obtenemos la nómina
        $participantes = $this->Informe->nominaLaboratorios($this->Anio);

        // fijamos la fuente
        $this->SetFont("DejaVu", "", 10);

        // recorremos el vector
        foreach($participantes AS $registro){

            // obtenemos el registro
            extract($registro);

            // lo agregamos
            $this->Cell(130,$this->Alto - 2, $laboratorio,1, 0, "L");
            $this->Cell(70,$this->Alto - 2, $localidad,1, 1, "L");

        }

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que presenta la nómina de determinaciones fuera
     * de término
     */
    protected function fueraTermino(){

        // agregamos la página
        $this->AddPage('P');

        // fijamos la fuente
        $this->SetFont("DejaVu", "B", 14);

        // presenta el título
        $this->MultiCell(0, $this->Alto, "Determinaciones Fuera de Término", 0, 'L');

        // fijamos la fuente normal
        $this->SetFont("DejaVu", "", 12);

        // presenta el texto
        $texto = "Durante el ejercicio anual se realizan seis (6) operativos ";
        $texto .= "de Control de Calidad, los cuales tienen una fecha de inicio ";
        $texto .= "y de finalización para la realización y carga de las ";
        $texto .= "determinaciones en la plataforma del sistema. Tal como se ";
        $texto .= "muestra en el Gráfico II, como parte ";
        $texto .= "del análisis se describen a continuación el número de ";
        $texto .= "determinaciones cargadas fuera de término, es decir fuera ";
        $texto .= "del período establecido para cada operativo (conste que ";
        $texto .= "esto nada nos dice sobre la validez de los resultados ";
        $texto .= "obtenidos sino sobre la oportunidad de la misma).";
        $this->MultiCell(0, $this->Alto, $texto, 0, 'J');

        // inserta un separador
        $this->Ln($this->Alto);

        // obtenemos la distribucion de determinaciones
        $distribucion = $this->Informe->totalFueraTermino($this->Anio);
        extract($distribucion);

        // presenta el título del gráfico
        $texto = "Gráfico III: Resultados Cargados Fuera de Término";
        $this->MultiCell(0, $this->Alto, $texto, 0, 'J');

        // creamos el vector de datos como un array multidimensional
        $datos = array(array("En Termino", $atermino),
                       array("Fuera", $fueratermino));

        // establecemos el tamaño
        $tamanio = array("Ancho" => 900,
                         "Alto" => 500);

        // fijamos los valores en la clase
        $this->Imagenes->setDatos($datos);
        $this->Imagenes->setTitulo("Oportunidad de Carga");
        $this->Imagenes->setTamanio($tamanio);
        $this->Imagenes->setArchivo("atermino.png");

        // graficamos los totales
        $this->Imagenes->Torta();

        // lo agregamos al documento
        $this->Image("../../temp/atermino.png", 20, 125);

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que presenta la metodología utilizada y la descripción
     * de la evaluación de muestras
     */
    protected function metodologiaUtilizada(){

        // agregamos la página
        $this->AddPage('P');

        // fijamos la fuente
        $this->SetFont("DejaVu", "B", 14);

        // presenta el título
        $this->MultiCell(0, $this->Alto, "Metodología Utilizada", 0, 'L');

        // fijamos la fuente normal
        $this->SetFont("DejaVu", "", 12);

        // presenta el texto
        $texto = "Los paneles enviados estaban conformados por seis (6) ";
        $texto .= "muestras de sueros en glicerina bufferada al 50%. ";
        $texto .= "Los resultados obtenidos han sido evaluados considerando ";
        $texto .= "la reactividad de las muestras y en la Tabla III ";
        $texto .= "se explicita la metodologí­a utilizada para considerar ";
        $texto .= "un resultado como correcto o incorrecto.";
        $this->MultiCell(0, $this->Alto, $texto, 0, 'J');

        // insertamos un separador
        $this->Ln($this->Alto);

        // presenta el tí­tulo de la tabla
        $texto = "Tabla III: Evaluación de Resultados";
        $this->MultiCell(0, $this->Alto, $texto, 0, 'J');

        // insertamos un separador
        $this->Ln($this->Alto);

        // fijamos la fuente de los títulos
        $this->SetFont("DejaVu", "B", 10);

        // agregamos los encabezados
        $this->Cell(20, $this->Alto, "", 0, 0);
        $this->Cell(40, $this->Alto, "Muestra", 1, 0, 'L');
        $this->Cell(50, $this->Alto, "Correcto", 1, 0, 'C');
        $this->Cell(50, $this->Alto, "Incorrecto", 1, 1, 'C');

        // fijamos la fuente de los títulos
        $this->SetFont("DejaVu", "", 10);

        // ahora agregamos la primer fila
        $this->Cell(20, $this->Alto, "", 0, 0);
        $this->Cell(40, $this->Alto, "Reactiva Alta", 1, 0, 'L');
        $this->Cell(50, $this->Alto, "Reactiva / Indeterminada", 1, 0, 'C');
        $this->Cell(50, $this->Alto, "No Reactiva", 1, 1, 'C');

        // la segunda fila
        $this->Cell(20, $this->Alto, "", 0, 0);
        $this->Cell(40, $this->Alto, "Reactiva Baja", 1, 0, 'L');
        $this->Cell(50, $this->Alto, "Reactiva / Indeterminada", 1, 0, 'C');
        $this->Cell(50, $this->Alto, "No Reactiva", 1, 1, 'C');

        // la tercera fila
        $this->Cell(20, $this->Alto, "", 0, 0);
        $this->Cell(40, $this->Alto, "No Reactiva", 1, 0, 'L');
        $this->Cell(50, $this->Alto, "No Reactiva", 1, 0, 'C');
        $this->Cell(50, $this->Alto, "Reactiva / Indeterminada", 1, 1, 'C');

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que presenta la grilla con la respuesta de los
     * laboratorios (nº de determinaciones por cada técnica)
     */
    protected function recepcionMuestras(){

        // agregamos la página
        $this->AddPage('P');

        // fijamos la fuente
        $this->SetFont("DejaVu", "B", 14);

        // presenta el título
        $this->MultiCell(0, $this->Alto, "Recepción y Procesamiento de Muestras", 0, 'L');

        // fijamos la fuente normal
        $this->SetFont("DejaVu", "", 12);

        // presenta el texto
        $texto = "En la Tabla IV se muestra el número de determinaciones ";
        $texto .= "efectuadas en función de la Técnica empleada ";
        $texto .= "para la Jurisdicción Informada:";
        $this->MultiCell(0, $this->Alto, $texto, 0, 'J');

        // insertamos un separador
        $this->Ln($this->Alto);

        // presenta el título de la tabla
        $texto = "Tabla IV: N° de Determinaciones según Técnica";
        $this->MultiCell(0, $this->Alto, $texto, 0, 'J');

        // obtenemos la nomina
        $muestras = $this->Informe->distribucionMuestras($this->Anio);

        // fijamos la fuente
        $this->SetFont("DejaVu", "B", 10);

        // presentamos los titulos de la tabla
        $this->Cell(65, $this->Alto, "Jurisdicción", 1, 0, 'L');
        $this->Cell(18, $this->Alto, "AP", 1, 0, 'C');
        $this->Cell(18, $this->Alto, "ELISA", 1, 0, 'C');
        $this->Cell(18, $this->Alto, "HAI", 1, 0, 'C');
        $this->Cell(18, $this->Alto, "IFI", 1, 0, 'C');
        $this->Cell(18, $this->Alto, "QUIMIO", 1, 0, 'C');
        $this->Cell(18, $this->Alto, "OTRA", 1, 0, 'C');
        $this->Cell(18, $this->Alto, "Total", 1, 1, 'C');

        // fijamos la fuente
        $this->SetFont("DejaVu", "", 10);

        // recorremos el array agregando los valores
        foreach($muestras AS $registro) {

            // obtenemos el registro
            extract($registro);

            // lo presentamos
            $this->Cell(65, $this->Alto - 3, $jurisdiccion, 1, 0, 'L');
            $this->Cell(18, $this->Alto - 3, $ap, 1, 0, 'C');
            $this->Cell(18, $this->Alto - 3, $elisa, 1, 0, 'C');
            $this->Cell(18, $this->Alto - 3, $hai, 1, 0, 'C');
            $this->Cell(18, $this->Alto - 3, $ifi, 1, 0, 'C');
            $this->Cell(18, $this->Alto - 3, $quimio, 1, 0, 'C');
            $this->Cell(18, $this->Alto - 3, $otra, 1, 0, 'C');

            // calculamos el total y lo presentamos
            $total = $ap + $elisa + $hai + $ifi + $quimio + $otra;
            $this->Cell(18, $this->Alto - 3, $total, 1, 1, 'C');

        }

        // inserta un salto de página
        $this->AddPage();

        // fijamos la fuente
        $this->SetFont("DejaVu", "", 12);

        // presenta el texto del gráfico
        $texto = "En el Gráfico V se pueden observar los porcentajes ";
        $texto .= "de determinaciones para el total País y clasificadas ";
        $texto .= "de acuerdo a la Técnica emplada .";
        $this->MultiCell(0, $this->Alto, $texto, 0, 'J');

        // insertamos un separador
        $this->Ln($this->Alto);

        // presenta el título del gráfico
        $texto = "Gráfico V: N° de Determinaciones según Técnica Empleada";
        $this->MultiCell(0, $this->Alto, $texto, 0, 'J');

        // creamos el vector de datos como un array multidimensional
        $datos = array(array("AP", $ap),
                       array("ELISA", $elisa),
                       array("HAI", $hai),
                       array("IFI", $ifi),
                       array("QUIMIO", $quimio),
                       array("OTRAS", $otra));

        // establecemos el tamaño
        $tamanio = array("Ancho" => 900,
                         "Alto" => 500);

        // fijamos los valores en la clase
        $this->Imagenes->setDatos($datos);
        $this->Imagenes->setTitulo("Distribución según Técnica");
        $this->Imagenes->setTamanio($tamanio);
        $this->Imagenes->setArchivo("frectecnicas.png");

        // graficamos los totales
        $this->Imagenes->Torta();

        // lo agregamos al documento
        $this->Image("../../temp/frectecnicas.png", 20, 80);

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que presenta la tabla con la distribución de
     * lector - visual
     */
    protected function lectorVisual(){

        // agregamos la página
        $this->AddPage('P');

        // fijamos la fuente
        $this->SetFont("DejaVu", "B", 14);

        // presenta el título
        $this->MultiCell(0, $this->Alto, "ELISA - Distribución Lector / Visual", 0, 'L');

        // fijamos la fuente normal
        $this->SetFont("DejaVu", "", 12);

        // presenta el texto
        $texto = "Para evaluar la calidad del diagnóstico de la enfermedad ";
        $texto .= "por T.cruzi también es importante conocer la cantidad / ";
        $texto .= "porcentaje de laboratorios que utilizan lector de placas ";
        $texto .= "para leer las observancias del ELISA. En la Tabla VI se ";
        $texto .= "puede observar el número de determinaciones de ELISA ";
        $texto .= "realizadas de manera visual o con lector de placas.";
        $this->MultiCell(0, $this->Alto, $texto, 0, 'J');

        // inserta un separador
        $this->Ln($this->Alto);

        // presenta el título de la tabla
        $texto = "Tabla VI: Nº de Laboratorios que utilizan lector de placas";
        $this->MultiCell(0, $this->Alto, $texto, 0, 'J');

        // obtenemos la distribucion
        $lectores = $this->Informe->lectorVisual($this->Anio);

        // fija la fuente de los tìtulos
        $this->SetFont("DejaVu", "B", 10);

        // presenta la tabla
        $this->Cell(30, $this->Alto, "", 0, 0);
        $this->Cell(70, $this->Alto, "Jurisdicción", 1, 0, 'L');
        $this->Cell(20, $this->Alto, "Lector", 1, 0, 'C');
        $this->Cell(20, $this->Alto, "Visual", 1, 0, 'C');
        $this->Cell(20, $this->Alto, "Total", 1, 1, 'C');

        // fijamos la fuente
        $this->SetFont("DejaVu", "", 10);

        // inicializamos los contadores
        $totallector = 0;
        $totalvisual = 0;

        // recorremos el array agregando los valores
        foreach($lectores AS $registro) {

            // obtenemos el registro
            extract($registro);

            // lo presentamos
            $this->Cell(30, $this->Alto - 3, "", 0, 0);
            $this->Cell(70, $this->Alto - 3, $provincia, 1, 0, 'L');
            $this->Cell(20, $this->Alto - 3, $lector, 1, 0, 'C');
            $this->Cell(20, $this->Alto - 3, $visual, 1, 0, 'C');
            $this->Cell(20, $this->Alto - 3, $lector + $visual , 1, 1, 'C');

        }

        // insertamos un separador
        $this->Ln($this->Alto);

        // fijamos la fuente
        $this->SetFont("DejaVu", "", 12);

        // presenta el texto
        $texto = "Tal como se observa en el Gráfico IV y considerando el total ";
        $texto .= "de la Jurisdicción, es posible representar el número de determinaciones ";
        $texto .= "ELISA realizadas mediante Lector o Visualmente";
        $this->MultiCell(0, $this->Alto, $texto, 0, 'J');

        // insertamos un salto de página
        $this->AddPage();

        // presenta el tí­tulo del grafico
        $texto = "Gráfico IV: Nº de Laboratorios que utilizan lector de placas";
        $this->MultiCell(0, $this->Alto, $texto, 0, 'J');

        // creamos el vector de datos como un array multidimensional
        $datos = array(array("Lector", $lector),
                       array("Visual", $visual));

        // establecemos el tamaño
        $tamanio = array("Ancho" => 900,
                         "Alto" => 500);

        // fijamos los valores en la clase
        $this->Imagenes->setDatos($datos);
        $this->Imagenes->setTitulo("Distribución Lector / Visual");
        $this->Imagenes->setTamanio($tamanio);
        $this->Imagenes->setArchivo("lector.png");

        // graficamos los totales
        $this->Imagenes->Torta();

        // lo agregamos al documento
        $this->Image("../../temp/lector.png", 20, 50);

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que presenta el número de técnicas utilizadas
     * por cada jurisdicción
     */
    protected function tecnicasUtilizadas(){

        // agregamos la página
        $this->AddPage('P');

        // fijamos la fuente
        $this->SetFont("DejaVu", "B", 14);

        // presenta el título
        $this->MultiCell(0, $this->Alto, "Técnicas Diagnósticas Utilizadas", 0, 'L');

        // fijamos la fuente normal
        $this->SetFont("DejaVu", "", 12);

        // presenta el texto
        $texto = "La Tabla VIII, clasifica el número de laboratorios de ";
        $texto .= "la Jurisdicción informada, según el número de técnicas ";
        $texto .= "que utiliza cada uno de ellos para realizar el diagnóstico ";
        $texto .= "de la Enfermedad de Chagas";
        $this->MultiCell(0, $this->Alto, $texto, 0, 'J');

        // inserta un separador
        $this->Ln($this->Alto);

        // presenta el tí­tulo de la tabla
        $texto = "Tabla VIII: Laboratorios según el Nº de Técnicas Utilizadas para el Diagnóstico";
        $this->MultiCell(0, $this->Alto, $texto, 0, 'J');

        // insertamos un separador
        $this->Ln($this->Alto);

        // obtenemos los datos
        $tecnicas = $this->Informe->nroTecnicas($this->Anio);

        // setea la fuente
        $this->SetFont("DejaVu", "B", 10);

        // presentamos los titulos
        $this->Cell(20, $this->Alto, "", 0, 0);
        $this->Cell(70, $this->Alto, "Jurisdicción", 1, 0, 'L');
        $this->Cell(20, $this->Alto, "1 Tec.", 1, 0, 'C');
        $this->Cell(20, $this->Alto, "2 Tec.", 1, 0, 'C');
        $this->Cell(20, $this->Alto, "3 Tec.", 1, 0, 'C');
        $this->Cell(20, $this->Alto, "4 Tec.", 1, 1, 'C');

        // seteamos la fuente
        $this->SetFont("DejaVu", "", 10);

        // recorremos el array agregando los valores
        foreach($tecnicas AS $registro) {

            // obtenemos el registro
            extract($registro);

            // lo presentamos
            $this->Cell(20, $this->Alto - 3, "", 0, 0);
            $this->Cell(70, $this->Alto - 3 ,$jurisdiccion, 1, 0, 'L');
            $this->Cell(20, $this->Alto - 3 ,$tecnica1, 1, 0, 'C');
            $this->Cell(20, $this->Alto - 3, $tecnica2, 1, 0, 'C');
            $this->Cell(20, $this->Alto - 3, $tecnica3, 1, 0, 'C');
            $this->Cell(20, $this->Alto - 3, $tecnica4, 1, 1, 'C');

        }

        // inserta un salto de pàgina
        $this->AddPage();

        // setea la fuente
        $this->SetFont("DejaVu", "", 12);

        // presenta la descripción del gráfico
        $texto = "En el Gráfico V es posible representar el total de la Jurisdicción ";
        $texto .= "de acuerdo al número de técnicas utilizadas por los Laboratorios ";
        $texto .= "participantes.";
        $this->MultiCell(0, $this->Alto, $texto, 0, 'J');

        // inserta un separador
        $this->Ln($this->Alto);

        // presenta el tí­tulo del gráfico
        $texto = "Gráfico V: % de Laboratorios según el Nº de Técnicas Utilizadas";
        $this->MultiCell(0, $this->Alto, $texto, 0, 'J');

        // creamos el vector de datos como un array multidimensional
        $datos = array(array("1 Tec.", $tecnica1),
                       array("2 Tec.", $tecnica2),
                       array("3 Tec.", $tecnica3),
                       array("4 Tec.", $tecnica4));

        // establecemos el tamaño
        $tamanio = array("Ancho" => 900,
                         "Alto" => 500);

        // fijamos los valores en la clase
        $this->Imagenes->setDatos($datos);
        $this->Imagenes->setTitulo("Distribución s/ Nº de Técnicas");
        $this->Imagenes->setTamanio($tamanio);
        $this->Imagenes->setArchivo("tecnicas.png");

        // graficamos los totales
        $this->Imagenes->Torta();

        // lo agregamos al documento
        $this->Image("../../temp/tecnicas.png", 20, 80);

        // fijamos la posición de impresión
        $this->setY(210);

        // presenta el tí­tulo de otras técnicas
        $this->SetFont("DejaVu", "B", 12);
        $texto = "Otras Técnicas";
        $this->MultiCell(0, $this->Alto, $texto, 0, 'J');
        $this->SetFont("DejaVu", "", 12);

        // presenta la descripción de otras técnicas
        $texto = "Al no encontrarse parametrizado el uso de Otras Técnicas ";
        $texto .= "no es posible realizar un anáisis estadístico de las ";
        $texto .= "mismas que posea consistencia y permita realizar comparaciones ";
        $texto .= "de tal forma, solo se hará un análisis descriptivo de las ";
        $texto .= "determinaciones realizadas.";
        $this->MultiCell(0, $this->Alto, $texto, 0, 'J');

        // insertamos un separador
        $this->Ln($this->Alto);

        $texto = "En la Tabla IX se detallan las técnicas cargadas bajo la ";
        $texto .= "denominación 'Otras' en la plataforma.";
        $this->MultiCell(0, $this->Alto, $texto, 0, 'J');

        // obtiene la nómina de otras técnicas
        $otras = $this->Informe->otrasPruebas($this->Anio);

        // si hubo otras determinaciones
        if (count($otras) != 0){

            // agrega un salto de página
            $this->AddPage('L');

            // presenta el tí­tulo de otras técnicas
            $texto = "Tabla IX: Determinaciones con Otras Técnicas ";
            $this->MultiCell(0, $this->Alto, $texto, 0, 'J');

            // setea la fuente
            $this->SetFont("DejaVu", "B", 10);

            // presentamos los titulos
            $this->Cell(50, $this->Alto, "Jurisdicción", 1, 0, 'L');
            $this->Cell(110, $this->Alto, "Laboratorio", 1, 0, 'L');
            $this->Cell(60, $this->Alto, "Prueba", 1, 0, 'L');
            $this->Cell(40, $this->Alto, "Marca", 1, 0, 'L');
            $this->Cell(13, $this->Alto, "ID", 1, 1, 'C');

            // setea la fuente
            $this->SetFont("DejaVu", "", 10);

            // recorremos el vector
            foreach($otras AS $registro){

                // obtenemos el registro
                extract($registro);

                // presentamos el registro
                $this->Cell(50, $this->Alto - 2, $jurisdiccion, 1, 0, 'L');
                $this->Cell(110, $this->Alto - 2, $laboratorio, 1, 0, 'L');
                $this->Cell(60, $this->Alto - 2, $prueba, 1, 0, 'L');
                $this->Cell(40, $this->Alto - 2, $marca, 1, 0, 'L');
                $this->Cell(13, $this->Alto - 2, $id, 1, 1, 'C');

            }

        // si no hubo otras determinaciones
        } else {

            // presenta el texto
            $texto = "No se han ingresado otras técnicas para el Operativo informado.";
            $this->MultiCell(0, $this->Alto, $texto, 0, 'J');

        }

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Presenta el análisis general de concordancia (independientemente
     * de la técnica utilziada)
     */
    protected function analisisConcordancia(){

        // agregamos la página
        $this->AddPage('P');

        // fijamos la fuente
        $this->SetFont("DejaVu", "B", 14);

        // presenta el título
        $this->MultiCell(0, $this->Alto, "Análisis de Concordancia", 0, 'L');

        // fijamos la fuente normal
        $this->SetFont("DejaVu", "", 12);

        // presenta el texto
        $texto = "En la Tabla X se puede observar el porcentaje de concordancia ";
        $texto .= "de la Jurisdicción.";
        $this->MultiCell(0, $this->Alto, $texto, 0, 'J');

        // inserta un separador
        $this->Ln($this->Alto);

        // presenta el titulo de la tabla
        $texto = "Tabla X: % de Concordancia";
        $this->MultiCell(0, $this->Alto, $texto, 0, 'J');

        // inserta un separador
        $this->Ln($this->Alto);

        // obtenemos los datos
        $concordantes = $this->Informe->obtenerConcordancia($this->Anio);

        // establece la fuente
        $this->SetFont("DejaVu", "B", 10);

        // presentamos los titulos
        $this->Cell(30, $this->Alto, "", 0, 0);
        $this->Cell(50, $this->Alto, "Jurisdicción", 1, 0, 'L');
        $this->Cell(20, $this->Alto, "Det.", 1, 0, 'C');
        $this->Cell(20, $this->Alto, "Concord.", 1, 0, 'C');
        $this->Cell(20, $this->Alto, "No Conc.", 1, 0, 'C');
        $this->Cell(20, $this->Alto, "%", 1, 1, 'C');

        // establece la fuente
        $this->SetFont("DejaVu", "", 10);

        // creamos la matriz que luego usaremos para graficar
        $datos = array();

        // recorremos el array agregando los valores
        foreach($concordantes AS $registro){

            // obtenemos el registro
            extract($registro);

            // lo presentamos
            $this->Cell(30, $this->Alto - 3, "", 0, 0);
            $this->Cell(50, $this->Alto - 3, $provincia, 1, 0, 'L');
            $this->Cell(20, $this->Alto - 3, $determinaciones, 1, 0, 'C');
            $this->Cell(20, $this->Alto - 3, $correctas, 1, 0, 'C');
            $this->Cell(20, $this->Alto - 3, $incorrectas, 1, 0, 'C');

            // calculamos el porcentaje
            $porcentaje = ($correctas / $determinaciones) * 100;

            // presentamos la eficiencia
            $this->Cell(20, $this->Alto - 3, number_format($porcentaje, 2, ",", "."), 1, 1, 'C');

        }

        // establece la fuente
        $this->SetFont("DejaVu", "", 12);

        // inserta un separador
        $this->Ln($this->Alto);

        // presenta el texto introductorio del grafico
        $texto = "Presentamos a continuacion el Grafico N° VI considerando ";
        $texto .= "los porcentajes de concordancia de las muestras procesadas ";
        $texto .= "de acuerdo a los criterios establecidos previamente en este documento. ";
        $this->MultiCell(0, $this->Alto, $texto, 0, 'J');

        // insertamos un salto
        $this->AddPage();

        // presenta el titulo del grafico
        $texto = "Grafico VI: Determinaciones Realizadas y Concordancia";
        $this->MultiCell(0, $this->Alto, $texto, 0, 'J');

        // establecemos el tamaño
        $tamanio = array("Ancho" => 1000,
                         "Alto" => 600);

        $datos = array(array("Correctas", $correctas),
                       array("Incorrectas", $incorrectas));

        // fijamos los valores en la clase
        $this->Imagenes->setDatos($datos);
        $this->Imagenes->setTitulo("% de Concordancia");
        $this->Imagenes->setTamanio($tamanio);
        $this->Imagenes->setArchivo("concordancia.png");

        // graficamos los totales
        $this->Imagenes->Torta();

        // lo agregamos al documento
        $this->Image("../../temp/concordancia.png", 20, 50);

    }

    /**
     * @uahtor Claudio Invernizzi <cinvernizzi@gmail.com>
     * Presentamos el anàlisis de concordancia según la técnica
     * utilizada
     */
    protected function concordanciaTecnica(){

        // agregamos la página
        $this->AddPage('P');

        // fijamos la fuente
        $this->SetFont("DejaVu", "B", 14);

        // presenta el título
        $this->MultiCell(0, $this->Alto, "Concordancia Según Técnica", 0, 'L');

        // fijamos la fuente normal
        $this->SetFont("DejaVu", "", 12);

        // presenta el texto de la tabla
        $texto = "La Tabla XI muestra el porcentaje de concordancia ";
        $texto .= "segun la Tecnica empleada en los Laboratorios de la ";
        $texto .= "Jurisdiccion (nótese que los valores están expresados ";
        $texto .= "como porcentajes de muestras concordantes en función ";
        $texto .= "del número de muestras procesadas por cada técnica).";
        $this->MultiCell(0, $this->Alto, $texto, 0, 'J');

        // inserta un separador
        $this->Ln($this->Alto);

        // presenta el titulo de la tabla
        $texto = "Tabla XI: % de Concordancia segun Tecnica";
        $this->MultiCell(0, $this->Alto, $texto, 0, 'J');

        // inserta un separador
        $this->Ln($this->Alto);

        // obtenemos la matriz de datos
        $eficiencia = $this->Informe->eficienciaTecnicas($this->Anio);

        // setea la fuente
        $this->SetFont("DejaVu", "B", 10);

        // presentamos los titulos
        $this->Cell(10, $this->Alto, "", 0, 0, 'L');
        $this->Cell(60, $this->Alto, "Jurisdicción", 1, 0, 'L');
        $this->Cell(20, $this->Alto, "AP", 1, 0, 'C');
        $this->Cell(20, $this->Alto, "ELISA", 1, 0, 'C');
        $this->Cell(20, $this->Alto, "HAI", 1, 0, 'C');
        $this->Cell(20, $this->Alto, "IFI", 1, 0, 'C');
        $this->Cell(20, $this->Alto, "QUIMIO", 1, 0, 'C');
        $this->Cell(20, $this->Alto, "OTRAS", 1, 1, 'C');

        // setea la fuente
        $this->SetFont("DejaVu", "", 10);

        // recorremos el array agregando los valores
        foreach ($eficiencia AS $registro) {

            // obtenemos el registro
            extract($registro);

            // calculamos la eficiencia por cada técnica verificando que
            // el número de determinaciones sea distinto de 0
            if ($ap != 0){
                $eficienciaap = ($correctasap / $ap) * 100;
                $eficienciaap = number_format($eficienciaap, 2, ',', '.');
            } else {
                $eficienciaap = "";
            }
            if ($elisa != 0){
                $eficienciaelisa = ($correctaselisa / $elisa) * 100;
                $eficienciaelisa = number_format($eficienciaelisa, 2, ',', '.');
            } else {
                $eficienciaelisa = "";
            }
            if ($hai != 0){
                $eficienciahai = ($correctashai / $hai) * 100;
                $eficienciahai = number_format($eficienciahai, 2, ',', '.');
            } else {
                $eficienciahai = "";
            }
            if ($ifi != 0){
                $eficienciaifi = ($correctasifi / $ifi) * 100;
                $eficienciaifi = number_format($eficienciaifi, 2, ',', '.');
            } else {
                $eficienciaifi = "";
            }
            if ($quimio != 0){
                $eficienciaquimio = ($correctasquimio / $quimio) * 100;
                $eficienciaquimio = number_format($eficienciaquimio, 2, ',', '.');
            } else {
                $eficienciaquimio = "";
            }
            if ($otras != 0){
                $eficienciaotras = ($correctasotras / $otras) * 100;
                $eficienciaotras = number_format($eficienciaotras, 2, ',', '.');
            } else {
                $eficienciaotras = "";
            }

            // presentamos el registro
            $this->Cell(10, $this->Alto - 3, "", 0, 0, 'L');
            $this->Cell(60, $this->Alto - 3, $provincia, 1, 0, 'L');
            $this->Cell(20, $this->Alto - 3, $eficienciaap, 1, 0, 'C');
            $this->Cell(20, $this->Alto - 3, $eficienciaelisa, 1, 0, 'C');
            $this->Cell(20, $this->Alto - 3, $eficienciahai, 1, 0, 'C');
            $this->Cell(20, $this->Alto - 3, $eficienciaifi, 1, 0, 'C');
            $this->Cell(20, $this->Alto - 3, $eficienciaquimio, 1, 0, 'C');
            $this->Cell(20, $this->Alto - 3, $eficienciaotras, 1, 1, 'C');

        }

        // setea la fuente
        $this->SetFont("DejaVu", "", 12);

        // insertamos un separador
        $this->Ln($this->Alto);

        // presenta la descripcion del grafico
        $texto = "El Gráfico VI se muestran los porcentajes de ";
        $texto .= "concordancia de las determinaciones realizadas para la ";
        $texto .= "Jurisdicción clasificados según la Técnica empleada.";
        $this->MultiCell(0, $this->Alto, $texto, 0, 'J');

        // insertamos un salto de página
        $this->AddPage();

        // inserta el titulo del grafico
        $texto = "Gráfico X: % de Concordancia segun Técnica";
        $this->MultiCell(0, $this->Alto, $texto, 0, 'J');

        // inserta un salto de línea
        $this->Ln($this->Alto);

        // creamos el vector de datos
        $datos = array(array("AP", $eficienciaap),
                       array("ELISA", $eficienciaelisa),
                       array("HAI", $eficienciahai),
                       array("IFI", $eficienciaifi),
                       array("QUIMIO", $eficienciaquimio),
                       array("OTRAS", $eficienciaotras));

        // establecemos el tamaño
        $tamanio = array("Ancho" => 750,
                         "Alto" => 600);

        // fijamos los valores en la clase
        $this->Imagenes->setDatos($datos);
        $this->Imagenes->setTitulo("% Concordancia por Técnica");
        $this->Imagenes->setTamanio($tamanio);
        $this->Imagenes->setArchivo("eficiencia.png");

        // graficamos los totales
        $this->Imagenes->barrasVerticales();

        // lo agregamos al documento
        $this->Image("../../temp/eficiencia.png", 10, 50);

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Presenta el análisis de los falsos positivos (muestras
     * negativas que fueron evaluadas como positivas)
     */
    protected function falsosPositivos(){

        // agregamos la página
        $this->AddPage('P');

        // fijamos la fuente
        $this->SetFont("DejaVu", "B", 14);

        // presenta el título
        $this->MultiCell(0, $this->Alto, "Análisis de Falsos Positivos", 0, 'L');

        // fijamos la fuente normal
        $this->SetFont("DejaVu", "", 12);

        // inserta un separador
        $this->Ln($this->Alto);

        // presenta el texto
        $texto = "En Tabla XII se observa el porcentaje de ";
        $texto .= "Falsos Positivos segun la Tecnica empleada ";
        $texto .= "en la Jurisdiccion (Nota: los valores son calculados a partir ";
        $texto .= "del numero total de muestras procesadas que fueron reportadas ";
        $texto .= "por los Laboratorios participantes como Reactivas, es decir, ";
        $texto .= "se considera como falso positivo a aquella muestra No ";
        $texto .= "Reactiva que es informada como Reactiva o Indeterminada, para ";
        $texto .= "mayor informacion sobre la metodologia utilizada, puede ";
        $texto .= "consultar el Glosario al final de este documento).";
        $this->MultiCell(0, $this->Alto, $texto, 0, 'J');

        // inserta un separador
        $this->Ln($this->Alto);

        // presenta el titulo de la tabla
        $texto = "Tabla XII: % de Falsos Positivos segun Tecnica";
        $this->MultiCell(0, $this->Alto, $texto, 0, 'J');

        // inserta un salto
        $this->Ln($this->Alto);

        // obtenemos la matriz de datos
        $errores = $this->Informe->falsosPositivos($this->Anio);

        // fija la fuente
        $this->SetFont("DejaVu", "B", 10);

        // presentamos los titulos
        $this->Cell(10, $this->Alto, "", 0, 0, 'L');
        $this->Cell(60, $this->Alto, "Jurisdicción", 1, 0, 'L');
        $this->Cell(20, $this->Alto, "AP", 1, 0, 'C');
        $this->Cell(20, $this->Alto, "ELISA", 1, 0, 'C');
        $this->Cell(20, $this->Alto, "HAI", 1, 0, 'C');
        $this->Cell(20, $this->Alto, "IFI", 1, 0, 'C');
        $this->Cell(20, $this->Alto, "QUIMIO", 1, 0, 'C');
        $this->Cell(20, $this->Alto, "OTRAS", 1, 1, 'C');

        // fija la fuente
        $this->SetFont("DejaVu", "", 10);

        // recorremos el array agregando los valores
        foreach ($errores AS $registro) {

            // obtenemos el registro
            extract($registro);

            // calculamos los porcentajes de cada uno verificando
            // no estar dividiendo por cero
            if ($ap != 0){
                $porcentajeap = ($falsosap / $ap) * 100;
                $porcentajeap = number_format($porcentajeap, 2, ',', '.');
            } else {
                $porcentajeap = "";
            }
            if ($elisa != 0){
                $porcentajeelisa = ($falsoselisa / $elisa) * 100;
                $porcentajeelisa = number_format($porcentajeelisa, 2, ',', '.');
            } else {
                $porcentajeelisa = "";
            }
            if ($hai != 0){
                $porcentajehai = ($falsoshai / $hai) * 100;
                $porcentajehai = number_format($porcentajehai, 2, ',', '.');
            } else {
                $porcentajehai = "";
            }
            if ($ifi != 0){
                $porcentajeifi = ($falsosifi / $ifi) * 100;
                $porcentajeifi = number_format($porcentajeifi, 2, ',', '.');
            } else {
                $porcentajeifi = "";
            }
            if ($quimio != 0){
                $porcentajequimio = ($falsosquimio / $quimio) * 100;
                $porcentajequimio = number_format($porcentajequimio, 2, ',', '.');
            } else {
                $porcentajequimio = "";
            }
            if ($otras != 0){
                $porcentajeotras = ($falsosotras / $otras) * 100;
                $porcentajeotras = number_format($porcentajeotras, 2, ',', '.');
            } else {
                $porcentajeotras = "";
            }

            // presentamos el registro
            $this->Cell(10, $this->Alto - 3, "", 0, 0, 'L');
            $this->Cell(60, $this->Alto - 3, $provincia, 1, 0, 'L');
            $this->Cell(20, $this->Alto - 3, $porcentajeap, 1, 0, 'C');
            $this->Cell(20, $this->Alto - 3, $porcentajeelisa, 1, 0, 'C');
            $this->Cell(20, $this->Alto - 3, $porcentajehai, 1, 0, 'C');
            $this->Cell(20, $this->Alto - 3, $porcentajeifi, 1, 0, 'C');
            $this->Cell(20, $this->Alto - 3, $porcentajequimio, 1, 0, 'C');
            $this->Cell(20, $this->Alto - 3, $porcentajeotras, 1, 1, 'C');

        }

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Analizamos los falsos negativos (muestras reactivas bajas
     * o altas que fueron informadas como negativas)
     */
    protected function falsosNegativos(){

        // agregamos la página
        $this->AddPage('P');

        // fijamos la fuente
        $this->SetFont("DejaVu", "B", 14);

        // presenta el título
        $this->MultiCell(0, $this->Alto, "Análisis de Falsos Negativos", 0, 'L');

        // fijamos la fuente normal
        $this->SetFont("DejaVu", "", 12);

        // inserta separador
        $this->Ln($this->Alto);

        // presenta el texto
        $texto = "En la Tabla XIII se observa el porcentaje de ";
        $texto .= "Falsos Negativos según la Técnica empleada en ";
        $texto .= "la Jurisdiccion (Nota: los valores son calculados a partir ";
        $texto .= "del numero total de muestras procesadas que fueron reportadas ";
        $texto .= "por los Laboratorios participantes como No Reactivas, es decir, ";
        $texto .= "se considera como falso negativo a aquella muestra ";
        $texto .= "Reactiva o Indeterminada que es informada como No Reactiva, para ";
        $texto .= "mayor informacion sobre la metodologia utilizada, puede ";
        $texto .= "consultar el Glosario al final de este documento).";
        $this->MultiCell(0, $this->Alto, $texto, 0, 'J');

        // inserta un separador
        $this->Ln($this->Alto);

        // presenta el titulo de la tabla
        $texto = "Tabla XIII: % de Falsos Negativos segun Tecnica";
        $this->MultiCell(0, $this->Alto, $texto, 0, 'J');

        // inserta un salto
        $this->Ln($this->Alto);

        // obtenemos la matriz de datos
        $errores = $this->Informe->falsosNegativos($this->Anio);

        // fija la fuente
        $this->SetFont("DejaVu", "B", 10);

        // presentamos los titulos
        $this->Cell(10, $this->Alto, "", 0, 0, 'L');
        $this->Cell(60, $this->Alto, "Jurisdicción", 1, 0, 'L');
        $this->Cell(20, $this->Alto, "AP", 1, 0, 'C');
        $this->Cell(20, $this->Alto, "ELISA", 1, 0, 'C');
        $this->Cell(20, $this->Alto, "HAI", 1, 0, 'C');
        $this->Cell(20, $this->Alto, "IFI", 1, 0, 'C');
        $this->Cell(20, $this->Alto, "QUIMIO", 1, 0, 'C');
        $this->Cell(20, $this->Alto, "OTRAS", 1, 1, 'C');

        // fija la fuente
        $this->SetFont("DejaVu", "", 10);

        // recorremos el array agregando los valores
        foreach ($errores AS $registro) {

            // obtenemos el registro
            extract($registro);

            // calculamos los porcentajes de cada uno verificando
            // no estar dividiendo por cero
            if ($ap != 0){
                $porcentajeap = ($falsosap / $ap) * 100;
                $porcentajeap = number_format($porcentajeap, 2, ',', '.');
            } else {
                $porcentajeap = "";
            }
            if ($elisa != 0){
                $porcentajeelisa = ($falsoselisa / $elisa) * 100;
                $porcentajeelisa = number_format($porcentajeelisa, 2, ',', '.');
            } else {
                $porcentajeelisa = "";
            }
            if ($hai != 0){
                $porcentajehai = ($falsoshai / $hai) * 100;
                $porcentajehai = number_format($porcentajehai, 2, ',', '.');
            } else {
                $porcentajehai = "";
            }
            if ($ifi != 0){
                $porcentajeifi = ($falsosifi / $ifi) * 100;
                $porcentajeifi = number_format($porcentajeifi, 2, ',', '.');
            } else {
                $porcentajeifi = "";
            }
            if ($quimio != 0){
                $porcentajequimio = ($falsosquimio / $quimio) * 100;
                $porcentajequimio = number_format($porcentajequimio, 2, ',', '.');
            } else {
                $porcentajequimio = "";
            }
            if ($otras != 0){
                $porcentajeotras = ($falsosotras / $otras) * 100;
                $porcentajeotras = number_format($porcentajeotras, 2, ',', '.');
            } else {
                $porcentajeotras = "";
            }

            // presentamos el registro
            $this->Cell(10, $this->Alto - 3, "", 0, 0, 'L');
            $this->Cell(60, $this->Alto - 3, $provincia, 1, 0, 'L');
            $this->Cell(20, $this->Alto - 3, $porcentajeap, 1, 0, 'C');
            $this->Cell(20, $this->Alto - 3, $porcentajeelisa, 1, 0, 'C');
            $this->Cell(20, $this->Alto - 3, $porcentajehai, 1, 0, 'C');
            $this->Cell(20, $this->Alto - 3, $porcentajeifi, 1, 0, 'C');
            $this->Cell(20, $this->Alto - 3, $porcentajequimio, 1, 0, 'C');
            $this->Cell(20, $this->Alto - 3, $porcentajeotras, 1, 1, 'C');

        }

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Analizamos la concordancia según el reactivo utilizado
     */
    protected function concordanciaReactivos(){

        // agregamos la página
        $this->AddPage('P');

        // fijamos la fuente
        $this->SetFont("DejaVu", "B", 14);

        // presenta el título
        $this->MultiCell(0, $this->Alto, "Concordancia Según Reactivo", 0, 'L');

        // fijamos la fuente normal
        $this->SetFont("DejaVu", "", 12);

        // presenta el texto
        $texto = "A continuación, en las siguientes tablas para la ";
        $texto .= "Jurisdicción en donde se analiza la concordancia según la Técnica ";
        $texto .= "y la Marca de los reactivos empleados en las determinaciones ";
        $texto .= "cargadas por los Laboratorios participantes.";
        $this->MultiCell(0, $this->Alto, $texto, 0, 'J');

        // obtenemos la nómina de técnicas registradas
        $nomina = $this->Informe->nominaTecnicas();

        // definimos el contador de tablas
        $nroTabla = 14;
        $TablaLetras = "";

        // recorremos el vector
        foreach ($nomina AS $registro){

            // obtenemos el registro
            extract($registro);

            // obtenemos la eficiencia para esa técnica
            $eficiencia = $this->Informe->eficienciaMarcas($this->Anio, $tecnica);

            // si hubo determinaciones para esa técnica
            // y si hubo mas de una marca utilizada
            if (count($eficiencia) > 1){

                // según el número de tabla
                switch ($nroTabla){
                     case 14:
                        $TablaLetras = "XIV";
                        break;
                     case 15:
                        $TablaLetras = "XVI";
                        break;
                     case 16:
                        $TablaLetras = "XVII";
                        break;
                     case 17:
                        $TablaLetras = "XVIII";
                        break;
                     case 18:
                        $TablaLetras = "XIX";
                        break;
                }

                // insertamos un separador
                $this->Ln($this->Alto);

                // presentamos el título
                $texto = "$TablaLetras : Concordancia $tecnica según Reactivo Utilizado";
                $this->MultiCell(0, $this->Alto, $texto, 0, 'L');

                // definimos la fuente de los títulos
                $this->SetFont("DejaVu", "B", 10);

                // presenta los encabezados de columna
                $this->Cell(20, $this->Alto, "", 0, 0, 'L');
                $this->Cell(70, $this->Alto, "Marca", 1, 0, 'L');
                $this->Cell(40, $this->Alto, "Lecturas", 1, 0, 'C');
                $this->Cell(40, $this->Alto, "Eficiencia", 1, 1, 'C');

                // seteamos la fuente
                $this->SetFont("DejaVu", "", 10);

                // recorremos el array agregando los valores
                foreach($eficiencia AS $elemento) {

                    // obtenemos el registro
                    extract($elemento);

                    // presenta el registro
                    $this->Cell(20, $this->Alto - 3, "", 0, 0, 'L');
                    $this->Cell(70, $this->Alto - 3, $marca, 1, 0, 'L');
                    $this->Cell(40, $this->Alto - 3, number_format($determinaciones, 0, ',', '.'), 1, 0, 'C');

                    // calculamos la eficiencia
                    $eficiencia = ($correctas / $determinaciones) * 100;
                    $eficiencia = number_format($eficiencia, 2, ',', '.');
                    $this->Cell(40, $this->Alto - 3, $eficiencia, 1, 1, 'C');

                }

            }

        }

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Presenta la grilla con los valores de corte mal informados
     * (generalmente nulos o vacíos)
     */
    protected function valoresInformados(){

        // agregamos la página
        $this->AddPage('P');

        // fijamos la fuente
        $this->SetFont("DejaVu", "B", 14);

        // presenta el título
        $this->MultiCell(0, $this->Alto, "Valores de Corte y Lectura incorrectamente informados", 0, 'L');

        // fijamos la fuente normal
        $this->SetFont("DejaVu", "", 12);

        // presenta la introduccion
        $texto = "Cuando hablamos de Control de Calidad, no podemos referirnos ";
        $texto .= "exclusivamente a la concordancia de las determinaciones realizadas, ";
        $texto .= "el concepto de Calidad contiene al de concordancia, pero es al mismo ";
        $texto .= "tiempo mucho más abarcativo, debiendo incluir otros aspectos como el ";
        $texto .= "control de los procesos.";
        $this->MultiCell(0, $this->Alto, $texto, 0, 'J');

        // continuamos
        $texto = "De tal forma, en este apartado se consideraran los valores ";
        $texto .= "de Corte y Lectura que han sido incorrectamente informados ";
        $texto .= "por los Laboratorios participantes, sin tener en cuenta la ";
        $texto .= "concordancia de los resultados obtenidos (Nota: los casos de ";
        $texto .= "Determinaciones Elisa mediante el Metodo Visual son ignorados ";
        $texto .= "en este apartado).";
        $this->MultiCell(0, $this->Alto, $texto, 0, 'J');

        // terminamos
        $texto = "Se reportan como valores de Corte o Lectura mal informados ";
        $texto .= "los enumerados en la Tabla XX:";
        $this->MultiCell(0, $this->Alto, $texto, 0, 'J');

        // inserta un separador
        $this->Ln($this->Alto);

        // presenta el título de la tabla
        $texto = "Tabla XX: Evaluación de Valores de Corte y Lectura";
        $this->MultiCell(0, $this->Alto, $texto, 0, 'J');

        // inserta un separador
        $this->Ln($this->Alto);

        // setea la fuente
        $this->SetFont("DejaVu", "B", 10);

        // definimos los títulos de columna
        $this->Cell(20, $this->Alto, "", 0, 0, 'L');
        $this->Cell(35, $this->Alto, "", 1, 0, 'L');
        $this->Cell(35, $this->Alto, "Reactiva", 1, 0, 'C');
        $this->Cell(35, $this->Alto, "No Reactiva", 1, 0, 'C');
        $this->Cell(40, $this->Alto, "Indeterminada", 1, 1, 'C');

        // presenta la primera fila
        $this->Cell(20, $this->Alto, "", 0, 0, 'L');
        $this->Cell(35, $this->Alto, "Corte", 1, 0, 'L');
        $this->Cell(35, $this->Alto, "Ausente", 1, 0, 'C');
        $this->Cell(35, $this->Alto, "Ausente", 1, 0, 'C');
        $this->Cell(40, $this->Alto, "Ausente", 1, 1, 'C');

        // presenta la segunda fila
        $this->Cell(20, $this->Alto, "", 0, 0, 'L');
        $this->Cell(35, $this->Alto, "Lectura", 1, 0, 'L');
        $this->Cell(35, $this->Alto, "Ausente", 1, 0, 'C');
        $this->Cell(35, $this->Alto, "", 1, 0, 'C');
        $this->Cell(40, $this->Alto, "", 1, 1, 'C');

        // obtenemos la matriz
        $erroresCorte = $this->Informe->valoresCorte($this->Anio);

        // fijamos la fuente
        $this->SetFont("DejaVu", "", 12);

        // inserta un espacio
        $this->Ln($this->Alto);

        // si por casualidad no hubo errores ni de corte ni de lectura
        if (count($erroresCorte) == 0){

        	// presenta el texto
        	$texto = "Según los criterios establecidos en este punto, durante el ";
        	$texto .= "período informado no se han presentado errores de corte o ";
            $texto .= "de lectura en ninguno de las determinaciones realizadas.";
            $this->MultiCell(0, $this->Alto, $texto, 0, 'J');

        // si hubo errores
        } else {

	        // presenta el texto
	        $texto = "Segun los criterios previamente establecidos, la ";
	        $texto .= "Tabla XXI nos muestra los valores de Corte y Lectura que han ";
            $texto .= "sido incorrectamente informados clasificados por Jurisdiccion:";
            $this->MultiCell(0, $this->Alto, $texto, 0, 'J');

            // inserta un separador
            $this->Ln($this->Alto);

	        // presenta el titulo de la tabla
	        $texto = "Tabla XXI: Valores de Corte y Lectura mal Informados";
            $this->MultiCell(0, $this->Alto, $texto, 0, 'J');

            // inserta un espacio
            $this->Ln($this->Alto);

            // fijamos la fuente
            $this->SetFont("DejaVu", "B", 10);

            // presentamos los titulos
            $this->Cell(20, $this->Alto, "", 0, 0, 'L');
            $this->Cell(50, $this->Alto, "Jurisdicción", 1, 0, 'L');
            $this->Cell(30, $this->Alto, "Errores Corte", 1, 0, 'C');
	        $this->Cell(35, $this->Alto, "Errores Lectura", 1, 1, 'C');

            // fijamos la fuente
            $this->SetFont("DejaVu", "", 10);

	        // recorremos el array agregando los valores
	        foreach ($erroresCorte AS $registro) {

                // obtenemos el registro
                extract($registro);

                // si la jurisdicción tuvo errores de corte o lectura
                if ($malcorte != 0 || $mallectura != 0){

                    // presenta el registro
                    $this->Cell(20, $this->Alto, "", 0, 0, 'L');
                    $this->Cell(50, $this->Alto - 3, $jurisdiccion, 1, 0, 'L');
                    $this->Cell(30, $this->Alto - 3, $malcorte, 1, 0, 'C');
                    $this->Cell(35, $this->Alto - 3, $mallectura, 1, 1, 'C');

                }

	        }

        }

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Analizamos las muestras huérfanas (etiquetas informadas
     * que no se encuentran declaradas en el sistema, es decir
     * errores de tipeo al ingresar el número de muestra)
     */
    protected function muestrasHuerfanas(){

        // agregamos la página
        $this->AddPage('P');

        // fijamos la fuente
        $this->SetFont("DejaVu", "B", 14);

        // presenta el título
        $this->MultiCell(0, $this->Alto, "Etiquetas mal Informadas", 0, 'L');

        // fijamos la fuente normal
        $this->SetFont("DejaVu", "", 12);

        // presenta la descripcion
        $texto = "Como fue citado en el apartado anterior en el Sistema de Control de ";
        $texto .= "Calidad Externo se plantea como una evaluación de los procesos en situaciones ";
        $texto .= "cotidianas de trabajo para cada uno de los laboratorios participantes, las ";
        $texto .= "muestras enviadas por este Instituto contienen un número de serie que puede ";
        $texto .= "ser asimilado al número de protocolo, la falta de informacion de este en la carga ";
        $texto .= "se considera como una etiqueta huerfana.";
        $this->MultiCell(0, $this->Alto, $texto, 0, 'J');

        // continuamos
        $texto = "La plataforma cuenta con trazabilidad de las muestras enviadas, ";
        $texto .= "de tal forma, resulta posible identificar los laboratorios ";
        $texto .= "que no han cargado correctamente el número de las mismas. ";
        $this->MultiCell(0, $this->Alto, $texto, 0, 'J');

        // inserta un espacio
        $this->Ln($this->Alto);

        // obtenemos la nómina de huérfanas
        $huerfanas = $this->Informe->etiquetasHuerfanas($this->Anio);

        // según el número de huérfanas
        if ($huerfanas == 0){
            $texto = "Para el Operativo Informado la Jurisdicción de $this->Jurisdiccion ";
            $texto .= "no presenta etiquetas huérfanas (mal cargadas).";
        } else {
            $texto = "En el Operativo Informado la Jurisdicción de $this->Jurisdiccion ";
            $texto .= "presenta $huerfanas etiquetas huérfanas (mal cargadas).";
        }

        // agrega el texto
        $this->MultiCell(0, $this->Alto, $texto, 0, 'J');

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Análisis de discordantes, es decir, muestras que fueron
     * analizadas por dos o mas técnicas y cuyos valores informados
     * no son homogéneos
     */
    protected function discordantes(){

        // agregamos la página
        $this->AddPage('P');

        // fijamos la fuente
        $this->SetFont("DejaVu", "B", 14);

        // presenta el título
        $this->MultiCell(0, $this->Alto, "Muestras Discordantes", 0, 'L');

        // fijamos la fuente normal
        $this->SetFont("DejaVu", "", 12);

        // inserta un separador
        $this->Ln($this->Alto);

        // presenta la introducción
        $texto = "Ya se ha mencionado anteriormente en este documento el concepto de ";
        $texto .= "simulación, es decir, no se contempla el Programa Nacional de Control ";
        $texto .= "de Calidad Externo como un simple análisis de concordancia entre la ";
        $texto .= "muestra control enviada y el resultado obtenido.";
        $this->MultiCell(0, $this->Alto, $texto, 0, 'J');

        // insertamos un salto
        $this->Ln($this->Alto);

        // seguimos presentando
        $texto = "El concepto de simulación implica intentar replicar de la forma mas fiel ";
        $texto .= "posible las situaciones cotidianas de trabajo en un laboratorio dado y ";
        $texto .= "a partir de las incidencias reportadas, realizar una revisión de los ";
        $texto .= "procesos de trabajo y controles internos a fin de mejorar la calidad ";
        $texto .= "en un laboratorio determinado (entendiendo Calidad como un proceso de ";
        $texto .= "revisión y mejora continua).";
        $this->MultiCell(0, $this->Alto, $texto, 0, 'J');

        // insertamos un salto
        $this->Ln($this->Alto);

        // seguimos
        $texto = "Es en este aspecto que cobra sentido el análisis de los Falsos Positivos ";
        $texto .= "los Falsos Negativos, la concordancia según Técnica Diagnóstica Empleada, ";
        $texto .= "el análisis de concordancia de acuerdo al reactivo utilizado, etc.";
        $this->MultiCell(0, $this->Alto, $texto, 0, 'J');

        // insertamos un salto
        $this->Ln($this->Alto);

        // terminamos
        $texto = "En este sentido realizamos en este apartado un análisis de resultados ";
        $texto .= "discordantes, es decir, muestras que habiendose realizado mas de una ";
        $texto .= "determinación en ellas (seguimiento a través del número de etiqueta) ";
        $texto .= "ambas determinaciones son discordantes, es decir, arrojan resultados ";
        $texto .= "distintos (no se considera en este apartado la concordancia o no con ";
        $texto .= "la muestra control ya que si los resultados son distintos, obviamente ";
        $texto .= "uno de ellos será correcto y el otro incorrecto o no concordante.";
        $this->MultiCell(0, $this->Alto, $texto, 0, 'J');

        // insertamos un salto
        $this->Ln($this->Alto);

        // párrafo final
        $texto = "El sentido de este análisis es evidente desde la mirada sanitaria, ya ";
        $texto .= "que en una situación real de diagnóstico, estos datos son los que ";
        $texto .= "obligarían a realizar nuevamente la determinación, o mas difícil aún ";
        $texto .= "rastrear al paciente para obtener una nueva muestra.";
        $this->MultiCell(0, $this->Alto, $texto, 0, 'J');

        // inserta un separador
        $this->Ln($this->Alto);

        // presenta la descripción de la tabla
        $texto = "En la siguiente Tabla se presentan las frecuencias y los porcentajes ";
        $texto .= "de muestras discordantes para la Jurisdicción informada.";
        $this->MultiCell(0, $this->Alto, $texto, 0, 'J');

        // inserta un salto de página
        $this->AddPage();

        // presenta el título de la tabla
        $texto = "Tabla XXII - Muestras Discordantes";

        // inserta un separador
        $this->Ln($this->Alto);

        // seteamos la fuente
        $this->SetFont("DejaVu", "B", 10);

        // presenta los encabezados
        $this->Cell(20, $this->Alto, "", 0, 0, 'C');
        $this->Cell(60, $this->Alto, "Jurisdicción", 1, 0, 'L');
        $this->Cell(40, $this->Alto, "Determinaciones", 1, 0, 'C');
        $this->Cell(40, $this->Alto, "Discordantes", 1, 0, 'C');
        $this->Cell(20, $this->Alto, "%", 1, 1, 'C');

        // fijamos la fuente
        $this->SetFont("DejaVu", "", 10);

        // obtenemos la nómina de discordantes
        $discordantes = $this->Informe->discordantes($this->Anio);

        // inicializamos el array que usamos para los gráficos
        $nominadiscordantes = array();

        // recorremos el array agregando los valores
        foreach($discordantes AS $registro => $datos) {

            // agregamos las columnas (centradas las numericas)
            $this->Cell(20, $this->Alto - 3, "", 0, 0, 'L');
            $this->Cell(60, $this->Alto - 3, $registro, 1, 0, 'L');

            $this->Cell(40, $this->Alto - 3, $datos["determinaciones"], 1, 0, 'C');

            // si hubo discordantes
            if (array_key_exists('noconcordantes', $datos)) {

                // presenta el número de discordantes
                $this->Cell(40, $this->Alto - 3, $datos["noconcordantes"], 1, 0, 'C');

                // presenta el porcentaje
                $porcentaje = ($datos["noconcordantes"]/$datos["determinaciones"]) * 100;
                $porcentaje = round($porcentaje, 2);
                $this->Cell(20, $this->Alto - 3, $porcentaje, 1, 1, 'C');

                // agrega al array de gráficos los datos
                $nominadiscordantes[] = array($registro, $porcentaje);

            // si no hubo discordantes
            } else {

                // presenta las celdas en blanco
                $this->Cell(40, $this->Alto - 3, "0", 1, 0, 'C');
                $this->Cell(20, $this->Alto - 3, "0.00", 1, 1, 'C');

            }

        }

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Presenta el análisis estadístico de la muestra (media,
     * desvío estandar, error estandar)
     */
    public function analisisEstadistico(){

        // agregamos la página
        $this->AddPage('P');

        // fijamos la fuente
        $this->SetFont("DejaVu", "B", 14);

        // presenta el título
        $this->MultiCell(0, $this->Alto, "Análisis Estadístico", 0, 'L');

        // fijamos la fuente normal
        $this->SetFont("DejaVu", "", 12);

        // presenta el texto
        $texto = "Dado que cada Jurisdiccion presenta un numero diferente ";
        $texto .= " de laboratorios, y no todos los laboratorios han realizado el ";
        $texto .= "mismo numero de determinaciones, se hace necesario ";
        $texto .= "establecer algun tipo de indicador que permita analizar ";
        $texto .= "la muestra.";
        $this->MultiCell(0, $this->Alto, $texto, 0, 'J');

        // sigue presentando
        $texto = "Se utilizará entonces un indicador básico que es el número ";
        $texto .= "de determinaciones correctas realizadas en cada laboratorio ";
        $texto .= "sobre el número total de determinaciones realizadas, ";
        $texto .= "obteniendo de esta forma un 'índice de eficiencia' que ";
        $texto .= "permitirá realizar el análisis estadístico (este índice ";
        $texto .= "tendrá una amplitud desde cero (0) => todas las ";
        $texto .= "determinaciones incorrectas, hasta uno (1) => todas las ";
        $texto .= "determinaciones correctas, si desea puede consultar el ";
        $texto .= "Glosario, al final de este documento, en donde se realiza ";
        $texto .= "un análisis mas detallado de las herramientas estadísticas ";
        $texto .= "utilizadas).";
        $this->MultiCell(0, $this->Alto, $texto, 0, 'J');

        // inserta un salto
        $this->Ln($this->Alto);

        // presenta el titulo de la tabla
        $texto = "Tabla XXIII: Indice de Concordancia por Laboratorio";
        $this->MultiCell(0, $this->Alto, $texto, 0, 'J');

        // inserta un separador
        $this->Ln($this->Alto);

        // obtenemos la matriz de concordancia a partir de la cual
        // calculamos la eficiencia por jurisdicción
        $eficiencia = $this->Informe->obtenerEficiencia($this->Anio);

        // fijamos la fuente
        $this->SetFont("DejaVu", "B", 10);

        // presentamos los titulos
        $this->Cell(120, $this->Alto, "Laboratorio", 1, 0, 'L');
        $this->Cell(25, $this->Alto, "N° Determ.", 1, 0, 'C');
        $this->Cell(25, $this->Alto, "Correctas", 1, 0, 'C');
        $this->Cell(20, $this->Alto, "Indice", 1, 1, 'C');

        // fijamos la fuente
        $this->SetFont("DejaVu", "", 10);

        // definimos el array que usaremos para graficar los
        // datos y calcular media y desvío
        $nomina = array();

        // recorremos el array agregando los valores
        foreach($eficiencia AS $registro){

            // obtenemos el registro
            extract($registro);

            // si hubo determinaciones
            if ($determinaciones != 0){

                // agregamos el registro
                $this->Cell(120, $this->Alto - 3, $laboratorio, 1, 0, 'L');
                $this->Cell(25, $this->Alto - 3, $determinaciones, 1, 0, 'C');
                $this->Cell(25, $this->Alto - 3, $correctas, 1, 0, 'C');

                // calculamos el índice
                $indice = round(($correctas / $determinaciones), 2);
                $this->Cell(20, $this->Alto - 3, $indice, 1, 1, 'C');

                // agregamos los valores al array
                $nomina[] = array($laboratorio, $indice);

            }

        }

        // obtenemos la media, el desvio estandard y el error estandar
        $media = $this->Analisis->calculaMedia($nomina, 1);
        $desvio = $this->Analisis->calculaDesvio($nomina, 1);
        $error = $this->Analisis->calculaError($nomina, 1);

        // insertamos el separador
        $this->Ln($this->Alto);

        // fijamos la fuente
        $this->SetFont("DejaVu", "", 12);

        // presenta el texto
        $texto = "Esta distribucion arroja una media de $media, un ";
        $texto .= "Desvio Estandard de $desvio y un Error Estandard ";
        $texto .= "de $error para la muestra analizada.";
        $this->MultiCell(0, $this->Alto, $texto, 0, 'J');

        // verificamos si alguna laboratorio está por debajo de un desvío
        $nomina = $this->Informe->rendimientoMuestra($nomina, $media, $desvio);

        // insertamos un salto de página
        $this->AddPage();

        // si bien siempre tiene que haber algún laboratorio que se encuentre
        // por debajo de un desvío por las dudas verificamos
        if (count($nomina) != 0){

            // insertamos un salto de página
            $this->AddPage();

	        // presentamos el texto
	        $texto = "Del análisis realizado se desprende que ";

	        // si hubo una sola jurisdicción
	        if (count($nomina) == 1){

	        	// componemos en singular
		       $exto += "el Laboratorio ";

		    // si hubo mas de una
	        } else {

	        	// componemos en plural
	        	$exto += "los laboratorios ";

	        }

	        // recorremos el array
	        foreach($nomina AS $registro) {

                // obtenemos el registro
                extract($registro);

	        	// agregamos el elemento
	        	$texto += $laboratorio . ", ";

	        }

	        // ahora verificamos de nuevo la longitud
	        if (count($nomina) == 1){
	        	$exto += "se encontró ";
	        } else {
	        	$texto += "se encontraron ";
	        }

	        // terminamos de armar el texto
	        $texto += "por debajo de un desvío estandard ";
	        $texto += "(5% inferior de rendimiento). Los demás Laboratorios ";
	        $texto += "participantes del CCE presentaron una performance adecuada ";
	        $texto += "con un índice de eficiencia dentro del rango establecido.";

        // si no hubo ninguna
        } else {

            // componemos el texto
            $texto = "No se ha encontrado ningún Laboratorio con un rendimiento ";
            $texto .= "por debajo de un Desvío Standard (5% inferior de ";
            $texto .= "eficiencia).";

        }

        // agregamos el texto
        $this->MultiCell(0, $this->Alto, $texto, 0, 'J');

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Realiza un anàlisis comparativo de los resultados generales
     * de los últimos diez operativos
     */
    protected function analisisHistorico(){

        // agregamos la página
        $this->AddPage('P');

        // fijamos la fuente
        $this->SetFont("DejaVu", "B", 14);

        // presenta el título
        $this->MultiCell(0, $this->Alto, "Análisis Histórico", 0, 'L');

        // fijamos la fuente normal
        $this->SetFont("DejaVu", "", 12);

        // presenta el texto
        $texto = "Independientemente del análisis que se pueda realizar sobre ";
        $texto .= "el desempeño en un período de tiempo dado o en un operativo ";
        $texto .= "en particular, este tipo de análisis también presenta un ";
        $texto .= "factor espúreo ya que solo nos permite establecer el en un ";
        $texto .= "momento determinado de tiempo, sin poder realizar ninguna ";
        $texto .= "correlación con períodos u operativos anteriores.";
        $this->MultiCell(0, $this->Alto, $texto, 0, 'J');

        // continua
        $texto = "Es por ello que a fin de poder determinar variaciones en el ";
        $texto .= "rendimiento que pueden tener su origen en distintas fuentes ";
        $texto .= "(acciones de capacitación o variaciones estacionales), se ";
        $texto .= "presenta a continuación la Tabla XXIV y el Gráfico XIV con ";
        $texto .= "un análisis comparativo de los últimos diez (10) operativos ";
        $texto .= "realizados, considerando el total de determinaciones efectuadas, ";
        $texto .= "el número de resultados correctos y el promedio de eficiencia para ";
        $texto .= "cada Operativo consignado.";
        $this->MultiCell(0, $this->Alto, $texto, 0, 'J');

        // insertamos un separador
        $this->Ln($this->Alto);

        // presenta el titulo de la tabla
        $exto = "Tabla XXIV: Indice de Eficacia por Operativos";
        $this->MultiCell(0, $this->Alto, $texto, 0, 'J');

        // insertamos un separador
        $this->Ln($this->Alto);

        // obtenemos la matriz de eficacia
        $historia = $this->Informe->historiaEficiencia();

        // establecemos la fuente
        $this->SetFont("DejaVu", "B", 10);

        // presentamos los titulos
        $this->Cell(30, $this->Alto, "", 0, 0, 'C');
        $this->Cell(30, $this->Alto, "Operativo", 1, 0, 'L');
        $this->Cell(20, $this->Alto, "N° Det.", 1, 0, 'C');
        $this->Cell(25, $this->Alto, "Correctas", 1, 0, 'C');
        $this->Cell(20, $this->Alto, "Eficacia", 1, 1, 'C');

        // establecemos la fuente
        $this->SetFont("DejaVu", "", 10);

        // definimos el array para los datos del gráfico
        $datos = array();

        // recorremos el array agregando los valores
        foreach($historia AS $registro){

            // obtenemos el registro
            extract($registro);

            // lo agregamos
            $this->Cell(30, $this->Alto, "", 0, 0, 'C');
            $this->Cell(30, $this->Alto, $anio, 1, 0, 'L');
            $this->Cell(20, $this->Alto, $determinaciones, 1, 0, 'C');
            $this->Cell(25, $this->Alto, $correctas, 1, 0, 'C');

            // calculamos la eficacia y la presentamos
            $eficacia = round(($correctas/$determinaciones), 2);
            $this->Cell(20, $this->Alto, $eficacia, 1, 1, 'C');

            // agregamos los datos a la matriz
            $datos[] = array($anio, $eficacia);

        }

        // inserta un salto de pagina
        $this->AddPage();

        // establecemos la fuente
        $this->SetFont("DejaVu", "", 12);

        // presenta el titulo del gráfico
        $texto = "Gráfico XIV: Indice de Eficacia por Operativos";
        $this->MultiCell(0, $this->Alto, $texto, 0, 'J');

        // establecemos el tamaño del gráfico
        $tamanio = array("Ancho" => 700,
                         "Alto" => 600);

        // fijamos los valores en la clase
        $this->Imagenes->setDatos($datos);
        $this->Imagenes->setTitulo("Eficiencia Interanual");
        $this->Imagenes->setTamanio($tamanio);
        $this->Imagenes->setArchivo("historia.png");

        // graficamos los totales
        $this->Imagenes->Lineas();

        // lo agregamos al documento
        $this->Image("../../temp/historia.png", 10, 50);

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Presenta el apartado de observaciones
     */
    protected function observaciones(){

        // agregamos la página
        $this->AddPage('P');

        // fijamos la fuente
        $this->SetFont("DejaVu", "B", 14);

        // presenta el título
        $this->MultiCell(0, $this->Alto, "Observaciones", 0, 'L');

        // fijamos la fuente normal
        $this->SetFont("DejaVu", "", 12);

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Presenta el glosario de tèrminos
     */
    protected function glosario(){

        // agregamos la página
        $this->AddPage('P');

        // fijamos la fuente
        $this->SetFont("DejaVu", "B", 14);

        // presenta el título
        $this->MultiCell(0, $this->Alto, "Glosario de Términos", 0, 'L');

        // fijamos la fuente normal
        $this->SetFont("DejaVu", "", 12);

        // presenta la metodología
        $this->SetFont("DejaVu", "B", 12);
        $this->MultiCell(0, $this->Alto, "Metodología:", 0, 'L');
        $this->SetFont("DejaVu", "", 12);
        $texto = "Tal como se mencionó en la primer parte del presente ";
        $texto .= "documento se enviaron a los distintos laboratorios ";
        $texto .= "participantes del Programa de Control de Calidad Externo, ";
        $texto .= "diversos paneles etiquetados con una serie numérica ";
        $texto .= "identificatoria (número de muestra).";
        $this->MultiCell(0, $this->Alto, $texto, 0, 'J');
        $texto = "El sistema diseñado en el INP - Dr. Mario Fatala Chaben ";
        $texto .= "permite no solo generar las etiquetas sino además la ";
        $texto .= "trazabilidad de las mismas de forma tal de identificar ";
        $texto .= "unívocamente cada una de las muestras y el laboratorio al ";
        $texto .= "cual se destinó la misma.";
        $this->MultiCell(0, $this->Alto, $texto, 0, 'J');
        $texto = "En una etapa posterior, se incorporará además la ";
        $texto .= "trazabilidad en la preparación de las muestras para así ";
        $texto .= "también poder identificar de forma fehaciente las condiciones ";
        $texto .= "de preparación y estabilidad de las mismas (temperatura, ";
        $texto .= "condiciones de almacenamiento, tiempo de almacenamiento, etc.).";
        $this->MultiCell(0, $this->Alto, $texto, 0, 'J');
        $texto = "Por otra parte y dado que se aspira a medir la situación ";
        $texto .= "normal de trabajo y no una eficiencia abstracta en condiciones ";
        $texto .= "de ensayo es que se ha optado que cada uno de los ";
        $texto .= "laboratorios participantes recurran a los reactivos ";
        $texto .= "utilizados en la tarea cotidiana.";
        $this->MultiCell(0, $this->Alto, $texto, 0, 'J');
        $texto = "De esta forma se obtienen dos ventajas, la primera de ellas, ";
        $texto .= "tal como se mencionó en el párrafo anterior, es que se mide ";
        $texto .= "la eficiencia de cada uno de los laboratorios en las ";
        $texto .= "condiciones habituales de trabajo, permitiendo así analizar ";
        $texto .= "los procedimientos utilizados en función de los resultados ";
        $texto .= "obtenidos y proponer entonces acciones correctivas en el ";
        $texto .= "corto plazo.";
        $this->MultiCell(0, $this->Alto, $texto, 0, 'J');
        $texto = "La segunda ventaja que esta metodología presenta es que ";
        $texto .= "permitirá en el mediano plazo, y en la medida en que se ";
        $texto .= "cuente con un volumen de datos suficiente, comenzar a ";
        $texto .= "analizar la eficiencia y eficacia de los distintos métodos ";
        $texto .= "y reactivos utilizados en el procesamiento de las muestras.";
        $this->MultiCell(0, $this->Alto, $texto, 0, 'J');

        // insertamos un separador
        $this->Ln($this->Alto);

        // agregamos el título
        $this->SetFont("DejaVu", "B", 12);
        $this->MultiCell(0, $this->Alto, "Concordancia:", 0, 'L');
        $this->SetFont("DejaVu", "", 12);

        // presenta el texto
        $texto = "El análisis general de concordancia toma en consideración ";
        $texto .= "el número total de determinaciones realizadas y establece ";
        $texto .= "un cociente porcentual con respecto al número de ";
        $texto .= "determinaciones concordantes (positivas, negativas o ";
        $texto .= "positivas bajas) con las muestras. Este resultado es ";
        $texto .= "presentado bajo la forma de un 'porcentaje de eficiencia'.";
        $this->MultiCell(0, $this->Alto, $texto, 0, 'J');
        $texto = "Este concepto, si bien útil y sencillo de obtener tiene ";
        $texto .= "serias limitaciones, ya que valores extremos en uno u otro ";
        $texto .= "sentido, o una muestra reducida (bajo número de laboratorios ";
        $texto .= "participantes o un número pequeño de determinaciones) pueden ";
        $texto .= "afectar seriamente los resultados obtenidos.";
        $this->MultiCell(0, $this->Alto, $texto, 0, 'J');
        $texto = "Por ello, no puede ser utilizada para realizar comparaciones ";
        $texto .= "interlaboratorios, sino que debe ser considerado solamente ";
        $texto .= "como un indicador general de eficiencia.";
        $this->MultiCell(0, $this->Alto, $texto, 0, 'J');

        // insertamos un separador
        $this->Ln($this->Alto);

        // agregamos el título
        $this->SetFont("DejaVu", "B", 12);
        $this->MultiCell(0, $this->Alto, "Falsos Positivos:", 0, 'L');
        $this->SetFont("DejaVu", "", 12);

        // presenta el texto
        $texto = "Para el análisis de los falsos positivos se considera el ";
        $texto .= "número total de determinaciones realizadas en contraposición ";
        $texto .= "al número de muestras negativas que fueron evaluadas como ";
        $texto .= "positivas o positivas bajas.";
        $this->MultiCell(0, $this->Alto, $texto, 0, 'J');
        $texto = "Este indicador presenta las mismas ventajas (facilidad de ";
        $texto .= "cálculo) e inconvenientes (labilidad del resultado obtenido) ";
        $texto .= "que en el caso anterior, pero resulta útil para estimar ";
        $texto .= "el porcentaje de error en situación real.";
        $this->MultiCell(0, $this->Alto, $texto, 0, 'J');

        // inserta un separador
        $this->Ln($this->Alto);

        // agrega el título
        $this->SetFont("DejaVu", "B", 12);
        $this->MultiCell(0, $this->Alto, "Falsos Negativos:", 0, 'L');
        $this->SetFont("DejaVu", "", 12);

        // presenta el texto
        $texto = "En este análisis se ha procedido a considerar el número ";
        $texto .= "total determinaciones realizadas en función de las muestras ";
        $texto .= "positivas o positivas bajas que fueron evaluadas como ";
        $texto .= "negativas.";
        $this->MultiCell(0, $this->Alto, $texto, 0, 'J');
        $texto = "Si bien nos encontramos con la misma dificultad que en los ";
        $texto .= "dos casos anteriores, este porcentaje resulta de especial ";
        $texto .= "significación sanitaria como indicador de el error ";
        $texto .= "aproximado que podría darse en situación real.";
        $this->MultiCell(0, $this->Alto, $texto, 0, 'J');

        // inserta un separador
        $this->Ln($this->Alto);

        // presenta el título
        $this->SetFont("DejaVu", "B", 12);
        $this->MultiCell(0, $this->Alto, "Errores de Corte y Lectura:", 0, 'L');
        $this->SetFont("DejaVu", "", 12);

        // presenta el texto
        $texto = "Este porcentaje en realidad nada nos dice sobre la ";
        $texto .= "eficiencia de un laboratorio o jurisdicción en particular, ";
        $texto .= "sino que simplemente nos habla de lo que podríamos llamar ";
        $texto .= "la 'formalidad en la carga de resultados'.";
        $this->MultiCell(0, $this->Alto, $texto, 0, 'J');
        $texto = "Tal como se mencionó anteriormente en el apartado ";
        $texto .= "correspondiente, siguiendo el concepto de un Sistema de ";
        $texto .= "Control de Calidad, como una simulación de las situaciones ";
        $texto .= "diarias de trabajo y entendiendo que el apego a los ";
        $texto .= "procedimientos son una parte esencial de la Calidad, ";
        $texto .= "la correcta información de los valores de corte y lectura ";
        $texto .= "en un protocolo se convierte en un indicador de calidad.";
        $this->MultiCell(0, $this->Alto, $texto, 0, 'J');
        $texto = "En el calculo de este cociente se han considerado el ";
        $texto .= "número total de determinaciones realizadas y el número ";
        $texto .= "de las mismas en las que no se informó el valor de corte ";
        $texto .= "o lectura considerados para determinar si la muestra era ";
        $texto .= "reactiva o no (recuérdese que las determinaciones ";
        $texto .= "mediante la Técnica ELISA y método visual, no son ";
        $texto .= "consideradas en este indicador).";
        $this->MultiCell(0, $this->Alto, $texto, 0, 'J');
        $texto = "Como puede apreciarse, la concordancia de los resultados ";
        $texto .= "no tiene realación alguna con este porcentaje, y este ";
        $texto .= "responde simplemente a la 'formalidad' de informar cual es ";
        $texto .= "el criterio utilizado en la evaluación.";
        $this->MultiCell(0, $this->Alto, $texto, 0, 'J');

        // inserta un separador
        $this->Ln($this->Alto);

        // presenta el titulo
        $this->SetFont("DejaVu", "B", 12);
        $this->MultiCell(0, $this->Alto, "Etiquetas mal Informadas:", 0, 'L');
        $this->SetFont("DejaVu", "", 12);

        // presenta el texto
        $texto = "Tal como se menciono en el apartado correspondiente, el ";
        $texto .= "concepto utilizado en el Sistema de Control de Calidad ";
        $texto .= "Externo, es el de una simulacion de la realizacion de ";
        $texto .= "una determinacion de un hipotetico paciente.";
        $this->MultiCell(0, $this->Alto, $texto, 0, 'J');
        $texto = "En este sentido, y si bien la concordancia o eficiencia de ";
        $texto .= "un Laboratorio es un factor importante, al hablar de calidad ";
        $texto .= "se suman otros factores a tener en cuenta, como la forma ";
        $texto .= "correcta de informar los resultados o la carga correcta de ";
        $texto .= "los numeros de protocolo correspondientes.";
        $this->MultiCell(0, $this->Alto, $texto, 0, 'J');
        $texto = "Asi, podemos adscribir el numero de etiqueta o numero de ";
        $texto .= "muestra al numero de protocolo, en el analisis realizado ";
        $texto .= "al encontrarse mal cargado un protocolo en un caso real ";
        $texto .= "se torna imposible conocer a que paciente corresponde ";
        $texto .= "cierta determinacion.";
        $this->MultiCell(0, $this->Alto, $texto, 0, 'J');

        // inserta un separador
        $this->Ln($this->Alto);

        // presenta el titulo
        $this->SetFont("DejaVu", "B", 12);
        $this->MultiCell(0, $this->Alto, "Muestras Discordantes:", 0, 'L');
        $this->SetFont("DejaVu", "", 12);

        // la descripción

        // insertamos un separador
        $this->Ln($this->Alto);

        // presenta el titulo
        $this->SetFont("DejaVu", "B", 12);
        $this->MultiCell(0, $this->Alto, "Análisis Estadístico:", 0, 'L');

        // inserta un separador
        $this->Ln($this->Alto);

        // presenta el titulo
        $this->MultiCell(0, $this->Alto, "Media Aritmética:", 0, 'L');
        $this->SetFont("DejaVu", "", 12);

        // la descripcion
        $texto = "También llamada media o promedio. La media aritmética es el ";
        $texto .= "promedio de un conjunto de números, obtenida sumando todos ";
        $texto .= "los números y dividiéndola entre la cantidad de elementos.";
        $this->MultiCell(0, $this->Alto, $texto, 0, 'J');
        $texto = "Esta es una manera de encontrar un valor representativo de ";
        $texto .= "un conjunto de números. Una de las limitaciones de la media ";
        $texto .= "aritmética es que se trata de una medida muy sensible a los ";
        $texto .= "valores extremos; valores muy grandes tienden a aumentarla ";
        $texto .= "mientras que valores muy pequeños tienden a reducirla, ";
        $texto .= "lo que implica que puede dejar de ser representativa de ";
        $texto .= "la población.";
        $this->MultiCell(0, $this->Alto, $texto, 0, 'J');

        // inserta un separador
        $this->Ln($this->Alto);

        // presenta el titulo
        $this->SetFont("DejaVu", "B", 12);
        $this->MultiCell(0, $this->Alto, "Desvío Estandar:", 0, 'L');
        $this->SetFont("DejaVu", "", 12);

        // presenta la definicion
        $texto = "La desviación estándar es un índice numérico de la dispersión ";
        $texto .= "de un conjunto de datos (o población). Mientras mayor es la ";
        $texto .= "desviación estándar, mayor es la dispersión de la población. ";
        $texto .= "La desviación estándar es un promedio de las desviaciones ";
        $texto .= "individuales de cada observación con respecto a la media de ";
        $texto .= "una distribución. Así, la desviación estándar mide el grado ";
        $texto .= "de dispersión o variabilidad. En primer lugar, midiendo la ";
        $texto .= "diferencia entre cada valor del conjunto de datos y la media ";
        $texto .= "del conjunto de datos.";
        $this->MultiCell(0, $this->Alto, $texto, 0, 'J');
        $texto = "Estadísticamente hablando, esta es una herramienta que nos ";
        $texto .= "permite entonces realizar comparaciones inter - laboratorios ";
        $texto .= "con mucha mayor precisión que el simple análisis de ";
        $texto .= "frecuencias o porcentajes.";
        $this->MultiCell(0, $this->Alto, $texto, 0, 'J');
        $texto = "Consideremos nuestro caso particular, en donde tenemos ";
        $texto .= "distintas Jurisdicciones nacionales, con un número dispar ";
        $texto .= "de laboratorios participantes, y donde además, cada ";
        $texto .= "laboratorio puede haber procesado un número distinto de ";
        $texto .= "muestras.";
        $this->MultiCell(0, $this->Alto, $texto, 0, 'J');
        $texto = "Si nos limitásemos al análisis de frecuencias y a obtener ";
        $texto .= "un porcentaje de eficiencia, solo podríamos hablar del número ";
        $texto .= "de muestras procesadas y al porcentaje de muestras ";
        $texto .= "concordantes, pero de ninguna forma podemos realizar ";
        $texto .= "comparación alguna entre una Jurisdicción con 40 laboratorios ";
        $texto .= "que ha procesado 300 muestras y otra con 4 laboratorios que ";
        $texto .= "ha procesado 20 muestras.";
        $this->MultiCell(0, $this->Alto, $texto, 0, 'J');
        $texto = "Una primera aproximación es obtener lo que hemos denominado ";
        $texto .= "un 'Indice de Eficiencia', resultante de obtener el cociente ";
        $texto .= "entre el número de determinaciones correctas sobre el número ";
        $texto .= "de determinaciones realizadas (no dejamos de advertir que ";
        $texto .= "este es todavía un índice imperfecto pues en jurisdicciones ";
        $texto .= "con un bajo número de determinaciones una pequeña variación ";
        $texto .= "puede inducir cambios sustantivos en el índice pero se trata ";
        $texto .= "de una primera aproximación).";
        $this->MultiCell(0, $this->Alto, $texto, 0, 'J');
        $texto = "Este índice nos permitirá calcular la media o promedio de ";
        $texto .= "eficiencia Nacional para todos los laboratorios participantes ";
        $texto .= "y a partir de la misma, el desvío estandar de la muestra. ";
        $texto .= "Si además, tenemos en cuenta que estadísticamente, el 68% de ";
        $texto .= "los valores quedará incluido dentro de un desvío estandar y ";
        $texto .= "que el 96% de la muestra deberá tener un valor inferior a dos ";
        $texto .= "desvíos, podemos comenzar a caracterizar la muestra de acuerdo ";
        $texto .= "a un valor esperado en los desvios.";
        $this->MultiCell(0, $this->Alto, $texto, 0, 'J');
        $texto = "Sería deseable entonces que el desvío de la muestra fuese lo ";
        $texto .= "mas bajo posible (indicando que la muestra es homogenea), ";
        $texto .= "que los laboratorios que se encuentren una distancia de mas ";
        $texto .= "de un desvío por debajo de la media deberán ser examinados ";
        $texto .= "con mayor atención y que sería deseable que no existiese ";
        $texto .= "ningún laboratorio mas allá de dos desvíos por debajo de la ";
        $texto .= "media.";
        $this->MultiCell(0, $this->Alto, $texto, 0, 'J');

        // inserta un separador
        $this->Ln($this->Alto);

        // presenta el titulo
        $this->SetFont("DejaVu", "B", 12);
        $this->MultiCell(0, $this->Alto, "Error Estandar:", 0, 'L');
        $this->SetFont("DejaVu", "", 12);

        // presenta la definicion
        $texto = "El 'Error Estandar' puede ser definido como 'la media de los ";
        $texto .= "desvíos de una muestra' y como tal nos da una estimación del ";
        $texto .= "error de medida o incertibumbre en los datos obtenidos.";
        $this->MultiCell(0, $this->Alto, $texto, 0, 'J');
        $texto = "Debe prestarse atención al concepto de estimación, el error ";
        $texto .= "real de la muestra no puede ser determinado (pero si estimado) ";
        $texto .= "en función de los desvíos presentados en la muestra (en ";
        $texto .= "nuestro caso, las determinaciones realizadas).";
        $this->MultiCell(0, $this->Alto, $texto, 0, 'J');
        $texto = "En pocas palabras, cuando menor sea el error estandar ";
        $texto .= "obtenido, menor será el potencial de error en las mediciones ";
        $texto .= "realizadas.";
        $this->MultiCell(0, $this->Alto, $texto, 0, 'J');

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que lee el archivo con el reporte y lo almacena
     * en la base de datos
     */
    protected function grabaReporte(){

        // abrimos el archivo y lo leemos
        $reporte = fopen("../../temp/Informe.pdf", "r");
        $archivo = fread($reporte, filesize("../../temp/Informe.pdf"));
        fclose($reporte);

        // ahora llamamos la rutina
        $this->Informe->grabaReporte($archivo, $this->Anio);

    }

}

?>