<?php

/**
 *
 * Class InformeProvincial | informes/provincial/informeprovincial.class.php
 *
 * @package     CCE
 * @subpackage  Reportes
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.4.0 (29/01/2019)
 * @copyright   Copyright (c) 2019, INP
 *
 * Clase que contiene una serie de funciones estadísticas
 *
*/

// inclusion de archivos
require_once ("../../clases/conexion.class.php");
require_once ("../../clases/herramientas.class.php");

// convención para la nomenclatura de las propiedades, comienzan con una
// letra mayúscula, de tener mas de una palabra no se utilizan separadores
// y la inicial de cada palabra va en mayúscula
// para las variables recibidas como parámetro el criterio es todas en
// minúscula

// convención para la nomenclatura de los metodos, comienzan con set o get
// según asignen un valor o lo lean y luego el nombre del valor a obtener

// definicion de la clase
class InformeProvincial {

    // declaración de variables de la clase, las declaramos
    // como protected para que puedan ser heredadas pero
    // para asignarle el valor debemos crear los métodos
    protected $Link;                  // puntero a la base de datos
    protected $Pais;                  // pais del usuario activo
    protected $Funciones;             // clase de herramientas
    protected $CodProv;               // clave indec e la jurisdicción
    protected $Jurisdiccion;          // nombre de la jurisdicción

    // constructor de la clase
    function __construct (){

        // nos conectamos a la base de datos
        $this->Link = new Conexion();

        // iniciamos sesion y obtenemos el pais
        session_start();
        $this->Pais = $_SESSION["Pais"];
        $this->IdUsuario = $_SESSION["ID"];

        // cerramos la sesión
        session_write_close();

        // iniciamos la clase de herramientas
        $this->Funciones = new Herramientas();

    }

    // destructor de la clase
    function __destruct() {

        // elimina el enlace a la base
        $this->Link = null;

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param string $codprov - clave indec
     * Método que recibe como parámetro la clave indec de la
     * jurisdicción, la asigna en la variable de clase y
     * obtiene el nombre
     */
    public function setCodProv($codprov){

        // asignamos en la clase
        $this->CodProv = $codprov;

        // componemos la consulta
        $consulta = "SELECT diccionarios.v_provincias.provincia AS jurisdiccion
                     FROM diccionarios.v_provincias
                     WHERE diccionarios.v_provincias.cod_prov = '$this->CodProv'; ";
        $resultado = $this->Link->query($consulta);

        // obtenemos el registro y lo asignamos
        $registro = array_change_key_case($resultado->fetch(PDO::FETCH_ASSOC), CASE_LOWER);
        extract($registro);
        $this->Jurisdiccion = $jurisdiccion;

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @return string - nombre de la jurisdicción
     * Metodo que retorna el nombre de la jurisdicción
     */
    public function getJurisdiccion(){
        return $this->Jurisdiccion;
    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param anio - string con el año a reportar
     * @return resultset con la tabla de frecuencias
     * Método que recibe como parámetro año y retorna un vector con los
     * datos de llas jurisdicciones y el nùmero de determinaciones
     * cargadas en ese año
     */
    public function determinacionesCargadas($anio){

        // compone la consulta
        $consulta = "SELECT cce.vw_det_cargadas.jurisdiccion AS jurisdiccion,
                            cce.vw_det_cargadas.determinaciones AS determinaciones
                     FROM cce.vw_det_cargadas
                     WHERE cce.vw_det_cargadas.pais = '$this->Pais' AND
                           cce.vw_det_cargadas.jurisdiccion = '$this->Jurisdiccion' AND
                           cce.vw_det_cargadas.anio = '$anio';";

        // obtenemos el vector, lo convertimos a minusculas y retornamos
        $resultado = $this->Link->query($consulta);
        $registros = array_change_key_case($resultado->fetchAll(PDO::FETCH_ASSOC), CASE_LOWER);

        // retornamos el vector
        return $registros;

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param operativo string con el número de operativo
     * @return resultset con los laboratorios sin cargar determinaciones
     * Método que recibe como parámetro un número de operativo y retorna
     * un resultset con los laboratorios que no tienen determinaciones
     * cargadas, si recibe la jurisdiccion solo selecciona los laboratorios
     * correspondientes a esa jurisdiccion
     */
    public function determinacionesSinCargar($operativo){

        // componemos la consulta
        $consulta = "SELECT cce.laboratorios.ID AS id,
                            cce.laboratorios.NOMBRE AS laboratorio,
                            diccionarios.provincias.NOM_PROV AS jurisdiccion
                     FROM cce.laboratorios INNER JOIN diccionarios.localidades ON cce.laboratorios.LOCALIDAD = diccionarios.localidades.CODLOC
                                           INNER JOIN diccionarios.provincias ON diccionarios.localidades.CODPCIA = diccionarios.provincias.COD_PROV
                     WHERE cce.laboratorios.ACTIVO = 'Si' AND
                           cce.laboratorios.ID NOT IN (SELECT existentes.ID AS id_laboratorio
                                                       FROM cce.chag_datos INNER JOIN cce.laboratorios AS existentes ON cce.chag_datos.LABORATORIO = existentes.ID
                                                                           INNER JOIN cce.operativos_chagas ON cce.chag_datos.OPERATIVO = cce.operativos_chagas.ID
                                                       WHERE cce.operativos_chagas.OPERATIVO_NRO = '$operativo')
                     ORDER BY diccionarios.provincias.NOM_PROV,
                              cce.laboratorios.NOMBRE;";

        // obtenemos el vector, lo convertimos a minusculas y retornamos
        $resultado = $this->Link->query($consulta);
        $registros = array_change_key_case($resultado->fetchAll(PDO::FETCH_ASSOC), CASE_LOWER);

        // retornamos el vector
        return $registros;

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param anio string con el año a informar
     * @return paneles string con el número de paneles enviados
     * Método que recibe como parámetro un string con el año a informar
     * y retorna el número de paneles enviados a las jurisdicciones
     * en ese año
     */
    public function panelesEnviados ($anio){

        // Componemos y ejecutamos la consulta
        $consulta = "SELECT COUNT(cce.etiquetas.ETIQUETA_NRO) DIV 6 AS paneles
                     FROM cce.etiquetas INNER JOIN cce.laboratorios ON cce.etiquetas.id_laboratorio = cce.laboratorios.id
                                        INNER JOIN diccionarios.v_localidades ON cce.laboratorios.localidad = diccionarios.v_localidades.codloc
                     WHERE cce.etiquetas.OPERATIVO LIKE '%$anio' AND
                           diccionarios.v_localidades.provincia = '$this->Jurisdiccion';";

        // obtenemos el vector, lo convertimos a minusculas y retornamos
        $resultado = $this->Link->query($consulta);
        $registro = array_change_key_case($resultado->fetch(PDO::FETCH_ASSOC), CASE_LOWER);
        extract($registro);
        return $paneles;

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @return vector con la nomina de jurisdicciones
     * Metodo que retorna un vector con la nomina de jurisdicciones
     * del pais del usuario
     */
    protected function nominaJurisdicciones(){

        // componemos la consulta con la nomina de provincias
        $sql = "SELECT diccionarios.provincias.NOM_PROV AS jurisdiccion
                FROM diccionarios.provincias INNER JOIN diccionarios.paises ON diccionarios.provincias.PAIS = diccionarios.paises.ID
                WHERE diccionarios.paises.NOMBRE = '$this->Pais'
                ORDER BY diccionarios.provincias.NOM_PROV; ";
        $resultado = $this->Link->query($sql);

        // obtenemos la nomina de jurisdicciones
        $registros = array_change_key_case($resultado->fetchAll(PDO::FETCH_ASSOC), CASE_LOWER);

        // retornamos el vector
        return $registros;

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @return array con la nomina de laboratorios
     * Metodo que retorna un array con la nomina de jurisdicciones y el
     * numero de laboratorios por fuente de financiamiento usamos un array
     * para devolver la tabla al reporte lista para ser presentada
     */
    public function laboratoriosParticipantes(){

        // componemos la consulta
        $consulta = "SELECT cce.vw_financiamiento.provincia AS provincia,
                            cce.vw_financiamiento.nacional AS nacional,
                            cce.vw_financiamiento.provincial AS provincial,
                            cce.vw_financiamiento.municipal AS municipal,
                            cce.vw_financiamiento.universitario AS universitario,
                            cce.vw_financiamiento.privado AS privado,
                            cce.vw_financiamiento.obrasocial AS obrasocial
                     FROM cce.vw_financiamiento
                     WHERE cce.vw_financiamiento.pais = '$this->Pais' AND
                           cce.vw_financiamiento.provincia = '$this->Jurisdiccion'; ";
        $resultado = $this->Link->query($consulta);

        // obtenemos el vector
        $registros = array_change_key_case($resultado->fetchAll(PDO::FETCH_ASSOC), CASE_LOWER);

        // retornamos el vector
        return $registros;

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param anio string con el año a reportar
     * @return string porcentaje de respuesta de la
     * jurisdicción formateado
     */
    public function panelesProcesados($anio){

        // componemos la consulta de los laboratorios que recibieron paneles
        $consulta = "SELECT COUNT(DISTINCT(cce.laboratorios.ID)) AS enviados
                     FROM cce.laboratorios INNER JOIN diccionarios.localidades ON cce.laboratorios.LOCALIDAD = diccionarios.localidades.CODLOC
                                           INNER JOIN diccionarios.provincias ON diccionarios.localidades.CODPCIA = diccionarios.provincias.COD_PROV
                                           INNER JOIN diccionarios.paises ON diccionarios.provincias.PAIS = diccionarios.paises.ID
                                           INNER JOIN cce.etiquetas ON cce.laboratorios.ID = cce.etiquetas.ID_LABORATORIO
                     WHERE cce.laboratorios.activo = 'Si' AND
                           cce.etiquetas.OPERATIVO LIKE '%$anio' AND
                           diccionarios.paises.NOMBRE = '$this->Pais' AND
                           diccionarios.provincias.NOM_PROV = '$this->Jurisdiccion'
                     GROUP BY diccionarios.provincias.NOM_PROV;";

        // obtenemos el valor
        $resultado = $this->Link->query($consulta);
        $registro = array_change_key_case($resultado->fetch(PDO::FETCH_ASSOC), CASE_LOWER);
        extract($registro);

        // componemos la consulta de los laboratorios que procesaron paneles
        $consulta = "SELECT COUNT(DISTINCT(cce.laboratorios.ID)) AS procesados
                     FROM cce.laboratorios INNER JOIN diccionarios.localidades ON cce.laboratorios.LOCALIDAD = diccionarios.localidades.CODLOC
                                           INNER JOIN diccionarios.provincias ON diccionarios.localidades.CODPCIA = diccionarios.provincias.COD_PROV
                                           INNER JOIN diccionarios.paises ON diccionarios.provincias.PAIS = diccionarios.paises.ID
                                           INNER JOIN cce.chag_datos ON cce.laboratorios.ID = cce.chag_datos.LABORATORIO
                                           INNER JOIN cce.operativos_chagas ON cce.chag_datos.OPERATIVO = cce.operativos_chagas.ID
                     WHERE laboratorios.activo = 'Si' AND
                           operativos_chagas.OPERATIVO_NRO LIKE '%$anio' AND
                           diccionarios.paises.NOMBRE = '$this->Pais' AND
                           diccionarios.provincias.NOM_PROV = '$this->Jurisdiccion'
                     GROUP BY diccionarios.provincias.NOM_PROV;";

        // ejecutamos la consulta y obtenemos el registro
        $resultado = $this->Link->query($consulta);
        $registro = array_change_key_case($resultado->fetch(PDO::FETCH_ASSOC), CASE_LOWER);
        extract($registro);

        // obtenemos el cociente y retornamos
        $respuesta = ($procesados / $enviados) * 100;
        $respuesta = number_format($respuesta, 2, ",", ".");
        return $respuesta;

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @return array - vector con los laboratorios participantes
     * Método que recibe como parámetro el año a reportar y retorna
     * el vector con los laboratorios que recibieron muestras en
     * la jurisdicción activa
     */
    public function nominaLaboratorios($anio){

        // componemos la consulta
        $consulta = "SELECT DISTINCT(cce.laboratorios.nombre) AS laboratorio,
                            diccionarios.localidades.nomloc AS localidad
                     FROM cce.laboratorios INNER JOIN diccionarios.localidades ON cce.laboratorios.LOCALIDAD = diccionarios.localidades.CODLOC
                                           INNER JOIN diccionarios.provincias ON diccionarios.localidades.CODPCIA = diccionarios.provincias.COD_PROV
                                           INNER JOIN diccionarios.paises ON diccionarios.provincias.PAIS = diccionarios.paises.ID
                                           INNER JOIN cce.etiquetas ON cce.laboratorios.ID = cce.etiquetas.ID_LABORATORIO
                     WHERE cce.laboratorios.activo = 'Si' AND
                           cce.etiquetas.OPERATIVO LIKE '%$anio' AND
                           diccionarios.paises.NOMBRE = '$this->Pais' AND
                           diccionarios.provincias.NOM_PROV = '$this->Jurisdiccion'
                     ORDER BY cce.laboratorios.nombre;";

        // ejecutamos la consulta y obtenemos el registro
        $resultado = $this->Link->query($consulta);
        $registro = array_change_key_case($resultado->fetchAll(PDO::FETCH_ASSOC), CASE_LOWER);

        // retornamos el vector
        return $registro;

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param $anio string con el año a reportar
     * @return $determinaciones con la nomina de jurisdicciones y el numero de
     * determinaciones cargadas fuera de termino
     */
    public function fueraTermino($anio){

        // obtenemos la tabla de frecuencias
        $consulta = "SELECT cce.vw_termino.jurisdiccion AS jurisdiccion,
                            cce.vw_termino.atermino AS atermino,
                            cce.vw_termino.fueratermino AS fueratermino
                     FROM cce.vw_termino
                     WHERE cce.vw_termino.anio = '$anio' AND
                           cce.vw_termino.pais = '$this->Pais';";
        $resultado = $this->Link->query($consulta);
        $determinaciones = array_change_key_case($resultado->fetchAll(PDO::FETCH_ASSOC), CASE_LOWER);

        // retornamos la matriz
        return $determinaciones;

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param $anio - cadena con el año a calcular
     * @return $dedterminaciones - array
     * Método utilizado para graficar las determinaciones fuera de término
     * utiliza la misma vista pero calculando los totales nacionales
     */
    public function totalFueraTermino($anio){

        // obtenemos la tabla de frecuencias
        $consulta = "SELECT cce.vw_termino.atermino AS atermino,
                            cce.vw_termino.fueratermino AS fueratermino
                     FROM cce.vw_termino
                     WHERE cce.vw_termino.anio = '$anio' AND
                           cce.vw_termino.pais = '$this->Pais' AND
                           cce.vw_termino.jurisdiccion = '$this->Jurisdiccion';";
        $resultado = $this->Link->query($consulta);
        $determinaciones = array_change_key_case($resultado->fetch(PDO::FETCH_ASSOC), CASE_LOWER);

        // retornamos la matriz
        return $determinaciones;

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param anio string con el año a reportar
     * @return matriz con la distribucion de la carga de muestras
     * Metodo que recibe como parametro el año a reportar y retorna una
     * matriz con la distribucion por jurisdiccion de las tecnicas
     * mas empleadas
     */
    public function distribucionMuestras($anio){

        // componemos y ejecutamos la consulta de las provincias que
        // remitieron muestras y el numero de determinaciones de cada
        // tecnica
        $consulta = "SELECT cce.vw_muestras.jurisdiccion AS jurisdiccion,
                            cce.vw_muestras.ap AS ap,
                            cce.vw_muestras.elisa AS elisa,
                            cce.vw_muestras.hai AS hai,
                            cce.vw_muestras.ifi AS ifi,
                            cce.vw_muestras.quimio AS quimio,
                            cce.vw_muestras.otra AS otra
                     FROM cce.vw_muestras
                     WHERE cce.vw_muestras.anio = '$anio' AND
                           cce.vw_muestras.pais = '$this->Pais' AND
                           cce.vw_muestras.jurisdiccion = '$this->Jurisdiccion';";
        $resultado = $this->Link->query($consulta);
        $muestras = array_change_key_case($resultado->fetchAll(PDO::FETCH_ASSOC), CASE_LOWER);

        // retornamos el vector
        return $muestras;

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param anio string con el año a reportar
     * @return matriz con la distribucion de lector / visual en elisa
     * Metodo que recibe como parametro el año a reportar y retorna una
     * matriz con la distribucion de uso de lector de placas
     */
    public function lectorVisual($anio){

        // componemos la consulta
        $consulta = "SELECT cce.vw_lector.jurisdiccion AS provincia,
                            cce.vw_lector.lector AS lector,
                            cce.vw_lector.visual AS visual
                     FROM cce.vw_lector
                     WHERE cce.vw_lector.anio = '$anio' AND
                           cce.vw_lector.pais = '$this->Pais' AND
                           cce.vw_lector.jurisdiccion = '$this->Jurisdiccion';";
        $resultado = $this->Link->query($consulta);
        $distribucion = array_change_key_case($resultado->fetchAll(PDO::FETCH_ASSOC), CASE_LOWER);

        // retornamos el vector
        return $distribucion;

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param anio string con el año a reportar
     * @return matriz con la distribucion de tecnicas utilizadas
     * Metodo que recibe como parametro el año a reportar y genera la
     * matriz con el numero de tecnicas empleadas por cada jurisdiccion
     */
    public function nroTecnicas($anio){

        // componemos y ejecutamos la consulta no lo hacemos con vistas
        // porque para parametrizar tendríamos que crear una función
        $consulta = "SELECT consulta.provincia AS jurisdiccion,
                            SUM(CASE WHEN consulta.tecnicas = 1 THEN 1
                                ELSE 0
                                END) AS tecnica1,
                            SUM(CASE WHEN consulta.tecnicas = 2 THEN 1
                                ELSE 0
                                END) AS tecnica2,
                            SUM(CASE WHEN consulta.tecnicas = 3 THEN 1
                                ELSE 0
                                END) AS tecnica3,
                            SUM(CASE WHEN consulta.tecnicas = 4 THEN 1
                                ELSE 0
                                END) AS tecnica4
                     FROM (SELECT diccionarios.provincias.NOM_PROV AS provincia,
                                  cce.laboratorios.ID AS laboratorios,
                                  COUNT(DISTINCT(cce.chag_datos.TECNICA)) AS tecnicas
                           FROM cce.chag_datos INNER JOIN cce.laboratorios ON cce.chag_datos.LABORATORIO = cce.laboratorios.ID
                                               INNER JOIN diccionarios.localidades ON cce.laboratorios.LOCALIDAD = diccionarios.localidades.CODLOC
                                               INNER JOIN diccionarios.provincias ON diccionarios.localidades.CODPCIA = diccionarios.provincias.COD_PROV
                                               INNER JOIN diccionarios.paises ON diccionarios.provincias.PAIS = diccionarios.paises.ID
                                               INNER JOIN cce.operativos_chagas ON cce.operativos_chagas.ID = cce.chag_datos.OPERATIVO
                           WHERE cce.operativos_chagas.OPERATIVO_NRO LIKE '%$anio' AND
                                 diccionarios.paises.NOMBRE = '$this->Pais' AND
                                 diccionarios.provincias.nom_prov = '$this->Jurisdiccion'
                           GROUP BY cce.laboratorios.ID) AS consulta
                     GROUP BY consulta.provincia
                     ORDER BY consulta.provincia;";
        $resultado = $this->Link->query($consulta);
        $distribucion = array_change_key_case($resultado->fetchAll(PDO::FETCH_ASSOC), CASE_LOWER);

        // retorna la matriz
        return $distribucion;

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param anio string con el año a reportar
     * @return array con la eficiencia por jurisdicción
     * Método que recibe como parámetro el año a calcular y retorna una
     * matriz con el número de determinaciones, las correctas y la
     * eficiencia de cada jurisdicción
     */
    public function obtenerConcordancia($anio){

        // obtenemos la concordancia por jurisdicción
        $consulta = "SELECT vw_concordancia.provincia AS provincia,
                            SUM(vw_concordancia.determinaciones) AS determinaciones,
                            SUM(vw_concordancia.correctas) AS correctas,
                            SUM(vw_concordancia.incorrectas) AS incorrectas
                     FROM vw_concordancia
                     WHERE vw_concordancia.pais = '$this->Pais' AND
                           vw_concordancia.anio = '$anio' AND
                           vw_concordancia.provincia = '$this->Jurisdiccion'
                     GROUP BY vw_concordancia.provincia;; ";
        $resultado = $this->Link->query($consulta);
        $concordancia = array_change_key_case($resultado->fetchAll(PDO::FETCH_ASSOC), CASE_LOWER);

        // retornamos el vector
        return $concordancia;

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param anio string con el año a reportar
     * @return array con la eficiencia por jurisdicción
     * Método que recibe como parámetro el año a calcular y retorna una
     * matriz con el número de determinaciones, las correctas y la
     * eficiencia de cada laboratorio
     */
    public function obtenerEficiencia($anio){

        // obtenemos la concordancia por jurisdicción
        $consulta = "SELECT cce.vw_concordancia_laboratorios.laboratorio AS laboratorio,
                            cce.vw_concordancia_laboratorios.determinaciones AS determinaciones,
                            cce.vw_concordancia_laboratorios.correctas AS correctas,
                            cce.vw_concordancia_laboratorios.incorrectas AS incorrectas
                     FROM cce.vw_concordancia_laboratorios
                     WHERE cce.vw_concordancia_laboratorios.anio = '$anio' AND
                           cce.vw_concordancia_laboratorios.provincia = '$this->Jurisdiccion'
                     GROUP BY cce.vw_concordancia_laboratorios.laboratorio; ";
        $resultado = $this->Link->query($consulta);
        $concordancia = array_change_key_case($resultado->fetchAll(PDO::FETCH_ASSOC), CASE_LOWER);

        // retornamos el vector
        return $concordancia;

    }

    /**
     * @param anio string con el año a reportar
     * @return matriz con la eficiencia por tecnica
     * Metodo que recibe como parametro el año a reportar y retorna una
     * matriz con la eficiencia por cada una de las tecnicas
     */
    public function eficienciaTecnicas($anio){

        // consultamos la vista de concordancia
        $consulta = "SELECT cce.vw_conc_tecnica.provincia AS provincia,
                            cce.vw_conc_tecnica.ap AS ap,
                            cce.vw_conc_tecnica.correctasap AS correctasap,
                            cce.vw_conc_tecnica.elisa AS elisa,
                            cce.vw_conc_tecnica.correctaselisa AS correctaselisa,
                            cce.vw_conc_tecnica.hai AS hai,
                            cce.vw_conc_tecnica.correctashai AS correctashai,
                            cce.vw_conc_tecnica.ifi AS ifi,
                            cce.vw_conc_tecnica.correctasifi AS correctasifi,
                            cce.vw_conc_tecnica.quimio AS quimio,
                            cce.vw_conc_tecnica.correctasquimio AS correctasquimio,
                            cce.vw_conc_tecnica.otras AS otras,
                            cce.vw_conc_tecnica.correctasotras AS correctasotras
                     FROM cce.vw_conc_tecnica
                     WHERE cce.vw_conc_tecnica.pais = '$this->Pais' AND
                           cce.vw_conc_tecnica.anio = '$anio' AND
                           cce.vw_conc_tecnica.provincia = '$this->Jurisdiccion';";

        // ejecutamos la consulta
        $resultado = $this->Link->query($consulta);
        $concordancia = array_change_key_case($resultado->fetchAll(PDO::FETCH_ASSOC), CASE_LOWER);

        // retornamos la matriz
        return $concordancia;

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param anio string con el año a reportar
     * @return resultset con la nómina de jurisdicciones
     * Método que recibe como parámetro el año a reportar y retorna un
     * vector con las provincias que no participaron del control de calidad
     */
    public function provinciasFaltantes($anio){

        // componemos la consulta
        $consulta = "SELECT diccionarios.provincias.NOM_PROV AS jurisdiccion
                     FROM diccionarios.provincias INNER JOIN diccionarios.paises ON diccionarios.provincias.PAIS = diccionarios.paises.ID
                     WHERE diccionarios.provincias.NOM_PROV != 'Indeterminado' AND
                           diccionarios.paises.NOMBRE = '$this->Pais' AND
                           diccionarios.provincias.NOM_PROV NOT IN
                           (SELECT diccionarios.provincias.NOM_PROV AS provincia
                            FROM cce.chag_datos INNER JOIN cce.laboratorios ON cce.chag_datos.LABORATORIO = cce.laboratorios.ID
                                                INNER JOIN diccionarios.localidades ON cce.laboratorios.LOCALIDAD = diccionarios.localidades.CODLOC
                                                INNER JOIN diccionarios.provincias ON diccionarios.localidades.CODPCIA = diccionarios.provincias.COD_PROV
                                                INNER JOIN cce.operativos_chagas ON cce.chag_datos.OPERATIVO = cce.operativos_chagas.ID
                            WHERE cce.operativos_chagas.OPERATIVO_NRO LIKE '%$anio')
                     GROUP BY diccionarios.provincias.NOM_PROV
                     ORDER BY diccionarios.provincias.NOM_PROV;";
        $vector = $this->Link->query($consulta);
        $resultado = array_change_key_case($vector->fetchAll(PDO::FETCH_ASSOC), CASE_LOWER);

        // retornamos el vector
        return $resultado;

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @return resultset con la nómina de técnicas registradas
     */
    public function nominaTecnicas(){

        // componemos y ejecutamos la consulta
        $consulta = "SELECT cce.tecnicas.TECNICA AS tecnica
                     FROM cce.tecnicas
                     ORDER BY cce.tecnicas.TECNICA;";
        $vector = $this->Link->query($consulta);
        $row = $vector->fetchAll(PDO::FETCH_ASSOC);
        $resultado = array_change_key_case($row, CASE_LOWER);

        // retornamos el vector
        return $resultado;

    }

    /**
     * @param anio string con el año a reportar
     * @return matriz con la distribucion de valores mal informados
     * Metodo que recibe como parametro el año a reportar y retorna
     * una matriz con la distribucion de valores de corte y lectura
     * mal informados
     */
    public function valoresCorte($anio){

        // compone la consulta calculando los errores de corte
        // que son aquellos en donde se seleccionó otro o en
        // el caso elisa se seleccionó lector y el valor
        // ingresado es 0.000
        $consulta = "SELECT cce.vw_corte.jurisdiccion AS jurisdiccion,
                            cce.vw_corte.malcorte AS malcorte,
                            cce.vw_corte.mallectura AS mallectura
                     FROM cce.vw_corte
                     WHERE cce.vw_corte.anio = '$anio' AND
                           cce.vw_corte.jurisdiccion = '$this->Jurisdiccion' AND
                           cce.vw_corte.pais = '$this->Pais';";
        $resultado = $this->Link->query($consulta);
        $corte = array_change_key_case($resultado->fetchAll(PDO::FETCH_ASSOC), CASE_LOWER);

        // retornamos el vector
        return $corte;

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @return array
     * Mètodo que retorna la nòmina de otras pruebas
     */
    public function otrasPruebas($anio){

        // componemos y ejecutamos la consulta
        $consulta = "SELECT cce.vw_otras.jurisdiccion AS jurisdiccion,
                            cce.vw_otras.laboratorio AS laboratorio,
                            cce.vw_otras.prueba AS prueba,
                            cce.vw_otras.marca AS marca,
                            cce.vw_otras.id AS id
                     FROM vw_otras
                     WHERE cce.vw_otras.anio = '$anio' AND
                           cce.vw_otras.pais = '$this->Pais' AND
                           cce.vw_otras.jurisdiccion = '$this->Jurisdiccion';";
        $resultado = $this->Link->query($consulta);

        // obtenemos el vector y retornamos
        $otras = array_change_key_case($resultado->fetchAll(PDO::FETCH_ASSOC), CASE_LOWER);
        return $otras;

    }

    /**
     * @param anio string con el año a reportar
     * @return matriz con la distribucion de falsos negativos
     * Metodo que recibe como parametro el año a reportar y retorna una
     * matriz con la distribucion por provincia de los falsos negativos
     */
    public function falsosNegativos($anio){

        // componemos la consulta en la vista
        $consulta = "SELECT cce.vw_falsos_negativos.provincia AS provincia,
                            cce.vw_falsos_negativos.ap AS ap,
                            cce.vw_falsos_negativos.falsosap AS falsosap,
                            cce.vw_falsos_negativos.elisa AS elisa,
                            cce.vw_falsos_negativos.falsoselisa AS falsoselisa,
                            cce.vw_falsos_negativos.hai AS hai,
                            cce.vw_falsos_negativos.falsoshai AS falsoshai,
                            cce.vw_falsos_negativos.ifi AS ifi,
                            cce.vw_falsos_negativos.falsosifi AS falsosifi,
                            cce.vw_falsos_negativos.quimio AS quimio,
                            cce.vw_falsos_negativos.falsosquimio AS falsosquimio,
                            cce.vw_falsos_negativos.otras AS otras,
                            cce.vw_falsos_negativos.falsosotras AS falsosotras
                     FROM cce.vw_falsos_negativos
                     WHERE cce.vw_falsos_negativos.pais = '$this->Pais' AND
                           cce.vw_falsos_negativos.provincia = '$this->Jurisdiccion' AND
                           cce.vw_falsos_negativos.anio = '$anio';";
        $resultado = $this->Link->query($consulta);

        // obtenemos el vector y retornamos
        $falsos = array_change_key_case($resultado->fetchAll(PDO::FETCH_ASSOC), CASE_LOWER);
        return $falsos;

    }

    /**
     * @param anio string con el año a reportar
     * @return matriz con la distribucion de falsos negativos
     * Metodo que recibe como parametro el año a reportar y retorna una
     * matriz con la distribucion por provincia de los falsos positivos
     */
    public function falsosPositivos($anio){

        // componemos la consulta en la vista
        $consulta = "SELECT cce.vw_falsos_positivos.provincia AS provincia,
                            cce.vw_falsos_positivos.ap AS ap,
                            cce.vw_falsos_positivos.falsosap AS falsosap,
                            cce.vw_falsos_positivos.elisa AS elisa,
                            cce.vw_falsos_positivos.falsoselisa AS falsoselisa,
                            cce.vw_falsos_positivos.hai AS hai,
                            cce.vw_falsos_positivos.falsoshai AS falsoshai,
                            cce.vw_falsos_positivos.ifi AS ifi,
                            cce.vw_falsos_positivos.falsosifi AS falsosifi,
                            cce.vw_falsos_positivos.quimio AS quimio,
                            cce.vw_falsos_positivos.falsosquimio AS falsosquimio,
                            cce.vw_falsos_positivos.otras AS otras,
                            cce.vw_falsos_positivos.falsosotras AS falsosotras
                     FROM cce.vw_falsos_positivos
                     WHERE cce.vw_falsos_positivos.pais = '$this->Pais' AND
                           cce.vw_falsos_positivos.provincia = '$this->Jurisdiccion' AND
                           cce.vw_falsos_positivos.anio = '$anio';";
        $resultado = $this->Link->query($consulta);

        // obtenemos el vector y retornamos
        $falsos = array_change_key_case($resultado->fetchAll(PDO::FETCH_ASSOC), CASE_LOWER);
        return $falsos;

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param string - $anio
     * @param string - $tecnica
     * Método que recibe como parámetros el año y el nombre de una
     * técnica y presenta la nómina de eficiencia por cada una de
     * las marcas de esa técnica
     */
    public function eficienciaMarcas($anio, $tecnica){

        // componemos la consulta
        $consulta = "SELECT cce.vw_efic_marc_prov.marca AS marca,
                            cce.vw_efic_marc_prov.determinaciones AS determinaciones,
                            cce.vw_efic_marc_prov.correctas AS correctas
                     FROM cce.vw_efic_marc_prov
                     WHERE cce.vw_efic_marc_prov.pais = '$this->Pais' AND
                           cce.vw_efic_marc_prov.tecnica = '$tecnica' AND
                           cce.vw_efic_marc_prov.provincia = '$this->Jurisdiccion' AND
                           cce.vw_efic_marc_prov.anio = '$anio';";
        $resultado = $this->Link->query($consulta);

        // obtenemos y retornamos el vector
        $marcas = array_change_key_case($resultado->fetchAll(PDO::FETCH_ASSOC), CASE_LOWER);
        return $marcas;

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param string $anio - el año del reporte
     * @return array
     * Método que recibe como parámetro el año del reporte y retorna
     * un array con la nómina de etiquetas huérfanas de la jurisdicción
     */
    public function etiquetasHuerfanas($anio){

        // inicializamos las variables
        $determinaciones = 0;

    	// componemos la consulta
    	$consulta = "SELECT diccionarios.provincias.NOM_PROV AS jurisdiccion,
                            COUNT(chag_datos.ID) AS determinaciones
                     FROM chag_datos INNER JOIN laboratorios ON chag_datos.LABORATORIO = laboratorios.ID
                                     INNER JOIN diccionarios.localidades ON laboratorios.LOCALIDAD = diccionarios.localidades.CODLOC
                                     INNER JOIN diccionarios.provincias ON diccionarios.localidades.CODPCIA = diccionarios.provincias.COD_PROV
                                     INNER JOIN operativos_chagas ON chag_datos.OPERATIVO = operativos_chagas.ID
                                     INNER JOIN diccionarios.paises ON diccionarios.provincias.PAIS = diccionarios.paises.ID
                    WHERE diccionarios.provincias.nom_prov = '$this->Jurisdiccion' AND
                          chag_datos.MUESTRA_NRO NOT IN
                          (SELECT chag_datos.MUESTRA_NRO
                           FROM chag_datos INNER JOIN etiquetas ON chag_datos.MUESTRA_NRO = etiquetas.ETIQUETA_NRO) AND
                                operativos_chagas.OPERATIVO_NRO LIKE '%$anio' AND
                                diccionarios.paises.NOMBRE = '$this->Pais'
                    GROUP BY diccionarios.provincias.NOM_PROV
                    ORDER BY diccionarios.provincias.NOM_PROV; ";
        $resultado = $this->Link->query($consulta);
        $registros = $resultado->fetch(PDO::FETCH_ASSOC);

        // si la consulta tuvo registros
        if (!empty($registros)){

            // obtenemos el registro
            $huerfanas = array_change_key_case($registros, CASE_LOWER);
            extract($huerfanas);

        }

        // retornamos
        return $determinaciones;

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param array $eficiencia - array con los datos de eficiencia
     * @param double $media - la media de la muestra
     * @param double $desvio - el desvío estandard de la muestra
     * @return array
     * Método que recibe como parámetro un array con la eficiencia
     * la media de la muestra y el desvío estandar de la muestra
     * Retorna un array con los jurisdicciones que muestran un
     * rendimjiento por debajo de un desvío estandard
     */
    public function rendimientoMuestra($eficiencia, $media, $desvio){

        // como las funciones estadísticas retornan la media y el
        // desvío como una cadena formateada, primero los
        // convertirmos
        $media = floatval($media);
        $desvio = floatval($desvio);

        // definimos la variable de retorno
        $nomina = array();

        // obtenemos el piso
        $piso = $media - $desvio;

        // ahora recorremos el array
        for ($i = 0; $i < count($eficiencia); $i++){

            // si se encuentra por debajo del piso
            if ($eficiencia[$i][1] < $piso){

                // agregamos la jurisdicción
                $nomina[] = array("laboratorio" => $eficiencia[$i][0]);

            }

        }

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @return array
     * Método que retorna la eficiencia de los últimos diez años
     */
    public function historiaEficiencia(){

        // componemos la consulta
        $consulta = "SELECT vw_concordancia_laboratorios.anio AS anio,
                            vw_concordancia_laboratorios.determinaciones AS determinaciones,
                            vw_concordancia_laboratorios.correctas AS correctas
                     FROM vw_concordancia_laboratorios
                     WHERE vw_concordancia_laboratorios.provincia = '$this->Jurisdiccion'
                     GROUP BY vw_concordancia_laboratorios.anio
                     ORDER BY vw_concordancia_laboratorios.anio DESC
                     LIMIT 10;";
        $resultado = $this->Link->query($consulta);

        // obtenemos el vector y retornamos
        $historia = array_change_key_case($resultado->fetchAll(PDO::FETCH_ASSOC), CASE_LOWER);
        return $historia;

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param string $anio - año del reporte
     * @return array
     * Método que recibe como parámetro el año de un reporte y
     * retorna el vector con la nómina por jurisdicción de determinaciones
     * discordantes
     */
    public function discordantes($anio){

        // declaración de variables
        $discordantes = array();

        // obtenemos la nómina de jurisdicciones y el número de
        // determinaciones realizada por cada una de ellas
        $determinaciones = $this->determinacionesCargadas($anio);

        // ahora recorremos el vector armando la matriz de datos
        foreach($determinaciones AS $registro){

            // obtenemos el registro
            extract($registro);

            // lo agregamos
            $discordantes[$jurisdiccion] = array();

            // ahora agregamos la cantidad de laboratorios activos
            $discordantes[$jurisdiccion]["determinaciones"] = $determinaciones;

        }

        // ahora obtenemos la nómina de discordantes
        $consulta = "SELECT cce.vw_discordantes.provincia AS provincia,
                           cce.vw_discordantes.discordantes AS noconordantes
                    FROM cce.vw_discordantes
                    WHERE cce.vw_discordantes.pais = '$this->Pais' AND
                          cce.vw_discordantes.provincia = '$this->Jurisdiccion' AND
                          cce.vw_discordantes.anio = '$anio';";
        $resultado = $this->Link->query($consulta);

        // obtenemos el vector
        $paneles = array_change_key_case($resultado->fetchAll(PDO::FETCH_ASSOC), CASE_LOWER);

        // ahora recorremos el vector
        foreach($paneles AS $registro){

            // obtenemos el registro
            extract($registro);

            // agregamos el valor
            $discordantes[$provincia]["noconcordantes"] = $noconordantes;

        }

        // retornamos el vector
        return $discordantes;

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param $anio - año del reporte
     * @param $archivo - contenido del reporte
     * Método que recibe como parámetros el contenido de un
     * archivo y el año del reporte y lo guarda en la base
     * de datos
     */
    public function grabaReporte($archivo, $anio){

        // inicializamos las variables
        $registros = 0;

        // primero tenemos que verificar si el operativo
        // ya fue generado
        $consulta = "SELECT COUNT(cce.reportes.id) AS registros,
                            cce.reportes.id AS idreporte
                     FROM cce.reportes
                     WHERE cce.reportes.provincia = '$this->CodProv' AND
                           cce.reportes.operativo = '$anio';";
        $resultado = $this->Link->query($consulta);
        $registro = array_change_key_case($resultado->fetch(PDO::FETCH_ASSOC), CASE_LOWER);
        extract($registro);

        // si no existe
        if ($registros == 0){

            // lo inserta
            $this->nuevoReporte($archivo, $anio);

        } else {

            // lo edita
            $this->editaReporte($archivo, $idreporte);

        }

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param $anio - año del reporte
     * @param $archivo - contenido del reporte
     * Método que inserta un nuevo registro en la tabla de reportes
     */
    protected function nuevoReporte($archivo, $anio){

        // compone la consulta
        $consulta = "INSERT INTO cce.reportes
                            (operativo,
                             provincia,
                             usuario,
                             nivel_central,
                             reporte)
                            VALUES
                            (:operativo,
                             :provincia,
                             :usuario,
                             :nivel_central,
                             :reporte);";

        // asignamos la consulta
        $psInsertar = $this->Link->prepare($consulta);

        // asignamos los valores
        $psInsertar->bindParam(":operativo", $anio);
        $psInsertar->bindParam(":provincia", $this->CodProv);
        $psInsertar->bindParam(":usuario", $this->IdUsuario);
        $psInsertar->bindParam(":nivel_central", "No");
        $psInsertar->bindParam(":reporte", $archivo);

        // ejecutamos la consulta
        $resultado = $psInsertar->execute();

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param $id - clave del registro
     * @param $archivo - contenido del reporte
     * Método que edita el registro de la tabla de reportes
     */
    protected function editaReporte($archivo, $id){

        // componemos la consulta
        $consulta = "UPDATE cce.reportes SET
                            usuario = :usuario,
                            reporte = :reporte
                     WHERE cce.reportes.id = :id; ";

        // asignamos la consulta
        $psInsertar = $this->Link->prepare($consulta);

        // asignamos los valores
        $psInsertar->bindParam(":usuario", $this->IdUsuario);
        $psInsertar->bindParam(":reporte", $archivo);
        $psInsertar->bindParam(":id", $id);

        // ejecutamos la consulta
        $resultado = $psInsertar->execute();

    }

}
