<?php

/**
 *
 * Class InformeNacional | informes/nacional/informenacional.class.php
 *
 * @package     CCE
 * @subpackage  Reportes
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.4.0 (15/11/2018)
 * @copyright   Copyright (c) 2018, INP
 *
 * Clase que contiene una serie de funciones estadísticas
 *
*/

// inclusion de archivos
require_once ("../../clases/conexion.class.php");
require_once ("../../clases/herramientas.class.php");

// convención para la nomenclatura de las propiedades, comienzan con una
// letra mayúscula, de tener mas de una palabra no se utilizan separadores
// y la inicial de cada palabra va en mayúscula
// para las variables recibidas como parámetro el criterio es todas en
// minúscula

// convención para la nomenclatura de los metodos, comienzan con set o get
// según asignen un valor o lo lean y luego el nombre del valor a obtener

// definicion de la clase
class InformeNacional {

    // declaración de variables de la clase, las declaramos
    // como protected para que puedan ser heredadas pero
    // para asignarle el valor debemos crear los métodos
    protected $Link;                  // puntero a la base de datos
    protected $Pais;                  // pais del usuario activo
    protected $Funciones;             // clase de herramientas

    // constructor de la clase
    function __construct (){

        // nos conectamos a la base de datos
        $this->Link = new Conexion();

        // iniciamos sesion y obtenemos el pais
        session_start();
        $this->Pais = $_SESSION["Pais"];

        // cerramos la sesión
        session_write_close();

        // iniciamos la clase de herramientas
        $this->Funciones = new Herramientas();

    }

    // destructor de la clase
    function __destruct() {

        // elimina el enlace a la base
        $this->Link = null;

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param anio - string con el año a reportar
     * @return resultset con la tabla de frecuencias
     * Método que recibe como parámetro año y retorna un vector con los
     * datos de llas jurisdicciones y el nùmero de determinaciones
     * cargadas en ese año
     */
    public function determinacionesCargadas($anio){

        // compone la consulta
        $consulta = "SELECT cce.vw_det_cargadas.jurisdiccion AS jurisdiccion,
                            cce.vw_det_cargadas.determinaciones AS determinaciones
                     FROM cce.vw_det_cargadas
                     WHERE cce.vw_det_cargadas.pais = '$this->Pais' AND
                           cce.vw_det_cargadas.anio = '$anio';";

        // obtenemos el vector, lo convertimos a minusculas y retornamos
        $resultado = $this->Link->query($consulta);
        $registros = array_change_key_case($resultado->fetchAll(PDO::FETCH_ASSOC), CASE_LOWER);

        // retornamos el vector
        return $registros;

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param operativo string con el número de operativo
     * @return resultset con los laboratorios sin cargar determinaciones
     * Método que recibe como parámetro un número de operativo y retorna
     * un resultset con los laboratorios que no tienen determinaciones
     * cargadas, si recibe la jurisdiccion solo selecciona los laboratorios
     * correspondientes a esa jurisdiccion
     */
    public function determinacionesSinCargar($operativo){

        // componemos la consulta
        $consulta = "SELECT cce.laboratorios.ID AS id,
                            cce.laboratorios.NOMBRE AS laboratorio,
                            diccionarios.provincias.NOM_PROV AS jurisdiccion
                     FROM cce.laboratorios INNER JOIN diccionarios.localidades ON cce.laboratorios.LOCALIDAD = diccionarios.localidades.CODLOC
                                           INNER JOIN diccionarios.provincias ON diccionarios.localidades.CODPCIA = diccionarios.provincias.COD_PROV
                     WHERE cce.laboratorios.ACTIVO = 'Si' AND
                           cce.laboratorios.ID NOT IN (SELECT existentes.ID AS id_laboratorio
                                                       FROM cce.chag_datos INNER JOIN cce.laboratorios AS existentes ON cce.chag_datos.LABORATORIO = existentes.ID
                                                                           INNER JOIN cce.operativos_chagas ON cce.chag_datos.OPERATIVO = cce.operativos_chagas.ID
                                                       WHERE cce.operativos_chagas.OPERATIVO_NRO = '$operativo')
                     ORDER BY diccionarios.provincias.NOM_PROV,
                              cce.laboratorios.NOMBRE;";

        // obtenemos el vector, lo convertimos a minusculas y retornamos
        $resultado = $this->Link->query($consulta);
        $registros = array_change_key_case($resultado->fetchAll(PDO::FETCH_ASSOC), CASE_LOWER);

        // retornamos el vector
        return $registros;

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param anio string con el año a informar
     * @return paneles string con el número de paneles enviados
     * Método que recibe como parámetro un string con el año a informar
     * y retorna el número de paneles enviados a las jurisdicciones
     * en ese año
     */
    public function panelesEnviados ($anio){

        // Componemos y ejecutamos la consulta
        $consulta = "SELECT COUNT(cce.etiquetas.ETIQUETA_NRO) DIV 6 AS paneles
                     FROM cce.etiquetas
                     WHERE cce.etiquetas.OPERATIVO LIKE '%$anio';";

        // obtenemos el vector, lo convertimos a minusculas y retornamos
        $resultado = $this->Link->query($consulta);
        $registro = array_change_key_case($resultado->fetch(PDO::FETCH_ASSOC), CASE_LOWER);
        extract($registro);
        return $paneles;

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @return vector con la nomina de jurisdicciones
     * Metodo que retorna un vector con la nomina de jurisdicciones
     * del pais del usuario
     */
    protected function nominaJurisdicciones(){

        // componemos la consulta con la nomina de provincias
        $sql = "SELECT diccionarios.provincias.NOM_PROV AS jurisdiccion
                FROM diccionarios.provincias INNER JOIN diccionarios.paises ON diccionarios.provincias.PAIS = diccionarios.paises.ID
                WHERE diccionarios.paises.NOMBRE = '$this->Pais'
                ORDER BY diccionarios.provincias.NOM_PROV; ";
        $resultado = $this->Link->query($sql);

        // obtenemos la nomina de jurisdicciones
        $registros = array_change_key_case($resultado->fetchAll(PDO::FETCH_ASSOC), CASE_LOWER);

        // retornamos el vector
        return $registros;

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @return array con la nomina de laboratorios
     * Metodo que retorna un array con la nomina de jurisdicciones y el
     * numero de laboratorios por fuente de financiamiento usamos un array
     * para devolver la tabla al reporte lista para ser presentada
     */
    public function laboratoriosParticipantes(){

        // componemos la consulta
        $consulta = "SELECT cce.vw_financiamiento.provincia AS provincia,
                            cce.vw_financiamiento.nacional AS nacional,
                            cce.vw_financiamiento.provincial AS provincial,
                            cce.vw_financiamiento.municipal AS municipal,
                            cce.vw_financiamiento.universitario AS universitario,
                            cce.vw_financiamiento.privado AS privado,
                            cce.vw_financiamiento.obrasocial AS obrasocial
                     FROM cce.vw_financiamiento
                     WHERE cce.vw_financiamiento.pais = '$this->Pais';";
        $resultado = $this->Link->query($consulta);

        // obtenemos el vector
        $registros = array_change_key_case($resultado->fetchAll(PDO::FETCH_ASSOC), CASE_LOWER);

        // retornamos el vector
        return $registros;

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param anio string con el año a reportar
     * @return matriz con la nomina de jurisdicciones, el numero de
     * paneles enviados a cada jurisdiccion, el numero de laboratorios
     * participantes, el numero de determinaciones cargadas y el
     * porcentaje de respuesta
     */
    public function panelesProcesados($anio){

        // inicializamos las variables
        $totalLaboratorios = 0;
        $totalPanelesEnviados = 0;
        $totalPanelesProcesados = 0;

        // componemos la consulta de los laboratorios activos
        $consulta = "SELECT diccionarios.provincias.NOM_PROV AS jurisdiccion,
                            COUNT(cce.laboratorios.ID) AS laboratorios
                     FROM cce.laboratorios INNER JOIN diccionarios.localidades ON cce.laboratorios.LOCALIDAD = diccionarios.localidades.CODLOC
                                           INNER JOIN diccionarios.provincias ON diccionarios.localidades.CODPCIA = diccionarios.provincias.COD_PROV
                                           INNER JOIN diccionarios.paises ON diccionarios.provincias.PAIS = diccionarios.paises.ID
                     WHERE diccionarios.paises.NOMBRE = '$this->Pais' AND
                           cce.laboratorios.ACTIVO = 'Si'
                     GROUP BY diccionarios.provincias.NOM_PROV
                     ORDER BY diccionarios.provincias.NOM_PROV;";
        $resultado = $this->Link->query($consulta);
        $registros = array_change_key_case($resultado->fetchAll(PDO::FETCH_ASSOC), CASE_LOWER);

        // recorremos el vector cargando las provincias y los laboratorios
        foreach($registros AS $record){

            // obtenemos el registro
            extract($record);

            // lo agregamos
            $panelesEnviados[$jurisdiccion] = array();

            // ahora agregamos la cantidad de laboratorios activos
            $panelesEnviados[$jurisdiccion]["activos"] = $laboratorios;

            // incrementamos el total de laboratorios
            $totalLaboratorios += $laboratorios;

        }

        // agregamos la ultima fila
        $panelesEnviados["Total"] = array();
        $panelesEnviados["Total"]["activos"] = $totalLaboratorios;

        // componemos la consulta de los laboratorios a los que se enviaron
        // muestras
        $sql = "SELECT diccionarios.provincias.NOM_PROV AS jurisdiccion,
                       COUNT(DISTINCT(cce.laboratorios.ID)) AS laboratorios
                FROM cce.laboratorios INNER JOIN diccionarios.localidades ON cce.laboratorios.LOCALIDAD = diccionarios.localidades.CODLOC
                                      INNER JOIN diccionarios.provincias ON diccionarios.localidades.CODPCIA = diccionarios.provincias.COD_PROV
                                      INNER JOIN diccionarios.paises ON diccionarios.provincias.PAIS = diccionarios.paises.ID
                                      INNER JOIN cce.etiquetas ON cce.laboratorios.ID = cce.etiquetas.ID_LABORATORIO
                WHERE cce.laboratorios.activo = 'Si' AND
                      cce.etiquetas.OPERATIVO LIKE '%$anio' AND
                      diccionarios.paises.NOMBRE = '$this->Pais'
                GROUP BY diccionarios.provincias.NOM_PROV
                ORDER BY diccionarios.provincias.NOM_PROV;";
        $muestras = $this->Link->query($sql);
        $sueros = array_change_key_case($muestras->fetchAll(PDO::FETCH_ASSOC), CASE_LOWER);

        // recorremos el vector
        foreach ($sueros AS $record){

            // obtenemos el registro
            extract($record);

            // agregamos los paneles enviados
            $panelesEnviados[$jurisdiccion]["enviados"] = $laboratorios;

            // incrementa el total de paneles
            $totalPanelesEnviados += $laboratorios;

        }

        // agregamos el total de paneles enviados
        $panelesEnviados["Total"]["enviados"] = $totalPanelesEnviados;

        // componemos la consulta de los laboratorios que cargaron muestras
        $query = "SELECT diccionarios.provincias.NOM_PROV AS jurisdiccion,
                         COUNT(DISTINCT(cce.laboratorios.ID)) AS laboratorios
                  FROM cce.laboratorios INNER JOIN diccionarios.localidades ON cce.laboratorios.LOCALIDAD = diccionarios.localidades.CODLOC
                                        INNER JOIN diccionarios.provincias ON diccionarios.localidades.CODPCIA = diccionarios.provincias.COD_PROV
                                        INNER JOIN diccionarios.paises ON diccionarios.provincias.PAIS = diccionarios.paises.ID
                                        INNER JOIN cce.chag_datos ON cce.laboratorios.ID = cce.chag_datos.LABORATORIO
                                        INNER JOIN cce.operativos_chagas ON cce.chag_datos.OPERATIVO = cce.operativos_chagas.ID
                  WHERE laboratorios.activo = 'Si' AND
                        operativos_chagas.OPERATIVO_NRO LIKE '%$anio' AND
                        diccionarios.paises.NOMBRE = '$this->Pais'
                  GROUP BY diccionarios.provincias.NOM_PROV
                  ORDER BY diccionarios.provincias.NOM_PROV; ";
        $paneles = $this->Link->query($query);
        $procesados = array_change_key_case($paneles->fetchAll(PDO::FETCH_ASSOC), CASE_LOWER);

        // recorremos el vector de paneles
        foreach($procesados AS $registro){

            // obtenemos el registro
            extract($registro);

            // lo agregamos a la matriz
            $panelesEnviados[$jurisdiccion]["procesados"] = $laboratorios;

            // incrementa el total de paneles
            $totalPanelesProcesados += $laboratorios;

        }

        // agregamos el total y el porcentaje
        $panelesEnviados["Total"]["procesados"] = $totalPanelesProcesados;

        // ahora recorremos el array calculando los porcentajes
        foreach ($panelesEnviados AS $provincia => $datos){

            // verificamos que halla remitido muestras
            if (array_key_exists('procesados', $datos)) {

                // calculamos el porcentaje dejando el punto decimal
                $respuesta = ($datos["procesados"] / $datos["enviados"]) * 100;
                $porcentaje = number_format($respuesta, 2);
            } else {
                $porcentaje = "0.00";
            }

            // lo asignamos en la matriz
            $panelesEnviados[$provincia]["respuesta"] = $porcentaje;

        }

        // retornamos el vector
        return $panelesEnviados;

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param $anio string con el año a reportar
     * @return $determinaciones con la nomina de jurisdicciones y el numero de
     * determinaciones cargadas fuera de termino
     */
    public function fueraTermino($anio){

        // obtenemos la tabla de frecuencias
        $consulta = "SELECT cce.vw_termino.jurisdiccion AS jurisdiccion,
                            cce.vw_termino.atermino AS atermino,
                            cce.vw_termino.fueratermino AS fueratermino
                     FROM cce.vw_termino
                     WHERE cce.vw_termino.anio = '$anio' AND
                           cce.vw_termino.pais = '$this->Pais';";
        $resultado = $this->Link->query($consulta);
        $determinaciones = array_change_key_case($resultado->fetchAll(PDO::FETCH_ASSOC), CASE_LOWER);

        // retornamos la matriz
        return $determinaciones;

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param $anio - cadena con el año a calcular
     * @return $dedterminaciones - array
     * Método utilizado para graficar las determinaciones fuera de término
     * utiliza la misma vista pero calculando los totales nacionales
     */
    public function totalFueraTermino($anio){

        // obtenemos la tabla de frecuencias
        $consulta = "SELECT SUM(cce.vw_termino.atermino) AS atermino,
                            SUM(cce.vw_termino.fueratermino) AS fueratermino
                     FROM cce.vw_termino
                     WHERE cce.vw_termino.anio = '$anio' AND
                           cce.vw_termino.pais = '$this->Pais';";
        $resultado = $this->Link->query($consulta);
        $determinaciones = array_change_key_case($resultado->fetch(PDO::FETCH_ASSOC), CASE_LOWER);

        // retornamos la matriz
        return $determinaciones;

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param anio string con el año a reportar
     * @return matriz con la distribucion de la carga de muestras
     * Metodo que recibe como parametro el año a reportar y retorna una
     * matriz con la distribucion por jurisdiccion de las tecnicas
     * mas empleadas
     */
    public function distribucionMuestras($anio){

        // componemos y ejecutamos la consulta de las provincias que
        // remitieron muestras y el numero de determinaciones de cada
        // tecnica
        $consulta = "SELECT cce.vw_muestras.jurisdiccion AS jurisdiccion,
                            cce.vw_muestras.ap AS ap,
                            cce.vw_muestras.elisa AS elisa,
                            cce.vw_muestras.hai AS hai,
                            cce.vw_muestras.ifi AS ifi,
                            cce.vw_muestras.quimio AS quimio,
                            cce.vw_muestras.otra AS otra
                     FROM cce.vw_muestras
                     WHERE cce.vw_muestras.anio = '$anio' AND
                           cce.vw_muestras.pais = '$this->Pais';";
        $resultado = $this->Link->query($consulta);
        $muestras = array_change_key_case($resultado->fetchAll(PDO::FETCH_ASSOC), CASE_LOWER);

        // retornamos el vector
        return $muestras;

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param anio string con el año a reportar
     * @return matriz con la distribucion de lector / visual en elisa
     * Metodo que recibe como parametro el año a reportar y retorna una
     * matriz con la distribucion de uso de lector de placas
     */
    public function lectorVisual($anio){

        // componemos la consulta
        $consulta = "SELECT cce.vw_lector.jurisdiccion AS provincia,
                            cce.vw_lector.lector AS lector,
                            cce.vw_lector.visual AS visual
                     FROM cce.vw_lector
                     WHERE cce.vw_lector.anio = '$anio' AND
                           cce.vw_lector.pais = '$this->Pais';";
        $resultado = $this->Link->query($consulta);
        $distribucion = array_change_key_case($resultado->fetchAll(PDO::FETCH_ASSOC), CASE_LOWER);

        // retornamos el vector
        return $distribucion;

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param anio string con el año a reportar
     * @return matriz con la distribucion de tecnicas utilizadas
     * Metodo que recibe como parametro el año a reportar y genera la
     * matriz con el numero de tecnicas empleadas por cada jurisdiccion
     */
    public function nroTecnicas($anio){

        // componemos y ejecutamos la consulta no lo hacemos con vistas
        // porque para parametrizar tendríamos que crear una función
        $consulta = "SELECT consulta.provincia AS jurisdiccion,
                            SUM(CASE WHEN consulta.tecnicas = 1 THEN 1
                                ELSE 0
                                END) AS tecnica1,
                            SUM(CASE WHEN consulta.tecnicas = 2 THEN 1
                                ELSE 0
                                END) AS tecnica2,
                            SUM(CASE WHEN consulta.tecnicas = 3 THEN 1
                                ELSE 0
                                END) AS tecnica3,
                            SUM(CASE WHEN consulta.tecnicas = 4 THEN 1
                                ELSE 0
                                END) AS tecnica4
                     FROM (SELECT diccionarios.provincias.NOM_PROV AS provincia,
                                  cce.laboratorios.ID AS laboratorios,
                                  COUNT(DISTINCT(cce.chag_datos.TECNICA)) AS tecnicas
                           FROM cce.chag_datos INNER JOIN cce.laboratorios ON cce.chag_datos.LABORATORIO = cce.laboratorios.ID
                                               INNER JOIN diccionarios.localidades ON cce.laboratorios.LOCALIDAD = diccionarios.localidades.CODLOC
                                               INNER JOIN diccionarios.provincias ON diccionarios.localidades.CODPCIA = diccionarios.provincias.COD_PROV
                                               INNER JOIN diccionarios.paises ON diccionarios.provincias.PAIS = diccionarios.paises.ID
                                               INNER JOIN cce.operativos_chagas ON cce.operativos_chagas.ID = cce.chag_datos.OPERATIVO
                           WHERE cce.operativos_chagas.OPERATIVO_NRO LIKE '%$anio' AND
                                 diccionarios.paises.NOMBRE = '$this->Pais'
                           GROUP BY cce.laboratorios.ID) AS consulta
                     GROUP BY consulta.provincia
                     ORDER BY consulta.provincia;";
        $resultado = $this->Link->query($consulta);
        $distribucion = array_change_key_case($resultado->fetchAll(PDO::FETCH_ASSOC), CASE_LOWER);

        // retorna la matriz
        return $distribucion;

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param anio string con el año a reportar
     * @return array con la eficiencia por jurisdicción
     * Método que recibe como parámetro el año a calcular y retorna una
     * matriz con el número de determinaciones, las correctas y la
     * eficiencia de cada jurisdicción
     */
    public function obtenerConcordancia($anio){

        // obtenemos la concordancia por jurisdicción
        $consulta = "SELECT vw_concordancia.provincia AS provincia,
                            SUM(vw_concordancia.determinaciones) AS determinaciones,
                            SUM(vw_concordancia.correctas) AS correctas,
                            SUM(vw_concordancia.incorrectas) AS incorrectas
                     FROM vw_concordancia
                     WHERE vw_concordancia.pais = '$this->Pais' AND
                           vw_concordancia.anio = '$anio'
                     GROUP BY vw_concordancia.provincia;; ";
        $resultado = $this->Link->query($consulta);
        $concordancia = array_change_key_case($resultado->fetchAll(PDO::FETCH_ASSOC), CASE_LOWER);

        // retornamos el vector
        return $concordancia;

    }

    /**
     * @param anio string con el año a reportar
     * @return matriz con la eficiencia por tecnica
     * Metodo que recibe como parametro el año a reportar y retorna una
     * matriz con la eficiencia por cada una de las tecnicas
     */
    public function eficienciaTecnicas($anio){

        // consultamos la vista de concordancia
        $consulta = "SELECT cce.vw_conc_tecnica.provincia AS provincia,
                            cce.vw_conc_tecnica.ap AS ap,
                            cce.vw_conc_tecnica.correctasap AS correctasap,
                            cce.vw_conc_tecnica.elisa AS elisa,
                            cce.vw_conc_tecnica.correctaselisa AS correctaselisa,
                            cce.vw_conc_tecnica.hai AS hai,
                            cce.vw_conc_tecnica.correctashai AS correctashai,
                            cce.vw_conc_tecnica.ifi AS ifi,
                            cce.vw_conc_tecnica.correctasifi AS correctasifi,
                            cce.vw_conc_tecnica.quimio AS quimio,
                            cce.vw_conc_tecnica.correctasquimio AS correctasquimio,
                            cce.vw_conc_tecnica.otras AS otras,
                            cce.vw_conc_tecnica.correctasotras AS correctasotras
                     FROM cce.vw_conc_tecnica
                     WHERE cce.vw_conc_tecnica.pais = '$this->Pais' AND
                           cce.vw_conc_tecnica.anio = '$anio';";

        // ejecutamos la consulta
        $resultado = $this->Link->query($consulta);
        $concordancia = array_change_key_case($resultado->fetchAll(PDO::FETCH_ASSOC), CASE_LOWER);

        // retornamos la matriz
        return $concordancia;

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param anio string con el año a reportar
     * @return resultset con la nómina de jurisdicciones
     * Método que recibe como parámetro el año a reportar y retorna un
     * vector con las provincias que no participaron del control de calidad
     */
    public function provinciasFaltantes($anio){

        // componemos la consulta
        $consulta = "SELECT diccionarios.provincias.NOM_PROV AS jurisdiccion
                     FROM diccionarios.provincias INNER JOIN diccionarios.paises ON diccionarios.provincias.PAIS = diccionarios.paises.ID
                     WHERE diccionarios.provincias.NOM_PROV != 'Indeterminado' AND
                           diccionarios.paises.NOMBRE = '$this->Pais' AND
                           diccionarios.provincias.NOM_PROV NOT IN
                           (SELECT diccionarios.provincias.NOM_PROV AS provincia
                            FROM cce.chag_datos INNER JOIN cce.laboratorios ON cce.chag_datos.LABORATORIO = cce.laboratorios.ID
                                                INNER JOIN diccionarios.localidades ON cce.laboratorios.LOCALIDAD = diccionarios.localidades.CODLOC
                                                INNER JOIN diccionarios.provincias ON diccionarios.localidades.CODPCIA = diccionarios.provincias.COD_PROV
                                                INNER JOIN cce.operativos_chagas ON cce.chag_datos.OPERATIVO = cce.operativos_chagas.ID
                            WHERE cce.operativos_chagas.OPERATIVO_NRO LIKE '%$anio')
                     GROUP BY diccionarios.provincias.NOM_PROV
                     ORDER BY diccionarios.provincias.NOM_PROV;";
        $vector = $this->Link->query($consulta);
        $resultado = array_change_key_case($vector->fetchAll(PDO::FETCH_ASSOC), CASE_LOWER);

        // retornamos el vector
        return $resultado;

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @return resultset con la nómina de técnicas registradas
     */
    public function nominaTecnicas(){

        // componemos y ejecutamos la consulta
        $consulta = "SELECT cce.tecnicas.TECNICA AS tecnica
                     FROM cce.tecnicas
                     ORDER BY cce.tecnicas.TECNICA;";
        $vector = $this->Link->query($consulta);
        $row = $vector->fetchAll(PDO::FETCH_ASSOC);
        $resultado = array_change_key_case($row, CASE_LOWER);

        // retornamos el vector
        return $resultado;

    }

    /**
     * @param anio string con el año a reportar
     * @return matriz con la distribucion de valores mal informados
     * Metodo que recibe como parametro el año a reportar y retorna
     * una matriz con la distribucion de valores de corte y lectura
     * mal informados
     */
    public function valoresCorte($anio){

        // compone la consulta calculando los errores de corte
        // que son aquellos en donde se seleccionó otro o en
        // el caso elisa se seleccionó lector y el valor
        // ingresado es 0.000
        $consulta = "SELECT cce.vw_corte.jurisdiccion AS jurisdiccion,
                            cce.vw_corte.malcorte AS malcorte,
                            cce.vw_corte.mallectura AS mallectura
                     FROM cce.vw_corte
                     WHERE cce.vw_corte.anio = '$anio' AND
                           cce.vw_corte.pais = '$this->Pais';";
        $resultado = $this->Link->query($consulta);
        $corte = array_change_key_case($resultado->fetchAll(PDO::FETCH_ASSOC), CASE_LOWER);

        // retornamos el vector
        return $corte;

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @return array
     * Mètodo que retorna la nòmina de otras pruebas
     */
    public function otrasPruebas($anio){

        // componemos y ejecutamos la consulta
        $consulta = "SELECT cce.vw_otras.jurisdiccion AS jurisdiccion,
                            cce.vw_otras.laboratorio AS laboratorio,
                            cce.vw_otras.prueba AS prueba,
                            cce.vw_otras.marca AS marca,
                            cce.vw_otras.id AS id
                     FROM vw_otras
                     WHERE cce.vw_otras.anio = '$anio' AND
                           cce.vw_otras.pais = '$this->Pais';";
        $resultado = $this->Link->query($consulta);

        // obtenemos el vector y retornamos
        $otras = array_change_key_case($resultado->fetchAll(PDO::FETCH_ASSOC), CASE_LOWER);
        return $otras;

    }

    /**
     * @param anio string con el año a reportar
     * @return matriz con la distribucion de falsos negativos
     * Metodo que recibe como parametro el año a reportar y retorna una
     * matriz con la distribucion por provincia de los falsos negativos
     */
    public function falsosNegativos($anio){

        // componemos la consulta en la vista
        $consulta = "SELECT cce.vw_falsos_negativos.provincia AS provincia,
                            cce.vw_falsos_negativos.ap AS ap,
                            cce.vw_falsos_negativos.falsosap AS falsosap,
                            cce.vw_falsos_negativos.elisa AS elisa,
                            cce.vw_falsos_negativos.falsoselisa AS falsoselisa,
                            cce.vw_falsos_negativos.hai AS hai,
                            cce.vw_falsos_negativos.falsoshai AS falsoshai,
                            cce.vw_falsos_negativos.ifi AS ifi,
                            cce.vw_falsos_negativos.falsosifi AS falsosifi,
                            cce.vw_falsos_negativos.quimio AS quimio,
                            cce.vw_falsos_negativos.falsosquimio AS falsosquimio,
                            cce.vw_falsos_negativos.otras AS otras,
                            cce.vw_falsos_negativos.falsosotras AS falsosotras
                     FROM cce.vw_falsos_negativos
                     WHERE cce.vw_falsos_negativos.pais = '$this->Pais' AND
                     cce.vw_falsos_negativos.anio = '$anio';";
        $resultado = $this->Link->query($consulta);

        // obtenemos el vector y retornamos
        $falsos = array_change_key_case($resultado->fetchAll(PDO::FETCH_ASSOC), CASE_LOWER);
        return $falsos;

    }

    /**
     * @param anio string con el año a reportar
     * @return matriz con la distribucion de falsos negativos
     * Metodo que recibe como parametro el año a reportar y retorna una
     * matriz con la distribucion por provincia de los falsos positivos
     */
    public function falsosPositivos($anio){

        // componemos la consulta en la vista
        $consulta = "SELECT cce.vw_falsos_positivos.provincia AS provincia,
                            cce.vw_falsos_positivos.ap AS ap,
                            cce.vw_falsos_positivos.falsosap AS falsosap,
                            cce.vw_falsos_positivos.elisa AS elisa,
                            cce.vw_falsos_positivos.falsoselisa AS falsoselisa,
                            cce.vw_falsos_positivos.hai AS hai,
                            cce.vw_falsos_positivos.falsoshai AS falsoshai,
                            cce.vw_falsos_positivos.ifi AS ifi,
                            cce.vw_falsos_positivos.falsosifi AS falsosifi,
                            cce.vw_falsos_positivos.quimio AS quimio,
                            cce.vw_falsos_positivos.falsosquimio AS falsosquimio,
                            cce.vw_falsos_positivos.otras AS otras,
                            cce.vw_falsos_positivos.falsosotras AS falsosotras
                     FROM cce.vw_falsos_positivos
                     WHERE cce.vw_falsos_positivos.pais = '$this->Pais' AND
                     cce.vw_falsos_positivos.anio = '$anio';";
        $resultado = $this->Link->query($consulta);

        // obtenemos el vector y retornamos
        $falsos = array_change_key_case($resultado->fetchAll(PDO::FETCH_ASSOC), CASE_LOWER);
        return $falsos;

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param string - $anio
     * @param string - $tecnica
     * Método que recibe como parámetros el año y el nombre de una
     * técnica y presenta la nómina de eficiencia por cada una de
     * las marcas de esa técnica
     */
    public function eficienciaMarcas($anio, $tecnica){

        // componemos la consulta
        $consulta = "SELECT cce.vw_eficiencia_marcas.marca AS marca,
                            cce.vw_eficiencia_marcas.determinaciones AS determinaciones,
                            cce.vw_eficiencia_marcas.correctas AS correctas
                     FROM cce.vw_eficiencia_marcas
                     WHERE cce.vw_eficiencia_marcas.pais = '$this->Pais' AND
                           cce.vw_eficiencia_marcas.tecnica = '$tecnica' AND
                           cce.vw_eficiencia_marcas.anio = '$anio';";
        $resultado = $this->Link->query($consulta);

        // obtenemos y retornamos el vector
        $marcas = array_change_key_case($resultado->fetchAll(PDO::FETCH_ASSOC), CASE_LOWER);
        return $marcas;

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param string $anio - el año del reporte
     * @return array
     * Método que recibe como parámetro el año del reporte y retorna
     * un array con la nómina de etiquetas huérfanas por jurisdicción
     */
    public function etiquetasHuerfanas($anio){

    	// componemos la consulta
    	$consulta = "SELECT diccionarios.provincias.NOM_PROV AS jurisdiccion,
                            COUNT(chag_datos.ID) AS determinaciones
                     FROM chag_datos INNER JOIN laboratorios ON chag_datos.LABORATORIO = laboratorios.ID
                                     INNER JOIN diccionarios.localidades ON laboratorios.LOCALIDAD = diccionarios.localidades.CODLOC
                                     INNER JOIN diccionarios.provincias ON diccionarios.localidades.CODPCIA = diccionarios.provincias.COD_PROV
                                     INNER JOIN operativos_chagas ON chag_datos.OPERATIVO = operativos_chagas.ID
                                     INNER JOIN diccionarios.paises ON diccionarios.provincias.PAIS = diccionarios.paises.ID
                    WHERE chag_datos.MUESTRA_NRO NOT IN
                          (SELECT chag_datos.MUESTRA_NRO
                           FROM chag_datos INNER JOIN etiquetas ON chag_datos.MUESTRA_NRO = etiquetas.ETIQUETA_NRO) AND
                                operativos_chagas.OPERATIVO_NRO LIKE '%$anio' AND
                                diccionarios.paises.NOMBRE = '$this->Pais'
                    GROUP BY diccionarios.provincias.NOM_PROV
                    ORDER BY diccionarios.provincias.NOM_PROV; ";
        $resultado = $this->Link->query($consulta);

        // obtenemos y retornamos el vector
        $huerfanas = array_change_key_case($resultado->fetchAll(PDO::FETCH_ASSOC), CASE_LOWER);
        return $huerfanas;

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param array $eficiencia - array con los datos de eficiencia
     * @param double $media - la media de la muestra
     * @param double $desvio - el desvío estandard de la muestra
     * @return array
     * Método que recibe como parámetro un array con la eficiencia
     * la media de la muestra y el desvío estandar de la muestra
     * Retorna un array con los jurisdicciones que muestran un
     * rendimjiento por debajo de un desvío estandard
     */
    public function rendimientoMuestra($eficiencia, $media, $desvio){

        // como las funciones estadísticas retornan la media y el
        // desvío como una cadena formateada, primero los
        // convertirmos
        $media = floatval($media);
        $desvio = floatval($desvio);

        // definimos la variable de retorno
        $nomina = array();

        // obtenemos el piso
        $piso = $media - $desvio;

        // ahora recorremos el array
        for ($i = 0; $i < count($eficiencia); $i++){

            // si se encuentra por debajo del piso
            if ($eficiencia[$i][1] < $piso){

                // agregamos la jurisdicción
                $nomina[] = array("jurisdiccion" => $eficiencia[$i][0]);

            }

        }

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @return array
     * Método que retorna la eficiencia de los últimos diez años
     */
    public function historiaEficiencia(){

        // componemos la consulta
        $consulta = "SELECT vw_concordancia.anio AS anio,
                            SUM(vw_concordancia.determinaciones) AS determinaciones,
                            SUM(vw_concordancia.correctas) AS correctas
                     FROM vw_concordancia
                     GROUP BY vw_concordancia.anio
                     ORDER BY vw_concordancia.anio DESC
                     LIMIT 10;";
        $resultado = $this->Link->query($consulta);

        // obtenemos el vector y retornamos
        $historia = array_change_key_case($resultado->fetchAll(PDO::FETCH_ASSOC), CASE_LOWER);
        return $historia;

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param string $anio - año del reporte
     * @return array
     * Método que recibe como parámetro el año de un reporte y
     * retorna el vector con la nómina por jurisdicción de determinaciones
     * discordantes
     */
    public function discordantes($anio){

        // declaración de variables
        $discordantes = array();

        // obtenemos la nómina de jurisdicciones y el número de
        // determinaciones realizada por cada una de ellas
        $determinaciones = $this->determinacionesCargadas($anio);

        // ahora recorremos el vector armando la matriz de datos
        foreach($determinaciones AS $registro){

            // obtenemos el registro
            extract($registro);

            // lo agregamos
            $discordantes[$jurisdiccion] = array();

            // ahora agregamos la cantidad de laboratorios activos
            $discordantes[$jurisdiccion]["determinaciones"] = $determinaciones;

        }

        // ahora obtenemos la nómina de discordantes
        $consulta = "SELECT cce.vw_discordantes.provincia AS provincia,
                           cce.vw_discordantes.discordantes AS noconordantes
                    FROM cce.vw_discordantes
                    WHERE cce.vw_discordantes.pais = '$this->Pais' AND
                          cce.vw_discordantes.anio = '$anio';";
        $resultado = $this->Link->query($consulta);

        // obtenemos el vector
        $paneles = array_change_key_case($resultado->fetchAll(PDO::FETCH_ASSOC), CASE_LOWER);

        // ahora recorremos el vector
        foreach($paneles AS $registro){

            // obtenemos el registro
            extract($registro);

            // agregamos el valor
            $discordantes[$provincia]["noconcordantes"] = $noconordantes;

        }

        // retornamos el vector
        return $discordantes;

    }

}
