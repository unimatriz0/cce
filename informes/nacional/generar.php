<?php

/**
 *
 * generar.php
 *
 * @package     CCE
 * @subpackage  Reportes
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.4.0 (15/11/2018)
 * @copyright   Copyright (c) 2018, INP
 *
 * Método que recibe por get los datos del reporte nacional a
 * generar
 *
*/

// incluimos e instanciamos las clases
require_once ("reporte.class.php");
$reporte = new Reporte();

// fijamos las propiedades
$reporte->setAnio($_POST["Anio"]);
$reporte->setEncabezado($_POST["Encabezado"]);

// generamos el reporte
$reporte->initReporte();

// retornamos verdadero
echo json_encode(array("Error" => true));

?>