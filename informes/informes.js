/*
 * Nombre: informes.js
 * Autor: Lic. Claudio Invernizzi
 * E-Mail: cinvernizzi@gmail.com
 * Fecha: 16/11/2018
 * Licencia: GPL
 * Comentarios: Serie de funciones y procedimientos utilizados por el
 *              sistema de control de calidad en la generación de
 *              reportes
 */

 /**
 *
 * Atención, utilizamos el nuevo estandard definido en ECMAScript 2015 (ES6)
 * este permite definir clases con el mètodo ya utilizado en php y java
 * como creo que es mas claro lo utilizaremos, la compatibilidad está
 * anunciada para firefox y chrome así que no deberìa haber problemas
 *
 */

/* jshint esversion: 6 */

/**
 * @author Claudio Invernizzi <cinvernizzi@gmail.com>
 * @class Clase que controla las operaciones del formulario de marcas
 * Definición de la clase
 */
class Informes {

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @constructor
     */
    constructor(){

        // inicializamos las variables de clase
        this.initReportes();

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que inicializa las variables de clase
     */
    initReportes(){

        // inicializa las variables
        this.Anio = 0;                  // año a reportar
        this.Provincia = "";            // clave indec de la provincia
        this.Encabezado = "";           // texto del encabezado
        this.layerReportes = "";        // layer emergente

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que carga en el contenido el formulario de
     * generación de reportes
     */
    Generar(){

        // reiniciamos la sesión
        cce.contador();

        // cargamos el formulario
        $("#form_reportes").load("informes/form_generar.html");

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método llamado al cambiar el select de provincia que
     * actualiza el select del tipo de reporte
     */
    selNacional(){

        // reiniciamos la sesión
        cce.contador();

        // si hay una jurisdicción seleccionada
        if (document.getElementById("jurisdiccion_reporte").value != 0){

            // fija el reporte en provincial
            document.getElementById("tipo_reporte").value = "Provincial";

        // si no hay ninguna
        } else {

            // fija el reporte en nacional
            document.getElementById("tipo_reporte").value = "Nacional";

        }

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método llamado al cambiar el valor del tipo de reporte
     * verifica que cuando se elije nacional dejar el combo
     * de jurisdicción en blanco
     */
    tipoReporte(){

        // reiniciamos la sesión
        cce.contador();

        // si se eligió nacional o está en blanco
        if (document.getElementById("tipo_reporte").value == "Nacional" || document.getElementById("tipo_reporte").value == ""){

            // fija la provincia en blanco
            document.getElementById("jurisdiccion_reporte").value = 0;

        }

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que simplemente reinicia el formulario
     */
    Cancelar(){

        // reiniciamos la sesión
        cce.contador();

        // reiniciamos el formulario y fijamos el foco
        document.getElementById("generar_reporte").reset();
        document.getElementById("anio_reporte").focus();

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método llamado al pulsar el botón generar que verifica
     * los datos del formulario antes de enviarlo al servidor
     */
    Validar(){

        // reiniciamos la sesión
        cce.contador();

        // declaración de variables
        var mensaje;

        // si no indicó el año
        if (document.getElementById("anio_reporte").value == 0){

            // presenta el mensaje y retorna
            mensaje = "Debe indicar el año a generar";
            new jBox('Notice', {content: mensaje, color: 'red'});
            document.getElementById("anio_reporte").focus();
            return false;

        // si seleccionó
        } else {

            // almacena en la variable de clase
            this.Anio = document.getElementById("anio_reporte").value;

        }

        // si no indicó el tipo de reporte
        if (document.getElementById("tipo_reporte").value == ""){

            // presenta el mensaje y retorna
            mensaje = "Indique el tipo de reporte";
            new jBox('Notice', {content: mensaje, color: 'red'});
            document.getElementById("tipo_reporte").focus();
            return false;

        }

        // si es provincial y no seleccionó jurisdicción
        if (document.getElementById("tipo_reporte").value == "Provincial" && document.getElementById("jurisdiccion_reporte").value == ""){

            // presenta el mensaje y retorna
            mensaje = "Indique la jurisdicción a informar";
            new jBox('Notice', {content: mensaje, color: 'red'});
            document.getElementById("jurisdiccion_reporte").focus();
            return false;

        // si indicó
        } else if (document.getElementById("tipo_reporte").value == "Provincial" && document.getElementById("jurisdiccion_reporte").value != ""){

            // almacena en la variable de clase
            this.Provincia = document.getElementById("jurisdiccion_reporte").value;

        }

        // si no ingresó el texto presenta un alerta y sigue
        if (document.getElementById("encabezado_reporte").value == ""){

            // presenta el mensaje
            mensaje = "El reporte no tendrá encabezado";
            new jBox('Notice', {content: mensaje, color: 'red'});

        }

        // almacena en la variable de clase
        this.Encabezado = document.getElementById("encabezado_reporte").value;

        // genera el reporte
        this.generaReporte();

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que envía los datos del formulario al servidor
     * y presenta el reporte en pantalla
     */
    generaReporte(){

        // reiniciamos la sesión
        cce.contador();

        // creamos el formulario
        var datosReporte = new FormData();
        datosReporte.append("Anio", this.Anio);
        datosReporte.append("Jurisdiccion", this.Provincia);
        datosReporte.append("Encabezado", this.Encabezado);

        // si no hay una jurisdicción seleccionada
        if (this.Provincia == "" || typeof(this.Provincia) == "undefined"){

            // el reporte es nacional
            $.ajax({
                type: "POST",
                url: "informes/nacional/generar.php",
                data: datosReporte,
                cahe: false,
                contentType: false,
                processData: false,
                dataType: 'json',
                success: function(data) {

                    // si salió todo bien
                    if (data.Error == true){

                        // presenta el mensaje
                        new jBox('Notice', {content: "Reporte Generado ... ", color: 'green'});

                        // ahora lo mostramos
                        informes.verReporte();

                    // si hubo un error
                    } else {

                        // presenta el mensaje
                        new jBox('Notice', {content: "Ha ocurrido un error ... ", color: 'red'});

                    }

                },

            });

        // si hay seleccionada
        } else {

            // el reporte es provincial
            $.ajax({
                type: "POST",
                url: "informes/provincial/generar.php",
                data: datosReporte,
                cahe: false,
                contentType: false,
                processData: false,
                dataType: 'json',
                success: function(data) {

                    // si salió todo bien
                    if (data.Error == true){

                        // presenta el mensaje
                        new jBox('Notice', {content: "Reporte Generado ... ", color: 'green'});

                        // ahora lo mostramos
                        informes.verReporte();

                    // si hubo un error
                    } else {

                        // presenta el mensaje
                        new jBox('Notice', {content: "Ha ocurrido un error ... ", color: 'red'});

                    }

                },

            });

        }

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método llamado luego de generar el reporte que lo muestra en
     * el cuadro de diálogo emergente
     */
    verReporte(){

        // reiniciamos la sesión
        cce.contador();

        // mostramos el diálogo
        this.layerReportes = new jBox('Modal', {
                        animation: 'flip',
                        closeOnEsc: true,
                        closeOnClick: false,
                        closeOnMouseleave: false,
                        closeButton: true,
                        overlay: false,
                        onCloseComplete: function(){
                                this.destroy();
                        },
                        title: 'Reportes de Concordancia',
                        draggable: 'title',
                        repositionOnContent: true,
                        theme: 'TooltipBorder',
                        target: '#top',
                                position: {
                                    x: 'left',
                                    y: 'top'
                                },
                        ajax: {
                            url: 'informes/ver_reporte.php',
                            reload: 'strict'
                        }
                    });
            this.layerReportes.open();

    }

}