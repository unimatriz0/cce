<?php

/**
 *
 * informes/ver_reporte.php
 *
 * @package     CCE
 * @subpackage  Informes
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.4.0 (20/11/2018)
 * @copyright   Copyright (c) 2018, INP
 *
 * Método llamado luego de generar el reporte de eficiencia por un
 * administrador, presenta el pantalla el reporte generado
 *
*/

// enviamos los headers
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);
header("Pragma: no-cache");

// ahora lo presentamos
?>
<object data="../temp/Informe.pdf"
        type="application/pdf"
        width="850" height="500">
</object>