-- -- Estructura de la base de datos de calidad
-- ojo la migramsos a otra base y accedemos a las tablas auxiliares
-- haciendo referencia a la notación de objetos

-- Establecemos la página de códigos
SET CHARACTER_SET_CLIENT=utf8;
SET CHARACTER_SET_RESULTS=utf8;
SET COLLATION_CONNECTION=utf8_spanish_ci;

-- eliminamos la tabla si existe
DROP DATABASE IF EXISTS cce;

-- Creamos la tabla
CREATE DATABASE IF NOT EXISTS `cce`
DEFAULT CHARACTER SET utf8 COLLATE utf8_spanish_ci;

-- seleccionamos la base
USE `cce`;

-- verificar las procedures que hay muchas que quedaron de la vieja
-- generación de reportes
-- las tablas de estadísticas también han quedado como basura
-- cambian los usuarios que ahora usan la clave del registro de
-- responsables
-- es necesario agregar las claves foráneas

-- Estructura de tabla para la tabla `auditoria_resp_chagas`
-- atención aquí modificamos la tabla, la fecha alta ahora
-- es timestamp
-- id del registro ahora es int (6)
-- se agrega LABORATORIO que es la clave del laboratorio al
-- que pertenece en caso de no ser responsable ni referente
--

/***************************************************************************/
/*                                                                         */
/*                    Tabla de Responsables                                */
/*                                                                         */
/***************************************************************************/

-- la tabla de datos de personas autorizadas en el sistema y sus permisos
--
-- Estructura de tabla para la tabla `resp_chagas`
-- eliminamos el campo imagen
-- eliminamos el campo responsable lepra
-- eliminamos el campo referente lepra
-- fecha alta cambia a timestamp automático
-- agregamos LABORATORIO la id del laboratorio autorizado si no es
-- ni responsable ni referente
-- id del registro ahora es int(6)
-- chagas ahora es solo responsable si / no
-- leishmania ahora es solo responsable si / no

-- elimina la tabla si existe
DROP TABLE IF EXISTS `responsables`;

-- la recreamos
CREATE TABLE IF NOT EXISTS `responsables` (
  `ID` int(6) UNSIGNED NOT NULL AUTO_INCREMENT,
  `NOMBRE` varchar(250) DEFAULT NULL,
  `INSTITUCION` varchar(250) NOT NULL,
  `CARGO` varchar(250) NOT NULL,
  `E_MAIL` varchar(100) NOT NULL,
  `TELEFONO` varchar(50) DEFAULT NULL,
  `PAIS` smallint(3) UNSIGNED DEFAULT NULL,
  `LOCALIDAD` varchar(9) COLLATE utf8_spanish_ci NOT NULL,
  `DIRECCION` varchar(250) DEFAULT NULL,
  `CODIGO_POSTAL` varchar(10) DEFAULT NULL,
  `COORDENADAS` varchar(100) DEFAULT NULL,
  `RESPONSABLE_CHAGAS` enum('Si','No') NOT NULL,
  `RESPONSABLE_LEISH` enum('Si','No') DEFAULT 'No',
  `LABORATORIO` int(6) UNSIGNED DEFAULT NULL,
  `ACTIVO` enum('Si','No') NOT NULL,
  `NUEVO_PASS` enum('Si','No') DEFAULT 'No',
  `OBSERVACIONES` text,
  `USUARIO` varchar(20) DEFAULT NULL,
  `PASSWORD` varchar(60) NOT NULL,
  `FECHA_ALTA` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `NIVEL_CENTRAL` enum('Si','No') NOT NULL DEFAULT 'No',
  PRIMARY KEY (`ID`),
  KEY `NOMBRE` (`NOMBRE`),
  KEY `PAIS` (`PAIS`),
  KEY `USUARIO` (`USUARIO`),
  KEY `E_MAIL` (`E_MAIL`),
  KEY `LOCALIDAD` (`LOCALIDAD`),
  KEY `LABORATORIO` (`LABORATORIO`)
)
ENGINE=InnoDB
DEFAULT CHARSET=utf8
COLLATE=utf8_spanish_ci
COMMENT='Datos de usuarios autorizados';

-- la tabla de auditoría de cambios

-- eliminamos si existe
DROP TABLE IF EXISTS `auditoria_responsables`;

-- la recreamos desde cero
CREATE TABLE IF NOT EXISTS `auditoria_responsables` (
  `ID` int(6) UNSIGNED DEFAULT NULL,
  `NOMBRE` varchar(250) DEFAULT NULL,
  `INSTITUCION` varchar(250) NOT NULL,
  `CARGO` varchar(250) NOT NULL,
  `E_MAIL` varchar(100) NOT NULL,
  `TELEFONO` varchar(50) DEFAULT NULL,
  `PAIS` smallint(3) UNSIGNED DEFAULT NULL,
  `LOCALIDAD` varchar(9) NOT NULL,
  `DIRECCION` varchar(250) DEFAULT NULL,
  `CODIGO_POSTAL` varchar(10) DEFAULT NULL,
  `COORDENADAS` varchar(100) DEFAULT NULL,
  `RESPONSABLE_CHAGAS` enum('Si','No') DEFAULT 'No',
  `RESPONSABLE_LEISH` enum('Si','No') DEFAULT 'No',
  `LABORATORIO` int(6) DEFAULT NULL,
  `ACTIVO` enum('Si','No') DEFAULT 'Si',
  `OBSERVACIONES` text,
  `USUARIO` varchar(20) DEFAULT NULL,
  `FECHA_ALTA` date NOT NULL,
  `FECHA_OPERACION` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `NIVEL_CENTRAL` enum('Si','No') DEFAULT 'No',
  `EVENTO` enum('Edicion','Eliminacion') COLLATE utf8_spanish_ci NOT NULL,
  KEY `ID` (`ID`),
  KEY `NOMBRE` (`NOMBRE`),
  KEY `PAIS` (`PAIS`),
  KEY `USUARIO` (`USUARIO`),
  KEY `LOCALIDAD` (`LOCALIDAD`)
)
ENGINE=InnoDB
DEFAULT CHARSET=utf8
COLLATE=utf8_spanish_ci
COMMENT='Auditoría Responsables';

--
-- Disparadores `resp_chagas`
--

-- elimina el trigger si existe
DROP TRIGGER IF EXISTS `auditoria_edita_responsables`;

-- lo creamos desde cero el trigger de edición
CREATE TRIGGER `auditoria_edita_responsables`
AFTER UPDATE ON `responsables`
FOR EACH ROW
INSERT INTO auditoria_responsables
     (ID,
      NOMBRE,
      INSTITUCION,
      CARGO,
      E_MAIL,
      TELEFONO,
      PAIS,
      LOCALIDAD,
      DIRECCION,
      CODIGO_POSTAL,
      COORDENADAS,
      RESPONSABLE_CHAGAS,
      RESPONSABLE_LEISH,
      LABORATORIO,
      ACTIVO,
      OBSERVACIONES,
      USUARIO,
      FECHA_ALTA,
      NIVEL_CENTRAL,
      EVENTO)
     VALUES
     (OLD.ID,
      OLD.NOMBRE,
      OLD.INSTITUCION,
      OLD.CARGO,
      OLD.E_MAIL,
      OLD.TELEFONO,
      OLD.PAIS,
      OLD.LOCALIDAD,
      OLD.DIRECCION,
      OLD.CODIGO_POSTAL,
      OLD.COORDENADAS,
      OLD.RESPONSABLE_CHAGAS,
      OLD.RESPONSABLE_LEISH,
      OLD.LABORATORIO,
      OLD.ACTIVO,
      OLD.OBSERVACIONES,
      OLD.USUARIO,
      OLD.FECHA_ALTA,
      OLD.NIVEL_CENTRAL,
      'Edicion');

-- elimina el trigger si existe
DROP TRIGGER IF EXISTS `auditoria_borra_responsables`;

-- lo creamos desde cero
CREATE TRIGGER `auditoria_borra_responsables`
AFTER DELETE ON `responsables`
FOR EACH ROW
INSERT INTO auditoria_responsables
     (ID,
      NOMBRE,
      INSTITUCION,
      CARGO,
      E_MAIL,
      TELEFONO,
      PAIS,
      LOCALIDAD,
      DIRECCION,
      CODIGO_POSTAL,
      COORDENADAS,
      RESPONSABLE_CHAGAS,
      RESPONSABLE_LEISH,
      LABORATORIO,
      ACTIVO,
      OBSERVACIONES,
      USUARIO,
      FECHA_ALTA,
      NIVEL_CENTRAL,
      EVENTO)
     VALUES
     (OLD.ID,
      OLD.NOMBRE,
      OLD.INSTITUCION,
      OLD.CARGO,
      OLD.E_MAIL,
      OLD.TELEFONO,
      OLD.PAIS,
      OLD.LOCALIDAD,
      OLD.DIRECCION,
      OLD.CODIGO_POSTAL,
      OLD.COORDENADAS,
      OLD.RESPONSABLE_CHAGAS,
      OLD.RESPONSABLE_LEISH,
      OLD.LABORATORIO,
      OLD.ACTIVO,
      OLD.OBSERVACIONES,
      OLD.USUARIO,
      OLD.FECHA_ALTA,
      OLD.NIVEL_CENTRAL,
      'Eliminacion');

/***************************************************************************/
/*                                                                         */
/*                    Tabla de Laboratorios                                */
/*                                                                         */
/***************************************************************************/

--
-- Estructura de tabla para la tabla `lab_chagas`
-- Campos Alterados:
-- se eliminó IMAGEN (blob)
-- se eliminó el USUARIO (ahora en responsables)
-- se eliminó PASSWORD (ahora en responsables)
-- FECHA_ALTA ahora es timestamp automático
-- se eliminó ACT_LEPRA (ya no tenemos el programa)
-- se eliminó NUEVO_PASS (ahora en responsables)
-- USUARIO ahora es int(6) la clave de la tabla de responsables
-- se agrega la clave foránea de usuario
-- cambia IDRECIBE A int(6) que es la clave de responsables
-- se eliminó país
-- se agrega logo laboratorio, utilizado en la base de diagnóstico
-- para todos los reportes e impresiones

-- la eliminamos si existe
DROP TABLE IF EXISTS `laboratorios`;

-- creamos la tabla desde cero
CREATE TABLE IF NOT EXISTS `laboratorios` (
  `ID` smallint(6) UNSIGNED NOT NULL AUTO_INCREMENT,
  `NOMBRE` varchar(250) DEFAULT NULL,
  `RESPONSABLE` varchar(255) DEFAULT NULL,
  `PAIS` smallint(3) UNSIGNED DEFAULT NULL,
  `LOCALIDAD` varchar(9) COLLATE utf8_spanish_ci NOT NULL,
  `DIRECCION` varchar(250) DEFAULT NULL,
  `CODIGO_POSTAL` varchar(10) DEFAULT NULL,
  `COORDENADAS` varchar(100) DEFAULT NULL,
  `DEPENDENCIA` tinyint(2) UNSIGNED NOT NULL,
  `E_MAIL` varchar(100) DEFAULT NULL,
  `FECHA_ALTA` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `ACTIVO` enum('Si','No') NOT NULL DEFAULT 'Si',
  `RECIBE_MUESTRAS_CHAGAS` enum('Si','No') NOT NULL DEFAULT 'No',
  `ID_RECIBE` int(6) UNSIGNED DEFAULT NULL,
  `ACT_LEISH` enum('Si','No') NOT NULL DEFAULT 'No',
  `OBSERVACIONES` text,
  `USUARIO` int(6) UNSIGNED DEFAULT NULL,
  `LOGO` BLOB DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `NOMBRE` (`NOMBRE`),
  KEY `PAIS` (`PAIS`),
  KEY `LOCALIDAD` (`LOCALIDAD`),
  KEY `RESPONSABLE` (`RESPONSABLE`),
  KEY `DEPENDENCIA` (`DEPENDENCIA`),
  KEY `E_MAIL` (`E_MAIL`),
  KEY `ID_RECIBE` (`ID_RECIBE`),
  KEY `USUARIO` (`USUARIO`)
)
ENGINE=InnoDB
DEFAULT CHARSET=utf8
COLLATE=utf8_spanish_ci
COMMENT='Datos de los laboratorios de la red';

-- Estructura de tabla para la tabla `auditoria_lab_chagas`
-- aquí cambian los mismos campos que en la tabla de laboratorios
-- Desaparece ACTIVO_LEPRA
-- Desaparece USUARIO
-- Desaparece PASSWORD (que ahora están en la tabla de usuarios)
-- Cambia la fecha a timestamp
-- USUARIO ahora es int(6) con el registro del editor
--

-- eliminamos la tabla si existe
DROP TABLE IF EXISTS `auditoria_laboratorios`;

-- la recreamos desde cero
CREATE TABLE IF NOT EXISTS `auditoria_laboratorios` (
  `ID` smallint(6) UNSIGNED DEFAULT NULL,
  `NOMBRE` varchar(250) DEFAULT NULL,
  `RESPONSABLE` varchar(255) DEFAULT NULL,
  `PAIS` smallint(3) UNSIGNED DEFAULT NULL,
  `LOCALIDAD` varchar(9) NOT NULL,
  `DIRECCION` varchar(250) DEFAULT NULL,
  `CODIGO_POSTAL` varchar(10) DEFAULT NULL,
  `COORDENADAS` varchar(100) DEFAULT NULL,
  `DEPENDENCIA` tinyint(2) UNSIGNED NOT NULL,
  `E_MAIL` varchar(100) DEFAULT NULL,
  `FECHA_ALTA` date DEFAULT NULL,
  `FECHA_OPERACION` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `ACTIVO` enum('Si','No') NOT NULL DEFAULT 'Si',
  `RECIBE_MUESTRAS_CHAGAS` enum('Si','No') NOT NULL DEFAULT 'No',
  `ID_RECIBE` int(6) UNSIGNED DEFAULT NULL,
  `ACT_LEISH` enum('Si','No') NOT NULL DEFAULT 'No',
  `OBSERVACIONES` text,
  `USUARIO` int(6) UNSIGNED NOT NULL,
  `EVENTO` enum('Edicion','Eliminacion') NOT NULL,
  KEY `ID` (`ID`),
  KEY `NOMBRE` (`NOMBRE`),
  KEY `PAIS` (`PAIS`),
  KEY `LOCALIDAD` (`LOCALIDAD`),
  KEY `DEPENDENCIA` (`DEPENDENCIA`)
)
ENGINE=InnoDB
DEFAULT CHARSET=utf8
COLLATE=utf8_spanish_ci
COMMENT='Auditoria de Laboratorios';

-- Disparadores `lab_chagas`

-- eliminamos si existe
DROP TRIGGER IF EXISTS `auditoria_edita_laboratorios`;

-- creamos el trigger de edición
CREATE TRIGGER `auditoria_edita_laboratorios`
AFTER UPDATE ON `laboratorios`
FOR EACH ROW
INSERT INTO auditoria_laboratorios
     (ID,
      NOMBRE,
      RESPONSABLE,
      PAIS,
      LOCALIDAD,
      DIRECCION,
      CODIGO_POSTAL,
      COORDENADAS,
      DEPENDENCIA,
      E_MAIL,
      FECHA_ALTA,
      ACTIVO,
      RECIBE_MUESTRAS_CHAGAS,
      ID_RECIBE,
      ACT_LEISH,
      OBSERVACIONES,
      USUARIO,
      EVENTO)
     VALUES
     (OLD.ID,
      OLD.NOMBRE,
      OLD.RESPONSABLE,
      OLD.PAIS,
      OLD.LOCALIDAD,
      OLD.DIRECCION,
      OLD.CODIGO_POSTAL,
      OLD.COORDENADAS,
      OLD.DEPENDENCIA,
      OLD.E_MAIL,
      OLD.FECHA_ALTA,
      OLD.ACTIVO,
      OLD.RECIBE_MUESTRAS_CHAGAS,
      OLD.ID_RECIBE,
      OLD.ACT_LEISH,
      OLD.OBSERVACIONES,
      OLD.USUARIO,
      'Edicion');

-- eliminamos el trigger
DROP TRIGGER IF EXISTS `auditoria_borra_laboratorios`;

-- lo creamos desde cero
CREATE TRIGGER `auditoria_borra_laboratorios`
AFTER DELETE ON `laboratorios`
FOR EACH ROW
INSERT INTO auditoria_laboratorios
     (ID,
      NOMBRE,
      RESPONSABLE,
      PAIS,
      LOCALIDAD,
      DIRECCION,
      CODIGO_POSTAL,
      COORDENADAS,
      DEPENDENCIA,
      E_MAIL,
      FECHA_ALTA,
      ACTIVO,
      ACT_LEISH,
      OBSERVACIONES,
      USUARIO,
      EVENTO)
     VALUES
     (OLD.ID,
      OLD.NOMBRE,
      OLD.RESPONSABLE,
      OLD.PAIS,
      OLD.LOCALIDAD,
      OLD.DIRECCION,
      OLD.CODIGO_POSTAL,
      OLD.COORDENADAS,
      OLD.DEPENDENCIA,
      OLD.E_MAIL,
      OLD.FECHA_ALTA,
      OLD.ACTIVO,
      OLD.ACT_LEISH,
      OLD.USUARIO,
      OLD.OBSERVACIONES,
      'Eliminacion');


/***************************************************************************/
/*                                                                         */
/*                         Tablas Auxiliares                               */
/*                                                                         */
/***************************************************************************/

-- estas tablas tienen claves externas con los laboratorios o los responsables

-- la tabla técnicas es nueva, de forma tal que para ingresar una nueva
-- técnica solo es necesario agregar un registro en la tabla y luego en
-- la tabla heredada los valores para esa técnica

-- eliminamos si existe
DROP TABLE IF EXISTS tecnicas;

-- la creamos desde cero
CREATE TABLE IF NOT EXISTS tecnicas (
    ID smallint(3) UNSIGNED NOT NULL AUTO_INCREMENT,
    TECNICA varchar(30) NOT NULL,
    USUARIO int(6) UNSIGNED NOT NULL,
    FECHA_ALTA timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    PRIMARY KEY(ID),
    KEY TECNICA(TECNICA),
    KEY USUARIO(USUARIO)
)
ENGINE=InnoDB
DEFAULT CHARSET=utf8
COLLATE=utf8_spanish_ci
COMMENT='Diccionario de Técnicas';

-- agregamos los registros de técnicas
INSERT INTO tecnicas
    (ID, TECNICA, USUARIO) VALUES
    (1, "AP", 1),
    (2, "ELISA", 1),
    (3, "HAI", 1),
    (4, "IFI", 1),
    (5, "QUIMIOLUMINISCENCIA", 1),
    (6, "OTRA", 1);


-- ahora creamos la tabla de valores posibles para
-- cada técnica (esta tabla es nueva)
DROP TABLE IF EXISTS valores_tecnicas;

-- creamos la tabla desde cero
CREATE TABLE IF NOT EXISTS valores_tecnicas (
    ID smallint(4) UNSIGNED NOT NULL AUTO_INCREMENT,
    TECNICA smallint(3) UNSIGNED NOT NULL,
    VALOR varchar(10) NOT NULL,
    USUARIO int(6) UNSIGNED NOT NULL,
    FECHA_ALTA timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    PRIMARY KEY(ID),
    KEY TECNICA(TECNICA),
    KEY USUARIO(USUARIO)
)
ENGINE=InnoDB
DEFAULT CHARSET=utf8
COLLATE=utf8_spanish_ci
COMMENT='Valores posibles según la técnica';

-- agregamos los valores de las distintas técnicas
INSERT INTO valores_tecnicas
        (TECNICA, VALOR, USUARIO, FECHA_ALTA) VALUES
        (1,"1/4",1,"2016-11-08"),
        (1,"1/8",1,"2016-11-08"),
        (1,"1/16",1,"2016-11-08"),
        (1,"1/32",1,"2016-11-08"),
        (1,"1/64",1,"2016-11-08"),
        (1,"1/128",1,"2016-11-08"),
        (1,"1/256",1,"2016-11-08"),
        (1,"1/512",1,"2016-11-08"),
        (1,"1/1024",1,"2016-11-08"),
        (1,"1/2048",1,"2016-11-08"),
        (1,"1/4096",1,"2016-11-08"),
        (3,"1/4",1,"2016-11-08"),
        (3,"1/8",1,"2016-11-08"),
        (3,"1/16",1,"2016-11-08"),
        (3,"1/32",1,"2016-11-08"),
        (3,"1/64",1,"2016-11-08"),
        (3,"1/128",1,"2016-11-08"),
        (3,"1/256",1,"2016-11-08"),
        (3,"1/512",1,"2016-11-08"),
        (3,"1/1024",1,"2016-11-08"),
        (3,"1/2048",1,"2016-11-08"),
        (3,"1/4096",1,"2016-11-08"),
        (4,"1/4",1,"2016-11-08"),
        (4,"1/8",1,"2016-11-08"),
        (4,"1/16",1,"2016-11-08"),
        (4,"1/32",1,"2016-11-08"),
        (4,"1/64",1,"2016-11-08"),
        (4,"1/128",1,"2016-11-08"),
        (4,"1/256",1,"2016-11-08"),
        (4,"1/512",1,"2016-11-08"),
        (4,"1/1024",1,"2016-11-08"),
        (4,"1/2048",1,"2016-11-08"),
        (4,"1/4096",1,"2016-11-08"),
        (2,"#.###",1,"2016-11-08"),
        (5,"##.###",1,"2016-11-08");

-- Estructura de tabla para la tabla `chag_marcas` atención
-- cambia la fecha de alta que ahora es timestamp automático
-- se agrega el índice de técnica
-- usuario cambia a la clave de responsables
-- técnica ahora es un registro relacionado con la tabla de técnicas

-- eliminamos la tabla si existe
DROP TABLE IF EXISTS `marcas_chagas`;

-- recreamos desde cero
CREATE TABLE IF NOT EXISTS `marcas_chagas` (
  `ID` tinyint(2) UNSIGNED NOT NULL AUTO_INCREMENT,
  `MARCA` varchar(40) NOT NULL,
  `TECNICA` smallint(3) UNSIGNED NOT NULL,
  `FECHA_ALTA` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `USUARIO` int(6) UNSIGNED NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `MARCA` (`MARCA`),
  KEY TECNICA(TECNICA),
  KEY USUARIO(USUARIO)
)
ENGINE=InnoDB
DEFAULT CHARSET=utf8
COLLATE=utf8_spanish_ci
COMMENT='Diccionario de las marcas utilizadas';

-- agregamos los registros iniciales
INSERT INTO marcas_chagas (ID,MARCA,TECNICA,USUARIO) VALUES (1,'Serodia','1','1');
INSERT INTO marcas_chagas (ID,MARCA,TECNICA,USUARIO) VALUES (3,'Biomerieux','2','1');
INSERT INTO marcas_chagas (ID,MARCA,TECNICA,USUARIO) VALUES (4,'Lemos','2','1');
INSERT INTO marcas_chagas (ID,MARCA,TECNICA,USUARIO) VALUES (5,'Bioschile','2','1');
INSERT INTO marcas_chagas (ID,MARCA,TECNICA,USUARIO) VALUES (6,'Biozima','2','1');
INSERT INTO marcas_chagas (ID,MARCA,TECNICA,USUARIO) VALUES (7,'Lisado de Wiener','2','1');
INSERT INTO marcas_chagas (ID,MARCA,TECNICA,USUARIO) VALUES (8,'Polychaco','3','1');
INSERT INTO marcas_chagas (ID,MARCA,TECNICA,USUARIO) VALUES (9,'Fatalakit','3','1');
INSERT INTO marcas_chagas (ID,MARCA,TECNICA,USUARIO) VALUES (10,'Hemave','3','1');
INSERT INTO marcas_chagas (ID,MARCA,TECNICA,USUARIO) VALUES (11,'Biocientifica','4','1');
INSERT INTO marcas_chagas (ID,MARCA,TECNICA,USUARIO) VALUES (12,'Inmunofluor','4','1');
INSERT INTO marcas_chagas (ID,MARCA,TECNICA,USUARIO) VALUES (13,'Ag Fatala','4','1');
INSERT INTO marcas_chagas (ID,MARCA,TECNICA,USUARIO) VALUES (14,'Otra','4','1');
INSERT INTO marcas_chagas (ID,MARCA,TECNICA,USUARIO) VALUES (15,'Otra','2','1');
INSERT INTO marcas_chagas (ID,MARCA,TECNICA,USUARIO) VALUES (19,'Otra','3','1');
INSERT INTO marcas_chagas (ID,MARCA,TECNICA,USUARIO) VALUES (21,'Otra','1','1');
INSERT INTO marcas_chagas (ID,MARCA,TECNICA,USUARIO) VALUES (24,'Recombinante 3.0 de Wiener','2','1');
INSERT INTO marcas_chagas (ID,MARCA,TECNICA,USUARIO) VALUES (25,'Recombinante 4.0 de Wiener','2','1');
INSERT INTO marcas_chagas (ID,MARCA,TECNICA,USUARIO) VALUES (26,'Wiener','3','1');
INSERT INTO marcas_chagas (ID,MARCA,TECNICA,USUARIO) VALUES (27,'ABBOT Architect','2','1');
INSERT INTO marcas_chagas (ID,MARCA,TECNICA,USUARIO) VALUES (29,'Otra','Otra','1');
INSERT INTO marcas_chagas (ID,MARCA,TECNICA,USUARIO) VALUES (30,'Lemos Recombinante','2','1');
INSERT INTO marcas_chagas (ID,MARCA,TECNICA,USUARIO) VALUES (31,'ABBOT Architect','5','1');
INSERT INTO marcas_chagas (ID,MARCA,TECNICA,USUARIO) VALUES (32,'Otra','5','1');

/***************************************************************************/
/*                                                                         */
/*                 Tabla Determinaciones de Chagas                         */
/*                                                                         */
/***************************************************************************/

-- Estructura de tabla para la tabla `chag_datos`
-- atención se modifica la fecha de alta ahora es timestamp automático
-- usuario cambia a int(6) la clave de la tabla de responsables
-- laboratorio cambia a int(6)
-- tecnica cambia a smallint(3) que es la clave de la tabla auxiliar
-- cambia valor a VALOR_LECTURA
--

-- eliminamos si existe
DROP TABLE IF EXISTS `chag_datos`;

-- recreamos desde cero
CREATE TABLE IF NOT EXISTS `chag_datos` (
  `ID` int(6) UNSIGNED NOT NULL AUTO_INCREMENT,
  `OPERATIVO` smallint(6) UNSIGNED NOT NULL,
  `LABORATORIO` int(6) UNSIGNED NOT NULL,
  `MARCA` tinyint(2) UNSIGNED NOT NULL,
  `LOTE` varchar(10) NOT NULL,
  `VENCIMIENTO` date DEFAULT NULL,
  `MUESTRA_NRO` smallint(4) UNSIGNED NOT NULL,
  `VALOR_CORTE` varchar(10) NOT NULL,
  `VALOR_LECTURA` varchar(10) NOT NULL,
  `METODO` enum('Lector','Visual') COLLATE utf8_spanish_ci DEFAULT NULL,
  `TECNICA` smallint(3) UNSIGNED NOT NULL,
  `DETERMINACION` enum('Reactivo','No Reactivo','Indeterminado') NOT NULL,
  `CORRECTO` enum('Reactivo','No Reactivo','Indeterminado') DEFAULT NULL,
  `COMENTARIOS` text,
  `FECHA_ALTA` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `USUARIO` int(6) UNSIGNED NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `MARCA` (`MARCA`),
  KEY `LOTE` (`LOTE`),
  KEY `TECNICA` (`TECNICA`),
  KEY `OPERATIVO` (`OPERATIVO`),
  KEY `LABORATORIO` (`LABORATORIO`),
  KEY `MUESTRA` (`MUESTRA_NRO`),
  KEY `USUARIO` (`USUARIO`)
)
ENGINE=InnoDB
DEFAULT CHARSET=utf8
COLLATE=utf8_spanish_ci
COMMENT='Datos de las muestras obtenidas';

-- Estructura de tabla para la tabla `auditoria_chag_datos`
-- cambia la clave de usuario

-- elimina la tabla
DROP TABLE IF EXISTS `auditoria_chag_datos`;

-- la recreamos desde cero
CREATE TABLE IF NOT EXISTS `auditoria_chag_datos` (
  `ID` int(6) UNSIGNED NOT NULL,
  `OPERATIVO` smallint(6) UNSIGNED NOT NULL,
  `LABORATORIO` int(6) UNSIGNED NOT NULL,
  `MARCA` tinyint(2) UNSIGNED NOT NULL,
  `LOTE` varchar(10),
  `VENCIMIENTO` date DEFAULT NULL,
  `MUESTRA_NRO` smallint(4) UNSIGNED NOT NULL,
  `VALOR_CORTE` varchar(10) NOT NULL,
  `VALOR_LECTURA` varchar(10) NOT NULL,
  `METODO` enum('Lector','Visual') COLLATE utf8_spanish_ci DEFAULT NULL,
  `TECNICA` smallint(3) UNSIGNED NOT NULL,
  `DETERMINACION` enum('Reactivo','No Reactivo','Indeterminado') COLLATE utf8_spanish_ci NOT NULL,
  `CORRECTO` enum('Reactivo','No Reactivo','Indeterminado') COLLATE utf8_spanish_ci DEFAULT NULL,
  `COMENTARIOS` text,
  `FECHA_ALTA` date NOT NULL,
  `FECHA_OPERACION` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `USUARIO` int(6) UNSIGNED NOT NULL,
  `EVENTO` enum('Edicion','Eliminacion'),
  KEY `ID` (`ID`),
  KEY `MARCA` (`MARCA`),
  KEY `LOTE` (`LOTE`),
  KEY `TECNICA` (`TECNICA`),
  KEY `OPERATIVO` (`OPERATIVO`),
  KEY `LABORATORIO` (`LABORATORIO`),
  KEY `MUESTRA` (`MUESTRA_NRO`)
)
ENGINE=InnoDB
DEFAULT CHARSET=utf8
COLLATE=utf8_spanish_ci
COMMENT='Auditoría de la Carga de muestras';

-- Disparadores `chag_datos`

-- eliminamos el trigger si existe
DROP TRIGGER IF EXISTS `auditoria_edita_datos`;

-- creamos el trigger de edición
CREATE TRIGGER `auditoria_edita_datos`
AFTER UPDATE ON `chag_datos`
FOR EACH ROW
INSERT INTO auditoria_chag_datos
     (ID,
      OPERATIVO,
      LABORATORIO,
      MARCA,
      LOTE,
      VENCIMIENTO,
      MUESTRA_NRO,
      VALOR_CORTE,
      VALOR_LECTURA,
      METODO,
      TECNICA,
      DETERMINACION,
      CORRECTO,
      COMENTARIOS,
      FECHA_ALTA,
      USUARIO,
      EVENTO)
     VALUES
     (OLD.ID,
      OLD.OPERATIVO,
      OLD.LABORATORIO,
      OLD.MARCA,
      OLD.LOTE,
      OLD.VENCIMIENTO,
      OLD.MUESTRA_NRO,
      OLD.VALOR_CORTE,
      OLD.VALOR_LECTURA,
      OLD.METODO,
      OLD.TECNICA,
      OLD.DETERMINACION,
      OLD.CORRECTO,
      OLD.COMENTARIOS,
      OLD.FECHA_ALTA,
      OLD.USUARIO,
      'Edicion');

-- eliminamos el trigger si existe
DROP TRIGGER IF EXISTS `auditoria_borra_datos`;

-- creamos el trigger de eliminación de datos
CREATE TRIGGER `auditoria_borra_datos`
AFTER DELETE ON `chag_datos`
FOR EACH ROW
INSERT INTO auditoria_chag_datos
     (ID,
      OPERATIVO,
      LABORATORIO,
      MARCA,
      LOTE,
      VENCIMIENTO,
      MUESTRA_NRO,
      VALOR_CORTE,
      VALOR_LECTURA,
      METODO,
      TECNICA,
      DETERMINACION,
      CORRECTO,
      COMENTARIOS,
      FECHA_ALTA,
      USUARIO,
      EVENTO)
     VALUES
     (OLD.ID,
      OLD.OPERATIVO,
      OLD.LABORATORIO,
      OLD.MARCA,
      OLD.LOTE,
      OLD.VENCIMIENTO,
      OLD.MUESTRA_NRO,
      OLD.VALOR_CORTE,
      OLD.VALOR_LECTURA,
      OLD.METODO,
      OLD.TECNICA,
      OLD.DETERMINACION,
      OLD.CORRECTO,
      OLD.COMENTARIOS,
      OLD.FECHA_ALTA,
      OLD.USUARIO,
      'Eliminacion');


/***************************************************************************/
/*                                                                         */
/*             Tabla Determinaciones de Otras Técnicas                     */
/*                                                                         */
/***************************************************************************/

-- Estructura de tabla para la tabla `chag_otras` utilizada al seleccionar
-- otra técnica y solicitar la descripción
-- cambia la id del registro a int(6)

-- eliminamos la tabla si existe
DROP TABLE IF EXISTS `chag_otras`;

-- la recreamos desde cero
CREATE TABLE IF NOT EXISTS `chag_otras` (
  `ID` int(6) UNSIGNED NOT NULL AUTO_INCREMENT,
  `ID_DATOS` int(6) UNSIGNED NOT NULL,
  `PRUEBA` varchar(50) NOT NULL,
  `MARCA` varchar(50) NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `ID_DATOS` (`ID_DATOS`),
  KEY `PRUEBA` (`PRUEBA`),
  KEY `MARCA` (`MARCA`)
)
ENGINE=InnoDB
DEFAULT CHARSET=utf8
COLLATE=utf8_spanish_ci
COMMENT='Diccionario de otras pruebas';


/***************************************************************************/
/*                                                                         */
/*         Tabla con las notas enviadas a los responsables                 */
/*                                                                         */
/***************************************************************************/

-- Estructura de tabla para la tabla `notas_resp`
-- cambia la clave de usuario a int(6)
-- cambia la clave del registro a int(6)
-- cambia la fecha de alta a timestamp automático

-- elimina la tabla si existe
DROP TABLE IF EXISTS `notas_responsables`;

-- creamos la tabla desde cero
CREATE TABLE IF NOT EXISTS `notas_responsables` (
  `ID` int(6) UNSIGNED NOT NULL AUTO_INCREMENT,
  `TEXTO` mediumblob NOT NULL,
  `FECHA_ALTA` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `USUARIO` int(6) UNSIGNED NOT NULL,
  `MOTIVO` varchar(150) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `FECHA_ALTA` (`FECHA_ALTA`),
  KEY `USUARIO` (`USUARIO`)
)
ENGINE=InnoDB
DEFAULT CHARSET=utf8
COLLATE=utf8_spanish_ci
COMMENT='Notas enviadas a los responsables';


/***************************************************************************/
/*                                                                         */
/*     Tabla con las notas de los responsables al nivel central            */
/*                                                                         */
/***************************************************************************/

-- esta tabla es nueva

-- elimina la tabla si existe
DROP TABLE IF EXISTS `notas_recibidas`;

-- creamos la tabla desde cero
CREATE TABLE IF NOT EXISTS `notas_recibidas` (
  `ID` int(6) UNSIGNED NOT NULL AUTO_INCREMENT,
  `TEXTO` mediumblob NOT NULL,
  `FECHA_ALTA` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `USUARIO` int(6) UNSIGNED NOT NULL,
  `MOTIVO` varchar(150) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `FECHA_ALTA` (`FECHA_ALTA`),
  KEY `USUARIO` (`USUARIO`)
)
ENGINE=InnoDB
DEFAULT CHARSET=utf8
COLLATE=utf8_spanish_ci
COMMENT='Notas recibidas de los responsables';

/***************************************************************************/
/*                                                                         */
/*         Tabla con los datos de los operativos de calidad                */
/*                                                                         */
/***************************************************************************/

-- Estructura de tabla para la tabla `operativos_chagas`
-- cambia USUARIO a int(6) la clave del registro
-- cambia fecha alta a timestamp automático

-- eliminamos la tabla si existe
DROP TABLE IF EXISTS `operativos_chagas`;

-- la recreamos
CREATE TABLE IF NOT EXISTS `operativos_chagas` (
  `ID` smallint(6) UNSIGNED NOT NULL AUTO_INCREMENT,
  `OPERATIVO_NRO` varchar(6) NOT NULL,
  `ABIERTO` enum('Si','No') NOT NULL,
  `FECHA_INICIO` date NOT NULL,
  `FECHA_FIN` date NOT NULL,
  `REPORTE` longblob DEFAULT NULL,
  `FECHA_ALTA` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `USUARIO` int(6) UNSIGNED NOT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `OPERATIVO_NRO` (`OPERATIVO_NRO`),
  KEY `FECHA_INICIO` (`FECHA_INICIO`),
  KEY `FECHA_FIN` (`FECHA_FIN`)
)
ENGINE=InnoDB
DEFAULT CHARSET=utf8
COLLATE=utf8_spanish_ci
COMMENT='Datos de los operativos de calidad';

-- Estructura de tabla para la tabla `auditoria_operativos_chagas`
-- cambia la fecha a timestamp
-- cambia usuario a int(6)

-- eliminamos si existe
DROP TABLE IF EXISTS `auditoria_operativos_chagas`;

-- la recreamos desde cero
CREATE TABLE IF NOT EXISTS `auditoria_operativos_chagas` (
  `ID` smallint(6) UNSIGNED NOT NULL,
  `OPERATIVO_NRO` varchar(6) NOT NULL,
  `ABIERTO` enum('Si','No') NOT NULL,
  `FECHA_INICIO` date NOT NULL,
  `FECHA_FIN` date NOT NULL,
  `FECHA_ALTA` timestamp NOT NULL,
  `USUARIO` int(6) UNSIGNED NOT NULL,
  `EVENTO` enum('Edicion','Eliminacion') NOT NULL,
  KEY `ID` (`ID`),
  KEY `OPERATIVO_NRO` (`OPERATIVO_NRO`),
  KEY `FECHA_INICIO` (`FECHA_INICIO`),
  KEY `FECHA_FIN` (`FECHA_FIN`)
)
ENGINE=InnoDB
DEFAULT CHARSET=utf8
COLLATE=utf8_spanish_ci
COMMENT='Auditorìa de los operativos de calidad';

-- disparadores para los eventos de los operativos
DROP TRIGGER IF EXISTS auditoria_edita_operativo;

-- lo recreamos desde cero
CREATE TRIGGER auditoria_edita_operativo
AFTER UPDATE ON `operativos_chagas`
FOR EACH ROW
INSERT INTO auditoria_operativos_chagas
        (ID,
         OPERATIVO_NRO,
         ABIERTO,
         FECHA_INICIO,
         FECHA_FIN,
         FECHA_ALTA,
         USUARIO,
         EVENTO)
        VALUES
        (OLD.ID,
         OLD.OPERATIVO_NRO,
         OLD.ABIERTO,
         OLD.FECHA_INICIO,
         OLD.FECHA_FIN,
         OLD.FECHA_ALTA,
         OLD.USUARIO,
         'Edicion');

-- eliminamos el trigger de borrado
DROP TRIGGER IF EXISTS auditoria_borra_operativo;

-- lo recreamos desde cero
CREATE TRIGGER auditoria_borra_operativo
AFTER DELETE ON operativos_chagas
FOR EACH ROW
INSERT INTO auditoria_operativos_chagas
        (ID,
         OPERATIVO_NRO,
         ABIERTO,
         FECHA_INICIO,
         FECHA_FIN,
         FECHA_ALTA,
         USUARIO,
         EVENTO)
        VALUES
        (OLD.ID,
         OLD.OPERATIVO_NRO,
         OLD.ABIERTO,
         OLD.FECHA_INICIO,
         OLD.FECHA_FIN,
         OLD.FECHA_ALTA,
         OLD.USUARIO,
         'Eliminacion');

/***************************************************************************/
/*                                                                         */
/*                Tabla con los datos de las etiquetas                     */
/*                                                                         */
/***************************************************************************/


-- Estructura de tabla para la tabla `lab_etiquetas`
-- cambia USUARIO a id del registro de responsables
-- cambia ID_LABORATORIO a int(6)
-- se agrega la fecha de generación como timestamp automático
-- se agrega el número de caja
-- se agrega la fecha de salida del inp
-- se agrega la fecha de recepción del laboratorio

-- eliminamos la tabla si existe
DROP TABLE IF EXISTS `etiquetas`;

-- la recreamos
CREATE TABLE IF NOT EXISTS `etiquetas` (
  `ID_LABORATORIO` int(6) UNSIGNED NOT NULL,
  `ETIQUETA_NRO` smallint(4) UNSIGNED NOT NULL,
  `OPERATIVO` varchar(6) NOT NULL,
  `ID_OPERATIVO` smallint(6) UNSIGNED NOT NULL,
  `ID_CAJA` smallint(6) UNSIGNED DEFAULT NULL,
  `PATOLOGIA` enum('Chagas','Leishmaniasis') NOT NULL,
  `FECHA_ALTA` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `FECHA_SALIDA` date DEFAULT NULL,
  `FECHA_RECEPCION` date DEFAULT NULL,
  `USUARIO` int(6) UNSIGNED NOT NULL,
  KEY `ID_LABORATORIO` (`ID_LABORATORIO`),
  KEY `ID_OPERATIVO` (`ID_OPERATIVO`),
  KEY `ETIQUETA` (`ETIQUETA_NRO`),
  KEY `OPERATIVO` (`OPERATIVO`),
  KEY `ID_CAJA` (`ID_CAJA`),
  KEY `USUARIO` (`USUARIO`)
) ENGINE=InnoDB
DEFAULT CHARSET=utf8
COLLATE=utf8_spanish_ci
COMMENT='Etiquetas de las muestras';

-- Estructura de tabla para la tabla `auditoria_lab_etiquetas`
-- cambia USUARIO a la clave de la tabla responsables

-- eliminamos la tabla si existe
DROP TABLE IF EXISTS `auditoria_etiquetas`;

-- la recreamos desde cero
CREATE TABLE IF NOT EXISTS `auditoria_etiquetas` (
  `ID_LABORATORIO` int(6) UNSIGNED NOT NULL,
  `ETIQUETA_NRO` smallint(4) UNSIGNED NOT NULL,
  `OPERATIVO` varchar(6) NOT NULL,
  `ID_OPERATIVO` smallint(6) UNSIGNED NOT NULL,
  `ID_CAJA` smallint(6) UNSIGNED DEFAULT NULL,
  `PATOLOGIA` enum('Chagas','Leishmaniasis') NOT NULL,
  `USUARIO` INT(6) UNSIGNED NOT NULL,
  `EVENTO` enum('Edicion','Eliminacion') NOT NULL,
  `FECHA_ALTA` date NOT NULL,
  `FECHA_SALIDA` date DEFAULT NULL,
  `FECHA_RECEPCION` date DEFAULT NULL,
  `FECHA_OPERACION` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  KEY `ID_LABORATORIO` (`ID_LABORATORIO`),
  KEY `ID_OPERATIVO` (`ID_OPERATIVO`),
  KEY `USUARIO` (`USUARIO`),
  KEY `ID_CAJA` (`ID_CAJA`),
  KEY `ETIQUETA` (`ETIQUETA_NRO`),
  KEY `OPERATIVO` (`OPERATIVO`)
)
ENGINE=InnoDB
DEFAULT CHARSET=utf8
COLLATE=utf8_spanish_ci
COMMENT='Auditoría de las Etiquetas de las muestras';

-- Disparadores `lab_etiquetas`

-- eliminamos el trigger si existe
DROP TRIGGER IF EXISTS `auditoria_edita_etiquetas`;

-- lo creamos desde cero
CREATE TRIGGER `auditoria_edita_etiquetas`
AFTER UPDATE ON `etiquetas`
FOR EACH ROW
INSERT INTO auditoria_etiquetas
        (ID_LABORATORIO,
         ETIQUETA_NRO,
         OPERATIVO,
         ID_OPERATIVO,
         ID_CAJA,
         PATOLOGIA,
         USUARIO,
         FECHA_ALTA,
         FECHA_SALIDA,
         FECHA_RECEPCION,
         EVENTO)
        VALUES
        (OLD.ID_LABORATORIO,
         OLD.ETIQUETA_NRO,
         OLD.OPERATIVO,
         OLD.ID_OPERATIVO,
         OLD.ID_CAJA,
         OLD.PATOLOGIA,
         OLD.USUARIO,
         OLD.FECHA_ALTA,
         OLD.FECHA_SALIDA,
         OLD.FECHA_RECEPCION,
         'Edicion');

-- eliminamos el trigger si existe
DROP TRIGGER IF EXISTS `auditoria_borra_etiquetas`;

-- lo creamos desde cero
CREATE TRIGGER `auditoria_borra_etiquetas`
AFTER DELETE ON `etiquetas`
FOR EACH ROW
INSERT INTO auditoria_etiquetas
        (ID_LABORATORIO,
         ETIQUETA_NRO,
         OPERATIVO,
         ID_OPERATIVO,
         ID_CAJA,
         PATOLOGIA,
         USUARIO,
         FECHA_ALTA,
         FECHA_SALIDA,
         FECHA_RECEPCION,
         EVENTO)
        VALUES
        (OLD.ID_LABORATORIO,
         OLD.ETIQUETA_NRO,
         OLD.OPERATIVO,
         OLD.ID_OPERATIVO,
         OLD.ID_CAJA,
         OLD.PATOLOGIA,
         OLD.USUARIO,
         OLD.FECHA_ALTA,
         OLD.FECHA_SALIDA,
         OLD.FECHA_RECEPCION,
         'Eliminacion');

/***************************************************************************/
/*                                                                         */
/*                  Datos con las Cajas Enviadas                           */
/*                                                                         */
/***************************************************************************/

-- Estructura de las tablas con los datos de las cajas conteniendo
-- las muestras

-- primero eliminamos si existe
DROP TABLE IF EXISTS palets;

-- creamos la tabla
CREATE TABLE IF NOT EXISTS palets (
    ID_PALET int(6) unsigned NOT NULL AUTO_INCREMENT,
    FECHA_SALIDA date DEFAULT NULL,
    FECHA_RECEPCION date DEFAULT NULL,
    USUARIO int(6) unsigned DEFAULT NULL,
    RECIBIO int(6) unsigned DEFAULT NULL,
    ANIO varchar(4) DEFAULT NULL,
    PRIMARY KEY (ID_PALET),
    KEY USUARIO(USUARIO),
    KEY RECIBIO(RECIBIO),
    KEY ANIO(ANIO)
)
ENGINE=InnoDB
DEFAULT CHARSET=utf8
COLLATE=utf8_spanish_ci
COMMENT='Datos de las cajas con muestras';

-- eliminamos la tabla de auditoria si existe
DROP TABLE IF EXISTS auditoria_palets;

-- creamos la tabla de auditoria desde cero
CREATE TABLE IF NOT EXISTS auditoria_palets (
    ID_PALET int(6) unsigned NOT NULL,
    FECHA_SALIDA date DEFAULT NULL,
    FECHA_RECEPCION date DEFAULT NULL,
    USUARIO int(6) unsigned DEFAULT NULL,
    RECIBIO int(6) unsigned DEFAULT NULL,
    ANIO varchar(4) DEFAULT NULL,
    EVENTO enum("Edicion", "Eliminacion"),
    KEY (ID_PALET),
    KEY USUARIO(USUARIO),
    KEY RECIBIO(RECIBIO),
    KEY ANIO(ANIO)
)
ENGINE=InnoDB
DEFAULT CHARSET=utf8
COLLATE=utf8_spanish_ci
COMMENT='Auditoria de las cajas con muestras';

-- eliminamos el trigger de edición si existe
DROP TRIGGER IF EXISTS auditoria_edita_palets;

-- creamos el trigger de edición
CREATE TRIGGER `auditoria_edita_palets`
AFTER UPDATE ON `palets`
FOR EACH ROW
INSERT INTO auditoria_palets
    (ID_PALET,
     FECHA_SALIDA,
     FECHA_RECEPCION,
     USUARIO,
     RECIBIO,
     ANIO,
     EVENTO)
    VALUES
    (OLD.ID_PALET,
     OLD.FECHA_SALIDA,
     OLD.FECHA_RECEPCION,
     OLD.USUARIO,
     OLD.RECIBIO,
     OLD.ANIO,
     "Edicion");

-- eliminamos el trigger de eliminacion si existe
DROP TRIGGER IF EXISTS auditoria_borra_palets;

-- creamos el trigger de eliminacion
CREATE TRIGGER `auditoria_borra_palets`
AFTER DELETE ON `palets`
FOR EACH ROW
INSERT INTO auditoria_palets
    (ID_PALET,
     FECHA_SALIDA,
     FECHA_RECEPCION,
     USUARIO,
     RECIBIO,
     ANIO,
     EVENTO)
    VALUES
    (OLD.ID_PALET,
     OLD.FECHA_SALIDA,
     OLD.FECHA_RECEPCION,
     OLD.USUARIO,
     OLD.RECIBIO,
     OLD.ANIO,
     "Eliminacion");

-- eliminamos la vista de laboratorios que remitieron muestras
DROP VIEW IF EXISTS vw_laboratorios_anios;

-- la recreamos
CREATE ALGORITHM = UNDEFINED
       SQL SECURITY INVOKER
       VIEW vw_laboratorios_anios AS
       SELECT RIGHT(cce.operativos_chagas.OPERATIVO_NRO,4) AS anio,
              COUNT(DISTINCT(cce.laboratorios.ID)) AS laboratorios
       FROM cce.operativos_chagas INNER JOIN cce.chag_datos ON cce.chag_datos.OPERATIVO = cce.operativos_chagas.ID
                                  INNER JOIN cce.laboratorios ON cce.chag_datos.LABORATORIO = cce.laboratorios.ID
       GROUP BY RIGHT(cce.operativos_chagas.OPERATIVO_NRO,4)
       ORDER BY RIGHT(cce.operativos_chagas.OPERATIVO_NRO,4);

-- eliminamos la vista de responsables
DROP VIEW IF EXISTS vw_responsables;

-- la recreamos
CREATE ALGORITHM = UNDEFINED
       SQL SECURITY INVOKER
       VIEW vw_responsables AS
       SELECT cce.responsables.id AS id,
              UPPER(cce.responsables.nombre) AS responsable,
              UPPER(cce.responsables.institucion) AS institucion,
              UPPER(cce.responsables.cargo) AS cargo,
              LOWER(cce.responsables.e_mail) AS mail,
              cce.responsables.telefono AS telefono,
              cce.responsables.pais AS idpais,
              diccionarios.paises.nombre AS pais,
              cce.responsables.localidad AS idlocalidad,
              diccionarios.localidades.nomloc AS localidad,
              diccionarios.provincias.cod_prov AS idprovincia,
              diccionarios.provincias.nom_prov AS provincia,
              UPPER(cce.responsables.direccion) AS direccion,
              cce.responsables.codigo_postal AS codigopostal,
              cce.responsables.coordenadas AS coordenadas,
              cce.responsables.responsable_chagas AS responsable_chagas,
              cce.responsables.laboratorio AS idlaboratorio,
              cce.laboratorios.nombre AS laboratorio,
              cce.responsables.activo AS activo,
              cce.responsables.observaciones AS observaciones,
              cce.responsables.usuario AS usuario,
              DATE_FORMAT(cce.responsables.fecha_alta, '%d/%m/%d') AS fecha_alta,
              cce.responsables.nivel_central AS nivelcentral
       FROM cce.responsables INNER JOIN diccionarios.paises ON cce.responsables.pais = diccionarios.paises.id
                             INNER JOIN diccionarios.localidades ON cce.responsables.localidad = diccionarios.localidades.codloc
                             INNER JOIN diccionarios.provincias ON diccionarios.localidades.codpcia = diccionarios.provincias.cod_prov
                             LEFT JOIN cce.laboratorios ON cce.responsables.laboratorio = cce.laboratorios.id;

-- eliminamos la vista de laboratorios
DROP VIEW IF EXISTS vw_laboratorios;

-- la recreamos
CREATE ALGORITHM = UNDEFINED
       SQL SECURITY INVOKER
       VIEW vw_laboratorios AS
       SELECT cce.laboratorios.id AS id,
              UPPER(cce.laboratorios.nombre) AS laboratorio,
              UPPER(cce.laboratorios.responsable) AS responsable,
              cce.laboratorios.pais AS idpais,
              diccionarios.paises.nombre AS pais,
              diccionarios.provincias.nom_prov AS provincia,
              diccionarios.provincias.cod_prov AS idprovincia,
              cce.laboratorios.localidad AS idlocalidad,
              diccionarios.localidades.nomloc AS localidad,
              UPPER(cce.laboratorios.direccion) AS direccion,
              cce.laboratorios.codigo_postal AS codigopostal,
              cce.laboratorios.coordenadas AS coordenadas,
              cce.laboratorios.dependencia AS iddependencia,
              diccionarios.dependencias.dependencia AS dependencia,
              LOWER(cce.laboratorios.e_mail) AS mail,
              DATE_FORMAT(cce.laboratorios.fecha_alta, '%d/%m/%Y') AS fecha_alta,
              cce.laboratorios.activo AS activo,
              cce.laboratorios.recibe_muestras_chagas AS recibe_muestras,
              cce.laboratorios.id_recibe AS idrecibe,
              cce.responsables.nombre AS nombre_recibe,
              cce.laboratorios.observaciones AS observaciones,
              cce.laboratorios.usuario AS idusuario,
              usuarios.nombre AS usuario
       FROM cce.laboratorios INNER JOIN diccionarios.paises ON cce.laboratorios.pais = diccionarios.paises.id
                             INNER JOIN diccionarios.localidades ON cce.laboratorios.localidad = diccionarios.localidades.codloc
                             INNER JOIN diccionarios.provincias ON diccionarios.localidades.codpcia = diccionarios.provincias.cod_prov
                             INNER JOIN diccionarios.dependencias ON cce.laboratorios.dependencia = diccionarios.dependencias.id_dependencia
                             LEFT JOIN cce.responsables ON cce.laboratorios.recibe_muestras_chagas = cce.responsables.id
                             INNER JOIN cce.responsables AS usuarios ON cce.laboratorios.usuario = usuarios.id;

-- eliminamos la vista de determinaciones si existe
DROP VIEW IF EXISTS vw_determinaciones;

-- la recreamos
CREATE ALGORITHM = UNDEFINED
       SQL SECURITY INVOKER
       VIEW vw_determinaciones AS
       SELECT cce.chag_datos.id AS id,
              cce.chag_datos.operativo AS idoperativo,
              operativos_chagas.operativo_nro AS operativo,
              cce.chag_datos.laboratorio AS idlaboratorio,
              cce.laboratorios.nombre AS laboratorio,
              cce.chag_datos.marca AS idmarca,
              cce.marcas_chagas.marca AS marca,
              cce.chag_datos.lote AS lote,
              DATE_FORMAT(cce.chag_datos.vencimiento, '%m/%Y') AS vencimiento,
              cce.chag_datos.muestra_nro AS muestra_nro,
              cce.chag_datos.valor_corte AS valor_corte,
              cce.chag_datos.valor_lectura AS valor_lectura,
              cce.chag_datos.metodo AS metodo,
              cce.chag_datos.tecnica AS idtecnica,
              cce.tecnicas.tecnica AS tecnica,
              cce.chag_datos.determinacion AS determinacion,
              cce.chag_datos.correcto AS correcto,
              DATE_FORMAT(cce.chag_datos.fecha_alta, '%d/%m/%Y') AS fecha_alta,
              cce.chag_datos.usuario AS id_usuario,
              cce.responsables.nombre AS usuario
       FROM cce.chag_datos INNER JOIN cce.operativos_chagas ON cce.chag_datos.operativo = cce.operativos_chagas.id
                           INNER JOIN cce.laboratorios ON cce.chag_datos.laboratorio = cce.laboratorios.id
                           INNER JOIN cce.marcas_chagas ON cce.chag_datos.marca = cce.marcas_chagas.id
                           INNER JOIN cce.tecnicas ON cce.chag_datos.tecnica = cce.tecnicas.id
                           INNER JOIN cce.responsables ON cce.chag_datos.usuario = cce.responsables.id;

/*
 * Esta vista presenta las determinaciones que a pesar de tener el operativo cerrado
 * no presentan cargado el resultado correcto (probable error en la migración)
 * utilizada para fines de verificación
 */

-- eliminamos la vista de errores
DROP VIEW IF EXISTS vw_sin_resultados;

-- la recreamos
CREATE ALGORITHM = UNDEFINED
       SQL SECURITY INVOKER
       VIEW vw_sin_resultados AS
       SELECT cce.chag_datos.determinacion AS determinacion,
              cce.laboratorios.nombre AS laboratorio,
              diccionarios.localidades.nomloc AS localidad,
              diccionarios.provincias.nom_prov AS provincia,
              cce.operativos_chagas.operativo_nro AS operativo
       FROM cce.chag_datos INNER JOIN cce.laboratorios ON cce.chag_datos.laboratorio = cce.laboratorios.id
                           INNER JOIN diccionarios.localidades ON cce.laboratorios.localidad = diccionarios.localidades.codloc
                           INNER JOIN diccionarios.provincias ON diccionarios.localidades.codpcia = diccionarios.provincias.cod_prov
                           INNER JOIN cce.operativos_chagas ON cce.chag_datos.operativo = cce.operativos_chagas.id
       WHERE ISNULL(cce.chag_datos.correcto) AND
             cce.operativos_chagas.abierto = 'No';

/*
 * Esta consulta presenta la tabla de frecuencias de los resultados de las
 * determinaciones, utilizamos el CASE para sumar según el resultado
 * declarado - util en los casos de dudas de cual es el resultado
 * correcto
 */

 -- eliminamos la vista si existe
 DROP VIEW IF EXISTS vw_frecuencias;

-- la recreamos
CREATE ALGORITHM = UNDEFINED
       SQL SECURITY INVOKER
       VIEW vw_frecuencias AS
       SELECT cce.operativos_chagas.operativo_nro AS operativo,
              SUM(CASE WHEN cce.chag_datos.determinacion = 'Reactivo' THEN 1
                       ELSE 0
                       END) AS reactivos,
              SUM(CASE WHEN cce.chag_datos.determinacion = 'No Reactivo' THEN 1
                       ELSE 0
                       END) AS noreactivo,
              SUM(CASE WHEN cce.chag_datos.determinacion = 'Indeterminado' THEN 1
                       ELSE 0
                       END) AS indeterminado,
              COUNT(cce.chag_datos.id) AS total
      FROM cce.operativos_chagas INNER JOIN cce.chag_datos ON cce.operativos_chagas.id = cce.chag_datos.operativo
      GROUP BY cce.operativos_chagas.id;

/*
 Esta consulta arroja el número total de determinaciones y la concordancia
 la idea es que luego se pueda filtrar por año del operativo ya tenemos el
 join con la tabla de operativos que lo presenta como el campo año, de
 forma que para filtrar solo debemos hacer WHERE anio = '????'
 También es posible agrupar por provincia para obtener los totales

 SELECT vw_concordancia.provincia AS provincia,
       SUM(vw_concordancia.determinaciones) AS determinaciones,
       SUM(vw_concordancia.correctas) AS correctas,
       SUM(vw_concordancia.incorrectas) AS incorrectas
 FROM vw_concordancia
 GROUP BY vw_concordancia.provincia;

 o por año para obtener los totales por año

 SELECT vw_concordancia.anio AS anio,
        SUM(vw_concordancia.determinaciones) AS determinaciones,
        SUM(vw_concordancia.correctas) AS correctas,
        SUM(vw_concordancia.incorrectas) AS incorrectas
 FROM vw_concordancia
 GROUP BY vw_concordancia.anio;

 */

 -- eliminamos la vista si existe
 DROP VIEW IF EXISTS vw_concordancia;

 -- la recreamos
CREATE ALGORITHM = UNDEFINED
       SQL SECURITY INVOKER
       VIEW vw_concordancia AS
       SELECT diccionarios.paises.nombre AS pais,
              diccionarios.provincias.nom_prov AS provincia,
              SUBSTRING(cce.operativos_chagas.operativo_nro, 3) AS anio,
              COUNT(chag_datos.id) AS determinaciones,
              SUM(CASE WHEN cce.chag_datos.determinacion = cce.chag_datos.correcto THEN 1
                       WHEN cce.chag_datos.determinacion = 'Reactivo' AND chag_datos.correcto = 'Indeterminado' THEN 1
                       WHEN cce.chag_datos.determinacion = 'Indeterminado' AND chag_datos.correcto = 'Reactivo' THEN 1
                       ELSE 0
                       END) AS correctas,
              SUM(CASE WHEN cce.chag_datos.determinacion = 'Reactivo' AND chag_datos.correcto = 'No Reactivo' THEN 1
                       WHEN cce.chag_datos.determinacion = 'Indeterminado' AND chag_datos.correcto = 'No Reactivo' THEN 1
                       WHEN cce.chag_datos.determinacion = 'No Reactivo' AND chag_datos.correcto = 'Reactivo' THEN 1
                       WHEN cce.chag_datos.determinacion = 'No Reactivo' AND chag_datos.correcto = 'Indeterminado' THEN 1
                       ELSE 0
                       END) AS incorrectas
       FROM cce.chag_datos INNER JOIN cce.laboratorios ON cce.chag_datos.laboratorio = cce.laboratorios.id
                           INNER JOIN diccionarios.localidades ON cce.laboratorios.localidad = diccionarios.localidades.codloc
                           INNER JOIN diccionarios.provincias ON diccionarios.localidades.codpcia = diccionarios.provincias.cod_prov
                           INNER JOIN cce.operativos_chagas ON cce.chag_datos.operativo = cce.operativos_chagas.id
                           INNER JOIN diccionarios.paises ON diccionarios.paises.id = diccionarios.provincias.pais
       WHERE NOT ISNULL(cce.chag_datos.correcto)
       GROUP BY diccionarios.provincias.nom_prov,
                SUBSTRING(cce.operativos_chagas.operativo_nro, 3)
       ORDER BY diccionarios.provincias.nom_prov;

/*

    Esta consulta hace lo mismo que la anterior pero agrupa los totales por
    laboratorio, la utilizamos para el análisis estadístico de los
    reportes provinciales

*/

-- eliminamos la consulta si existe
DROP VIEW IF EXISTS vw_concordancia_laboratorios;

-- la recreamos
CREATE ALGORITHM = UNDEFINED
       SQL SECURITY INVOKER
       VIEW vw_concordancia_laboratorios AS
       SELECT diccionarios.paises.nombre AS pais,
              diccionarios.provincias.nom_prov AS provincia,
              cce.laboratorios.nombre AS laboratorio,
              SUBSTRING(cce.operativos_chagas.operativo_nro, 3) AS anio,
              COUNT(chag_datos.id) AS determinaciones,
              SUM(CASE WHEN cce.chag_datos.determinacion = cce.chag_datos.correcto THEN 1
                       WHEN cce.chag_datos.determinacion = 'Reactivo' AND chag_datos.correcto = 'Indeterminado' THEN 1
                       WHEN cce.chag_datos.determinacion = 'Indeterminado' AND chag_datos.correcto = 'Reactivo' THEN 1
                       ELSE 0
                  END) AS correctas,
             SUM(CASE WHEN cce.chag_datos.determinacion = 'Reactivo' AND chag_datos.correcto = 'No Reactivo' THEN 1
                      WHEN cce.chag_datos.determinacion = 'Indeterminado' AND chag_datos.correcto = 'No Reactivo' THEN 1
                      WHEN cce.chag_datos.determinacion = 'No Reactivo' AND chag_datos.correcto = 'Reactivo' THEN 1
                      WHEN cce.chag_datos.determinacion = 'No Reactivo' AND chag_datos.correcto = 'Indeterminado' THEN 1
                      ELSE 0
                 END) AS incorrectas
       FROM cce.chag_datos INNER JOIN cce.laboratorios ON cce.chag_datos.laboratorio = cce.laboratorios.id
                           INNER JOIN diccionarios.localidades ON cce.laboratorios.localidad = diccionarios.localidades.codloc
                           INNER JOIN diccionarios.provincias ON diccionarios.localidades.codpcia = diccionarios.provincias.cod_prov
                           INNER JOIN cce.operativos_chagas ON cce.chag_datos.operativo = cce.operativos_chagas.id
                           INNER JOIN diccionarios.paises ON diccionarios.paises.id = diccionarios.provincias.pais
       WHERE NOT ISNULL(cce.chag_datos.correcto)
       GROUP BY cce.laboratorios.id,
             SUBSTRING(cce.operativos_chagas.operativo_nro, 3)
       ORDER BY cce.laboratorios.nombre;

/*
  Esta consulta arroja la distribución por fuente de financiamiento de todos los
  laboratorios registrados (activos o no)
*/

-- eliminamos la consulta si existe
DROP VIEW IF EXISTS vw_financiamiento;

-- la recreamos
CREATE ALGORITHM = UNDEFINED
       SQL SECURITY INVOKER
       VIEW vw_financiamiento AS
       SELECT diccionarios.paises.nombre AS pais,
              diccionarios.provincias.nom_prov AS provincia,
             SUM(CASE WHEN diccionarios.dependencias.dependencia = "Nacional" THEN 1
                 ELSE 0
                 END) AS nacional,
             SUM(CASE WHEN diccionarios.dependencias.dependencia = "Provincial" THEN 1
                 ELSE 0
                 END) AS provincial,
             SUM(CASE WHEN diccionarios.dependencias.dependencia = "Municipal" THEN 1
                 ELSE 0
                 END) AS municipal,
             SUM(CASE WHEN diccionarios.dependencias.dependencia = "Universitario" THEN 1
                 ELSE 0
                 END) AS universitario,
             SUM(CASE WHEN diccionarios.dependencias.dependencia = "Privado" THEN 1
                 ELSE 0
                 END) AS privado,
             SUM(CASE WHEN diccionarios.dependencias.dependencia = "Obra Social" THEN 1
                 ELSE 0
                 END) AS obrasocial
       FROM cce.laboratorios INNER JOIN diccionarios.localidades ON cce.laboratorios.localidad = diccionarios.localidades.codloc
                             INNER JOIN diccionarios.provincias ON diccionarios.localidades.codpcia = diccionarios.provincias.cod_prov
                             INNER JOIN diccionarios.paises ON diccionarios.provincias.pais = diccionarios.paises.id
                             INNER JOIN diccionarios.dependencias ON cce.laboratorios.dependencia = diccionarios.dependencias.id_dependencia
       GROUP BY diccionarios.provincias.nom_prov
       ORDER BY diccionarios.provincias.nom_prov;

/*
  Esta vista provee el nùmero de determinaciones cargadas
  clasificadas por jurisdicción
*/

-- eliminamos la vista si existe
DROP VIEW IF EXISTS vw_det_cargadas;

-- la recreamos
CREATE ALGORITHM = UNDEFINED
       SQL SECURITY INVOKER
       VIEW vw_det_cargadas AS
       SELECT SUBSTRING(cce.operativos_chagas.operativo_nro, 3) AS anio,
              diccionarios.paises.nombre AS pais,
              diccionarios.provincias.NOM_PROV AS jurisdiccion,
              COUNT(cce.chag_datos.ID) AS determinaciones
       FROM cce.chag_datos INNER JOIN cce.laboratorios ON cce.chag_datos.LABORATORIO = cce.laboratorios.ID
                           INNER JOIN cce.operativos_chagas ON cce.chag_datos.OPERATIVO = cce.operativos_chagas.ID
                           INNER JOIN diccionarios.localidades ON cce.laboratorios.LOCALIDAD = diccionarios.localidades.CODLOC
                           INNER JOIN diccionarios.provincias ON diccionarios.localidades.CODPCIA = diccionarios.provincias.COD_PROV
                           INNER JOIN diccionarios.paises ON diccionarios.provincias.pais = diccionarios.paises.id
       GROUP BY SUBSTRING(cce.operativos_chagas.operativo_nro, 3),
                diccionarios.provincias.nom_prov
       ORDER BY diccionarios.provincias.NOM_PROV;

/*
  Esta vista presenta la tabla de frecuencias por jurisdicciòn del nùmero
  de paneles enviados y procesados por cada una de ellas
*/

-- eliminamos la vista si existe
DROP VIEW IF EXISTS vw_paneles;

-- la recreamos
CREATE ALGORITHM = UNDEFINED
       SQL SECURITY INVOKER
       VIEW vw_paneles AS
       SELECT diccionarios.paises.nombre AS pais,
              diccionarios.provincias.nom_prov AS jurisdiccion,
              SUM(CASE WHEN cce.laboratorios.activo = "Si" THEN 1
                  ELSE 0
                  END) AS laboratorios
       FROM cce.laboratorios INNER JOIN diccionarios.localidades ON cce.laboratorios.localidad = diccionarios.localidades.codloc
                             INNER JOIN diccionarios.provincias ON diccionarios.localidades.codpcia = diccionarios.provincias.cod_prov
                             INNER JOIN diccionarios.paises ON diccionarios.provincias.pais = diccionarios.paises.id
       WHERE cce.laboratorios.activo = 'Si'
       GROUP BY diccionarios.provincias.nom_prov
       ORDER BY diccionarios.provincias.nom_prov;

/*
   Esta vista presenta la distribuciòn de muestras cargadas a tèrmino
   y fuera de tèrmino, hacemos un cast en la comparación porque la
   fecha de alta está en formato timestamp y la fecha de finalización
   del operativo está en formato date
*/

-- eliminamos la vista si existe
DROP VIEW IF EXISTS vw_termino;

-- la recreamos
CREATE ALGORITHM = UNDEFINED
       SQL SECURITY INVOKER
       VIEW vw_termino AS
       SELECT SUBSTRING(cce.operativos_chagas.operativo_nro, 3) AS anio,
              diccionarios.paises.nombre AS pais,
              diccionarios.provincias.nom_prov AS jurisdiccion,
              SUM(CASE WHEN CAST(cce.chag_datos.fecha_alta AS DATE) <= CAST(cce.operativos_chagas.fecha_fin AS DATE) THEN 1
                  ELSE 0
                  END) AS atermino,
              SUM(CASE WHEN CAST(cce.chag_datos.fecha_alta AS DATE) > CAST(cce.operativos_chagas.fecha_fin AS DATE) THEN 1
                  ELSE 0
                  END) AS fueratermino
       FROM cce.chag_datos INNER JOIN cce.operativos_chagas ON cce.chag_datos.operativo = cce.operativos_chagas.id
                           INNER JOIN cce.laboratorios ON cce.chag_datos.laboratorio = cce.laboratorios.id
                           INNER JOIN diccionarios.localidades ON cce.laboratorios.localidad = diccionarios.localidades.codloc
                           INNER JOIN diccionarios.provincias ON diccionarios.localidades.codpcia = diccionarios.provincias.cod_prov
                           INNER JOIN diccionarios.paises ON diccionarios.provincias.pais = diccionarios.paises.id
       GROUP BY SUBSTRING(cce.operativos_chagas.operativo_nro, 3),
                diccionarios.provincias.nom_prov
       ORDER BY diccionarios.provincias.nom_prov;

/*
   Esta vista presenta la distribuciòn de las muestras por
   jurisdicciòn segùn la tècnica empleada
*/

-- eliminamos la vista si existe
DROP VIEW IF EXISTS vw_muestras;

-- la recreamos
CREATE ALGORITHM = UNDEFINED
       SQL SECURITY INVOKER
       VIEW vw_muestras AS
       SELECT SUBSTRING(cce.operativos_chagas.operativo_nro, 3) AS anio,
              diccionarios.paises.nombre AS pais,
              diccionarios.provincias.nom_prov AS jurisdiccion,
              SUM(CASE WHEN cce.tecnicas.tecnica = "AP" THEN 1
                  ELSE 0
                  END) AS ap,
              SUM(CASE WHEN cce.tecnicas.tecnica = "ELISA" THEN 1
                  ELSE 0
                  END) AS elisa,
              SUM(CASE WHEN cce.tecnicas.tecnica = "HAI" THEN 1
                  ELSE 0
                  END) AS hai,
              SUM(CASE WHEN cce.tecnicas.tecnica = "IFI" THEN 1
                  ELSE 0
                  END) AS ifi,
              SUM(CASE WHEN cce.tecnicas.tecnica = "QUIMIOLUMINISCENCIA" THEN 1
                  ELSE 0
                  END) AS quimio,
              SUM(CASE WHEN cce.tecnicas.tecnica = "OTRA" THEN 1
                  ELSE 0
                  END) AS otra
       FROM cce.chag_datos INNER JOIN cce.laboratorios ON cce.chag_datos.laboratorio = cce.laboratorios.id
                           INNER JOIN cce.tecnicas ON cce.chag_datos.tecnica = cce.tecnicas.id
                           INNER JOIN cce.operativos_chagas ON cce.chag_datos.operativo = cce.operativos_chagas.id
                           INNER JOIN diccionarios.localidades ON cce.laboratorios.localidad = diccionarios.localidades.codloc
                           INNER JOIN diccionarios.provincias ON diccionarios.localidades.codpcia = diccionarios.provincias.cod_prov
                           INNER JOIN diccionarios.paises ON diccionarios.provincias.pais = diccionarios.paises.id
       GROUP BY SUBSTRING(cce.operativos_chagas.operativo_nro, 3),
                diccionarios.provincias.nom_prov
       ORDER BY diccionarios.provincias.nom_prov;

/*
  Esta vista presenta la distribución lector / visual para elisa
*/

-- eliminamos la vista si existe
DROP VIEW IF EXISTS vw_lector;

-- la recreamos
CREATE ALGORITHM = UNDEFINED
       SQL SECURITY INVOKER
       VIEW vw_lector AS
       SELECT SUBSTRING(cce.operativos_chagas.operativo_nro, 3) AS anio,
              diccionarios.paises.nombre AS pais,
              diccionarios.provincias.nom_prov AS jurisdiccion,
              SUM(CASE WHEN cce.chag_datos.metodo = "Lector" THEN 1
                  ELSE 0
                  END) AS lector,
              SUM(CASE WHEN cce.chag_datos.metodo = "Visual" THEN 1
                  ELSE 0
                  END) AS visual
       FROM cce.chag_datos INNER JOIN cce.operativos_chagas ON cce.chag_datos.operativo = cce.operativos_chagas.id
                           INNER JOIN cce.tecnicas ON cce.chag_datos.tecnica = cce.tecnicas.id
                           INNER JOIN cce.laboratorios ON cce.chag_datos.laboratorio = cce.laboratorios.id
                           INNER JOIN diccionarios.localidades ON cce.laboratorios.localidad = diccionarios.localidades.codloc
                           INNER JOIN diccionarios.provincias ON diccionarios.localidades.codpcia = diccionarios.provincias.cod_prov
                           INNER JOIN diccionarios.paises ON diccionarios.provincias.pais = diccionarios.paises.id
       WHERE cce.tecnicas.tecnica = 'ELISA'
       GROUP BY SUBSTRING(cce.operativos_chagas.operativo_nro, 3),
                diccionarios.provincias.nom_prov
       ORDER BY diccionarios.provincias.nom_prov;


/*
    Vista que presenta la tabla de frecuencias de errores de corte y lectura
*/

-- la eliminamos si existe
DROP VIEW IF EXISTS vw_corte;

-- la recreamos
CREATE ALGORITHM = UNDEFINED
       SQL SECURITY INVOKER
       VIEW vw_corte AS
       SELECT SUBSTRING(cce.operativos_chagas.operativo_nro, 3) AS anio,
              diccionarios.paises.nombre AS pais,
              diccionarios.provincias.NOM_PROV AS jurisdiccion,
              SUM(CASE WHEN cce.chag_datos.VALOR_CORTE = 'OTRO' OR (cce.chag_datos.METODO = 'Lector' AND cce.chag_datos.VALOR_CORTE = '0.000') THEN 1
                       ELSE 0
                       END) AS malcorte,
              SUM(CASE WHEN ((cce.chag_datos.VALOR_LECTURA = 'OTRO' OR (cce.chag_datos.METODO = 'Lector' AND cce.chag_datos.VALOR_LECTURA = '0.000')) AND
                             (cce.chag_datos.DETERMINACION = 'Reactivo' OR cce.chag_datos.DETERMINACION = 'Indeterminada')) THEN 1
                       ELSE 0
                       END) AS mallectura
       FROM cce.laboratorios INNER JOIN diccionarios.localidades ON cce.laboratorios.LOCALIDAD = diccionarios.localidades.CODLOC
                             INNER JOIN diccionarios.provincias ON diccionarios.localidades.CODPCIA = diccionarios.provincias.COD_PROV
                             INNER JOIN cce.chag_datos ON cce.laboratorios.ID = cce.chag_datos.LABORATORIO
                             INNER JOIN diccionarios.paises ON diccionarios.provincias.PAIS = diccionarios.paises.ID
                             INNER JOIN cce.operativos_chagas ON cce.chag_datos.OPERATIVO = cce.operativos_chagas.ID
       GROUP BY SUBSTRING(cce.operativos_chagas.operativo_nro, 3),
                diccionarios.provincias.NOM_PROV
       ORDER BY diccionarios.provincias.NOM_PROV;

/*
    Esta vista presenta la nómina de determinaciones bajo la categoría
    otras
*/

-- la eliminamos si existe
DROP VIEW IF EXISTS vw_otras;

-- la recreamos
CREATE ALGORITHM = UNDEFINED
       SQL SECURITY INVOKER
       VIEW vw_otras AS
       SELECT SUBSTRING(cce.operativos_chagas.operativo_nro, 3) AS anio,
              diccionarios.paises.nombre AS pais,
              diccionarios.provincias.NOM_PROV AS jurisdiccion,
    	      cce.laboratorios.NOMBRE AS laboratorio,
              cce.chag_otras.PRUEBA AS prueba,
              cce.chag_otras.MARCA AS marca,
              cce.chag_datos.ID AS id
       FROM chag_datos INNER JOIN chag_otras ON chag_otras.ID_DATOS = chag_datos.ID
                       INNER JOIN operativos_chagas ON chag_datos.OPERATIVO = operativos_chagas.ID
                       INNER JOIN laboratorios ON chag_datos.LABORATORIO = laboratorios.ID
                       INNER JOIN diccionarios.localidades ON laboratorios.LOCALIDAD = diccionarios.localidades.CODLOC
                       INNER JOIN diccionarios.provincias ON diccionarios.localidades.CODPCIA = diccionarios.provincias.COD_PROV
                       INNER JOIN diccionarios.paises ON diccionarios.provincias.PAIS = diccionarios.paises.ID
       ORDER BY diccionarios.provincias.nom_prov,
                cce.laboratorios.nombre,
                chag_otras.PRUEBA,
	            chag_datos.MARCA;

/*
    Esta vista presenta la nómina de determinaciones según técnica y los concordantes
    para cada una de las técnicas
*/

-- eliminamos la vista si existe
DROP VIEW IF EXISTS vw_conc_tecnica;

 -- la recreamos
CREATE ALGORITHM = UNDEFINED
       SQL SECURITY INVOKER
       VIEW vw_conc_tecnica AS
       SELECT diccionarios.paises.nombre AS pais,
              diccionarios.provincias.nom_prov AS provincia,
              SUBSTRING(cce.operativos_chagas.operativo_nro, 3) AS anio,
              SUM(CASE WHEN cce.tecnicas.tecnica = "AP" THEN 1
                  ELSE 0
                  END) AS ap,
              SUM(CASE WHEN cce.chag_datos.determinacion = cce.chag_datos.correcto AND cce.tecnicas.tecnica = "AP" THEN 1
                       WHEN cce.chag_datos.determinacion = 'Reactivo' AND chag_datos.correcto = 'Indeterminado' AND cce.tecnicas.tecnica = "AP" THEN 1
                       WHEN cce.chag_datos.determinacion = 'Indeterminado' AND chag_datos.correcto = 'Reactivo' AND cce.tecnicas.tecnica = "AP" THEN 1
                       ELSE 0
                       END) AS correctasap,
              SUM(CASE WHEN cce.tecnicas.tecnica = "ELISA" THEN 1
                  ELSE 0
                  END) AS elisa,
              SUM(CASE WHEN cce.chag_datos.determinacion = cce.chag_datos.correcto AND cce.tecnicas.tecnica = "ELISA" THEN 1
                       WHEN cce.chag_datos.determinacion = 'Reactivo' AND chag_datos.correcto = 'Indeterminado' AND cce.tecnicas.tecnica = "ELISA" THEN 1
                       WHEN cce.chag_datos.determinacion = 'Indeterminado' AND chag_datos.correcto = 'Reactivo' AND cce.tecnicas.tecnica = "ELISA" THEN 1
                       ELSE 0
                       END) AS correctaselisa,
              SUM(CASE WHEN cce.tecnicas.tecnica = "HAI" THEN 1
                  ELSE 0
                  END) AS hai,
              SUM(CASE WHEN cce.chag_datos.determinacion = cce.chag_datos.correcto AND cce.tecnicas.tecnica = "HAI" THEN 1
                       WHEN cce.chag_datos.determinacion = 'Reactivo' AND chag_datos.correcto = 'Indeterminado' AND cce.tecnicas.tecnica = "HAI" THEN 1
                       WHEN cce.chag_datos.determinacion = 'Indeterminado' AND chag_datos.correcto = 'Reactivo' AND cce.tecnicas.tecnica = "HAI" THEN 1
                       ELSE 0
                       END) AS correctashai,
              SUM(CASE WHEN cce.tecnicas.tecnica = "IFI" THEN 1
                  ELSE 0
                  END) AS ifi,
              SUM(CASE WHEN cce.chag_datos.determinacion = cce.chag_datos.correcto AND cce.tecnicas.tecnica = "IFI" THEN 1
                       WHEN cce.chag_datos.determinacion = 'Reactivo' AND chag_datos.correcto = 'Indeterminado' AND cce.tecnicas.tecnica = "IFI" THEN 1
                       WHEN cce.chag_datos.determinacion = 'Indeterminado' AND chag_datos.correcto = 'Reactivo' AND cce.tecnicas.tecnica = "IFI" THEN 1
                       ELSE 0
                       END) AS correctasifi,
              SUM(CASE WHEN cce.tecnicas.tecnica = "QUIMIOLUMINISCENCIA" THEN 1
                  ELSE 0
                  END) AS quimio,
              SUM(CASE WHEN cce.chag_datos.determinacion = cce.chag_datos.correcto AND cce.tecnicas.tecnica = "QUIMIOLUMINISCENCIA" THEN 1
                       WHEN cce.chag_datos.determinacion = 'Reactivo' AND chag_datos.correcto = 'Indeterminado' AND cce.tecnicas.tecnica = "QUIMIOLUMINISCENCIA" THEN 1
                       WHEN cce.chag_datos.determinacion = 'Indeterminado' AND chag_datos.correcto = 'Reactivo' AND cce.tecnicas.tecnica = "QUIMIOLUMINISCENCIA" THEN 1
                       ELSE 0
                       END) AS correctasquimio,
              SUM(CASE WHEN cce.tecnicas.tecnica = "OTRA" THEN 1
                  ELSE 0
                  END) AS otras,
              SUM(CASE WHEN cce.chag_datos.determinacion = cce.chag_datos.correcto AND cce.tecnicas.tecnica = "OTRA" THEN 1
                       WHEN cce.chag_datos.determinacion = 'Reactivo' AND chag_datos.correcto = 'Indeterminado' AND cce.tecnicas.tecnica = "OTRA" THEN 1
                       WHEN cce.chag_datos.determinacion = 'Indeterminado' AND chag_datos.correcto = 'Reactivo' AND cce.tecnicas.tecnica = "OTRA" THEN 1
                       ELSE 0
                       END) AS correctasotras
       FROM cce.chag_datos INNER JOIN cce.laboratorios ON cce.chag_datos.laboratorio = cce.laboratorios.id
                           INNER JOIN diccionarios.localidades ON cce.laboratorios.localidad = diccionarios.localidades.codloc
                           INNER JOIN diccionarios.provincias ON diccionarios.localidades.codpcia = diccionarios.provincias.cod_prov
                           INNER JOIN cce.operativos_chagas ON cce.chag_datos.operativo = cce.operativos_chagas.id
                           INNER JOIN cce.tecnicas ON cce.chag_datos.tecnica = cce.tecnicas.id
                           INNER JOIN diccionarios.paises ON diccionarios.paises.id = diccionarios.provincias.pais
       WHERE NOT ISNULL(cce.chag_datos.correcto)
       GROUP BY diccionarios.provincias.nom_prov,
                SUBSTRING(cce.operativos_chagas.operativo_nro, 3)
       ORDER BY diccionarios.provincias.nom_prov;

/*
    Esta vista nos provee los falsos positivos clasificados
    según técnica
*/

-- eliminamos la vista si existe
DROP VIEW IF EXISTS vw_falsos_positivos;

 -- la recreamos
CREATE ALGORITHM = UNDEFINED
       SQL SECURITY INVOKER
       VIEW vw_falsos_positivos AS
       SELECT diccionarios.paises.nombre AS pais,
              diccionarios.provincias.nom_prov AS provincia,
              SUBSTRING(cce.operativos_chagas.operativo_nro, 3) AS anio,
              SUM(CASE WHEN cce.tecnicas.tecnica = "AP" THEN 1
                  ELSE 0
                  END) AS ap,
              SUM(CASE WHEN cce.chag_datos.determinacion = 'Reactivo' AND chag_datos.correcto = 'No Reactivo' AND cce.tecnicas.tecnica = "AP" THEN 1
                       WHEN cce.chag_datos.determinacion = 'Indeterminado' AND chag_datos.correcto = 'No Reactivo' AND cce.tecnicas.tecnica = "AP" THEN 1
                       ELSE 0
                       END) AS falsosap,
              SUM(CASE WHEN cce.tecnicas.tecnica = "ELISA" THEN 1
                  ELSE 0
                  END) AS elisa,
              SUM(CASE WHEN cce.chag_datos.determinacion = 'Reactivo' AND chag_datos.correcto = 'No Reactivo' AND cce.tecnicas.tecnica = "ELISA" THEN 1
                       WHEN cce.chag_datos.determinacion = 'Indeterminado' AND chag_datos.correcto = 'No Reactivo' AND cce.tecnicas.tecnica = "ELISA" THEN 1
                       ELSE 0
                       END) AS falsoselisa,
              SUM(CASE WHEN cce.tecnicas.tecnica = "HAI" THEN 1
                  ELSE 0
                  END) AS hai,
              SUM(CASE WHEN cce.chag_datos.determinacion = 'Reactivo' AND chag_datos.correcto = 'No Reactivo' AND cce.tecnicas.tecnica = "HAI" THEN 1
                       WHEN cce.chag_datos.determinacion = 'Indeterminado' AND chag_datos.correcto = 'No Reactivo' AND cce.tecnicas.tecnica = "HAI" THEN 1
                       ELSE 0
                       END) AS falsoshai,
              SUM(CASE WHEN cce.tecnicas.tecnica = "IFI" THEN 1
                  ELSE 0
                  END) AS ifi,
              SUM(CASE WHEN cce.chag_datos.determinacion = 'Reactivo' AND chag_datos.correcto = 'No Reactivo' AND cce.tecnicas.tecnica = "IFI" THEN 1
                       WHEN cce.chag_datos.determinacion = 'Indeterminado' AND chag_datos.correcto = 'No Reactivo' AND cce.tecnicas.tecnica = "IFI" THEN 1
                       ELSE 0
                       END) AS falsosifi,
              SUM(CASE WHEN cce.tecnicas.tecnica = "QUIMIOLUMINISCENCIA" THEN 1
                  ELSE 0
                  END) AS quimio,
              SUM(CASE WHEN cce.chag_datos.determinacion = 'Reactivo' AND chag_datos.correcto = 'No Reactivo' AND cce.tecnicas.tecnica = "QUIMIOLUMINISCENCIA" THEN 1
                       WHEN cce.chag_datos.determinacion = 'Indeterminado' AND chag_datos.correcto = 'No Reactivo' AND cce.tecnicas.tecnica = "QUIMIOLUMINISCENCIA" THEN 1
                       ELSE 0
                       END) AS falsosquimio,
              SUM(CASE WHEN cce.tecnicas.tecnica = "OTRA" THEN 1
                  ELSE 0
                  END) AS otras,
              SUM(CASE WHEN cce.chag_datos.determinacion = 'Reactivo' AND chag_datos.correcto = 'No Reactivo' AND cce.tecnicas.tecnica = "OTRA" THEN 1
                       WHEN cce.chag_datos.determinacion = 'Indeterminado' AND chag_datos.correcto = 'No Reactivo' AND cce.tecnicas.tecnica = "OTRA" THEN 1
                       ELSE 0
                       END) AS falsosotras
       FROM cce.chag_datos INNER JOIN cce.laboratorios ON cce.chag_datos.laboratorio = cce.laboratorios.id
                           INNER JOIN diccionarios.localidades ON cce.laboratorios.localidad = diccionarios.localidades.codloc
                           INNER JOIN diccionarios.provincias ON diccionarios.localidades.codpcia = diccionarios.provincias.cod_prov
                           INNER JOIN cce.operativos_chagas ON cce.chag_datos.operativo = cce.operativos_chagas.id
                           INNER JOIN cce.tecnicas ON cce.chag_datos.tecnica = cce.tecnicas.id
                           INNER JOIN diccionarios.paises ON diccionarios.paises.id = diccionarios.provincias.pais
       WHERE NOT ISNULL(cce.chag_datos.correcto)
       GROUP BY diccionarios.provincias.nom_prov,
                SUBSTRING(cce.operativos_chagas.operativo_nro, 3)
       ORDER BY diccionarios.provincias.nom_prov;

/*
    Esta vista nos provee los falsos negativos clasificados
    según técnica
*/

-- eliminamos la vista si existe
DROP VIEW IF EXISTS vw_falsos_negativos;

 -- la recreamos
CREATE ALGORITHM = UNDEFINED
       SQL SECURITY INVOKER
       VIEW vw_falsos_negativos AS
       SELECT diccionarios.paises.nombre AS pais,
              diccionarios.provincias.nom_prov AS provincia,
              SUBSTRING(cce.operativos_chagas.operativo_nro, 3) AS anio,
              SUM(CASE WHEN cce.tecnicas.tecnica = "AP" THEN 1
                  ELSE 0
                  END) AS ap,
              SUM(CASE WHEN cce.chag_datos.determinacion = 'No Reactivo' AND chag_datos.correcto = 'Reactivo' AND cce.tecnicas.tecnica = "AP" THEN 1
                       WHEN cce.chag_datos.determinacion = 'No Reactivo' AND chag_datos.correcto = 'Indeterminado' AND cce.tecnicas.tecnica = "AP" THEN 1
                       ELSE 0
                       END) AS falsosap,
              SUM(CASE WHEN cce.tecnicas.tecnica = "ELISA" THEN 1
                  ELSE 0
                  END) AS elisa,
              SUM(CASE WHEN cce.chag_datos.determinacion = 'No Reactivo' AND chag_datos.correcto = 'Reactivo' AND cce.tecnicas.tecnica = "ELISA" THEN 1
                       WHEN cce.chag_datos.determinacion = 'No Reactivo' AND chag_datos.correcto = 'Indeterminado' AND cce.tecnicas.tecnica = "ELISA" THEN 1
                       ELSE 0
                       END) AS falsoselisa,
              SUM(CASE WHEN cce.tecnicas.tecnica = "HAI" THEN 1
                  ELSE 0
                  END) AS hai,
              SUM(CASE WHEN cce.chag_datos.determinacion = 'No Reactivo' AND chag_datos.correcto = 'Reactivo' AND cce.tecnicas.tecnica = "HAI" THEN 1
                       WHEN cce.chag_datos.determinacion = 'No Reactivo' AND chag_datos.correcto = 'Indeterminado' AND cce.tecnicas.tecnica = "HAI" THEN 1
                       ELSE 0
                       END) AS falsoshai,
              SUM(CASE WHEN cce.tecnicas.tecnica = "IFI" THEN 1
                  ELSE 0
                  END) AS ifi,
              SUM(CASE WHEN cce.chag_datos.determinacion = 'No Reactivo' AND chag_datos.correcto = 'Reactivo' AND cce.tecnicas.tecnica = "IFI" THEN 1
                       WHEN cce.chag_datos.determinacion = 'No Reactivo' AND chag_datos.correcto = 'Indeterminado' AND cce.tecnicas.tecnica = "IFI" THEN 1
                       ELSE 0
                       END) AS falsosifi,
              SUM(CASE WHEN cce.tecnicas.tecnica = "QUIMIOLUMINISCENCIA" THEN 1
                  ELSE 0
                  END) AS quimio,
              SUM(CASE WHEN cce.chag_datos.determinacion = 'No Reactivo' AND chag_datos.correcto = 'Reactivo' AND cce.tecnicas.tecnica = "QUIMIOLUMINISCENCIA" THEN 1
                       WHEN cce.chag_datos.determinacion = 'No Reactivo' AND chag_datos.correcto = 'Indeterminado' AND cce.tecnicas.tecnica = "QUIMIOLUMINISCENCIA" THEN 1
                       ELSE 0
                       END) AS falsosquimio,
              SUM(CASE WHEN cce.tecnicas.tecnica = "OTRA" THEN 1
                  ELSE 0
                  END) AS otras,
              SUM(CASE WHEN cce.chag_datos.determinacion = 'No Reactivo' AND chag_datos.correcto = 'Reactivo' AND cce.tecnicas.tecnica = "OTRA" THEN 1
                       WHEN cce.chag_datos.determinacion = 'No Reactivo' AND chag_datos.correcto = 'Indeterminado' AND cce.tecnicas.tecnica = "OTRA" THEN 1
                       ELSE 0
                       END) AS falsosotras
       FROM cce.chag_datos INNER JOIN cce.laboratorios ON cce.chag_datos.laboratorio = cce.laboratorios.id
                           INNER JOIN diccionarios.localidades ON cce.laboratorios.localidad = diccionarios.localidades.codloc
                           INNER JOIN diccionarios.provincias ON diccionarios.localidades.codpcia = diccionarios.provincias.cod_prov
                           INNER JOIN cce.operativos_chagas ON cce.chag_datos.operativo = cce.operativos_chagas.id
                           INNER JOIN cce.tecnicas ON cce.chag_datos.tecnica = cce.tecnicas.id
                           INNER JOIN diccionarios.paises ON diccionarios.paises.id = diccionarios.provincias.pais
       WHERE NOT ISNULL(cce.chag_datos.correcto)
       GROUP BY diccionarios.provincias.nom_prov,
                SUBSTRING(cce.operativos_chagas.operativo_nro, 3)
       ORDER BY diccionarios.provincias.nom_prov;

/*
    Esta vista arroja la nómina de marcas, las determinaciones por cada marca,
    el número de determinaciones correctas por cada marca y la técnica
    correspondiente, luego la filtramos para obtener la eficiencia por marca
*/

-- eliminamos la vista si existe
DROP VIEW IF EXISTS vw_eficiencia_marcas;

-- la recreamos
CREATE ALGORITHM = UNDEFINED
       SQL SECURITY INVOKER
       VIEW vw_eficiencia_marcas AS
       SELECT diccionarios.paises.nombre AS pais,
              SUBSTRING(cce.operativos_chagas.operativo_nro, 3) AS anio,
              cce.tecnicas.tecnica AS tecnica,
              cce.marcas_chagas.MARCA AS marca,
              COUNT(cce.chag_datos.ID) AS determinaciones,
              SUM(CASE WHEN cce.chag_datos.determinacion = cce.chag_datos.correcto THEN 1
                       WHEN cce.chag_datos.determinacion = 'Reactivo' AND chag_datos.correcto = 'Indeterminado' THEN 1
                       WHEN cce.chag_datos.determinacion = 'Indeterminado' AND chag_datos.correcto = 'Reactivo' THEN 1
                       ELSE 0
                       END) AS correctas
       FROM chag_datos INNER JOIN marcas_chagas ON chag_datos.MARCA = marcas_chagas.ID
                       INNER JOIN operativos_chagas ON chag_datos.OPERATIVO = operativos_chagas.ID
                       INNER JOIN tecnicas ON chag_datos.TECNICA = tecnicas.ID
                       INNER JOIN laboratorios ON chag_datos.LABORATORIO = laboratorios.ID
                       INNER JOIN diccionarios.localidades ON laboratorios.LOCALIDAD = diccionarios.localidades.CODLOC
                       INNER JOIN diccionarios.provincias ON diccionarios.localidades.CODPCIA = diccionarios.provincias.COD_PROV
                       INNER JOIN diccionarios.paises ON diccionarios.provincias.PAIS = diccionarios.paises.ID
       GROUP BY SUBSTRING(cce.operativos_chagas.operativo_nro, 3),
                cce.marcas_chagas.MARCA
       ORDER BY cce.marcas_chagas.MARCA;

/*
    Esta vista arroja la nómina de marcas, las determinaciones por cada marca,
    el número de determinaciones correctas por cada marca y la técnica
    correspondiente, agrupada por jurisdicción, luego la filtramos para
    obtener la eficiencia por marca
*/

-- eliminamos la vista si existe
DROP VIEW IF EXISTS vw_efic_marc_prov;

-- la recreamos
CREATE ALGORITHM = UNDEFINED
       SQL SECURITY INVOKER
       VIEW vw_efic_marc_prov AS
       SELECT diccionarios.paises.nombre AS pais,
              diccionarios.provincias.nom_prov AS provincia,
              SUBSTRING(cce.operativos_chagas.operativo_nro, 3) AS anio,
              cce.tecnicas.tecnica AS tecnica,
              cce.marcas_chagas.MARCA AS marca,
              COUNT(cce.chag_datos.ID) AS determinaciones,
              SUM(CASE WHEN cce.chag_datos.determinacion = cce.chag_datos.correcto THEN 1
                       WHEN cce.chag_datos.determinacion = 'Reactivo' AND chag_datos.correcto = 'Indeterminado' THEN 1
                       WHEN cce.chag_datos.determinacion = 'Indeterminado' AND chag_datos.correcto = 'Reactivo' THEN 1
                       ELSE 0
                       END) AS correctas
       FROM chag_datos INNER JOIN marcas_chagas ON chag_datos.MARCA = marcas_chagas.ID
                       INNER JOIN operativos_chagas ON chag_datos.OPERATIVO = operativos_chagas.ID
                       INNER JOIN tecnicas ON chag_datos.TECNICA = tecnicas.ID
                       INNER JOIN laboratorios ON chag_datos.LABORATORIO = laboratorios.ID
                       INNER JOIN diccionarios.localidades ON laboratorios.LOCALIDAD = diccionarios.localidades.CODLOC
                       INNER JOIN diccionarios.provincias ON diccionarios.localidades.CODPCIA = diccionarios.provincias.COD_PROV
                       INNER JOIN diccionarios.paises ON diccionarios.provincias.PAIS = diccionarios.paises.ID
       GROUP BY diccionarios.provincias.nom_prov,
                SUBSTRING(cce.operativos_chagas.operativo_nro, 3),
                cce.marcas_chagas.MARCA
       ORDER BY cce.marcas_chagas.MARCA;

/*
    Esta vista presenta las determinaciones discordantes (es decir la
    nómina de determinaciones en las cuales se aplicò mas de una
    tènica y arrojaron resultados distintos)

*/

-- eliminamos la vista si existe
DROP VIEW IF EXISTS vw_discordantes;

-- la recreamos
CREATE ALGORITHM = UNDEFINED
       SQL SECURITY INVOKER
       VIEW vw_discordantes AS
       SELECT diccionarios.paises.nombre AS pais,
              diccionarios.provincias.nom_prov AS provincia,
              SUBSTRING(cce.operativos_chagas.operativo_nro, 3) AS anio,
              TRUNCATE(COUNT(cce.chag_datos.id)/2,0) AS discordantes
       FROM cce.chag_datos INNER JOIN cce.chag_datos AS espejo ON cce.chag_datos.muestra_nro = espejo.muestra_nro
                           INNER JOIN cce.laboratorios ON cce.chag_datos.laboratorio = cce.laboratorios.id
                           INNER JOIN diccionarios.localidades ON cce.laboratorios.localidad = diccionarios.localidades.codloc
                           INNER JOIN diccionarios.provincias ON diccionarios.localidades.codpcia = diccionarios.provincias.cod_prov
                           INNER JOIN diccionarios.paises ON diccionarios.provincias.pais = diccionarios.paises.id
                           INNER JOIN cce.operativos_chagas ON cce.chag_datos.operativo = cce.operativos_chagas.id
       WHERE cce.chag_datos.id != espejo.id AND
             ((cce.chag_datos.determinacion = 'Reactivo' AND espejo.determinacion = 'No Reactivo') OR
              (cce.chag_datos.determinacion = 'Indeterminado' AND espejo.determinacion = 'No Reactivo') OR
              (cce.chag_datos.determinacion = 'No Reactivo' AND espejo.determinacion = 'Reactivo') OR
              (cce.chag_datos.determinacion = 'No Reactivo' AND espejo.determinacion = 'Indeterminado')) AND
             cce.chag_datos.operativo = espejo.operativo AND
             cce.chag_datos.laboratorio = espejo.laboratorio AND
             NOT ISNULL(chag_datos.correcto)
        GROUP BY SUBSTRING(cce.operativos_chagas.operativo_nro, 3),
                 diccionarios.provincias.nom_prov
        ORDER BY diccionarios.provincias.nom_prov;
