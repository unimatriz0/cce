<?php

/**
 *
 * seguridad/verificar.php
 *
 * @package     CCE
 * @subpackage  Seguridad
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.4.0 (10/08/2018)
 * @copyright   Copyright (c) 2018, INP
 *
 * Procedimiento que simplemente verifica si el usuario inició
 * sesión, en cuyo caso retorna true
 *
*/

// iniciamos las variables
$estado = "";

// iniciamos la sesión
session_start();

// si existe la sesión
if (isset($_SESSION["ID"])){
    $estado = true;
} else {
    $estado = false;
}

// retorna el estado
echo json_encode(array("Error" => $estado));
?>
