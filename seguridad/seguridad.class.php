<?php

/**
 *
 * Class Seguridad | seguridad/seguridad.class.php
 *
 * @package     CCE
 * @subpackage  Valores
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.4.0 (01/03/2018)
 * @copyright   Copyright (c) 2018, INP
 *
 * Clae que contiene los métodos para la gestión de usuarios y
 * contraseñas del sistema
 *
*/

// inclusión de archivos
require_once ("../clases/conexion.class.php");

// convención para la nomenclatura de las propiedades, comienzan con una
// letra mayúscula, de tener mas de una palabra no se utilizan separadores
// y la inicial de cada palabra va en mayúscula
// para las variables recibidas como parámetro el criterio es todas en
// minúscula

// convención para la nomenclatura de los metodos, comienzan con set o get
// según asignen un valor o lo lean y luego el nombre del valor a obtener

/**
 * Definición de la clase
 * @author Claudio Invernizzi <cinvernizzi@gmail.com>
 */
class Seguridad {

    // declaración de variables de la clase, las declaramos
    // como protected para que puedan ser heredadas pero
    // para asignarle el valor debemos crear los métodos
    protected $Link;                  // puntero a la base de datos
    protected $Id;                    // clave del registro
    protected $Usuario;               // nombre del usuario
    protected $Password;              // contraseña de acceso sin encriptar
    protected $NuevoPass;             // nuevo password del usuario
    protected $Jurisdiccion;          // jurisdicción del usuario
    protected $CodProv;               // código indec de la jurisdicción
    protected $Pais;                  // nombre del pais del usuario
    protected $NivelCentral;          // indica si es de nivel central
    protected $Responsable;           // indica si es responsable
    protected $Laboratorio;           // laboratorio autorizado si no es
                                      // responsable
    /**
     * Constructor de la clase, establece la conexión con la base
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     */
    function __construct (){

        // nos conectamos a la base de datos
        $this->Link = new Conexion();

        // inicializamos las variables
        $this->Usuario = "";
        $this->Password = "";
        $this->NuevoPass = "";
        $this->Jurisdiccion = "";
        $this->CodProv = "";
        $this->Pais = "";
        $this->NivelCentral = "";
        $this->Responsable = "";
        $this->Laboratorio = 0;

        // si inició sesión
        session_start();
        if(isset($_SESSION["ID"])){

            // obtenemos la id del usuario
            $this->IdUsuario = $_SESSION["ID"];

        // si no inició
        } else {

            // inicializa la variable
            $this->IdUsuario = 0;

        }

        // cerramos sesión
        session_write_close();


    }

    /**
     * Destructor de la clase, cierra la conexión con la base
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     */
    function __destruct(){

        // elimina el enlace a la base
        $this->Link = null;

    }

    // métodos públicos de asignación de variables
    public function setId($idagente){

        // verificamos que sea un número
        if (!is_numeric($idagente)){

            // presenta el mensaje y abandona
            echo "La clave del usuario debe ser un número";
            exit;

        // si es correcto
        } else {

            // lo asigna
            $this->Id = $idagente;

        }

    }
    public function setUsuario($usuario){
        $this->Usuario = $usuario;
    }
    public function setPassword($password){
        $this->Password = $password;
    }
    public function setNuevoPass($nuevopass){
        $this->NuevoPass = $nuevopass;
    }

    // métodos que retornan los valores
    public function getId(){
        return $this->Id;
    }
    public function getNivelCentral(){
        return $this->NivelCentral;
    }
    public function getResponsable(){
        return $this->Responsable;
    }
    public function getUsuario(){
        return $this->Usuario;
    }
    public function getJurisdiccion(){
        return $this->Jurisdiccion;
    }
    public function getCodProv(){
        return $this->CodProv;
    }
    public function getPais(){
        return $this->Pais;
    }
    public function getLaboratorio(){
        return $this->Laboratorio;
    }
    public function getNuevoPassword(){
        return $this->NuevoPass;
    }

    /**
     * Método que efectúa el cambio de contraseña en la base de datos
     * de usuarios, asume que ya fueron inicializadas las variables
     * de clase, verifica si los valores son correctos y actualiza
     * en la base de datos
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @return boolean
     */
    public function nuevoPassword(){

        // encriptamos el nuevo password usando la función de mysql Y
        // componemos la consulta de actualización
        $consulta = "UPDATE cce.responsables SET
                            cce.responsables.password = MD5('$this->NuevoPass')
                     WHERE cce.responsables.id = '$this->Id';";
        $this->Link->exec($consulta);

        // seteamos el estado de la operación
        return true;

    }

    /**
     * Método que verifica que exista el usuario en la base y que la
     * contraseña de ingreso sea correcta, en cuyo caso retorna
     * verdadero
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @return boolean
     */
    public function Validar(){

        // inicializamos las variables
        $id = 0;
        $usuario = "";
        $password = "";
        $responsable = "";
        $laboratorio = "";
        $provincia = "";
        $cod_provincia = "";
        $pais = "";
        $nivel_central = "";

        // buscamos el usuario en la base controlando que coincida la
        // contraseña encriptada y que esté activo
        $consulta = "SELECT cce.responsables.ID AS id,
                            cce.responsables.USUARIO AS usuario,
                            cce.responsables.PASSWORD AS password,
                            cce.responsables.RESPONSABLE_CHAGAS AS responsable,
                            cce.responsables.LABORATORIO AS laboratorio,
                            diccionarios.provincias.NOM_PROV AS provincia,
                            diccionarios.provincias.COD_PROV AS cod_provincia,
                            diccionarios.paises.NOMBRE AS pais,
                            cce.responsables.NIVEL_CENTRAL AS nivel_central
                     FROM cce.responsables INNER JOIN diccionarios.localidades ON cce.responsables.LOCALIDAD = diccionarios.localidades.CODLOC
                                           INNER JOIN diccionarios.provincias ON diccionarios.localidades.CODPCIA = diccionarios.provincias.COD_PROV
                                           INNER JOIN diccionarios.paises ON cce.responsables.PAIS = diccionarios.paises.ID
                     WHERE cce.responsables.USUARIO = '$this->Usuario' AND
                           cce.responsables.PASSWORD = MD5('$this->Password') AND
                           cce.responsables.ACTIVO = 'Si';";
        $resultado = $this->Link->query($consulta);

        // si encontró registros
        if ($resultado->rowCount() != 0){

            // lo pasamos a minúsculas porque según la versión de
            // pdo lo devuelve en mayúsculas o minúsculas
            $registro = array_change_key_case($resultado->fetch(PDO::FETCH_ASSOC), CASE_LOWER);

            // lo pasamos a variables
            extract($registro);

            // obtenemos el password de la base
            $this->Id = $id;
            $this->Usuario = $usuario;
            $this->Responsable = $responsable;
            $this->Laboratorio = $laboratorio;
            $this->Jurisdiccion = $provincia;
            $this->CodProv = $cod_provincia;
            $this->Pais = $pais;
            $this->NivelCentral = $nivel_central;

            // inicia la sesión
            session_start();

            // almacena la información
            $_SESSION["ID"] = $this->Id;
            $_SESSION["Usuario"] = $this->Usuario;
            $_SESSION["Responsable"] = $this->Responsable;
            $_SESSION["Laboratorio"] = $this->Laboratorio;
            $_SESSION["Jurisdiccion"] = $this->Jurisdiccion;
            $_SESSION["CodProv"] = $this->CodProv;
            $_SESSION["Pais"] = $this->Pais;
            $_SESSION["NivelCentral"] = $this->NivelCentral;

            // cerramos la sesión
            session_write_close();

            // retornamos verdadero
            return true;

        // si no encontró al usuario
        } else {

            // retorna falso
            return false;

        }

    }

    /**
     * Método que verifica si existe un nombre de usuario en la base de
     * datos en cuyo caso retorna el número de registros encontrados
     * utilizado para evitar el ingreso duplicado
     * @param string $nombreusuario - el nombre de usuario
     * @return int
     */
    public function VerificaUsuario($nombreusuario){

        // inicializamos las variables
        $registros = 0;

        // primero verifica que el nombre de usuario no se encuentre
        // repetido en la base de usuarios
        $consulta = "SELECT COUNT(*) AS registros
                     FROM cce.responsables
                     WHERE cce.responsables.USUARIO = '$nombreusuario'; ";
        $resultado = $this->Link->query($consulta);
        $fila = array_change_key_case($resultado->fetch(PDO::FETCH_ASOC), CASE_LOWER);
        extract($fila);

        // retornamos
        return $registros;

    }

}