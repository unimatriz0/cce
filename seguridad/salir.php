<?php

/**
 *
 * seguridad/salir.php
 *
 * @package     CCE
 * @subpackage  Seguridad
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.4.0 (10/08/2018)
 * @copyright   Copyright (c) 2018, INP
 *
 * Procedimiento que elimina las variables de sesión
 *
*/

// continuamos con la sesión
session_start();

// destruye la sesión
session_destroy();

// retorna siempre verdadero
echo json_encode(array("Error" => true));
?>
