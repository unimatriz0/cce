<?php

/*

    Archivo: generar_pass.php
    Autor: Claudio Invernizzi
    Fecha: 12/06/2017
    E-Mail: cinvernizzi@gmail.com
    Proyecto: CCE
    Licencia: GPL
    Comentarios: Método que recibe como parámetro una dirección de
                 correo, verifica que esta exista en la base y
                 reinicia la contraseña de ingreso del usuario
                 enviando un mail con información para el ingreso

*/

// incluimos e instanciamos las clases
require_once ("../responsables/responsables.class.php");
require_once ("seguridad.class.php");
$responsable = new Responsables();
$usuario = new Seguridad();

// inclusión de las clases de correo
require_once ("../correos/plantillacorreo.class.php");

// verificamos si existe el mail
if ($responsable->verificaMail($_GET["mail"])){

    // obtenemos los datos del usuario
    $id_responsable = $responsable->getIdMail($_GET["mail"]);
    $responsable->setIdResponsable($id_responsable);
    $responsable->getResponsable();

    // si está activo
    if ($responsable->getActivo() == "Si"){

        // actualizamos la contraseña
        $usuario->setId($id_responsable);
        $usuario->setNuevoPass($responsable->randomString());
        $usuario->nuevoPassword();

        // definimos el texto a enviar
        $mensaje = "<h2>Recuperación de Contraseña</h2>";
        $mensaje .= "<p>Estimado/a " . $responsable->getNombre() . "<br>";
        $mensaje .= $responsable->getInstitucion() . "</p>";
        $mensaje .= "<p align='justify'>";
        $mensaje .= "Recientemente usted ha solicitado la recuperación de la contraseña ";
        $mensaje .= "de acceso al Sistema de Control de Calidad de Laboratorios de ";
        $mensaje .= "Chagas, a continuación, le comunicamos que los datos de conexión ";
        $mensaje .= "son los siguientes:</p>";
        $mensaje .= "<p>Usuario: " . $responsable->getUsuario() . "<br>";
        $mensaje .= "Contraseña: " . $responsable->getPassword() . "</p>";
        $mensaje .= "<p align='justify'>";
        $mensaje .= "Al ingresar podrá actualizar la contraseña, por una solo conocida ";
        $mensaje .= "por usted.</p>";
        $mensaje .= "<p align='justify'>";
        $mensaje .= "Recuerde que la web de Control de Calidad Externo se encuentra ";
        $mensaje .= "disponible en <a href: http://fatalachaben.info.tm/calidad/>";
        $mensaje .= "http://fatalachaben.info.tm/calidad</a>.<br></p>";
        $mensaje .= "<p align='justify'>";
        $mensaje .= "Una vez que ingrese, le recomendamos que actualice la contraseña ";
        $mensaje .= "por una mas sencilla de recordar y solo conocida por usted, para ";
        $mensaje .= "ello, desde la pantalla inicial del sistema deberá presionar en ";
        $mensaje .= "menú lateral la opción <b>Cambiar Password</b>, el sistema ";
        $mensaje .= "le solicitará que ingrese la nueva contraseña y la repita por ";
        $mensaje .= "motivos de seguridad y posteriormente la actualizará.</p>";
        $mensaje .= "<p align='justify'>";
        $mensaje .= "No olvide que ante cualquier inconveniente puede comunicarse ";
        $mensaje .= "a <a mailto: cinvernizzi@gmail.com>cinvernizzi@gmail.com</a> o ";
        $mensaje .= "<a mailto: diagnostico.inp.anlis@gmail.com>diagnostico.inp.anlis@gmail.com</a> ";
        $mensaje .= "donde intentaremos resolver sus dificultades. </p>";

        // instanciamos el objeto
        $mail = new Plantilla();

        // destinatario del mensaje
        $mail->addAddress($_GET["mail"], $responsable->getNombre());

        // cargamos el contenido del texto
        $mail->setMensaje($mensaje);

        // enviamos el mensaje
        $mail->send();

        // fijamos el error
        $error = 1;

    // si el responsable no está activo
    } else {

        // fijamos el error
        $error = 0;

    }

// si no existe
} else {

    // fijamos el error
    $error = 0;

}

// retornamos el resultado de la operación
echo json_encode(array("Error" => $error));
?>