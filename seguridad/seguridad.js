/*
 * Nombre: cce.js
 * Autor: Lic. Claudio Invernizzi
 * E-Mail: cinvernizzi@gmail.com
 * Fecha: 15/08/2018
 * Licencia: GPL
 * Comentarios: Clase con métodos que controlan el acceso de los
 *              usuarios al sistema
 */

 /**
 *
 * Atención, utilizamos el nuevo estandard definido en ECMAScript 2015 (ES6)
 * este permite definir clases con el mètodo ya utilizado en php y java
 * como creo que es mas claro lo utilizaremos, la compatibilidad está
 * anunciada para firefox y chrome así que no deberìa haber problemas
 *
 */

/* jshint esversion: 6 */

/**
 * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @class Clase que controla el acceso dde los usuarios
 */
class Seguridad {

    /**
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * @constructor
     */
    constructor(){

        // inicializamos las variables
        this.initSeguridad();

    }

    /**
     * Método que inicializa las variables de clase
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     */
    initSeguridad(){

        // inicializamos las variables
        this.layerIngreso = "";            // layer de los diálogos emergentes
        this.layerCorreo = "";             // layer de recuperación de contraseña

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Propósito: destruye la sesión y pide contraseña
     */
    Salir(){

        // primero eliminamos la sesión javascript
        sessionStorage.removeItem("ID");
        sessionStorage.removeItem("Responsable");
        sessionStorage.removeItem("NivelCentral");
        sessionStorage.removeItem("Laboratorio");
        sessionStorage.removeItem("Jurisdiccion");
        sessionStorage.removeItem("CodProv");
        sessionStorage.removeItem("Usuario");

        // llama la rutina php de elimación de la sesión, llamamos
        // así para que ejecute en forma sincrónica
        $.get('seguridad/salir.php',
            function(data){

                // si retornó error (que siempre retorna correcto)
                if (data.Error == true){

                    // llama la rutina de verificación de usuario
                    seguridad.verificarIngreso();

                }

            }, "json");

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método llamado luego de iniciar la sesión, obtiene los valores de las
     * variables globales sessionstorage y muestra u oculta los menúes de
     * acuerdo al acceso de seguridad del usuario
     */
    fijarAcceso(){

        // reiniciamos el contador
        cce.contador();

        // si el usuario no es de nivel central
        if (sessionStorage.getItem("NivelCentral") != "Si"){

            // ocultamos el panel de administración
            $("#botonadministracion").hide();

            // ocultamos el alta de responsables
            $("#menuResponsables").hide();

            // ocultamos el reporte de etiquetas
            $("#reportes_etiquetas").hide();

            // ocultamos la generación de reportes
            $("#reportes_nacionales").hide();

            // ocultamos la georreferenciación de laboratorios
            $("#geoLaboratorios").hide();

            // ocultamos la georreferenciación de responsables
            $("#geoResponsables").hide();

        // si es de nivel central
        } else {

            // lo mostramos
            $("#botondministracion").show();

            // mostramos el alta de responsables
            $("#menuResponsables").show();

            // mostramos el reporte de etiquetas
            $("#reportes_etiquetas").show();

            // mostramos la generación de reportes
            $("#reportes_nacionales").show();

            // mostramos la georreferenciación de laboratorios
            $("#geoLaboratorios").show();

            // mostramos la georreferenciación de responsables
            $("#geoResponsables").show();

        }

        // si es responsable o es de nivel central
        if (sessionStorage.getItem("Responsable") == "Si" || sessionStorage.getItem("NivelCentral") == "Si"){

            // muestra los reportes
            $("#reportes_responsables").show();

            // mostramos el alta de laboratorios
            $("#menuLaboratorios").show();

        // si es de un laboratorio
        } else {

            // oculta los reportes
            $("#reportes_responsables").hide();

            // oculta el alta de laboratorios
            $("#menuLaboratorios").hide();

        }

        // por defecto ocultamos el reporte de concordancia de los
        // laboratorios y lo mostramos u ocultamos al cargar el
        // laboratorio
        $("#concordancialaboratorio").hide();

        // carga el usuario actual
        responsables.getDatosUsuario(sessionStorage.getItem("ID"));

    }

    /**
     * Propósito: Muestra el formulario de ingreso
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     */
    formIngreso(){

        // pedimos se autentique
        this.layerIngreso = new jBox('Modal', {
                                animation: 'flip',
                                closeOnEsc: false,
                                closeOnClick: false,
                                closeOnMouseleave: false,
                                closeButton: false,
                                repositionOnContent: true,
                                overlay: true,
                                minWidth: 400,
                                theme: 'TooltipBorder',
                                ajax: {
                                    url: 'seguridad/form_ingreso.html',
                                reload: 'strict'
                            }
                        });
        this.layerIngreso.open();

    }

    /**
     * Llama la rutina php que verifica si el usuario inició
     * sesión, caso contrario llama la rutina de ingreso
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     */
    verificarIngreso(){

        // llama la rutina php
        $.get('seguridad/verificar.php',
            function(data){

                // si retornó error
                if (data.Error !== true){

                    // pide el usuario y contraseña
                    seguridad.formIngreso();

                // si ya inición sesión
                } else {

                    // reiniciamos el contador
                    cce.contador();

                    // fijamos el nivel de acceso
                    seguridad.fijarAcceso();

                }

            }, "json");

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * función llamada al pulsar en el formulario de ingreso
     * verifica los datos del formulario y llama por ajax
     * la rutina php que confirma las credenciales e instancia
     * la sesión
     */
    validarIngreso(){

        // definición de variables
        var mensaje;

        // obtenemos los valores del formulario
        var usuario = document.form_ingreso.usuario.value;
        var password = document.form_ingreso.password.value;

        // si no ingresó el usuario
        if (usuario === "" || usuario.length === 0){

            // presenta el mensaje y retorna
            mensaje = "Ingrese su nombre de usuario";
            new jBox('Notice', {content: mensaje, color: 'red'});
            document.form_ingreso.usuario.focus();
            return false;

        }

        // si no ingresó el password
        if (password === "" || password.length === 0){

            // presenta el mensaje y retorna
            mensaje = "Indique su contraseña de ingreso";
            new jBox('Notice', {content: mensaje, color: 'red'});
            document.form_ingreso.password.focus();
            return false;

        }

        // ahora enviamos por get los datos
        $.get('seguridad/validar.php','usuario='+usuario+'&password='+password,
            function(data){

                // si retornó error
                if (data.Error == false){

                    // presenta el mensaje de error
                    mensaje = "El nombre de usuario o la contraseña\n";
                    mensaje += "son incorrectas. Por favor verifique.";
                    new jBox('Notice', {content: mensaje, color: 'red'});
                    document.form_ingreso.usuario.focus();
                    return false;

                // si está autenticado
                } else {

                    // usamos la propiedad localstorage de HTML5 para
                    // almacenar las variables de sesión
                    sessionStorage.setItem("ID", data.ID);
                    sessionStorage.setItem("Responsable", data.Responsable);
                    sessionStorage.setItem("NivelCentral", data.NivelCentral);
                    sessionStorage.setItem("Laboratorio", data.Laboratorio);
                    sessionStorage.setItem("Jurisdiccion", data.Jurisdiccion);
                    sessionStorage.setItem("CodProv", data.CodProv);
                    sessionStorage.setItem("Usuario", data.Usuario);

                    // reiniciamos el contador
                    cce.contador();

                    // oculta el formulario y carga los datos
                    seguridad.layerIngreso.destroy();

                    // fijamos el nivel de acceso
                    seguridad.fijarAcceso();

                }

            }, "json");

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Propósito: presenta el formulario de cambio de password
     */
    nuevoPassword(){

        // reiniciamos el contador
        cce.contador();

        // abrimos el formulario
        this.layerIngreso = new jBox('Modal', {
                                animation: 'flip',
                                closeOnEsc: true,
                                closeOnClick: false,
                                closeOnMouseleave: false,
                                closeButton: true,
                                overlay: false,
                                onCloseComplete: function(){
                                    this.destroy();
                                },
                                title: 'Nueva Contraseña',
                                draggable: 'title',
                                repositionOnContent: true,
                                theme: 'TooltipBorder',
                                width: 300,
                                ajax: {
                                    url: 'seguridad/form_password.html',
                                    reload: 'strict'
                                }
                            });
        this.layerIngreso.open();

    }

    /**
     * Verifica los datos del formulario de nuevo password y
     * llama la rutina php para actualizar la contraseña
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     */
    cambiaPassword(){

        // reiniciamos el contador
        cce.contador();

        // declaración de variables
        var mensaje;
        var password_actual = document.form_password.passwordactual.value;
        var nuevo_password = document.form_password.passwordnuevo.value;

        // verifica que halla ingresado la contraseña actual
        if (password_actual === ""){

            // presenta el mensaje y retorna
            mensaje = "Ingrese su contraseña actual";
            new jBox('Notice', {content: mensaje, color: 'red'});
            document.form_password.passwordactual.focus();
            return false;

        }

        // verifica que halla ingresado la nueva contraseña
        if (nuevo_password == ""){

            // presenta el mensaje y retorna
            mensaje = "Ingrese su nueva contraseña";
            new jBox('Notice', {content: mensaje, color: 'red'});
            document.form_password.passwordnuevo.focus();
            return false;

        }

        // verifica que halla ingresado la repetición
        if (document.form_password.verificapassword.value == ""){

            // presenta el mensaje y retorna
            mensaje = "Repita su nueva contraseña para verificación";
            new jBox('Notice', {content: mensaje, color: 'red'});
            document.form_password.verificapassword.focus();
            return false;

        }

        // verifica que la nueva contraseña y la repetición coincidan
        if (document.form_password.verificapassword.value != nuevo_password){

            // presenta el mensaje y retorna
            mensaje = "La nueva contraseña y su repetición no<br> ";
            mensaje += "coinciden, por favor verifique.";
            new jBox('Notice', {content: mensaje, color: 'red'});
            document.form_password.nuevopassword.focus();
            return false;

        }

        // envía el formulario por get
        $.get('seguridad/nuevo_pass.php', 'password='+password_actual+'&nuevo_password='+nuevo_password);

        // presenta el mensaje y cierra el formulario
        mensaje = "Contraseña actualizada, se ha enviado<br>";
        mensaje += "un correo con los datos de ingreso";
        new jBox('Notice', {content: mensaje, color: 'green'});
        this.layerIngreso.destroy();

    }

    /**
     * Nombre: recuperaUsuario
     * Método que abre un cuadro de diálogo para que el usuario ingrese su
     * correo electrónico y el sistema genere una nueva contraseña
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     */
    recuperaUsuario(){

        // abrimos el formulario
        this.layerCorreo = new jBox('Modal', {
                            animation: 'flip',
                            closeOnEsc: true,
                            closeOnClick: false,
                            closeOnMouseleave: false,
                            closeButton: true,
                            overlay: false,
                            onCloseComplete: function(){
                                this.destroy();
                            },
                            repositionOnContent: true,
                            title: 'Recuperar Usuario',
                            theme: 'TooltipBorder',
                            ajax: {
                                url: 'seguridad/recuperar.html',
                                reload: 'strict'
                            }
                        });
        this.layerCorreo.open();

    }

    /**
     * Nombre: generarPassword
     * Propósito: Método llamado en el evento onclick del formulario de recuperación de
     * ingreso, verifica que el correo sea válido y luego por ajax genera la nueva
     * contraseña de ingreso, presenta el mensaje con el resultado
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     */
    generarPassword(){

        // declaración de variables
        var mail = document.getElementById("recupera_mail").value;
        var mensaje;

        // desactiva el botón para evitar el reenvío
        document.getElementById("btnRecupera").disabled = true;

        // si no ingresó el correo
        if (mail == ""){

            // presenta el mensaje y retorna
            mensaje = "Indique una dirección de correo";
            new jBox('Notice', {content: mensaje, color: 'red'});
            document.getElementById("recupera_mail").focus();
            return false;

        // si no es válido
        } else if (!echeck(mail)){

            // presenta el mensaje y retorna
            mensaje = "El mail parece ser incorrecto. Verifique";
            new jBox('Notice', {content: mensaje, color: 'red'});
            document.getElementById("recupera_mail").focus();
            return false;

        }

        // ahora enviamos por ajax el mail (en forma sincrónica)
        // llama la rutina php en forma sincrónica
        $.ajax({
                url: 'seguridad/generar_pass.php?mail='+mail,
                type: "GET",
                cahe: false,
                contentType: false,
                processData: true,
                dataType: 'json',
                async: false,
                success: function(data) {

                    // si hubo un error
                    if (data.Error == 0){

                        // presenta el mensaje y setea el foco
                        mensaje = "El mail ingresado no se encuentra en\n";
                        mensaje += "la base de datos. Verifique por favor";
                        new jBox('Notice', {content: mensaje, color: 'red'});
                        document.getElementById("recupera_mail").focus();
                        return false;

                    // si pudo actualizar y enviar el correo
                    } else {

                        // presenta el mensaje y cierra el layer
                        mensaje = "Se ha enviado un mail a " + mail + "\n";
                        mensaje += "con información para su ingreso.";
                        new jBox('Notice', {content: mensaje, color: 'green'});
                        seguridad.layerCorreo.destroy();

                    }

            }});

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que muestra u oculta el texto de la contraseña
     * de ingreso
     */
    mostrarPassword(){

        // obtenemos el puntero al elemento
        var x = document.getElementById("password");

        // alterna el tipo de elemento
        if (x.type === "password") {
            x.type = "text";
        } else {
            x.type = "password";
        }

    }

}