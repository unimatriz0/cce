<?php

/**
 *
 * Class Valores | valores/valores.class.php
 *
 * @package     CCE
 * @subpackage  Valores
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.4.0 (01/03/2018)
 * @copyright   Copyright (c) 2018, INP
 *
 * Método que recibe por get el valor de una técnica y la clave
 * de la misma, verifica que no se encuentre repetido y retorna
 * el número de registros encontrados
 *
*/

// incluimos e instanciamos la clase
require_once ("valores.class.php");
$valor = new Valores();

// verificamos si existe el valor
$registros = $valor->verificaValor($_GET["tecnica"], $_GET["valor"]);

// retornamos el resultado de la operación
echo json_encode(array("Registros" => $registros));

?>