<?php

/**
 *
 * valores/nomina_valores.php
 *
 * @package     CCE
 * @subpackage  Valores
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.4.0 (01/03/2018)
 * @copyright   Copyright (c) 2018, INP
 *
 * Método que recibe por get la clave de una técnica y arma el formulario
 * para el abm de los valores correspondientes a esa técnica
 *
*/

// incluimos e instanciamos las clases
require_once ("valores.class.php");
$valores = new Valores();

// obtenemos la nómina
$nomina = $valores->nominaValores($_GET["tecnica"]);

// definimos la tabla
echo "<table width='90%' align='center' border='0'>";

// definimos el encabezado
echo "<thead>";
echo "<tr>";
echo "<th>Valor</th>";
echo "<th>Alta</th>";
echo "<th>Usuario</th>";
echo "<th></th>";
echo "<thead>";

// definimos el cuerpo
echo "<tbody>";

// recorremos el vector
foreach($nomina AS $registro){

    // obtenemos el registro
    extract($registro);

    // presentamos el registro
    echo "<tr>";
    echo "<td>";
    echo "<span class='tooltip'
           title='Valor aceptado'>";
    echo "<input type='text'
           name='valor'
           id='valor_$id_valor'
           size='15'
           value='$valor_tecnica'>";
    echo "</span>";
    echo "</td>";

    // presentamos oculta la clave de la técnica
    echo "<input type='hidden'
           name='tecnica'
           id='tecnica_$id_valor'
           value='$id_tecnica'>";

    // presentamos la fecha y el usuario
    echo "<td align='center'>$fecha_alta</td>";
    echo "<td align='center'>$usuario</td>";

    // agregamos el enlace para grabar
    echo "<td align='center'>";
    echo "<input type='button' name='btnGrabaValor'
           id='btnGrabaValor'
           onClick='valores.verificaValor($id_valor)'
           class='botongrabar'>";
    echo "</td>";

    // cerramos la fila
    echo "</tr>";

}

// agregamos una fila para insertar un nuevo registro
echo "<tr>";

    // presentamos el registro
    echo "<td align='center'>";
    echo "<span class='tooltip'
           title='Valor Aceptado'>";
    echo "<input type='text'
           name='valor'
           id='valor_nueva'
           size='15'>";
    echo "</span>";
    echo "</td>";

    // fecha de alta y usuario simplemente lo presentamos
    echo "<td align='center'>";
    echo "<input type='text'
                 name='alta_valor'
                 id='alta_valor'
                 size='10'
                 readonly>";
    echo "</td>";
    echo "<td align='center'>";
    echo "<input type='text'
                 name='usuario_valor'
                 id='usuario_valor'
                 size='10'
                 readonly>";
    echo "</td>";

    // agregamos el enlace para grabar
    echo "<td align='center'>";
    echo "<input type='button'
           name='btnNuevoValor'
           id='btnNuevoValor'
           onClick='valores.verificaValor()'
           class='botonagregar'>";
    echo "</td>";
echo "</tr>";

// cerramos la tabla
echo "</tbody>";
echo "</table>";

?>
<script>

    // instanciamos los tooltips
    new jBox('Tooltip', {
      attach: '.tooltip',
      theme: 'TooltipBorder'
    });

    // fijamos la fecha de alta y el usuario
    document.getElementById("alta_valor").value = fechaActual();
    document.getElementById("usuario_valor").value = sessionStorage.getItem("Usuario");

</script>