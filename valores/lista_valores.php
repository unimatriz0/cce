<?php

/**
 *
 * valores/lista_valores.php
 *
 * @package     CCE
 * @subpackage  Valores
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.4.0 (07/01/2018)
 * @copyright   Copyright (c) 2017, INP
 *
 * Método que recibe por get la clave de una técnica y retorna
 * un array json con los valores aprobados
 *
*/

// incluimos e instanciamos las clases
require_once("valores.class.php");
$valores = new Valores();

// obtenemos la nómina
$nomina = $valores->nominaValores($_GET["tecnica"]);

// declaramos la matriz
$jsondata = array();

// recorremos el vector
foreach($nomina AS $registro){

    // obtenemos el registro
    extract($registro);

    // lo agregamos a la matriz
    $jsondata[] = array("Id" => $id_valor,
                        "Valor" => $valor_tecnica);

}

// retornamos el vector
echo json_encode($jsondata);

?>