<?php

/**
 *
 * valores/graba_valor.php
 *
 * @package     CCE
 * @subpackage  Valores
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.4.0 (01/03/2018)
 * @copyright   Copyright (c) 2018, INP
 *
 * Método que recibe por post los datos del formulario y ejecuta
 * la consulta en la base de datos, retorna la clave del registro
 * afectado
 *
*/

// inclusión de clases e instanciación
require_once ("valores.class.php");
$valor = new Valores();

// si recibió la id del registro
if (!empty($_POST["Id"])){

    // asigna en la clase
    $valor->setIdValor($_POST["Id"]);

}

// asignamos el resto de los valores
$valor->setIdTecnicaValor($_POST["Tecnica"]);
$valor->setValor($_POST["Valor"]);

// grabamos el registro
$id = $valor->grabaValores();

// retornamos el resultado de la operación
echo json_encode(array("Id" => $id));

?>