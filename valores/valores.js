/*
 * Nombre: valores.js
 * Autor: Lic. Claudio Invernizzi
 * E-Mail: cinvernizzi@gmail.com
 * Fecha: 06/01/2017
 * Licencia: GPL
 * Comentarios: Clase utilizados por el sistema de control de calidad
 *              en los formularios de abm de valores de las técnicas
 */


 /**
 *
 * Atención, utilizamos el nuevo estandard definido en ECMAScript 2015 (ES6)
 * este permite definir clases con el mètodo ya utilizado en php y java
 * como creo que es mas claro lo utilizaremos, la compatibilidad está
 * anunciada para firefox y chrome así que no deberìa haber problemas
 *
 */

/* jshint esversion: 6 */

/**
 * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @class Clase que controla las operaciones de valores
 */
class Valores {

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @constructor
     */
    constructor(){

        // inicializamos las variables de clase
        this.initValores();

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que inicializa las variables de clase
     */
    initValores(){

        // inicializamos las variables
        this.IdValor = 0;             // clave del registro
        this.IdTecnica = 0;           // clave de la técnica
        this.Valor = "";              // valor declarado
        this.Correcto = true;         // switch de valor repetido

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que carga el formulario a dos columnas con la
     * nómina de valores
     */
    formValores(){

        // reiniciamos el contador
        cce.contador();

        // cargamos el formulario de marcas
        $("#form_administracion").load("valores/form_valores.html");

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método llamado en el onchange del combo de técnicas, obtiene
     * la clave de la técnica y se la pasa por ajax a la rutina
     * php para cargar la nómina en la segunda columna
     */
    listaValores(){

        // reiniciamos el contador
        cce.contador();

        // obtenemos la clave
        var idtecnica = document.nomina_tecnicas.lista_tecnicas.value;

        // si seleccionó el primer elemento
        if (idtecnica == 0){

            // simplemente retorna
            return false;

        }

        // cargamos la nómina
        $("#columna2").load("valores/nomina_valores.php?tecnica="+idtecnica);

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método llamado por callback que verifica no se encuentre repetido
     * un valor
     */
    validaValor(valorCorrecto){

        // inicialización de variables
        this.Correcto = false;

        // llamamos la rutina php
        $.ajax({
            url: 'valores/verifica_valor.php?valor='+this.Valor+'&tecnica='+this.IdTecnica,
            type: "GET",
            cahe: false,
            contentType: false,
            processData: true,
            dataType: 'json',
            async: false,
            success: function(data) {

                // si retornó verdadero
                if (data.Registros == 0){
                    valores.Correcto = true;
                } else {
                    valores.Correcto = false;
                }

            }});

        // retornamos
        valorCorrecto(this.Correcto);

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param {int} idvalor - clave del registro
     * Método que verifica el formulario antes de enviarlo
     * al servidor, si no recibe la clave del registro
     * asume que se trata de un alta
     */
    verificaValor(idvalor){

        // declaración de variables
        var mensaje;

        // si está dando un alta
        if (typeof(idvalor) == "undefined"){
            idvalor = "nueva";
        } else {
            this.IdValor = idvalor;
        }

        // obtenemos la clave de la técnica del formulario lateral
        this.IdTecnica = document.getElementById("lista_tecnicas").value;

        // verifica se halla declarado nombre
        if (document.getElementById("valor_" + idvalor).value == ""){

            // presenta el mensaje y retorna
            mensaje = "Debe ingresar el valor aceptado o en <br>";
            mensaje += "su defecto la máscara de entrada";
            new jBox('Notice', {content: mensaje, color: 'red'});
            document.getElementById("valor_" + id).focus();
            return false;

        // si declaró
        } else {

            // asigna en la variable de clase
            this.Valor = document.getElementById("valor_" + idvalor).value;

        }

        // si está dando un alta
        if (this.IdValor == 0){

            // verificamos por callback
            this.validaValor(function(valorCorrecto){

                // si está repetido
                if (!valores.Correcto){

                    // presenta el mensaje
                    mensaje = "Ese valor ya se encuentra declarado !!!";
                    new jBox('Notice', {content: mensaje, color: 'red'});

                }

            });

        }

        // grabamos el registro
        this.grabaValor();

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que verifica se halla ingresado el valor y luego la
     * envía por ajax al servidor
     */
    grabaValor(){

        // si está repetido
        if (!this.Correcto){
            return false;
        }

        // reiniciamos el contador
        cce.contador();

        // implementa el formulario usando objetos html5
        var formulario = new FormData();

        // si está editando
        if (this.IdValor != 0){
            formulario.append("Id", this.IdValor);
        }

        // agrega la marca y la técnica
        formulario.append("Valor", this.Valor);
        formulario.append("Tecnica", this.IdTecnica);

        // serializa el formulario y lo envía, recibe
        // como respuesta el resultado de la operación
        $.ajax({
            url: "valores/graba_valor.php",
            type: "POST",
            data: formulario,
            cahe: false,
            contentType: false,
            processData: false,
            success: function(data) {

                // si salío todo bien
                if (data.error != 0){

                    // presenta el mensaje
                    new jBox('Notice', {content: "Registro grabado ... ", color: 'green'});

                    // reiniciamos las variables de clase
                    valores.initValores();

                    // recarga el formulario para reflejar los cambios
                    valores.listaValores();

                // si hubo algún error
                } else {

                    // presenta la alerta
                    new jBox('Notice', {content: "Ha ocurrido un error, intente nuevamente", color: 'red'});

                }

            },

        });

    }

   /**
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param {string} idelemento - id de un objeto html
     * @param {int} idtecnica - clave de la técnica
     * @param {int} idvalor - clave a predeterminar
     * Método que recibe como parámetro la id de un select de un formulario
     * carga en ese elemento la nómina de valores, si además recibe
     * la clave de un elemento, lo predetermina
     */
    nominaValores(idelemento, idtecnica, idvalor){

        // obtenemos la nómina
        $.ajax({
            url: 'valores/lista_valores.php?tecnica='+idtecnica,
            type: "GET",
            cahe: false,
            contentType: false,
            processData: false,
            dataType: 'json',
            success: function(data) {

                // si hay una matriz
                if (data.length != 1){

                    // agrega el primer elemento en blanco
                    $("#" + idelemento).append("<option value='0'>Seleccione</option>");

                    // recorre el vector de resultados
                    for(var i=0; i < data.length; i++){

                        // si recibió la clave
                        if (typeof(idvalor) != "undefined"){

                            // si coindicen
                            if (idvalor == data[i].Id){

                                // lo agrega preseleccionado
                                $("#" + idelemento).append("<option selected value='" + data[i].Id + "'>" + data[i].Valor + "</option>");

                            // si no coincide
                            } else {

                                // simplemente lo agrega
                                $("#" + idelemento).append("<option value='" + data[i].Id + "'>" + data[i].Valor + "</option>");

                            }

                        // si no recibió
                        } else {

                            // simplemente lo agrega
                            $("#" + idelemento).append("<option value='" + data[i].Id + "'>" + data[i].Valor + "</option>");

                        }

                    }

                // si hay un solo valor
                } else {

                    // asumimos que se trata de una máscara
                    $("#" + idelemento).mask(data[0].Valor);

                }

            }});

    }

}