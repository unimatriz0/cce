# !/bin/bash

# script que genera la documentación del proyecto cce
# usamos el jsdoc de node que instalamos mediante
# npm install -g jsdoc (como root)
# el phpdocumentor se encuentra en
# /home/iceman/Aplicaciones/phpDocumentor
# al mismo tiempo las fuentes del CCE las busca
# en /home/iceman/Dropbox/HTML/CCE3

# primero generamos los documentos javascript recorremos recursivamente
# el árbol y generamos la documentación en una carpeta externa
jsdoc /home/iceman/Dropbox/HTML/CCE3/ -r -d /home/iceman/Documentos/CCE/Js

# ahora generamos la documentación php, asumimos que se encuentra
# en /home/iceman/Aplicaciones/phpDocumentor
/home/iceman/Aplicaciones/phpDocumentor/bin/phpdoc.php run -d /home/iceman/Dropbox/HTML/CCE3 -t /home/iceman/Documentos/CCE/php
