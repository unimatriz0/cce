<?php

/**
 *
 * Class Operativos | operativos.class.php
 *
 * @package     CCE
 * @subpackage  Operativos
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.4.0 (15/08/2018)
 * @copyright   Copyright (c) 2018, INP
 *
 * Clase con las operaciones sobre la tabla de operativos
 *
*/

// inclusión de archivos
require_once ("../clases/conexion.class.php");

// convención para la nomenclatura de las propiedades, comienzan con una
// letra mayúscula, de tener mas de una palabra no se utilizan separadores
// y la inicial de cada palabra va en mayúscula
// para las variables recibidas como parámetro el criterio es todas en
// minúscula

// convención para la nomenclatura de los metodos, comienzan con set o get
// según asignen un valor o lo lean y luego el nombre del valor a obtener

/**
 * Definición de la clase
 * @author Claudio Invernizzi <cinvernizzi@gmail.com>
 */
class Operativos {

    // declaración de variables de la clase, las declaramos
    // como protected para que puedan ser heredadas pero
    // para asignarle el valor debemos crear los métodos

    // definición de variables
    protected $Link;                       // puntero a la base de datos

    // declaración de variables de la base de datos
    protected $IdOperativo;                // clave del registro
    protected $OperativoNro;               // número de operativo
    protected $Abierto;                    // indica si está abierto o no
    protected $FechaAlta;                  // fecha de alta del registro
    protected $FechaInicio;                // fecha de inicio del operativo
    protected $FechaFin;                   // finalización del operativo
    protected $Reporte;                    // contenido del reporte
    protected $IdUsuario;                  // clave del usuario
    protected $Usuario;                    // nombre del usuario

    /**
     * Constructor de la clase, establece la conexión con la base
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     */
    function __construct (){

        // nos conectamos a la base de datos
        $this->Link = new Conexion();

        // inicializamos las variables
        $this->IdOperativo = 0;
        $this->OperativoNro = "";
        $this->Abierto = "No";
        $this->FechaInicio = "";
        $this->FechaFin = "";
        $this->Reporte = "";
        $this->IdUsuario = 0;

        // iniciamos sesión
        session_start();

        // si existe la sesión
        if(isset($_SESSION["ID"])){

            // obtenemos la id del usuario
            $this->IdUsuario = $_SESSION["ID"];

        }

        // cerramos sesión
        session_write_close();

    }

    /**
     * Destructor de la clase, cierra la conexión con la base
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     */
    function __destruct(){

        // elimina el enlace a la base
        $this->Link = null;

    }

    // métodos de asignación
    public function setIdOperativo($idoperativo){

        // verifica si es un número
        if (!is_numeric($idoperativo)){

            // abandona por error
            echo "La clave del operativo debe ser un número";
            exit;

        // si está correcto
        } else {

            // lo asigna
            $this->IdOperativo = $idoperativo;

        }

    }
    public function setOperativoNro($operativo){
        $this->OperativoNro = $operativo;
    }
    public function setFechaInicio($fechainicio){
        $this->FechaInicio = $fechainicio;
    }
    public function setFechaFin($fechafin){
        $this->FechaFin = $fechafin;
    }
    public function setAbierto($abierto){
        $this->Abierto = $abierto;
    }

    // métodos de retorno de valores
    public function getIdOperativo(){
        return $this->IdOperativo;
    }
    public function getOperativo(){
        return $this->OperativoNro;
    }
    public function getAbierto(){
        return $this->Abierto;
    }
    public function getFechaAlta(){
        return $this->FechaAlta;
    }
    public function getFechaInicio(){
        return $this->FechaInicio;
    }
    public function getFechaFin(){
        return $this->FechaFin;
    }
    public function getUsuario(){
        return $this->Usuario;
    }

    /**
     * Método que retorna el array completo de los operativos
     * ordenados por fecha, es utilizado tanto en los combos
     * como en la grilla de operativos
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @return array
     */
    public function nominaOperativos(){

        // componemos la consulta
        $consulta = "SELECT cce.operativos_chagas.ID AS id_operativo,
                            cce.operativos_chagas.OPERATIVO_NRO AS operativo,
                            cce.operativos_chagas.ABIERTO AS abierto,
                            DATE_FORMAT(cce.operativos_chagas.FECHA_INICIO, '%d/%m/%Y') AS fecha_inicio,
                            DATE_FORMAT(cce.operativos_chagas.FECHA_FIN, '%d/%m/%Y') AS fecha_fin,
                            DATE_FORMAT(cce.operativos_chagas.FECHA_ALTA, '%d/%m/%Y') AS fecha_alta,
                            cce.responsables.USUARIO AS usuario
                     FROM cce.operativos_chagas INNER JOIN cce.responsables ON cce.operativos_chagas.USUARIO = cce.responsables.ID
                     ORDER BY cce.operativos_chagas.fecha_fin DESC;";

        // ejecutamos la consulta
        $resultado = $this->Link->query($consulta);

        // lo pasamos a minúsculas porque según la versión de
        // pdo lo devuelve en mayúsculas o minúsculas
        $nomina = array_change_key_case($resultado->fetchAll(PDO::FETCH_ASSOC), CASE_LOWER);

        // retornamos el vector
        return $nomina;

    }

    /**
     * Método que retorna un array con la nómina de operativos abiertos,
     * utilizado en la carga de determinaciones
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @return array
     */
    public function listaAbiertos(){

        // componemos la consulta
        $consulta = "SELECT cce.operativos_chagas.ID AS id_operativo,
                            cce.operativos_chagas.OPERATIVO_NRO AS nro_operativo
                     FROM cce.operativos_chagas
                     WHERE cce.operativos_chagas.ABIERTO = 'Si'
                     ORDER BY cce.operativos_chagas.FECHA_FIN DESC; ";

        // ejecutamos la consulta
        $resultado = $this->Link->query($consulta);

        // lo pasamos a minúsculas porque según la versión de
        // pdo lo devuelve en mayúsculas o minúsculas
        $operativos = array_change_key_case($resultado->fetchAll(PDO::FETCH_ASSOC), CASE_LOWER);

        // retornamos el vector
        return $operativos;

    }

    /**
     * Método que retorna un array con la nómina de operativos cerrados
     * utilizado al establecer los resultados de las muestras
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @return array
     */
    public function listaCerrados(){

        // componemos la consulta
        $consulta = "SELECT cce.operativos_chagas.OPERATIVO_NRO AS operativo
                     FROM cce.operativos_chagas
                     WHERE cce.operativos_chagas.ABIERTO = 'No'
                     ORDER BY cce.operativos_chagas.FECHA_FIN DESC; ";

        // ejecutamos la consulta
        $resultado = $this->Link->query($consulta);

        // lo pasamos a minúsculas porque según la versión de
        // pdo lo devuelve en mayúsculas o minúsculas
        $operativos = array_change_key_case($resultado->fetchAll(PDO::FETCH_ASSOC), CASE_LOWER);

        // retornamos el vector
        return $operativos;

    }

    /**
     * Método que recibe como parámetro la cadena con el número
     * de operativo y retorna la clave del mismo
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param string $operativo - cadena con el operativo
     * @return int
     */
    public function getClaveOperativo($operativo){

        // inicializamos las variables
        $id_operativo = 0;

        // componemos la consulta
        $consulta = "SELECT cce.operativos_chagas.ID AS id_operativo
                     FROM cce.operativos_chagas
                     WHERE cce.operativos_chagas.OPERATIVO_NRO = '$operativo';";

        // ejecutamos la consulta
        $resultado = $this->Link->query($consulta);

        // lo pasamos a minúsculas porque según la versión de
        // pdo lo devuelve en mayúsculas o minúsculas
        $registro = array_change_key_case($resultado->fetch(PDO::FETCH_ASSOC), CASE_LOWER);

        // obtenemos el registro y retornamos la clave
        extract($registro);
        return $id_operativo;

    }

    /**
     * Método que recibe como parámetro la clave de un operativo
     * y retorna el contenido del reporte de ese operativo
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param int $idoperativo - clave del registro
     * @return string
     */
    public function getReporte($idoperativo){

        // inicializamos las variables
        $reporte = "";

        // componemos la consulta
        $consulta = "SELECT cce.operativos.REPORTE AS reporte
                     FROM cce.operativos
                     WHERE cce.operativos.ID = '$idoperativo';";

        // ejecutamos la consulta
        $resultado = $this->Link->query($consulta);

        // lo pasamos a minúsculas porque según la versión de
        // pdo lo devuelve en mayúsculas o minúsculas
        $registro = array_change_key_case($resultado->fetch(PDO::FETCH_ASSOC), CASE_LOWER);

        // obtenemos el registro retornamos el reporte
        extract($registro);
        return $reporte;

    }

    /**
     * Método que obtiene los datos de un operativo y los asigna a las
     * variables de clase
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param int $idoperativo - clave del registro
     */
    public function getDatosOperativo($idoperativo){

        // inicializamos las variables
        $id_operativo = 0;
        $abierto = "";
        $fecha_alta = "";
        $fecha_fin = "";
        $fecha_inicio = "";
        $operativo_nro = "";
        $usuario = "";

        // componemos la consulta
        $consulta =  "SELECT cce.operativos_chagas.ID AS id_operativo,
                             cce.operativos_chagas.ABIERTO AS abierto,
                             DATE_FORMAT(cce.operativos_chagas.FECHA_ALTA, '%d/%m/%Y') AS fecha_alta,
                             DATE_FORMAT(cce.operativos_chagas.FECHA_FIN, '%d/%m/%Y') AS fecha_fin,
                             DATE_FORMAT(cce.operativos_chagas.FECHA_INICIO, '%d/%m/%Y') AS fecha_inicio,
                             cce.operativos_chagas.OPERATIVO_NRO AS operativo_nro,
                             cce.responsables.USUARIO AS usuario
                     FROM cce.operativos_chagas INNER JOIN cce.responsables ON cce.operativos_chagas.USUARIO = cce.responsables.ID
                     WHERE cce.operativos_chagas.ID = '$idoperativo'; ";

        // ejecutamos la consulta
        $resultado = $this->Link->query($consulta);

        // lo pasamos a minúsculas porque según la versión de
        // pdo lo devuelve en mayúsculas o minúsculas
        $fila = array_change_key_case($resultado->fetch(PDO::FETCH_ASSOC), CASE_LOWER);

        // obtenemos el registro
        extract($fila);

        // lo asignamos a las variables de clase
        $this->IdOperativo = $id_operativo;
        $this->Abierto = $abierto;
        $this->FechaAlta = $fecha_alta;
        $this->OperativoNro = $operativo_nro;
        $this->FechaInicio = $fecha_inicio;
        $this->FechaFin = $fecha_fin;
        $this->Usuario = $usuario;

    }

    /**
     * Método que ejecuta la consulta en la base de datos y retorna la
     * id del registro afectado
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @return int - clave del registro afectado
     */
    public function grabaOperativo(){

        // si está insertando
        if ($this->IdOperativo == 0){
            $this->nuevoOperativo();
        } else {
            $this->editaOperativo();
        }

        // retorna la clave
        return $this->IdOperativo;

    }

    /**
     * Método que ejecuta la consulta de inserción de operativos
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     */
    protected function nuevoOperativo(){

        // compone la consulta de inserción
        $consulta = "INSERT INTO cce.operativos_chagas
                                 (ABIERTO,
                                  FECHA_FIN,
                                  FECHA_INICIO,
                                  OPERATIVO_NRO,
                                  USUARIO)
                                 VALUES
                                 (:abierto,
                                  STR_TO_DATE(:fecha_fin, '%d/%m/%Y'),
                                  STR_TO_DATE(:fecha_inicio, '%d/%m/%Y'),
                                  :operativo_nro,
                                  :idusuario);";

        // asignamos la consulta
        $psInsertar = $this->Link->prepare($consulta);

        // asignamos los parámetros de la consulta
        $psInsertar->bindParam(":abierto", $this->Abierto);
        $psInsertar->bindParam(":fecha_fin", $this->FechaFin);
        $psInsertar->bindParam(":fecha_inicio", $this->FechaInicio);
        $psInsertar->bindParam(":operativo_nro", $this->OperativoNro);
        $psInsertar->bindParam(":idusuario", $this->IdUsuario);

        // ejecutamos la edición
        $resultado = $psInsertar->execute();

        // si salió todo bien
        if ($resultado){

            // obtiene la id del registro insertado
            $this->IdOperativo = $this->Link->lastInsertId();

        // si hubo un error
        } else {

            // inicializamos la clave y retornamos el error
            $this->IdOperativo = 0;
            echo $resultado;

        }

    }

    /**
     * Método que ejecuta la consulta de edición de operativos
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     */
    protected function editaOperativo(){

        // compone la consulta de edición
        $consulta = "UPDATE cce.operativos_chagas SET
                            ABIERTO = :abierto,
                            FECHA_FIN = STR_TO_DATE(:fecha_fin, '%d/%m/%Y'),
                            FECHA_INICIO = STR_TO_DATE(:fecha_inicio, '%d/%m/%Y'),
                            OPERATIVO_NRO = :operativo_nro,
                            USUARIO = :idusuario
                      WHERE operativos_chagas.ID = :idoperativo;";

        // asignamos la consulta
        $psInsertar = $this->Link->prepare($consulta);

        // asignamos los parámetros de la consulta
        $psInsertar->bindParam(":abierto", $this->Abierto);
        $psInsertar->bindParam(":fecha_fin", $this->FechaFin);
        $psInsertar->bindParam(":fecha_inicio", $this->FechaInicio);
        $psInsertar->bindParam(":operativo_nro", $this->OperativoNro);
        $psInsertar->bindParam(":idusuario", $this->IdUsuario);
        $psInsertar->bindParam(":idoperativo", $this->IdOperativo);

        // ejecutamos la edición
        $resultado = $psInsertar->execute();

        // si hubo un error
        if (!$resultado){

            // inicializamos la clave y retornamos el error
            $this->IdOperativo = 0;
            echo $resultado;

        }

    }

    /**
     * Metodo que recibe como parámetro la clave del operativo
     * el resultado a establecer y el primer dígito de la
     * muestra y actualiza los resultados correctos, retorna
     * el número de registros afectados
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param int $idoperativo - clave con el número de operativo
     * @param string $resultado - cadena con el resultado correcto
     * @param int $muestra - entero con el primer dígito de la muestra
     * @return int
     */
    public function establecerResultados($idoperativo, $resultado, $muestra){

        // componemos la consulta
        $consulta = "UPDATE cce.chag_datos SET
                            CORRECTO = :resultado
                    WHERE cce.chag_datos.OPERATIVO = :idoperativo AND
                          INSTR(cce.chag_datos.MUESTRA_NRO, :muestra) = 1;";

        // asignamos la consulta
        $psInsertar = $this->Link->prepare($consulta);

        // asignamos los parámetros de la consulta
        $psInsertar->bindParam(":resultado", $resultado);
        $psInsertar->bindParam(":idoperativo", $idoperativo);
        $psInsertar->bindParam(":muestra", $muestra);

        // ejecutamos la edición
        $resultado = $psInsertar->execute();

        // si salió todo bien
        if ($resultado){

            // obtenemos el número de registros modificados
            $registros = $psInsertar->rowCount();

            // retornamos el número de registros
            return $registros;

        // si hubo un error
        } else {

            // mostramos el error
            echo $resultado;

        }

    }

    /**
     * Método que retorna un vector con los años en que se han realizado
     * operativos de control de calidad, y en los que los operativos
     * están cerrados, utilizado para la generación de reportes y
     * certificados
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @return array
     */
    public function listaAnios(){

        // componemos la consulta
        $consulta = "SELECT DISTINCT(RIGHT(cce.operativos_chagas.OPERATIVO_NRO,4)) AS anio
                     FROM cce.operativos_chagas
                     WHERE cce.operativos_chagas.ABIERTO = 'No'
                     ORDER BY RIGHT(cce.operativos_chagas.OPERATIVO_NRO,4) DESC;";

        // ejecutamos la consulta
        $resultado = $this->Link->query($consulta);

        // lo pasamos a minúsculas porque según la versión de
        // pdo lo devuelve en mayúsculas o minúsculas
        $registros = array_change_key_case($resultado->fetchAll(PDO::FETCH_ASSOC), CASE_LOWER);

        // retornamos
        return $registros;

    }

    /**
     * Método que retorna un vector con los años en que se han realizado
     * operativos de control de calidad, y en los que los operativos
     * están abiertos, utilizado para la eliminación de etiquetas
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @return array
     */
    public function listaAniosAbiertos(){

        // componemos la consulta
        $consulta = "SELECT DISTINCT(RIGHT(cce.operativos_chagas.OPERATIVO_NRO,4)) AS anio
                     FROM cce.operativos_chagas
                     WHERE cce.operativos_chagas.ABIERTO = 'Si'
                     ORDER BY RIGHT(cce.operativos_chagas.OPERATIVO_NRO,4) DESC;";

        // ejecutamos la consulta
        $resultado = $this->Link->query($consulta);

        // lo pasamos a minúsculas porque según la versión de
        // pdo lo devuelve en mayúsculas o minúsculas
        $registros = array_change_key_case($resultado->fetchAll(PDO::FETCH_ASSOC), CASE_LOWER);

        // retornamos
        return $registros;

    }

}
