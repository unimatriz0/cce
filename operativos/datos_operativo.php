<?php

/**
 *
 * operativos/datos_operativo.php
 *
 * @package     CCE
 * @subpackage  Operativos
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.1.0 (15/08/2018)
 * @copyright   Copyright (c) 2017, INP
 *
 * Procedimiento que recibe por get la clave de un operativo
 * y retorna los datos en formato json
*/

// incluimos e instanciamos la clase
require_once ("operativos.class.php");
$operativo = new Operativos();

// obtenemos el registro
$operativo->getDatosOperativo($_GET["id"]);

// armamos la matriz y retornamos
echo json_encode(array("Id" =>                $operativo->getIdOperativo(),
                       "Operativo" =>         $operativo->getOperativo(),
                       "Abierto" =>           $operativo->getAbierto(),
                       "FechaAlta" =>         $operativo->getFechaAlta(),
                       "FechaInicio" =>       $operativo->getFechaInicio(),
                       "FechaFin" =>          $operativo->getFechaFin(),
                       "Usuario" =>           $operativo->getUsuario()));

?>