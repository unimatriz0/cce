/*
 * Nombre: operativos.js
 * Autor: Lic. Claudio Invernizzi
 * E-Mail: cinvernizzi@gmail.com
 * Fecha: 14/06/2017
 * Licencia: GPL
 * Comentarios: Serie de funciones y procedimientos utilizados por el
 *              sistema de control de calidad en los formularios de
 *              operativos
 */

 /**
 *
 * Atención, utilizamos el nuevo estandard definido en ECMAScript 2015 (ES6)
 * este permite definir clases con el mètodo ya utilizado en php y java
 * como creo que es mas claro lo utilizaremos, la compatibilidad está
 * anunciada para firefox y chrome así que no deberìa haber problemas
 *
 */

/* jshint esversion: 6 */

/**
 * @author Claudio Invernizzi <cinvernizzi@gmail.com>
 * @class Clase que controla las operaciones de los formulario de
 * operativos
 */
class Operativos {

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @constructor
     */
    constructor(){

        // inicializamos las variables de clase
        this.initOperativos();

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que inicializa las variables de clase
     */
    initOperativos(){

        // inicializamos las variables
        this.IdOperativo = 0;             // clave del registro
        this.Operativo = "";              // número y año del operativo
        this.Abierto = "";                // estado del operativo
        this.FechaInicio = "";            // inicio de la carga
        this.FechaFin = "";               // fin de la carga
        this.FechaAlta = "";              // fecha de alta del registro
        this.Usuario = "";                // nombre del usuario
        this.layerOperativos = "";        // layer emergente

    }

    // métodos de asignación de valores
    setIdOperativo(idoperativo){
        this.IdOperativo = idoperativo;
    }
    setOperativo(operativo){
        this.Operativo = operativo;
    }
    setAbierto(abierto){
        this.Abierto = abierto;
    }
    setFechaInicio(fecha){
        this.FechaInicio = fecha;
    }
    setFechaFin(fecha){
        this.FechaFin = fecha;
    }
    setFechaAlta(fecha){
        this.FechaAlta = fecha;
    }
    setUsuario(usuario){
        this.Usuario = usuario;
    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que carga en el contenido la grilla con los operativos
     */
    grillaOperativos(){

        // carga la grilla
        $("#form_administracion").load("operativos/grilla_operativos.php");

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param {int} id clave del operativo
     * Método que recibe como parámetro la id de un operativo y carga el
     * layer para la edición, si no recibe la id carga el layer para el
     * alta de un nuevo registro
     */
    muestraOperativo(id){

        // reiniciamos el contador
        cce.contador();

        // iniciamos las variables de clase
        this.initOperativos();

        // definimos el layer y lo cargamos
        this.layerOperativos = new jBox('Modal', {
                            animation: 'flip',
                            closeOnEsc: false,
                            closeOnClick: false,
                            closeOnMouseleave: false,
                            closeButton: true,
                            overlay: false,
                            title: 'Operativos',
                            draggable: 'title',
                            repositionOnContent: true,
                            onCloseComplete: function(){
                                this.destroy();
                            },
                            theme: 'TooltipBorder',
                            width: 550,
                            ajax: {
                                url: 'operativos/form_operativos.html',
                                reload: 'strict'
                            }
                        });
        this.layerOperativos.open();

        // si recibió la id de un operativo
        if (typeof(id) != "undefined"){

            // obtiene los datos del registro
            this.getDatosOperativo(id);

        }

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que recibe la clave de un registro, obtiene los
     * datos del mismo y los carga en las variables de clase
     */
    getDatosOperativo(id){

        // obtiene los datos del operativo
        $.get('operativos/datos_operativo.php','id='+id,
            function(data){

                // asignamos en las variables de clase
                operativos.setIdOperativo(data.Id);
                operativos.setOperativo(data.Operativo);
                operativos.setAbierto(data.Abierto);
                operativos.setFechaInicio(data.FechaInicio);
                operativos.setFechaFin(data.FechaFin);
                operativos.setUsuario(data.Usuario);
                operativos.setFechaAlta(data.FechaAlta);

                // presentamos los datos
                operativos.cargaOperativo();

            }, "json");

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que a partir de las variables de clase presenta los datos
     * del operativo en el formulario
     */
    cargaOperativo(){

        // asignamos los valores en el formulario
        document.getElementById("id_operativo").value = this.IdOperativo;
        document.getElementById("operativo_nro").value = this.Operativo;

        // según el estado del operativo
        if (this.Abierto == "Si"){
            document.getElementById("operativo_abierto").checked = true;
        } else {
            document.getElementById("operativo_abierto").checked = false;
        }

        // terminamos de cargar
        document.getElementById("inicio_operativo").value = this.FechaInicio;
        document.getElementById("fin_operativo").value = this.FechaFin;
        document.getElementById("usuario_operativo").value = this.Usuario;
        document.getElementById("alta_operativo").value = this.FechaAlta;

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que verifica los datos del formulario antes de
     * enviarlo al servidor
     */
    verificaOperativo(){

        // reiniciamos el contador
        cce.contador();

        // declaración de variables
        var mensaje;

        // si está editando
        if (document.getElementById("id_operativo").value != ""){
            this.IdOperativo = document.getElementById("id_operativo").value;
        }

        // si no ingresó el número de operativo
        if (document.getElementById("operativo_nro").value == ""){

            // presenta el mensaje y retorna
            mensaje = "Debe ingresar el número de operativo";
            new jBox('Notice', {content: mensaje, color: 'red'});
            document.getElementById("operativo_nro").focus();
            return false;

        // si declaró
        } else {

            // asigna en la variable de clase
            this.Operativo = document.getElementById("operativo_nro").value;

        }

        // si está abierto
        if (document.getElementById("operativo_abierto").checked){
            this.Abierto = "Si";
        } else {
            this.Abierto = "No";
        }

        // si no ingresó la fecha de inicio
        if (document.getElementById("inicio_operativo").value == ""){

            // presenta el mensaje y retorna
            mensaje = "Debe ingresar la fecha de inicio";
            new jBox('Notice', {content: mensaje, color: 'red'});
            document.getElementById("inicio_operativo").focus();
            return false;

        // si ingresó
        } else {

            // asigna en la variable de clase
            this.FechaInicio = document.getElementById("inicio_operativo").value;

        }

        // si no ingresó la fecha de finalización
        if (document.getElementById("fin_operativo").value == ""){

            // presenta el mensaje y retorna
            mensaje = "Debe ingresar la fecha de finalización";
            new jBox('Notice', {content: mensaje, color: 'red'});
            document.getElementById("fin_operativo").focus();
            return false;

        // si ingresó
        } else {

            // asigna en la variable de clase
            this.FechaFin = document.getElementById("fin_operativo").value;

        }

        // graba el registro
        this.grabaOperativo();

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que envía los datos del formulario al servidor
     */
    grabaOperativo(){

        // declaramos el formulario html5
        var datosOperativo = new FormData();

        // si está editando
        if (this.IdOperativo != 0){
            datosOperativo.append("Id", this.IdOperativo);
        }

        // agregamos el resto de los valores
        datosOperativo.append("Operativo", this.Operativo);
        datosOperativo.append("FechaInicio", this.FechaInicio);
        datosOperativo.append("Abierto", this.Abierto);
        datosOperativo.append("FechaFin", this.FechaFin);

        // enviamos el formulario por ajax
        $.ajax({
            url: 'operativos/graba_operativo.php',
            type: "POST",
            data: datosOperativo,
            cahe: false,
            contentType: false,
            processData: false,
            dataType: 'json',
            success: function(data) {

                // si grabo
                if (data.Id != 0){

                    // presenta el mensaje, asigna la id y retorna
                    new jBox('Notice', {content: "Registro grabado ... ", color: 'green'});

                    // cierra el layer
                    operativos.layerOperativos.destroy();

                    // recarga la grilla
                    operativos.grillaOperativos();

                // si hubo un error
                } else {

                    // presenta el mensaje y retorna
                    new jBox('Notice', {content: "Ha ocurrido un error ... ", color: 'red'});
                    document.getElementById("operativo_nro").focus();
                    return false;

                }

            }});

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que simplemente cierra el formulario de operativos
     */
    cierraOperativo(){
        this.layerOperativos.destroy();
    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param {string} idelemento - id de un objeto html
     * Método que recibe como parámetro un objeto html (normalmente
     * un select) y carga en el select la nómina de años con
     * operativos cerrados
     */
    nominaAniosCerrados(idelemento){

        // obtenemos la nómina
        $.ajax({
            url: 'operativos/lista_anios_cerrados.php',
            type: "GET",
            cahe: false,
            contentType: false,
            processData: false,
            dataType: 'json',
            async: false,
            success: function(data) {

                // agrega el primer elemento en blanco
                $("#" + idelemento).append("<option value='0'>Seleccione</option>");

                // recorre el vector de resultados
                for(var i=0; i < data.length; i++){

                    // simplemente lo agrega
                    $("#" + idelemento).append("<option>" + data[i].Anio + "</option>");

                }

            }});

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param {string} idelemento - id de un objeto html
     * Método que recibe como parámetro un objeto html (normalmente
     * un select) y carga en el select la nómina de años con
     * operativos abiertos
     */
    nominaAniosAbiertos(idelemento){

        // obtenemos la nómina
        $.ajax({
            url: 'operativos/lista_anios_abiertos.php',
            type: "GET",
            cahe: false,
            contentType: false,
            processData: false,
            dataType: 'json',
            async: false,
            success: function(data) {

                // agrega el primer elemento en blanco
                $("#" + idelemento).append("<option value='0'>Seleccione</option>");

                // recorre el vector de resultados
                for(var i=0; i < data.length; i++){

                    // simplemente lo agrega
                    $("#" + idelemento).append("<option>" + data[i].Anio + "</option>");

                }

            }});

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param {string} idelemento - id de un objeto html
     * @param {int} idoperativo - clave del valor predeterminado
     * Método que recibe como parámetro un objeto html (normalmente
     * un select) y carga en el select la nómina operativos abiertos
     * si además recibe la clave, predetermina este valor
     */
    nominaOperativos(idelemento, idoperativo){

        // obtenemos la nómina
        $.ajax({
            url: 'operativos/lista_operativos.php',
            type: "GET",
            cahe: false,
            contentType: false,
            processData: false,
            dataType: 'json',
            success: function(data) {

                // agrega el primer elemento en blanco
                $("#" + idelemento).append("<option value='0'>Seleccione</option>");

                // recorre el vector de resultados
                for(var i=0; i < data.length; i++){

                    // si recibió la clave
                    if (typeof(idtecnica) != "undefined"){

                        // si coindicen
                        if (idoperativo == data[i].Id){

                            // lo agrega preseleccionado
                            $("#" + idelemento).append("<option selected value='" + data[i].Id + "'>" + data[i].Operativo + "</option>");

                        // si no coincide
                        } else {

                            // simplemente lo agrega
                            $("#" + idelemento).append("<option value='" + data[i].Id + "'>" + data[i].Operativo + "</option>");

                        }

                    // si no recibió
                    } else {

                        // simplemente lo agrega
                        $("#" + idelemento).append("<option value='" + data[i].Id + "'>" + data[i].Operativo + "</option>");

                    }

                }

            }});

    }

}