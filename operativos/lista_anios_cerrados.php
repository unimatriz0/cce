<?php

/**
 *
 * operativos/lista_anios_cerrados.php
 *
 * @package     CCE
 * @subpackage  Operativos
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.4.0 (17/08/2018)
 * @copyright   Copyright (c) 2017, INP
 *
 * Método que retorna un array json con la nómina de los años
 * con operativos cerrados
 *
*/

// incluimos e instanciamos las clases
require_once("operativos.class.php");
$operativo = new Operativos();

// obtenemos la nómina
$nomina = $operativo->listaAnios();

// declaramos la matriz
$jsondata = array();

// recorremos el vector
foreach($nomina AS $registro){

    // obtenemos el registro
    extract($registro);

    // lo agregamos a la matriz
    $jsondata[] = array("Anio" => $anio);

}

// retornamos el vector
echo json_encode($jsondata);

?>