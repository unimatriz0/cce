<?php

/**
 *
 * operativos/graba_operativo.php
 *
 * @package     CCE
 * @subpackage  Operativos
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.1.0 (15/08/2018)
 * @copyright   Copyright (c) 2017, INP
 *
 * Procedimiento que recibe por get los datos de un operativo y
 * ejecuta la consulta en la base, retorna la id del registro
 * afectado
 *
*/

// incluimos e instanciamos la clase
require_once ("operativos.class.php");
$operativo = new Operativos();

// si recibió la id
if (!empty($_POST["Id"])){
    $operativo->setIdOperativo($_POST["Id"]);
}

// fijamos los datos del formulario
$operativo->setOperativoNro($_POST["Operativo"]);
$operativo->setAbierto($_POST["Abierto"]);
$operativo->setFechaInicio($_POST["FechaInicio"]);
$operativo->setFechaFin($_POST["FechaFin"]);

// grabamos el registro
$id = $operativo->grabaOperativo();

// retornamos el resultado
echo json_encode(array("Id" => $id));

?>