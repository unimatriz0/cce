<?php

/**
 *
 * operativos/grilla_operativos.php
 *
 * @package     CCE
 * @subpackage  Operativos
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.1.0 (15/08/2018)
 * @copyright   Copyright (c) 2017, INP
 *
 * Procedimiento que arma la grilla con los datos de los operativos
 * de control de calidad y permita una edición sencilla
*/

// inclusión de archivos
require_once ("operativos.class.php");
$operativos = new Operativos();

// obtenemos la nómina
$nomina = $operativos->nominaOperativos();

// iniciamos sesión
session_start();

// si hay una sesión activa
if(isset($_SESSION["ID"])){

    // obtenemos la dependencia
    $nivelcentral = $_SESSION["NivelCentral"];

// si no está activa
} else {

    // asignamos falso
    $nivelcentral = "No";

}

// cerramos la sesión
session_write_close();

// abrimos la tabla
echo "<table width='90%' align='center' border='0' id='operativos'>";

// definimos los encabezados
echo "<thead>";
echo "<tr>";
echo "<th align='left'>Operativo</th>";
echo "<th align='left'>Abierto</th>";
echo "<th align='left'>Inicio</th>";
echo "<th align='left'>Fin</th>";
echo "<th align='left'>Alta</th>";
echo "<th align='left'>Usuario</th>";

// si es de nivel central
if ($nivelcentral == "Si"){

    // en la última columna agregamos el botón nuevo
    echo "<th align='left'>";
    echo "<input type='button'
                name='botonAgregar'
                id='botonAgregar'
                class='botonagregar'
                onClick='operativos.muestraOperativo()'>";
    echo "</th>";

}

// cerramos el encabezado
echo "</head>";

// definimos el cuerpo
echo "<tbody>";

// recorremos el vector
foreach ($nomina AS $registro){

    // obtenemos el registro
    extract($registro);

    // lo agregamos
    echo "<tr>";
    echo "<td align='center'>$operativo</td>";
    echo "<td align='center'>$abierto</td>";
    echo "<td align='center'>$fecha_inicio</td>";
    echo "<td align='center'>$fecha_fin</td>";
    echo "<td align='center'>$fecha_alta</td>";
    echo "<td align='center'>$usuario</td>";

    // si es de nivel central
    if ($nivelcentral == "Si"){

        // agrega el botón editar
        echo "<td>";
            echo "<input type='button'
                   name='btnVer'
                   class='botoneditar'
                   onClick='operativos.muestraOperativo($id_operativo)'>";
        echo "</td>";

    }

    // cerramos los tags
    echo "</tr>";

}

// cerramos la tabla
echo "</thead>";
echo "</table>";

// define el div para el paginador
echo "<div class='paging'></div>";

// si no es de nivel central
if ($nivelcentral != "Si"){
    ?>
    <script>

        // definimos las propiedades de la tabla
        $('#operativos').datatable({
            pageSize: 15,
            sort:    [true,     true,     true, true, false, true],
            filters: ['select', 'select', true, true, false, 'select'],
            filterText: 'Buscar ... '
        });

    </script>
    <?php

// si es de nivel central tiene una columna mas
} else {

    ?>
    <script>

        // definimos las propiedades de la tabla
        $('#operativos').datatable({
            pageSize: 15,
            sort:    [true,     true,     true, true, false, true,     false],
            filters: ['select', 'select', true, true, false, 'select', false],
            filterText: 'Buscar ... '
        });

    </script>
    <?php

}

?>