<?php

/**
 *
 * laboratorios/graba_coordenadas.php
 *
 * @package     CCE
 * @subpackage  Laboratorios
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.4.0 (09/08/2018)
 * @copyright   Copyright (c) 2018, INP
 *
 * Procedimiento que recibe por post las coordenadas gps del
 * laboratorio y la id y actualiza la base de datos
 *
*/

// incluimos e instanciamos las clases
require_once ("laboratorios.class.php");
$laboratorio = new Laboratorios();

// grabamos los datos recibidos
$laboratorio->grabaCoordenadas($_POST["id"], $_POST["coordenadas"]);

?>
