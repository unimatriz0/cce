/*
 * Nombre: laboratorios.js
 * Autor: Lic. Claudio Invernizzi
 * E-Mail: cinvernizzi@gmail.com
 * Fecha: 19/12/2016
 * Licencia: GPL
 * Comentarios: Serie de funciones y procedimientos utilizados por el
 *              sistema de control de calidad en los formularios de
 *              laboratorios
 */

 /**
 *
 * Atención, utilizamos el nuevo estandard definido en ECMAScript 2015 (ES6)
 * este permite definir clases con el mètodo ya utilizado en php y java
 * como creo que es mas claro lo utilizaremos, la compatibilidad está
 * anunciada para firefox y chrome así que no deberìa haber problemas
 *
 */

/* jshint esversion: 6 */

/**
 * @class Laboratorios
 * @author Claudio Invernizzi <cinvernizzi@gmail.com>
 * Definición de la clase
 */
class Laboratorios{

    /**
     * @constructor
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     */
    constructor(){

        // inicializamos las variables de clase
        this.initLaboratorios();

        // cargamos el formulario
        this.formLaboratorios();

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que inicializa las variables de clase
     */
    initLaboratorios(){

        // inicializamos las variables
        this.IdLaboratorio = 0;         // clave del registro
        this.Nombre = "";               // nombre del laboratorio
        this.Responsable = "";          // nombre del responsable
        this.IdPais = 0;                // clave del pais del laboratorio
        this.CodProv = "";              // clave indec de la jurisdicción
        this.CodLoc = "";               // clave indec de la localidad
        this.Direccion = "";            // dirección postal
        this.CodigoPostal = "";         // código postal
        this.Coordenadas = "";          // coordenadas gps
        this.IdDependencia = 0;         // clave de la dependencia
        this.EMail = "";                // correo electrónico oficial
        this.FechaAlta = "";            // fecha de alta del registro
        this.Activo = "";               // si participa de los operativos
        this.RecibeMuestras = "";       // si recibe muestras directamente
        this.IdRecibe = 0;              // clave del responsable que recibe muestras
        this.Observaciones = "";        // observaciones y comentarios
        this.Usuario = "";              // nombre del usuario
        this.layerLaboratorios = "";    // layer emergente
        this.Correcto = true;           // switch de laboratorio y mail repetidos

    }

    // métodos de asignación de valores
    setIdLaboratorio(idlaboratorio){
        this.IdLaboratorio = idlaboratorio;
    }
    setNombre(nombre){
        this.Nombre = nombre;
    }
    setResponsable(responsable){
        this.Responsable = responsable;
    }
    setIdPais(idpais){
        this.IdPais = idpais;
    }
    setCodProv(codprov){
        this.CodProv = codprov;
    }
    setCodLoc(codloc){
        this.CodLoc = codloc;
    }
    setDireccion(direccion){
        this.Direccion = direccion;
    }
    setCodigoPostal(codigo){
        this.CodigoPostal = codigo;
    }
    setCoordenadas(coordenadas){
        this.Coordenadas = coordenadas;
    }
    setIdDependencia(iddependencia){
        this.IdDependencia = iddependencia;
    }
    setEMail(mail){
        this.EMail = mail;
    }
    setFechaAlta(fecha){
        this.FechaAlta = fecha;
    }
    setActivo(activo){
        this.Activo = activo;
    }
    setRecibeMuestras(recibe){
        this.RecibeMuestras = recibe;
    }
    setIdRecibe(idrecibe){
        this.IdRecibe = idrecibe;
    }
    setObservaciones(observaciones){
        this.Observaciones = observaciones;
    }
    setUsuario(usuario){
        this.Usuario = usuario;
    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que carga en el layer el formulario de laboratorios
     */
    formLaboratorios(){

        // cargamos el formulario
        $("#form_laboratorios").load("laboratorios/form_laboratorios.html");

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param {string} idjurisdiccion - registro predeterminado
     * Método llamado en el onchange de país que actualiza
     * el select de provincias, opcionalmente recibe como
     * parámetro la clave predeterminada de la jurisdicción
     * cuando es llamado desde la carga del formulario
     */
    cargaProvinciaLaboratorio(idjurisdiccion){

        // reiniciamos el contador
        cce.contador();

        // declaración de variables
        var idpais = document.getElementById("paislaboratorio").value;

        // si no hay ninguno seleccionado
        if (idpais == 0){
            return false;
        }

        // si recibió la clave predeterminada
        if (typeof(idjurisdiccion) != "undefined"){
            jurisdicciones.listaJurisdicciones("jurisdiccionlaboratorio", idpais, idjurisdiccion);
        } else {
            jurisdicciones.listaJurisdicciones("jurisdiccionlaboratorio", idpais);
        }

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param {string} idjurisdiccion - clave de la jurisdiccion
     * @param {string} idlocalidad - clave de la localidad
     * Método llamado en el onchange del select de jurisdicciones
     * que actualiza el combo de localidades y el de responsables
     * opcionalmente recibe como parámetro la localidad predeterminada
     * cuando es llamado desde la carga del formulario
     */
    cargaLocalidadLaboratorio(idjurisdiccion, idlocalidad){

        // reiniciamos el contador
        cce.contador();

        // si no recibió la jurisdicción asumimos que fue llamado
        // desde el onchange
        if (typeof(idjurisdiccion) == "undefined"){
            idjurisdiccion = document.form_laboratorios.jurisdiccionlaboratorio.value;
        }

        // si no hay nada seleccionado
        if (idjurisdiccion == 0){
            return false;
        }

        // llamamos el método de la clase localidades
        if (typeof(idlocalidad) != "undefined"){
            localidades.nominaLocalidades("localidadlaboratorio", idjurisdiccion);
        } else {
            localidades.nominaLocalidades("localidadlaboratorio", idjurisdiccion, idlocalidad);
        }

        // ahora llamamos el método de la nómina de responsables
        responsables.nominaResponsables("responsablemuestras", idjurisdiccion);

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Metodo que deja en blanco el formulario de laboratorios
     */
    nuevoLaboratorio(){

        // reiniciamos el contador
        cce.contador();

        // limpiamos el formulario
        document.form_laboratorios.idlaboratorio.value = "";
        document.form_laboratorios.nombrelaboratorio.value = "";
        document.form_laboratorios.responsablelaboratorio.value = "";
        document.form_laboratorios.maillaboratorio.value = "";

        // fijamos los combos
        document.form_laboratorios.dependencia.selectedIndex = 0;
        document.form_laboratorios.paislaboratorio.selectedIndex = 0;
        document.form_laboratorios.jurisdiccionlaboratorio.selectedIndex = 0;
        document.form_laboratorios.localidadlaboratorio.selectedIndex = 0;

        // continúa inicializando
        document.form_laboratorios.direccionlaboratorio.value = "";
        document.form_laboratorios.codigopostallaboratorio.value = "";

        // los checkbox
        document.form_laboratorios.activolaboratorio.checked = false;
        document.form_laboratorios.recibemuestras.checked = false;

        // el combo del responsable
        document.getElementById("responsablemuestras").value = 0;

        // la fecha de alta y el usuario
        document.form_laboratorios.altalaboratorio.value = fechaActual();
        document.form_laboratorios.usuariolaboratorio.value = sessionStorage.getItem("Usuario");

        // las observaciones y también el editor
        document.form_laboratorios.observacioneslaboratorio.value = "";
        CKEDITOR.instances['observacioneslaboratorio'].setData();

        // ocultamos el botón mapa
        $("#btnMapaLaboratorio").hide();

        // carga en blanco las determinaciones
        $('#determinaciones').html("<h3>Seleccione primero un laboratorio para ver las determinaciones cargadas.</h3>");

        // setea el foco en el primer elemento
        document.form_laboratorios.nombrelaboratorio.focus();
        return false;

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método llamado por callback que verifica no se encuentre
     * repetido el mail en una inserción o edición
     */
    validaMailLaboratorio(mailCorrecto){

        // inicializamos las variables
        this.Correcto = false;

        // pasamos el mail y verificamos de forma sincrónica
        $.ajax({
            url: 'laboratorios/verificamail.php?mail='+this.Mail+'&id='+this.IdLaboratorio,
            type: "GET",
            cahe: false,
            contentType: false,
            processData: false,
            dataType: 'json',
            async: false,
            success: function(data) {

                // si retornó verdadero
                if (data.Registros == 0){
                    laboratorios.Correcto = true;
                } else {
                    laboratorios.Correcto = false;
                }

            }});

        // retornamos
        mailCorrecto(laboratorios.Correcto);

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que verifica el formulario de datos antes de
     * enviarlo al servidor
     */
    verificaLaboratorio(){

        // reiniciamos el contador
        cce.contador();

        // declaración de variables
        var mensaje;

        // si está dando un alta
        if (document.form_laboratorios.idlaboratorio.value != ""){
            this.IdLaboratorio = document.form_laboratorios.idlaboratorio.value;
        }

        // verifica si ingresó el nombre del laboratorio
        if (document.form_laboratorios.nombrelaboratorio.value === ""){

            // presenta el mensaje y retorna
            mensaje = "Debe ingresar el nombre del laboratorio";
            new jBox('Notice', {content: mensaje, color: 'red'});
            document.form_laboratorios.nombrelaboratorio.focus();
            return false;

        // si declaró
        } else {

            // asigna en la variable de clase
            this.Nombre = document.form_laboratorios.nombrelaboratorio.value;

        }

        // si no ingresó el nombre del responsable
        if (document.form_laboratorios.responsablelaboratorio.value === ""){

            // presenta el mensaje y retorna
            mensaje = "Indique quien es el responsable del laboratorio";
            new jBox('Notice', {content: mensaje, color: 'red'});
            document.form_laboratorios.responsablelaboratorio.focus();
            return false;

        // si ingresó
        } else {

            // asigna en la variable de clase
            this.Responsable = document.form_laboratorios.responsablelaboratorio.value;

        }

        // si no ingresó el mail
        if (document.form_laboratorios.maillaboratorio.value === ""){

            // presenta el mensaje y retorna
            mensaje = "Ingrese el mail institucional del laboratorio";
            new jBox('Notice', {content: mensaje, color: 'red'});
            document.form_laboratorios.maillaboratorio.focus();
            return false;

        // si cargó verifica si el mail es correcto
        } else if (!echeck(document.form_laboratorios.maillaboratorio.value)){

            // presenta el mensaje y retorna
            mensaje = "El mail del laboratorio parece incorrecto, verifique";
            new jBox('Notice', {content: mensaje, color: 'red'});
            document.form_laboratorios.maillaboratorio.focus();
            return false;

        // si llegó hasta aquí
        } else {

            // lo asigna a la variable de clase
            this.Mail = document.form_laboratorios.maillaboratorio.value;

        }

        // si no indicó la dependencia
        if (document.form_laboratorios.dependencia.selectedIndex === 0){

            // presenta el mensaje y retorna
            mensaje = "Seleccione de la lista la dependencia del laboratorio";
            new jBox('Notice', {content: mensaje, color: 'red'});
            document.form_laboratorios.dependencia.focus();
            return false;

        // si declaró
        } else {

            // asigna en la variable de clase
            this.IdDependencia = document.form_laboratorios.dependencia.value;

        }

        // si no seleccionó pais
        if (document.form_laboratorios.paislaboratorio.selectedIndex === 0){

            // presenta el mensaje y retorna
            mensaje = "Seleccione de la lista el país donde se ubica el laboratorio";
            new jBox('Notice', {content: mensaje, color: 'red'});
            document.form_laboratorios.paislaboratorio.focus();
            return false;

        // si declaró
        } else {

            // asigna en la variable de clase
            this.IdPais = document.form_laboratorios.paislaboratorio.value;

        }

        // si no seleccionó la localidad
        if (document.form_laboratorios.localidadlaboratorio.value == 0){

                // presenta el mensaje y retorna
                mensaje = "Seleccione de la lista la localidad";
                new jBox('Notice', {content: mensaje, color: 'red'});
                document.form_laboratorios.localidadlaboratorio.focus();
                return false;

        // si seleccionó
        } else {

            // asigna en la variable de clase
            this.CodLoc = document.form_laboratorios.localidadlaboratorio.value;

        }

        // si no indicó la dirección
        if (document.form_laboratorios.direccionlaboratorio.value === ""){

            // presenta el mensaje y retorna
            mensaje = "Indique la dirección postal del laboratorio";
            new jBox('Notice', {content: mensaje, color: 'red'});
            document.form_laboratorios.direccionlaboratorio.focus();
            return false;

        // si ingresó
        } else {

            // asigna en la variable de clase
            this.Direccion = document.form_laboratorios.direccionlaboratorio.value;

        }

        // si no ingresó el código postal
        if (document.form_laboratorios.codigopostallaboratorio.value === ""){

            // presenta el mensaje y retorna
            mensaje = "Indique el Código Postal de la Dirección";
            new jBox('Notice', {content: mensaje, color: 'red'});
            document.form_laboratorios.codigopostallaboratorio.focus();
            return false;

        // si ingresó
        } else {

            // asigna en la variable de clase
            this.CodigoPostal = document.form_laboratorios.codigopostallaboratorio.value;

        }

        // si está activo
        if (document.form_laboratorios.activolaboratorio.checked){
            this.Activo = "Si";
        } else {
            this.Activo = "No";
        }

        // si no recibe muestras pide se seleccione el responsable
        if (document.form_laboratorios.recibemuestras.checked == false){

            // verifica si seleccionó responsable
            if (document.form_laboratorios.responsablemuestras.selectedIndex === 0){

                // presenta el mensaje y retorna
                mensaje = "Si el laboratorio no recibe muestras, indique<br>";
                mensaje += "que responsable las recibirá";
                new jBox('Notice', {content: mensaje, color: 'red'});
                document.form_laboratorios.responsablemuestras.focus();
                return false;

            // si indicó responsable de las muestras
            } else {

                // asigna en las variables de clase
                this.IdRecibe = document.form_laboratorios.responsablemuestras.value;
                this.RecibeMuestras = "No";

            }

        // si recibe muestras verifica que no halla seleccionado responsable
        } else {

            // verifica si hay un responsable seleccionado
            if (document.form_laboratorios.responsablemuestras.selectedIndex != 0){

                // presenta el mensaje y retorna
                mensaje = "Si el laboratorio recibe muestras directamente<br>";
                mensaje += "deje en blanco el campo del responsable de las muestras";
                new jBox('Notice', {content: mensaje, color: 'red'});
                document.form_laboratorios.responsablemuestras.focus();
                return false;

            }

            // marcamos que recibe muestras
            this.RecibeMuestras = "Si";

        }

        // asigna los comentarios
        var contenido = CKEDITOR.instances['observacioneslaboratorio'].getData();
        this.Observaciones = contenido;

        // verificamos por callback
        this.validaMailLaboratorio(function(mailCorrecto){

            // si está repetido
            if (!laboratorios.Correcto){

                // presenta el mensaje
                mensaje = "Ese correo ya se encuentra declarada !!!";
                new jBox('Notice', {content: mensaje, color: 'red'});

            }

        });

        // grabamos el registro
        this.grabaLaboratorio();

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que instancia el formulario y envía los datos al
     * servidor
     */
    grabaLaboratorio(){

        // si hubo un registro repetido
        if (!this.Correcto){
            return false;
        }

        // declaramos el formulario html5
        var datosLaboratorio = new FormData();

        // si está dando un alta
        if (this.IdLaboratorio != 0){
            datosLaboratorio.append("Id", this.IdLaboratorio);
        }

        // agregamos el contenido de los campos
        datosLaboratorio.append("Nombre", this.Nombre);
        datosLaboratorio.append("Responsable", this.Responsable);
        datosLaboratorio.append("Mail", this.Mail);
        datosLaboratorio.append("Dependencia", this.IdDependencia);
        datosLaboratorio.append("Pais", this.IdPais);
        datosLaboratorio.append("Localidad", this.CodLoc);
        datosLaboratorio.append("Direccion", this.Direccion);
        datosLaboratorio.append("CodigoPostal", this.CodigoPostal);
        datosLaboratorio.append("Activo", this.Activo);
        datosLaboratorio.append("ResponsableMuestras", this.IdRecibe);
        datosLaboratorio.append("RecibeMuestras", this.RecibeMuestras);
        datosLaboratorio.append("Comentarios", this.Observaciones);

        // envía el formulario por ajax en forma asincrónica
        $.ajax({
            type: "POST",
            url: "laboratorios/graba_laboratorio.php",
            data: datosLaboratorio,
            cahe: false,
            contentType: false,
            processData: false,
            dataType: 'json',
            success: function(data) {

                // si ocurrió un error
                if (data.Id == 0){

                    // presentamos el mensaje
                    new jBox('Notice', {content: "Ha ocurrido un error", color: 'red'});

                } else {

                    // almacenamos la id en el formulario para poder
                    // agregar unidades inmediatamente
                    document.form_laboratorios.idlaboratorio.value = data.Id;

                    // inicializamos las variables de clase
                    laboratorios.initLaboratorios();

                    // muestra el mapa
                    $("#mapalaboratorio").show();

                    // presenta el mensaje
                    new jBox('Notice', {content: "Registro grabado ... ", color: 'green'});

                    // setea el foco
                    document.form_laboratorios.nombrelaboratorio.focus();

                }

            },

        });

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param idProvincia
     * Método que recibe como parámetro la clave de la provincia
     * y de acuerdo a las variables de sesión muestra u oculta
     * los botones de acción
     */
    seguridadLaboratorios(idProvincia){

        // si es de nivel central
        if (sessionStorage.getItem("NivelCentral") == "Si"){

            // muestra la botonera
            $("#btnGrabaLaboratorio").show();
            $("#btnCancelaLaboratorio").show();

            // muestra el panel de concordancia
            $("#concordancialaboratorio").show();

        // si es responsable de la misma provincia
        } else if (sessionStorage.getItem("Responsable") == "Si" && sessionStorage.getItem("CodProv") == idProvincia){

            // muestra la botonera
            $("##btnGrabaLaboratorio").show();
            $("#btnCancelaLaboratorio").show();

            // muestra el panel de concordancia
            $("#concordancialaboratorio").show();

        // si es usuario del mismo laboratorio
        } else if (document.getElementById("idlaboratorio").value == sessionStorage.getItem("Laboratorio")) {

            // muestra la botonera
            $("##btnGrabaLaboratorio").show();
            $("#btnCancelaLaboratorio").show();

            // muestra el panel de concordancia
            $("#concordancialaboratorio").show();

        // en cualquier otro caso
        } else {

            // la oculta
            $("##btnGrabaLaboratorio").hide();
            $("#btnCancelaLaboratorio").hide();

            // oculta el panel de concordancia
            $("#concordancialaboratorio").hide();

        }

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param {int} id - clave del registro
     * Método que obtiene los datos del registro que recibe
     * como parámetro y los asigna a las variables de clase
     */
    getDatosLaboratorio(id){

        // reiniciamos el contador
        cce.contador();

        // llama la rutina php para obtener los datos del usuario
        $.get('laboratorios/getlaboratorio.php', 'id='+id,
            function(data){

                // asignamos los valores
                laboratorios.setIdLaboratorio(data.ID);
                laboratorios.setNombre(data.Nombre);
                laboratorios.setResponsable(data.Responsable);
                laboratorios.setEMail(data.Mail);
                laboratorios.setIdDependencia(data.Dependencia);
                laboratorios.setIdPais(data.IdPais);
                laboratorios.setCodProv(data.IdProvincia);
                laboratorios.setCodLoc(data.IdLocalidad);
                laboratorios.setDireccion(data.Direccion);
                laboratorios.setCodigoPostal(data.CodigoPostal);
                laboratorios.setActivo(data.Activo);
                laboratorios.setRecibeMuestras(data.RecibeMuestras);
                laboratorios.setIdRecibe(data.IdRecibe);
                laboratorios.setCoordenadas(data.Coordenadas);
                laboratorios.setFechaAlta(data.FechaAlta);
                laboratorios.setUsuario(data.Usuario);
                laboratorios.setObservaciones(data.Observaciones);

                // cargamos los datos en el formulario
                laboratorios.muestraLaboratorio();

            }, "json");

        // destruimos el layer
        this.layerLaboratorios.destroy();

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que a partir de las variables de clase carga los
     * datos del formulario
     */
    muestraLaboratorio(id){

        // reiniciamos el contador
        cce.contador();

        // asignamos los valores en el formulario
        document.form_laboratorios.idlaboratorio.value = this.IdLaboratorio;
        document.form_laboratorios.nombrelaboratorio.value = this.Nombre;
        document.form_laboratorios.responsablelaboratorio.value = this.Responsable;
        document.form_laboratorios.maillaboratorio.value = this.EMail;
        document.form_laboratorios.dependencia.value = this.IdDependencia;
        document.form_laboratorios.paislaboratorio.value = this.IdPais;

        // llamamos la clase de jurisdicciones y actualizamos el select
        jurisdicciones.listaJurisdicciones("jurisdiccionlaboratorio", this.IdPais, this.CodProv);

        // llamamos la clase de localidades y cargamos el select
        localidades.nominaLocalidades("localidadlaboratorio", this.CodProv, this.CodLoc);

        // seguimos cargando
        document.form_laboratorios.direccionlaboratorio.value = this.Direccion;
        document.form_laboratorios.codigopostallaboratorio.value = this.CodigoPostal;

        // si está activo
        if (this.Activo == "Si"){
            document.form_laboratorios.activolaboratorio.checked = true;
        } else {
            document.form_laboratorios.activolaboratorio.checked = false;
        }

        // si recibe muestras
        if (this.RecibeMuestras === "Si"){

            // marca el checkbox
            document.form_laboratorios.recibemuestras.checked = true;

            // inicializa el select
            document.form_laboratorios.responsablemuestras.value = 0;

        // si no recibe muestras
        } else {

            // desmarca el checkbox
            document.form_laboratorios.recibemuestras.checked = false;

            // llama la clase de responsables
            responsables.nominaResponsables("responsablemuestras", this.CodProv, this.IdRecibe);

        }

        // si recibió las coordenadas
        if (this.Coordenadas !== ""){

            // las carga en el campo oculto y muestra el botón
            document.form_laboratorios.coordenadaslaboratorio.value = this.Coordenadas;
            $("#btnMapaLaboratorio").show();

        // si no recibió las coordenadas
        } else {

            // carga el campo
            document.form_laboratorios.coordenadaslaboratorio.value = "";

            // oculta el div
            $("#btnMapaLaboratorio").hide();

        }

        // terminamos de asignar
        document.form_laboratorios.altalaboratorio.value = this.FechaAlta;
        document.form_laboratorios.usuariolaboratorio.value = this.Usuario;
        document.form_laboratorios.observacioneslaboratorio.value = this.Observaciones;
        CKEDITOR.instances['observacioneslaboratorio'].setData(this.Observaciones);

        // si es de nivel central
        if (sessionStorage.getItem("NivelCentral") == "Si"){

            // muestra las determinaciones del laboratorio
            determinaciones.muestraDeterminaciones(this.IdLaboratorio);

        // si coincide la provincia y es responsable
        } else if ((sessionStorage.getItem("CodProv") == this.CodProv) && sessionStorage.getItem("Responsable") == "Si") {

            // muestra las determinaciones del laboratorio
            determinaciones.muestraDeterminaciones(this.IdLaboratorio);

        // si es un usuario del mismo laboratorio
        } else if (sessionStorage.getItem("Laboratorio") == this.IdLaboratorio) {

            // muestra las determinaciones del laboratorio
            determinaciones.muestraDeterminaciones(this.IdLaboratorio);

        // de otra forma
        } else {

            // carga en blanco las determinaciones
            $('#determinaciones').html("<h3>Seleccione primero un laboratorio para ver las determinaciones cargadas.</h3>");

        }

        // fija el nivel de acceso
        this.seguridadLaboratorios(this.CodProv);

        // si llamó desde el cuadro de diálogo
        if (typeof(this.layerLaboratorios) != "undefined"){
            this.layerLaboratorios.destroy();
        }

        // setea el foco
        document.form_laboratorios.nombrelaboratorio.focus();

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que abre el cuadro de diálogo de búsqueda de laboratorios
     */
    buscaLaboratorio(){

        // reiniciamos el contador
        cce.contador();

        // definimos el contenido
        var contenido = "Ingrese parte del nombre del laboratorio a buscar.<br>";
        contenido += "<form name='busca_laboratorio'>";
        contenido += "<p><b>Texto:</b> ";
        contenido += "<input type='text' name='texto' size='25'>";
        contenido += "</p>";
        contenido += "<p align='center'>";
        contenido += "<input type='button' name='btnBuscar' ";
        contenido += "       value='Buscar' ";
        contenido += "       class='boton_confirmar' ";
        contenido += "       onClick='laboratorios.encuentraLaboratorio()'>";
        contenido += "</p>";
        contenido += "</form>";

        // abrimos el formulario
        this.layerLaboratorios = new jBox('Modal', {
                                animation: 'flip',
                                closeOnEsc: true,
                                closeOnClick: false,
                                closeOnMouseleave: false,
                                closeButton: true,
                                overlay: false,
                                onCloseComplete: function(){
                                    this.destroy();
                                },
                                repositionOnContent: true,
                                content: contenido,
                                title: 'Buscar Laboratorio',
                                draggable: 'title',
                                theme: 'TooltipBorder',
                                width: 300,
                        });
        this.layerLaboratorios.open();

        // fijamos el foco
        document.busca_laboratorio.texto.focus();

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que erifica el formulario de búsqueda de laboratorios y luego
     * envía la consulta al servidor
     */
    encuentraLaboratorio(){

        // reiniciamos el contador
        cce.contador();

        // definición de variables
        var mensaje;
        var texto = document.busca_laboratorio.texto.value;

        // si no ingresó texto
        if (texto === ""){

            // presenta el mensaje y retorna
            mensaje = "Debe indicar un texto a buscar !!!";
            new jBox('Notice', {content: mensaje, color: 'red'});
            document.busca_laboratorio.texto.focus();
            return false;

        }

        // cierra el cuadro de diálogo
        this.layerLaboratorios.destroy();

        // abre el cuadro de resultados y le pasa por ajax
        // el texto a buscar
        this.layerLaboratorios = new jBox('Modal', {
                                    animation: 'flip',
                                    closeOnEsc: true,
                                    closeOnClick: false,
                                    closeOnMouseleave: false,
                                    closeButton: true,
                                    overlay: false,
                                    title: 'Resultados de la Búsqueda',
                                    draggable: 'title',
                                    theme: 'TooltipBorder',
                                    repositionOnContent: true,
                                    onCloseComplete: function(){
                                        this.destroy();
                                    },
                                    width: 800,
                                    ajax: {
                                        url: 'laboratorios/buscar.php?texto='+texto,
                                        reload: 'strict'
                                    }
                                });
        this.layerLaboratorios.open();

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que resetea el formulario de laboratorios y
     * luego fija el foco en el primer elemento
     */
    cancelaLaboratorio(){

        // reiniciamos el contador
        cce.contador();

        // reinicia el formulario y setea el foco
        document.form_laboratorios.reset();
        document.form_laboratorios.nombrelaboratorio.focus();
        return false;

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método llamado en el evento click del botón del mapa, obtiene
     * las coordenadas del formulario y luego muestra el mapa con
     * la librería de google
     */
    mapaLaboratorio(){

        // reiniciamos el contador
        cce.contador();

        // obtenemos el valor del formulario
        var coordenadas = document.getElementById("coordenadaslaboratorio").value;

        // verifica que existan las coordenadas
        if (coordenadas == ""){

            // presenta el mensaje y retorna
            mensaje = "Lo siento, no tenemos las coordenadas del laboratorio";
            new jBox('Notice', {content: mensaje, color: 'red'});
            return false;

        }

        // obtenemos las coordenadas recortando el paréntesis y
        // los signos de puntuación
        var latitud = parseFloat(coordenadas.substring(1,coordenadas.indexOf(",")));
        var longitud = parseFloat(coordenadas.substring(coordenadas.indexOf(",") + 1, coordenadas.length -1));

        // definimos el texto de la leyenda
        var leyenda = document.getElementById("nombrelaboratorio").value;
        leyenda += "<br>";
        leyenda += document.getElementById("responsablelaboratorio").value;
        leyenda += "<br>";
        leyenda += document.getElementById("direccionlaboratorio").value;

        // inicializamos el mapa
        mapas.initMap(leyenda, latitud, longitud);

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que carga en un layer emergente los laboratorios sin georreferenciar
     */
    geoLaboratorios(){

        // reiniciamos el contador
        cce.contador();

        // abrimos el formulario
        this.layerLaboratorios = new jBox('Modal', {
                                animation: 'flip',
                                closeOnEsc: true,
                                closeOnClick: false,
                                closeOnMouseleave: false,
                                closeButton: true,
                                overlay: false,
                                title: 'Georreferenciar Laboratorios',
                                draggable: 'title',
                                repositionOnContent: true,
                                theme: 'TooltipBorder',
                                onCloseComplete: function(){
                                    this.destroy();
                                },
                                ajax: {
                                    url: 'laboratorios/georreferenciar.php',
                                reload: 'strict'
                            }
                        });
        this.layerLaboratorios.open();

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param {int} id - clave del registro (y también de objeto html)
     * @param {string} domicilio - dirección a georreferenciar
     * Método que recibe como parámetros la id del registro (que también es la id
     * del texto con las coordenadas) obtiene las coordenadas gps a través del
     * servicio de google maps y luego en caso de éxito, ejecuta la consulta
     * de actualización en el servidor y muestra las coordenadas en la página
     */
    detectaLaboratorios(id, domicilio){

        // reiniciamos el contador
        cce.contador();

        // declaración de variables
        var coordenadas;

        // instancia el geocoder
        var geocoder = new google.maps.Geocoder();

        // busca la dirección
        geocoder.geocode( { 'address': domicilio}, function(results, status) {

            // si hubo resultados
            if (status == google.maps.GeocoderStatus.OK) {

                // presenta las coordenadas en el formulario
                var coordenadas = results[0].geometry.location;
                document.getElementById(id).value = coordenadas;

                // las graba en la base
                $.post('laboratorios/graba_coordenadas.php','id='+id+'&coordenadas='+coordenadas);

            // si no encontró
            } else {

                // presenta el mensaje
                mensaje = "No se ha podido detectar una\n";
                mensaje += "serie de coordenadas válidas, el\n";
                mensaje += "error fue: " + status;
                new jBox('Notice', {content: mensaje, color: 'red'});

            }

        });

    }

}