<?php

/**
 *
 * laboratorios/buscar.php
 *
 * @package     CCE
 * @subpackage  Laboratorios
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.4.0 (11/08/2018)
 * @copyright   Copyright (c) 2018, INP
 *
 * Procedimiento que recibe por get un texto a buscar y arma la
 * tabla con la grilla de resultados
 *
*/

// incluimos e instanciamos la clase
require_once ("laboratorios.class.php");
$laboratorio = new Laboratorios();

// buscamos el texto
$resultado = $laboratorio->buscaLaboratorio($_GET["texto"]);

// si no hubo resultados
if (count($resultado) == 0){

    // presenta el mensaje
    echo "<h2>No se han encontrado registros coincidentes</h2>";

// si encontró
} else {

    // define la tabla
    echo "<table width='90%' align='center' border='0' id='busca_laboratorios'>";

    // define los encabezados
    echo "<thead>";
    echo "<tr>";
    echo "<th>Nombre</th>";
    echo "<th>Responsable</th>";
    echo "<th>Jurisdicción</th>";
    echo "<th>Localidad</th>";
    echo "<th></th>";
    echo "</tr>";
    echo "</thead>";

    // define el cuerpo
    echo "<tbody>";

    // recorremos el vector
    foreach ($resultado AS $registro){

        // obtenemos el registro
        extract($registro);

        // abrimos la fila
        echo "<tr>";

        // lo agregamos
        echo "<td><small>$laboratorio</small></td>";
        echo "<td><small>$responsable</small></td>";
        echo "<td><small>$provincia_laboratorio</small></td>";
        echo "<td><small>$localidad_laboratorio</small></td>";

        // armamos el enlace
        echo "<td>";
        echo "<input type='button'
               name='btnVer'
               class='botoneditar'
               onClick='laboratorios.getDatosLaboratorio($idlaboratorio)'>";
        echo "</td>";

        // cerramos la fila
        echo "</tr>";

    }

    // cierra la tabla
    echo "</tbody>";
    echo "</table>";

    // define el div para el paginador
    echo "<div class='paging'></div>";

}

?>
<SCRIPT>

    // definimos las propiedades de la tabla
     $('#busca_laboratorios').datatable({
        pageSize: 15,
        sort: [true, true, true, true, false],
        filters: [true, true, 'select', true, false],
        filterText: 'Buscar ... '
    });

</SCRIPT>