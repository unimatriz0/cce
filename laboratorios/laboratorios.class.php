<?php

/**
 *
 * Class Laboratorios | laboratorios/laboratorios.class.php
 *
 * @package     CCE
 * @subpackage  Laboratorios
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.4.0 (12/08/2018)
 * @copyright   Copyright (c) 2018, INP
 *
 * Clase que controla las operaciones sobre la tabla de laboratorios
 *
*/

// inclusión de archivos
require_once ("../clases/conexion.class.php");

// convención para la nomenclatura de las propiedades, comienzan con una
// letra mayúscula, de tener mas de una palabra no se utilizan separadores
// y la inicial de cada palabra va en mayúscula
// para las variables recibidas como parámetro el criterio es todas en
// minúscula

// convención para la nomenclatura de los metodos, comienzan con set o get
// según asignen un valor o lo lean y luego el nombre del valor a obtener

/**
 * Definición de la clase
 * @author Claudio Invernizzi <cinvernizzi@gmail.com>
 */
class Laboratorios {

    // declaración de variables de la clase, las declaramos
    // como protected para que puedan ser heredadas pero
    // para asignarle el valor debemos crear los métodos
    protected $Link;                  // puntero a la base de datos

    // definición de las variables de la base de datos
    protected $IdLaboratorio;             // clave del laboratorio
    protected $Nombre;                    // nombre del laboratorio
    protected $Responsable;               // responsable del laboratorio
    protected $Pais;                      // país donde queda
    protected $IdPais;                    // clave del país
    protected $Localidad;                 // nombre de la localidad
    protected $IdLocalidad;               // clave indec de la localidad
    protected $Provincia;                 // nombre de la provincia
    protected $IdProvincia;               // clave indec de la provincia
    protected $Direccion;                 // dirección postal
    protected $CodigoPostal;              // código postal correspondiente
    protected $Coordenadas;               // coordenadas GPS del laboratorio
    protected $Dependencia;               // nombre de la dependencia
    protected $IdDependencia;             // clave de la dependencia
    protected $EMail;                     // dirección de correo electrónico
    protected $FechaAlta;                 // fecha de alta del registro
    protected $Activo;                    // indica si participa de los controles
    protected $RecibeMuestras;            // indica si recibe las muestras
                                          // directamente
    protected $IdRecibe;                  // clave del responsable que
                                          // recibe las muestras
    protected $NombreRecibe;              // nombre del responsable que
                                          // recibe las muestras
    protected $Observaciones;             // observaciones y comentarios
    protected $Usuario;                   // usuario que actualizó / insertó
    protected $IdUsuario;                 // clave del responsable

    // declaración de variables usadas por la clase
    protected $NivelCentral;              // si el usuario es de nivel central
    protected $RespJurisdiccional;        // indica si el usuario es reponsable
    protected $RespLaboratorio;           // el laboratorio del usuario actual
    protected $Jurisdiccion;              // jurisdicción del responsable

    /**
     * Constructor de la clase, establece la conexión con la base
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     */
    function __construct (){

        // nos conectamos a la base de datos
        $this->Link = new Conexion();

        // inicializamos las variables de clase
        $this->IdLaboratorio = 0;
        $this->Nombre = "";
        $this->Responsable = "";
        $this->Pais = "";
        $this->IdPais = 0;
        $this->Localidad = "";
        $this->IdLocalidad = "";
        $this->Provincia = "";
        $this->IdProvincia = "";
        $this->Direccion = "";
        $this->CodigoPostal = "";
        $this->Dependencia = "";
        $this->Coordenadas = "";
        $this->IdDependencia = 0;
        $this->EMail = "";
        $this->FechaAlta = "";
        $this->Activo = "";
        $this->RecibeMuestras = "";
        $this->IdRecibe = 0;
        $this->NombreRecibe = "";
        $this->Observaciones = "";
        $this->Usuario = "";

        // iniciamos sesión
        session_start();

        // si existe la sesión
        if(isset($_SESSION["ID"])){

            // obtenemos los valores de la sesión
            $this->IdUsuario = $_SESSION["ID"];
            $this->NivelCentral = $_SESSION["NivelCentral"];
            $this->RespJurisdiccional = $_SESSION["Responsable"];
            $this->RespLaboratorio = $_SESSION["Laboratorio"];
            $this->Jurisdiccion = $_SESSION["Jurisdiccion"];

        }

        // cerramos sesión
        session_write_close();

    }

    /**
     * Destructor de la clase, cierra la conexión con la base
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     */
    function __destruct(){

        // elimina el enlace a la base
        $this->Link = null;

    }

    // métodos de asignación de valores
    public function setIdLaboratorio($idlaboratorio) {

        // verifica que sea un número
        if (!is_numeric($idlaboratorio)){

            // abandona por error
            echo "La clave del laboratorio debe ser un nùmero";
            exit;

        // si es correcto
        } else {

            // lo asigna
            $this->IdLaboratorio = $idlaboratorio;

        }

    }
    public function setNombre($nombre){
        $this->Nombre = $nombre;
    }
    public function setResponsable($responsable){
        $this->Responsable = $responsable;
    }
    public function setIdPais($idpais){

        // si no es un número
        if (!is_numeric($idpais)){

            // abandona por error
            echo "La clave del país debe ser un número";
            exit;

        // si está correcto
        } else {

            // lo asigna
            $this->IdPais = $idpais;

        }

    }
    public function setIdLocalidad($idlocalidad){
        $this->IdLocalidad = $idlocalidad;
    }
    public function setDireccion($direccion){
        $this->Direccion = $direccion;
    }
    public function setCodigoPostal($codigopostal){
        $this->CodigoPostal = $codigopostal;
    }
    public function setCoordenadas($coordenadas){
        $this->Coordenadas = $coordenadas;
    }
    public function setIdDependencia($iddependencia){

        // si no es un número
        if (!is_numeric($iddependencia)){

            // abandona por error
            echo "La clave de la dependencia debe ser un número";
            exit;

        // si está correcto
        } else {

            // lo asigna
            $this->IdDependencia = $iddependencia;

        }

    }
    public function setEmail($email){
        $this->EMail = $email;
    }
    public function setActivo($activo){
        $this->Activo = $activo;
    }
    public function setRecibeMuestras($recibemuestras){
        $this->RecibeMuestras = $recibemuestras;
    }
    public function setIdRecibe($idrecibe){

        // si no es un número
        if (!is_numeric($idrecibe)){

            // abandona por error
            echo "La clave del Responsable que recibe debe ser un número";
            exit;

        // si es correcto
        } else {

            // lo asigna
            $this->IdRecibe = $idrecibe;

        }

    }
    public function setObservaciones($observaciones){
        $this->Observaciones = $observaciones;
    }

    // métodos de obtención de datos
    public function getIdLaboratorio(){
        return $this->IdLaboratorio;
    }
    public function getNombre(){
        return $this->Nombre;
    }
    public function getResponsable(){
        return $this->Responsable;
    }
    public function getPais(){
        return $this->Pais;
    }
    public function getIdPais(){
        return $this->IdPais;
    }
    public function getLocalidad(){
        return $this->Localidad;
    }
    public function getIdLocalidad(){
        return $this->IdLocalidad;
    }
    public function getProvincia(){
        return $this->Provincia;
    }
    public function getIdProvincia(){
        return $this->IdProvincia;
    }
    public function getDireccion(){
        return $this->Direccion;
    }
    public function getCodigoPostal(){
        return $this->CodigoPostal;
    }
    public function getCoordenadas(){
        return $this->Coordenadas;
    }
    public function getDependencia(){
        return $this->Dependencia;
    }
    public function getIdDependencia(){
        return $this->IdDependencia;
    }
    public function getEmail(){
        return $this->EMail;
    }
    public function getFechaAlta(){
        return $this->FechaAlta;
    }
    public function getActivo(){
        return $this->Activo;
    }
    public function getRecibeMuestras(){
        return $this->RecibeMuestras;
    }
    public function getNombreRecibe(){
        return $this->NombreRecibe;
    }
    public function getIdRecibe(){
        return $this->IdRecibe;
    }
    public function getObservaciones(){
        return $this->Observaciones;
    }
    public function getUsuario(){
        return $this->Usuario;
    }

    /**
     * Método que recibe un texto y lo busca en la tabla de laboratorios
     * retorna el vector con los registros encontrados
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param string $texto - cadena a buscar
     * @return array
     */
    public function buscaLaboratorio($texto){

        // componemos la consulta
        $consulta = "SELECT cce.laboratorios.ID AS idlaboratorio,
                            UCASE(cce.laboratorios.NOMBRE) AS laboratorio,
                            cce.laboratorios.RESPONSABLE AS responsable,
                            diccionarios.localidades.NOMLOC AS localidad_laboratorio,
                            diccionarios.provincias.NOM_PROV AS provincia_laboratorio
                     FROM cce.laboratorios INNER JOIN diccionarios.localidades ON cce.laboratorios.LOCALIDAD = diccionarios.localidades.CODLOC
                                           INNER JOIN diccionarios.provincias ON diccionarios.localidades.CODPCIA = diccionarios.provincias.COD_PROV
                     WHERE cce.laboratorios.NOMBRE LIKE '%$texto%' OR
                           cce.laboratorios.RESPONSABLE LIKE '%$texto%' OR
                           diccionarios.localidades.NOMLOC LIKE '%$texto%' OR
                           diccionarios.provincias.NOM_PROV LIKE '%$texto%'
                     ORDER BY cce.laboratorios.NOMBRE; ";

        // ejecutamos la consulta
        $resultado = $this->Link->query($consulta);

        // lo pasamos a minúsculas porque según la versión de
        // pdo lo devuelve en mayúsculas o minúsculas
        $nomina = array_change_key_case($resultado->fetchAll(PDO::FETCH_ASSOC), CASE_LOWER);

        // retornamos el vector
        return $nomina;

    }

    /**
     * Método que recibe la clave del laboratorio e instancia las variables
     * de clase con los valores del registro
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param int $idlaboratorio - clave del registro
     */
    public function getDatosLaboratorio($idlaboratorio){

        // inicializamos las variables
        $id_laboratorio = "";
        $nombre = "";
        $responsable = "";
        $pais = "";
        $idpais = "";
        $localidad = "";
        $idlocalidad = "";
        $provincia = "";
        $idprovincia = "";
        $direccion = "";
        $codigo_postal = "";
        $coordenadas = "";
        $dependencia = "";
        $iddependencia = "";
        $e_mail = "";
        $fecha_alta = "";
        $activo = "";
        $recibe_muestras = "";
        $id_recibe = "";
        $observaciones = "";
        $usuario = "";

        // componemos la consulta
        $consulta = "SELECT cce.laboratorios.ID AS id_laboratorio,
                            UCASE(cce.laboratorios.NOMBRE) AS nombre,
                            UCASE(cce.laboratorios.RESPONSABLE) AS responsable,
                            diccionarios.paises.NOMBRE AS pais,
                            diccionarios.paises.ID AS idpais,
                            diccionarios.localidades.NOMLOC AS localidad,
                            diccionarios.localidades.CODLOC AS idlocalidad,
                            diccionarios.provincias.NOM_PROV AS provincia,
                            diccionarios.provincias.COD_PROV AS idprovincia,
                            UCASE(cce.laboratorios.DIRECCION) AS direccion,
                            cce.laboratorios.CODIGO_POSTAL AS codigo_postal,
                            cce.laboratorios.COORDENADAS AS coordenadas,
                            diccionarios.dependencias.DEPENDENCIA AS dependencia,
                            diccionarios.dependencias.id_dependencia AS iddependencia,
                            cce.laboratorios.E_MAIL AS e_mail,
                            DATE_FORMAT(cce.laboratorios.FECHA_ALTA, '%d/%m/%Y') AS fecha_alta,
                            cce.laboratorios.ACTIVO AS activo,
                            cce.laboratorios.RECIBE_MUESTRAS_CHAGAS AS recibe_muestras,
                            UCASE(cce.responsables.NOMBRE) AS responsable_muestras,
                            cce.laboratorios.ID_RECIBE AS id_recibe,
                            cce.laboratorios.OBSERVACIONES AS observaciones,
                            cce.responsables.USUARIO AS usuario
                     FROM cce.laboratorios INNER JOIN diccionarios.dependencias ON cce.laboratorios.DEPENDENCIA = diccionarios.dependencias.id_dependencia
                                           INNER JOIN diccionarios.localidades ON cce.laboratorios.LOCALIDAD = diccionarios.localidades.CODLOC
                                           INNER JOIN diccionarios.provincias ON diccionarios.localidades.CODPCIA = diccionarios.provincias.COD_PROV
                                           LEFT JOIN diccionarios.paises ON cce.laboratorios.PAIS = diccionarios.paises.ID
                                           LEFT JOIN cce.responsables ON cce.laboratorios.id_recibe = cce.responsables.id
                                           INNER JOIN cce.responsables AS usuarios ON cce.laboratorios.USUARIO = usuarios.ID
                     WHERE cce.laboratorios.ID = '$idlaboratorio';";

        // ejecutamos la consulta
        $resultado = $this->Link->query($consulta);

        // lo pasamos a minúsculas porque según la versión de
        // pdo lo devuelve en mayúsculas o minúsculas
        $fila = array_change_key_case($resultado->fetch(PDO::FETCH_ASSOC), CASE_LOWER);

        // obtenemos el registro
        extract($fila);

        // asignamos los valores a las variables de clase
        $this->IdLaboratorio = $id_laboratorio;
        $this->Nombre = $nombre;
        $this->Responsable = $responsable;
        $this->Pais = $pais;
        $this->IdPais = $idpais;
        $this->Localidad = $localidad;
        $this->IdLocalidad = $idlocalidad;
        $this->Provincia = $provincia;
        $this->IdProvincia = $idprovincia;
        $this->Direccion = $direccion;
        $this->CodigoPostal = $codigo_postal;
        $this->Coordenadas = $coordenadas;
        $this->Dependencia = $dependencia;
        $this->IdDependencia = $iddependencia;
        $this->EMail = $e_mail;
        $this->FechaAlta = $fecha_alta;
        $this->Activo = $activo;
        $this->RecibeMuestras = $recibe_muestras;
        $this->NombreRecibe = $responsable_muestras;
        $this->IdRecibe = $id_recibe;
        $this->Observaciones = $observaciones;
        $this->Usuario = $usuario;

    }

    /**
     * Método que produce la consulta de inserción o edición según
     * el caso y retorna la id del registro afectado
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @return int
     */
    public function grabarLaboratorio(){

        // si está definida la clave
        if ($this->IdLaboratorio != 0){
            $this->editaLaboratorio();
        } else {
            $this->nuevoLaboratorio();
        }

        // retorna la id
        return $this->IdLaboratorio;

    }

    /**
     * Método que inserta un nuevo registro en la tabla de laboratorios
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     */
    protected function nuevoLaboratorio(){

        // produce la consulta de inserción
        $consulta = "INSERT INTO cce.laboratorios
                            (NOMBRE,
                             RESPONSABLE,
                             PAIS,
                             LOCALIDAD,
                             DIRECCION,
                             CODIGO_POSTAL,
                             DEPENDENCIA,
                             E_MAIL,
                             ACTIVO,
                             RECIBE_MUESTRAS_CHAGAS,
                             ID_RECIBE,
                             OBSERVACIONES,
                             USUARIO)
                             VALUES
                            (:nombre,
                             :responsable,
                             :idpais,
                             :idlocalidad,
                             :direccion,
                             :codigo_postal,
                             :iddependencia,
                             :e_mail,
                             :activo,
                             :recibe_muestras,
                             :id_recibe,
                             :observaciones,
                             :idusuario);";

        // asignamos la consulta
        $psInsertar = $this->Link->prepare($consulta);

        // asignamos los parámetros de la consulta
        $psInsertar->bindParam(":nombre", $this->Nombre);
        $psInsertar->bindParam(":responsable", $this->Responsable);
        $psInsertar->bindParam(":idpais", $this->IdPais);
        $psInsertar->bindParam(":idlocalidad", $this->IdLocalidad);
        $psInsertar->bindParam(":direccion", $this->Direccion);
        $psInsertar->bindParam(":codigo_postal", $this->CodigoPostal);
        $psInsertar->bindParam(":iddependencia", $this->IdDependencia);
        $psInsertar->bindParam(":e_mail", $this->EMail);
        $psInsertar->bindParam(":activo", $this->Activo);
        $psInsertar->bindParam(":recibe_muestras", $this->RecibeMuestras);
        $psInsertar->bindParam(":id_recibe", $this->IdRecibe);
        $psInsertar->bindParam(":observaciones", $this->Observaciones);
        $psInsertar->bindParam(":idusuario", $this->IdUsuario);

        // ejecutamos la edición
        $resultado = $psInsertar->execute();

        // si salió todo bien
        if ($resultado){

            // obtiene la id del registro insertado
            $this->IdLaboratorio = $this->Link->lastInsertId();

        // si hubo un error
        } else {

            // inicializa la clave y muestra el error
            $this->IdLaboratorio = 0;
            echo $resultado;

        }

    }

    /**
     * Método que edita el registro de la tabla de laboratorios
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     */
    protected function editaLaboratorio(){

        // produce la consulta de edición
        $consulta = "UPDATE cce.laboratorios SET
                            NOMBRE = :nombre,
                            RESPONSABLE = :responsable,
                            PAIS = :idpais,
                            LOCALIDAD = :idlocalidad,
                            DIRECCION = :direccion,
                            CODIGO_POSTAL = :codigo_postal,
                            DEPENDENCIA = :iddependencia,
                            E_MAIL = :e_mail,
                            ACTIVO = :activo,
                            RECIBE_MUESTRAS_CHAGAS = :recibe_muestras,
                            ID_RECIBE = :idrecibe,
                            OBSERVACIONES = :observaciones,
                            USUARIO = :idusuario
                     WHERE cce.laboratorios.ID = :idlaboratorio;";

        // asignamos la consulta
        $psInsertar = $this->Link->prepare($consulta);

        // asignamos los parámetros de la consulta
        $psInsertar->bindParam(":nombre", $this->Nombre);
        $psInsertar->bindParam(":responsable", $this->Responsable);
        $psInsertar->bindParam(":idpais", $this->IdPais);
        $psInsertar->bindParam(":idlocalidad", $this->IdLocalidad);
        $psInsertar->bindParam(":direccion", $this->Direccion);
        $psInsertar->bindParam(":codigo_postal", $this->CodigoPostal);
        $psInsertar->bindParam(":iddependencia", $this->IdDependencia);
        $psInsertar->bindParam(":e_mail", $this->EMail);
        $psInsertar->bindParam(":activo", $this->Activo);
        $psInsertar->bindParam(":recibe_muestras", $this->RecibeMuestras);
        $psInsertar->bindParam(":idrecibe", $this->IdRecibe);
        $psInsertar->bindParam(":observaciones", $this->Observaciones);
        $psInsertar->bindParam(":idusuario", $this->IdUsuario);
        $psInsertar->bindParam(":idlaboratorio", $this->IdLaboratorio);

        // ejecutamos la edición
        $resultado = $psInsertar->execute();

        // si hubo un error
        if (!$resultado){

            // inicializa la clave y muestra el error
            $this->IdLaboratorio = 0;
            echo $resultado;

        }

    }

    /**
     * Método que recibe como parámetro el nombre de un laboratorio y la
     * provincia del mismo (puede haber mas de un laboratorio con el mismo
     * nombre) y retorna la clave del laboratorio
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param string $provincia - nombre de la provincia
     * @param string $laboratorio - nombre del laboratorio
     * @return int
     */
    public function getClaveLaboratorio($provincia, $laboratorio){

        // inicializamos las variables
        $id_laboratorio = 0;

        // componemos la consulta
        $consulta = "SELECT cce.laboratorios.ID AS id_laboratorio
                     FROM cce.laboratorios INNER JOIN diccionarios.localidades ON cce.laboratorios.LOCALIDAD = diccionarios.localidades.CODLOC
                                           INNER JOIN diccionarios.provincias ON diccionarios.localidades.CODPCIA = diccionarios.provincias.COD_PROV
                     WHERE cce.laboratorios.NOMBRE = '$laboratorio' AND
                           diccionarios.provincias.NOM_PROV = '$provincia'; ";

        // ejecutamos la consulta
        $resultado = $this->Link->query($consulta);

        // lo pasamos a minúsculas porque según la versión de
        // pdo lo devuelve en mayúsculas o minúsculas
        $registro = array_change_key_case($resultado->fetch(PDO::FETCH_ASSOC), CASE_LOWER);

        // obtenemos el registro y retornamos
        extract($registro);
        return $id_laboratorio;

    }

    /**
     * Método utilizado en el abm de usuarios para autorizar a cargar
     * específicamente un laboratorio, recibe parte del nombre del
     * laboratorio y la provincia y retorna los registros coincidentes
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param string $laboratorio - nombre del laboratorio
     * @param string $provincia - nombre de la provincia
     * @return array
     */
    public function nominaLaboratorios($laboratorio, $provincia){

        // componemos la consulta
        $consulta = "SELECT cce.laboratorios.ID AS id_laboratorio,
                            cce.laboratorios.NOMBRE AS laboratorio
                     FROM cce.laboratorios INNER JOIN diccionarios.localidades ON cce.laboratorios.LOCALIDAD = diccionarios.localidades.CODLOC
                                           INNER JOIN diccionarios.provincias ON diccionarios.localidades.CODPCIA = diccionarios.provincias.COD_PROV
                     WHERE diccionarios.provincias.NOM_PROV = '$provincia' AND
                           cce.laboratorios.NOMBRE LIKE '$laboratorio'
                     ORDER BY cce.laboratorios.NOMBRE; ";

        // ejecutamos la consulta
        $resultado = $this->Link->query($consulta);

        // lo pasamos a minúsculas porque según la versión de
        // pdo lo devuelve en mayúsculas o minúsculas
        $registros = array_change_key_case($resultado->fetchAll(PDO::FETCH_ASSOC), CASE_LOWER);

        // retornamos el vector
        return $registros;

    }

    /**
     * Método llamado en la edición de usuarios, recibe como parámetros
     * el pais, la jurisdicción y parte del nombre de un laboratorio
     * retorna un array con los registros que cumplen con el criterio
     * que luego el sistema muestra en una grilla
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param string $pais - nombre del país
     * @param string $provincia - nombre de la provincia
     * @param string $laboratorio - nombre del laboratorio
     */
    public function listaLaboratorios($pais, $provincia = null, $laboratorio){

        // si la provincia es nulo
        if ($provincia == null){

            // Compone la consulta
            $consulta = "SELECT cce.laboratorios.ID AS idlaboratorio,
                                diccionarios.paises.NOMBRE AS pais,
                                cce.laboratorios.NOMBRE AS laboratorio
                         FROM cce.laboratorios INNER JOIN diccionarios.paises ON cce.laboratorios.PAIS = diccionarios.paises.ID
                         WHERE cce.laboratorios.NOMBRE LIKE '%$laboratorio%' AND
                               diccionarios.paises.NOMBRE = '$pais'
                         ORDER BY cce.laboratorios.NOMBRE; ";

        // si recibió provincia
        } else {

            // Compone la consulta
            $consulta = "SELECT cce.laboratorios.ID AS idlaboratorio,
                                diccionarios.paises.NOMBRE AS pais,
                                diccionarios.provincias.NOM_PROV AS jurisdiccion,
                                cce.laboratorios.NOMBRE AS laboratorio
                         FROM cce.laboratorios INNER JOIN diccionarios.paises ON cce.laboratorios.PAIS = diccionarios.paises.ID
                                               INNER JOIN diccionarios.localidades ON cce.laboratorios.LOCALIDAD = diccionarios.localidades.CODLOC
                                               INNER JOIN diccionarios.provincias ON diccionarios.localidades.CODPCIA = diccionarios.provincias.COD_PROV
                         WHERE cce.laboratorios.NOMBRE LIKE '%$laboratorio%' AND
                               diccionarios.paises.NOMBRE = '$pais' AND
                               diccionarios.provincias.NOM_PROV = '$provincia'
                         ORDER BY cce.laboratorios.NOMBRE; ";

        }

        // ejecutamos la consulta
        $resultado = $this->Link->query($consulta);

        // lo pasamos a minúsculas porque según la versión de
        // pdo lo devuelve en mayúsculas o minúsculas
        $registros = array_change_key_case($resultado->fetchAll(PDO::FETCH_ASSOC), CASE_LOWER);

        // retornamos el vector
        return $registros;

    }

    /**
     * Método que retorna un vector  vector con los laboratorios
     * activos de esa provincia que recibe como parámetro
     * junto con el referente de quien recibe las muestras
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param string $provincia - nombre de la jurisdicción
     */
    public function laboratoriosParticipantes($provincia){

        // componemos la consulta
        $consulta = "SELECT cce.laboratorios.ID AS id_laboratorio,
                            cce.laboratorios.NOMBRE AS laboratorio,
                            diccionarios.localidades.NOMLOC AS localidad,
                            cce.laboratorios.DIRECCION AS direccion,
                            diccionarios.dependencias.DEPENDENCIA AS dependencia,
                            cce.laboratorios.RECIBE_MUESTRAS_CHAGAS AS recibe,
                            cce.responsables.NOMBRE AS recibe_muestras
                     FROM cce.laboratorios INNER JOIN diccionarios.dependencias ON cce.laboratorios.DEPENDENCIA = diccionarios.dependencias.ID
                                           INNER JOIN diccionarios.localidades ON cce.laboratorios.LOCALIDAD = diccionarios.localidades.CODLOC
                                           INNER JOIN diccionarios.provincias ON cce.localidades.CODPCIA = diccionarios.provincias.COD_PROV
                                           LEFT JOIN cce.responsables ON cce.laboratorios.ID_RECIBE = responsables.ID
                     WHERE diccionarios.provincias.NOM_PROV = '$provincia' AND
                           cce.laboratorios.ACTIVO = 'Si'
                     ORDER BY cce.laboratorios.NOMBRE,
                              diccionarios.localidades.NOMLOC;";

        // ejecutamos la consulta
        $resultado = $this->Link->query($consulta);

        // lo pasamos a minúsculas porque según la versión de
        // pdo lo devuelve en mayúsculas o minúsculas
        $registros = array_change_key_case($resultado->fetchAll(PDO::FETCH_ASSOC), CASE_LOWER);

        // retornamos el vector
        return $registros;

    }

    /**
     * Método utilizado en las altas y ediciones para evitar el duplicado
     * de direcciones de correo, retorna el número de registros encontrados
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param string $mail - la dirección a verificar
     * @param int $id - clave del registro
     * @return int
     */
    public function verificaMail($mail, $id = false){

        // inicializamos las variables
        $registros = 0;

        // si no recibió la clave
        if ($id == false){

            // compone la consulta
            $consulta = "SELECT COUNT(*) AS registros
                         FROM cce.laboratorios
                         WHERE cce.laboratorios.E_MAIL = '$mail';";

        // si recibió la clave
        } else {

            // verificamos que no la tenga otro usuario
            $consulta = "SELECT COUNT(*) AS registros
                         FROM cce.laboratorios
                         WHERE cce.laboratorios.E_MAIL = '$mail' AND
                               cce.laboratorios.ID != '$id';";

        }

        // obtenemos el registro y retornamos
        $resultado = $this->Link->query($consulta);
        $registro = array_change_key_case($resultado->fetch(PDO::FETCH_ASSOC), CASE_LOWER);
        extract($registro);
        return $registros;

    }

    /**
     * Método que retorna el array con la nómina de laboratorios sin georreferenciar
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @return array
     */
     public function sinGeorreferenciar(){

         // componemos la consulta en la vista
         $consulta = "SELECT cce.vw_laboratorios.id AS id,
                             UPPER(cce.vw_laboratorios.laboratorio) AS laboratorio,
                             cce.vw_laboratorios.pais AS pais,
                             cce.vw_laboratorios.provincia AS provincia,
                             cce.vw_laboratorios.localidad AS localidad,
                             cce.vw_laboratorios.direccion AS direccion
                       FROM vw_laboratorios
                       WHERE vw_laboratorios.coordenadas = '' OR
                             ISNULL(vw_laboratorios.coordenadas) OR
                             vw_laboratorios.coordenadas = 'undefined'
                       ORDER BY cce.vw_laboratorios.provincia,
                                cce.vw_laboratorios.laboratorio;";

        // obtenemos el array y retornamos
        $resultado = $this->Link->query($consulta);
        $fila = array_change_key_case($resultado->fetchAll(PDO::FETCH_ASSOC), CASE_LOWER);
        return $fila;

     }

    /**
     * Método que recibe como parámetros la id de un laboratorio y las
     * coordenadas gps, actualiza entonces la base de datos
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param int $id - clave del laboratorio
     * @param string $coordenadas - coordenadas gps
     */
    public function grabaCoordenadas($id, $coordenadas){

        // componemos la consulta y ejecutamos la consulta
        $consulta = "UPDATE cce.laboratorios SET
                            coordenadas = '$coordenadas'
                     WHERE cce.laboratorios.id = '$id';";
        $this->Link->exec($consulta);

    }

}

?>