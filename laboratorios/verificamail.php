<?php

/**
 *
 * laboratorios/verifica_mail.php
 *
 * @package     CCE
 * @subpackage  Laboratorios
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.4.0 (11/08/2018)
 * @copyright   Copyright (c) 2018, INP
 *
 * Procedimiento que recibe por get una dirección de correo y verifica
 * si se encuentra en la base, retorna el número de registros encontrados
 *
*/

// incluimos e instanciamos la clase
require_once ("laboratorios.class.php");
$laboratorio = new Laboratorios();

// si recibió la id y está editando
if (!empty($_GET["id"])){

    // verificamos si no está repitiendo
    $registros = $laboratorio->verificaMail($_GET["mail"], $_GET["id"]);

} else {

    // verificamos si se encuentra el laboratorio
    $registros = $laboratorio->verificaMail($_GET["mail"]);

}

// retornamos el estado
echo json_encode(array("Registros" => $registros));

?>