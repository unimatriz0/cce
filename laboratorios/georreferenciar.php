<?php

/**
 *
 * laboratorios/georreferenciar.php
 *
 * @package     CCE
 * @subpackage  Laboratorios
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.4.0 (09/08/2018)
 * @copyright   Copyright (c) 2018, INP
 *
 * Procedimiento que presenta la grilla con los laboratorios sin
 * georreferenciar
 *
*/

// inclusión de archivos
require_once("laboratorios.class.php");
$laboratorios = new Laboratorios();

// obtenemos la nómina de laboratorios sin georreferenciar
$georreferenciar = $laboratorios->sinGeorreferenciar();

// si hubo registros
if (count($georreferenciar) != 0){

    // definimos la tabla
    echo "<table width='90%' align='center' border='0' id='geolaboratorios'>";

    // definimos los encabezados
    echo "<thead>";
    echo "<tr>";
    echo "<td>País</td>";
    echo "<td>Jurisdicción</td>";
    echo "<td>Laboratorio</td>";
    echo "<td>Dirección</td>";
    echo "<td>Coordenadas</td>";
    echo "<td></td>";
    echo "</tr>";
    echo "</thead>";

    // definimos el cuerpo
    echo "<tbody>";

    // recorremos el vector
    foreach($georreferenciar AS $registro){

        // obtenemos el registro
        extract($registro);

        // abrimos la fila
        echo "<tr>";

        // lo presentamos
        echo "<td>$pais</td>";
        echo "<td>$provincia</td>";
        echo "<td>$laboratorio</td>";
        echo "<td>$direccion</td>";

        // definimos un imput para las coordenadas
        echo "<td>";
        echo "<input type='text'
                     name='nuevacoordlaboratorio'
                     id='$id'
                     size='15'
                     title='Coordenadas GPS'
                     readonly>";
        echo "</td>";

        // componemos el domicilio
        $domicilio = $direccion . " - " . $localidad . " - " . $provincia . " - " . $pais;

        // el botón georreferenciar
        echo "<td>";
        echo "<input type='button'
                     name='btnCoordenadas'
                     id='btnCoordenadas'
                     class='botonbuscar'
                     title='Busca las coordenadas GPS'
                     onClick='laboratorios.detectaLaboratorios(" . chr(34) . $id . chr(34) . ", " . chr(34) . $domicilio . chr(34) . ")'>";
        echo "</td>";

        // cerramos la fila
        echo "</tr>";

    }

    // cerramos la tabla
    echo "</tbody></table>";

    // define el div para el paginador
    echo "<div class='paging'></div>";

// si están todos georreferenciados
} else{

    // presenta el mensaje
    echo "<h2>Todos los laboratorios están georreferenciados</h2>";

}
?>
<SCRIPT>

    // definimos las propiedades de la tabla
     $('#geolaboratorios').datatable({
        pageSize: 15,
        sort:    [true,     true,     true, true, false, false],
        filters: ['select', 'select', true, true, false, false],
        filterText: 'Buscar ... '
    });

</SCRIPT>
