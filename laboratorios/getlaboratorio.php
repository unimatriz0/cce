<?php

/**
 *
 * laboratorios/getlaboratorio.php
 *
 * @package     CCE
 * @subpackage  Laboratorios
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.4.0 (11/08/2018)
 * @copyright   Copyright (c) 2018, INP
 *
 * Procedimiento que recibe por get la clave de un laboratorio y
 * obtiene los datos del mismo, los que retorna en un array json
 *
*/

// incluimos e instanciamos la clase
require_once ("laboratorios.class.php");
$laboratorio = new Laboratorios();

// obtenemos los datos del registro
$laboratorio->getDatosLaboratorio($_GET["id"]);

// armamos la matriz y retornamos
echo json_encode(array("ID" =>                $laboratorio->getIdLaboratorio(),
                       "Nombre" =>            $laboratorio->getNombre(),
                       "Responsable" =>       $laboratorio->getResponsable(),
                       "IdLocalidad" =>       $laboratorio->getIdLocalidad(),
                       "IdProvincia" =>       $laboratorio->getIdProvincia(),
                       "IdPais" =>            $laboratorio->getIdPais(),
                       "Direccion" =>         $laboratorio->getDireccion(),
                       "Coordenadas" =>       $laboratorio->getCoordenadas(),
                       "CodigoPostal" =>      $laboratorio->getCodigoPostal(),
                       "Dependencia" =>       $laboratorio->getIdDependencia(),
                       "Mail" =>              $laboratorio->getEmail(),
                       "FechaAlta" =>         $laboratorio->getFechaAlta(),
                       "Activo" =>            $laboratorio->getActivo(),
                       "RecibeMuestras" =>    $laboratorio->getRecibeMuestras(),
                       "IdRecibe" =>          $laboratorio->getIdRecibe(),
                       "Observaciones" =>     $laboratorio->getObservaciones(),
                       "Usuario" =>           $laboratorio->getUsuario()));

?>