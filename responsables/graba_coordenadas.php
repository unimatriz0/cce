<?php

/**
 *
 * responsables/graba_coordenadas.php
 *
 * @package     CCE
 * @subpackage  Responsables
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.4.0 (14/08/2018)
 * @copyright   Copyright (c) 2018, INP
 *
 * Procedimiento que recibe por post las coordenadas gps del
 * responsable y su id, actualiza la base de datos
 *
*/

// incluimos e instanciamos las clases
require_once ("responsables.class.php");
$responsable = new Responsables();

// grabamos los datos recibidos
$responsable->grabaCoordenadas($_POST["id"], $_POST["coordenadas"]);

?>
