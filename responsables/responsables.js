/*
 * Nombre: responsables.js
 * Autor: Lic. Claudio Invernizzi
 * E-Mail: cinvernizzi@gmail.com
 * Fecha: 19/12/2016
 * Licencia: GPL
 * Comentarios: Serie de funciones y procedimientos utilizados por el
 *              sistema de control de calidad en los formularios de
 *              responsables
 */

 /**
 *
 * Atención, utilizamos el nuevo estandard definido en ECMAScript 2015 (ES6)
 * este permite definir clases con el mètodo ya utilizado en php y java
 * como creo que es mas claro lo utilizaremos, la compatibilidad está
 * anunciada para firefox y chrome así que no deberìa haber problemas
 *
 */

/* jshint esversion: 6 */

/**
 * @author Claudio Invernizzi <cinvernizzi@gmail.com>
 * @class Clase que controla las operaciones del formulario de
 * responsables
 */
class Responsables {

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @constructor
     */
    constructor(){

        // inicializamos las variables de clase
        this.initResponsables();

        // cargamos el formulario
        $("#form_responsables").load("responsables/form_responsables.html");

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@ŋmail.com>
     * Método que inicializa las variables de clase
     */
    initResponsables(){

        // inicializamos las variables
        this.IdResponsable = 0;           // clave del registro
        this.Nombre = "";                 // nombre del responsable
        this.Institucion = "";            // nombre de la institución
        this.Cargo = "";                  // descripción del cargo que ocupa
        this.Email = "";                  // correo electrónico del responsable
        this.Telefono = "";               // teléfono con prefijo
        this.IdPais = 0;                  // clave del país
        this.CodProv = "";                // clave indec de la jurisdicción
        this.CodLoc = "";                 // clave indec de la localidad
        this.Direccion = "";              // dirección postal
        this.CodigoPostal = "";           // código postal de la dirección
        this.Coordenadas = "";            // coordenadas gps
        this.Responsable = "";            // si es o no responsable
        this.IdLaboratorio = 0;           // laboratorio autorizado a cargar
        this.Laboratorio = "";            // nombre del laboratorio
        this.Activo = "";                 // si está activo
        this.Observaciones = "";          // observaciones y comentarios
        this.Usuario = "";                // nombre de usuario
        this.FechaAlta = "";              // fecha de alta del registro
        this.NivelCentral = "";           // si es del nivel central
        this.Correcto = true;             // switch de usuario y mail repetido
        this.layerResponsables = "";      // layer emergente

    }

    // métodos de asignación de valores
    setIdResponsable(idresponsable){
        this.IdResponsable = idresponsable;
    }
    setNombre(nombre){
        this.Nombre = nombre;
    }
    setInstitucion(institucion){
        this.Institucion = institucion;
    }
    setCargo(cargo){
        this.Cargo = cargo;
    }
    setEmail(email){
        this.Email = email;
    }
    setTelefono(telefono){
        this.Telefono = telefono;
    }
    setIdPais(idpais){
        this.IdPais = idpais;
    }
    setCodProv(codprov){
        this.CodProv = codprov;
    }
    setCodLoc(codloc){
        this.CodLoc = codloc;
    }
    setDireccion(direccion){
        this.Direccion = direccion;
    }
    setCodigoPostal(codigopostal){
        this.CodigoPostal = codigopostal;
    }
    setCoordenadas(coordenadas){
        this.Coordenadas = coordenadas;
    }
    setResponsable(responsable){
        this.Responsable = responsable;
    }
    setIdLaboratorio(idlaboratorio){
        this.IdLaboratorio = idlaboratorio;
    }
    setLaboratorio(laboratorio){
        this.Laboratorio = laboratorio;
    }
    setActivo(activo){
        this.Activo = activo;
    }
    setObservaciones(observaciones){
        this.Observaciones = observaciones;
    }
    setUsuario(usuario){
        this.Usuario = usuario;
    }
    setFechaAlta(fecha){
        this.FechaAlta = fecha;
    }
    setNivelCentral(nivel){
        this.NivelCentral = nivel;
    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método llamado en el evento onchange del select, obtiene el
     * valor seleccionado del formulario y llama la clase de
     * jurisdicciones para cargar el select de provincias
     */
    cargaProvincias(){

        // obtenemos la clave del país
        var idpais = document.getElementById("pais").value;

        // verificamos que exista un país seleccionado
        if (idpais != 0){

            // llamamos al método de la clase de jurisdicciones
            jurisdicciones.listaJurisdicciones("jurisdiccion", idpais);

        }

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método llamado el evento onchange del select de provincias del
     * formulario, obtiene el valor seleccionado y llama la clase de
     * localidades para actualizar el select
     */
    cargaLocalidades(){

        // obtenemos la provincia
        var codprov = document.getElementById("jurisdiccion").value;

        // verificamos que exista una provincia seleccionada
        if (codprov != 0){

            // llamamos el método de la clase localidades
            localidades.nominaLocalidades("localidad", codprov);

        }

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param {string} idProvincia - clave indec de la provincia
     * Método que recibe como parámetro la id de la provincia del
     * responsable y muestra u oculta el div con los botones
     * de acuerdo a su nivel de acceso
     */
    seguridadResponsables(idProvincia){

        // si es de nivel central
        if (sessionStorage.getItem("NivelCentral") === "Si"){

            // mostramos los botones
            $("#btnGrabar").show();
            $("#btnCancelar").show();

        // si es responsable de la misma provincia
        } else if (sessionStorage.getItem("Responsable") === "Si" && sessionStorage.getItem("CodProv") === idProvincia){

            // mostramos los botones
            $("#btnGrabar").show();
            $("#btnCancelar").show();

        // si es el registro de si mismo
        } else if (sessionStorage.getItem("ID") === document.getElementById("idresponsable").value){

            // mostramos los botones
            $("#btnGrabar").show();
            $("#btnCancelar").show();

        // en cualquier otro caso
        } else {

            // ocultamos los botones
            $("#btnGrabar").hide();
            $("#btnCancelar").hide();

        }

    }

    /**
     * @author Claudio Invernizzi  <cinvernizzi@gmail.com>
     * @param {int} id - clave del registro
     * Método que recibe como parámetro la id de un usuario, llama
     * por ajax y obtiene los datos del registro y asigna los valores
     * en las variables de clase
     */
    getDatosUsuario(id){

        // reiniciamos el contador
        cce.contador();

        // llama la rutina php para obtener los datos del usuario
        $.get('responsables/getusuario.php', 'id='+id,
            function(data){

                // asignamos los valores en las variables de clase
                responsables.setIdResponsable(data.ID);
                responsables.setNombre(data.Nombre);
                responsables.setUsuario(data.Usuario);
                responsables.setInstitucion(data.Institucion);
                responsables.setCargo(data.Cargo);
                responsables.setEmail(data.Mail);
                responsables.setTelefono(data.Telefono);
                responsables.setIdPais(data.IdPais);
                responsables.setCodProv(data.IdProvincia);
                responsables.setCodLoc(data.IdLocalidad);
                responsables.setDireccion(data.Direccion);
                responsables.setCodigoPostal(data.CodigoPostal);
                responsables.setResponsable(data.ResponsableChagas);
                responsables.setCoordenadas(data.Coordenadas);
                responsables.setLaboratorio(data.Laboratorio);
                responsables.setIdLaboratorio(data.IdLaboratorio);
                responsables.setActivo(data.Activo);
                responsables.setNivelCentral(data.NivelCentral);
                responsables.setFechaAlta(data.FechaAlta);
                responsables.setObservaciones(data.Observaciones);

                // llamamos la carga del registro
                responsables.muestraUsuario();

            }, "json");

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que carga el formulario a partir de los valores de las
     * variables de clase
     */
    muestraUsuario(){

        // asignamos los valores en el formulario
        document.form_responsables.idresponsable.value = this.IdResponsable;
        document.form_responsables.nombre.value = this.Nombre;
        document.form_responsables.usuario.value = this.Usuario;

        // fijamos el usuario como readonly
        document.getElementById("usuario").readOnly = true;

        // seguimos cargando
        document.form_responsables.institucion.value = this.Institucion;
        document.form_responsables.cargo.value = this.Cargo;
        document.form_responsables.e_mail.value = this.Email;
        document.form_responsables.telefono.value = this.Telefono;

        // fijamos los valores del select de país
        document.form_responsables.pais.value = this.IdPais;

        // llamamos el método de la clase jurisdicciones
        jurisdicciones.listaJurisdicciones("jurisdiccion", this.IdPais, this.CodProv);

        // llamamos el método de la clase localidades
        localidades.nominaLocalidades("localidad", this.CodProv, this.CodLoc);

        // seguimos cargando
        document.form_responsables.direccion.value = this.Direccion;
        document.form_responsables.codigopostal.value = this.CodigoPostal;

        // si es responsable
        if (this.Responsable == "Si"){
            document.form_responsables.responsable.checked = true;
        } else {
            document.form_responsables.responsable.checked = false;
        }

        // si recibió las coordenadas
        if (this.Coordenadas != ""){

            // las carga en el campo oculto y muestra el botón
            document.form_responsables.coordenadas.value = this.Coordenadas;
            $("#btnMapa").show();

        // si no recibió las coordenadas
        } else {

            // oculta el div e inicializa el campo
            document.form_responsables.coordenadas.value = "";
            $("#btnMapa").hide();

        }

        // continúa cargando
        document.form_responsables.idlaboratorioresponsable.value = this.IdLaboratorio;
        document.form_responsables.laboratorio.value = this.Laboratorio;

        // si está activo
        if (this.Activo === "Si"){
            document.form_responsables.activo.checked = true;
        } else {
            document.form_responsables.activo.checked = false;
        }

        // si es de nivel central
        if (this.NivelCentral === "Si"){
            document.form_responsables.nivelcentral.checked = true;
        } else {
            document.form_responsables.nivelcentral.checked = false;
        }

        // terminamos de asignar
        document.form_responsables.fechaalta.value = this.FechaAlta;
        document.form_responsables.observaciones.value = this.Observaciones;
        CKEDITOR.instances['observaciones'].setData(this.Observaciones);

        // configuramos el acceso de seguridad
        this.seguridadResponsables(this.CodLoc);

        // verificamos si puede mostrar el laboratorio
        if (sessionStorage.getItem("Responsable") == "Si"){

            // llama la rutina simplemente para estar seguros
            // que el formulario está en blanco
            laboratorios.nuevoLaboratorio();

        // si es el responsable de un laboratorio
        } else if (this.IdLaboratorio == sessionStorage.getItem("Laboratorio")){

            // muestra el laboratorio
            laboratorios.muestraLaboratorio(this.IdLaboratorio);

        // si no es responsable y no es usuario de ese laboratorio
        } else {

            // cargamos el formulario en blanco
            laboratorios.nuevoLaboratorio();

        }

        // si fue llamado desde el diálogo de búsqueda
        if (typeof(this.layerResponsables) != "string"){
            this.layerResponsables.destroy();
        }

        // reiniciamos el contador
        cce.contador();

        // fijamos el foco
        document.getElementById("nombre").focus();

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que limpia el formulario de responsables preparándolo
     * para un alta
     */
    nuevoResponsable(){

        // inicializamos las variables de clase
        this.initResponsables();

        // asignamos los valores en el formulario
        document.form_responsables.idresponsable.value = "";
        document.form_responsables.nombre.value = "";
        document.form_responsables.usuario.value = "";

        // cambiamos el atributo readonly del usuario
        document.getElementById("usuario").readOnly = false;

        // seguimos inicializando
        document.form_responsables.institucion.value = "";
        document.form_responsables.cargo.value = "";
        document.form_responsables.e_mail.value = "";
        document.form_responsables.telefono.value = "";

        // fijamos los valores de los select
        document.form_responsables.pais.selectedIndex = 0;
        document.form_responsables.jurisdiccion.selectedIndex = 0;
        document.form_responsables.localidad.selectedIndex = 0;

        // seguimos cargando
        document.form_responsables.direccion.value = "";
        document.form_responsables.codigopostal.value = "";
        document.form_responsables.responsable.checked = false;
        document.form_responsables.laboratorio.value = "";
        document.form_responsables.idlaboratorioresponsable.value = "";
        document.form_responsables.activo.checked = false;
        document.form_responsables.nivelcentral.checked = false;

        // obtenemos la fecha actual y la fijamos
        document.form_responsables.fechaalta.value = fechaActual();

        // terminamos de asignar
        document.form_responsables.observaciones.value = "";
        CKEDITOR.instances['observaciones'].setData();
        document.form_responsables.coordenadas.value = "";

        // ocultamos el botón mapa
        $("#btnMapa").hide();

        // reiniciamos el contador
        cce.contador();

        // fijamos el foco
        document.form_responsables.nombre.focus();

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que abre el cuadro de diálogo pidiendo el texto
     * a buscar en la tabla de responsables
     */
    buscaResponsable(){

        // verificamos que no haya un layer activo
        if (typeof(this.layerResponsables) != "string"){
            this.layerResponsables.destroy();
        }

        // reiniciamos el contador
        cce.contador();

        // definimos el contenido
        var contenido =  "Ingrese parte del nombre del usuario a buscar.<br>";
        contenido += "<form name='busca_usuario'>";
        contenido += "<p><b>Texto:</b> ";
        contenido += "<input type='text' name='texto' id='texto' size='25'>";
        contenido += "</p>";
        contenido += "<p align='center'>";
        contenido += "<input type='button' name='btnBuscar' ";
        contenido += "       value='Buscar' ";
        contenido += "       class='boton_confirmar' ";
        contenido += "       onClick='responsables.encuentraResponsable()'>";
        contenido += "</p>";
        contenido += "</form>";

        // abrimos el formulario
        this.layerResponsables = new jBox('Modal', {
                                    animation: 'flip',
                                    closeOnEsc: true,
                                    closeOnClick: false,
                                    closeOnMouseleave: false,
                                    closeButton: true,
                                    title: 'Buscar',
                                    draggable: 'title',
                                    repositionOnContent: true,
                                    overlay: false,
                                    onCloseComplete: function(){
                                        this.destroy();
                                    },
                                    content: contenido,
                                    theme: 'TooltipBorder',
                                    width: 300,
                            });
        this.layerResponsables.open();

        // fijamos el foco
        document.busca_usuario.texto.focus();

    }

    /**
     * Verifica el cuadro de diálogo de búsqueda de responsable y
     * si está correcto oculta el cuadro de diálogo y envía la
     * cadena al servidor
     */
    encuentraResponsable(){

        // reiniciamos el contador
        cce.contador();

        // definición de variables
        var mensaje;
        var texto = document.busca_usuario.texto.value;

        // si no ingresó texto
        if (texto === ""){

            // presenta el mensaje y retorna
            mensaje = "Debe indicar un texto a buscar !!!";
            new jBox('Notice', {content: mensaje, color: 'red'});
            document.busca_usuario.texto.focus();
            return false;

        }

        // cierra el cuadro de diálogo
        this.layerResponsables.destroy();

        // abre el cuadro de resultados y le pasa por ajax
        // el texto a buscar
        this.layerResponsables = new jBox('Modal', {
                                    animation: 'flip',
                                    closeOnEsc: true,
                                    closeOnClick: false,
                                    closeOnMouseleave: false,
                                    closeButton: true,
                                    overlay: false,
                                    onCloseComplete: function(){
                                        this.destroy();
                                    },
                                    title: 'Resultados de la Búsqueda',
                                    draggable: 'title',
                                    repositionOnContent: true,
                                    theme: 'TooltipBorder',
                                    width: 800,
                                    ajax: {
                                        url: 'responsables/buscar.php?texto='+texto,
                                        reload: 'strict'
                                    }
                                });
        this.layerResponsables.open();

    }

    /**
     * Método que presenta un layer emergente con los laboratorios coincidentes
     * con el texto ingresado en el campo, usado en el formulario de
     * responsables para seleccionar el laboratorio que administra
     */
    buscaLabResponsable(){

        // reiniciamos el contador
        cce.contador();

        // obtenemos los valores del formulario
        var laboratorio = document.form_responsables.laboratorio.value;
        var provincia = $("#jurisdiccion option:selected").text();
        var pais = $("#pais option:selected").text();
        var mensaje;

        // si no ingresó parte del nombre del laboratorio
        if (laboratorio == ""){

            // presenta el mensaje y retorna
            mensaje = "Debe ingresar parte del nombre del laboratorio";
            new jBox('Notice', {content: mensaje, color: 'red'});
            document.form_responsables.laboratorio.focus();
            return false;

        }

        // si no seleccionó jurisdicción
        if (provincia == "Seleccione"){

            // pide selecciona la jurisdicción
            mensaje = "Indique la jurisdicción del responable";
            new jBox('Notice', {content: mensaje, color: 'red'});
            document.form_responsables.jurisdiccion.focus();
            return false;

        }

        // abre el cuadro de diálogo y carga por ajax los laboratorios hallados
        // abrimos el formulario
        this.layerResponsables = new jBox('Modal', {
                            animation: 'flip',
                            closeOnEsc: true,
                            closeOnClick: false,
                            closeOnMouseleave: false,
                            closeButton: true,
                            overlay: false,
                            title: 'Asignar Laboratorio',
                            draggable: 'title',
                            theme: 'TooltipBorder',
                            onCloseComplete: function(){
                                this.destroy();
                            },
                            repositionOnContent: true,
                            width: 500,
                            heigth: 300,
                            ajax: {
                                url: 'responsables/buscalaboratorio.php?laboratorio='+laboratorio+'&pais='+pais+'&provincia='+provincia,
                                reload: 'strict'
                            }
                        });
        this.layerResponsables.open();

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param {int} idlaboratorio - clave de  un laboratorio
     * @param {string} nombre - nombre del laboratorio
     * Método llamado desde el layer emergente de búsqueda de
     * laboratorios, recibe los datos del laboratorio y los asigna
     * en el formulario como el laboratorio que el usuario está
     * habilitado cargar
     */
    asignaLaboratorio(idLaboratorio, nombre){

        // asigna los valores en el formulario
        document.form_responsables.idlaboratorioresponsable.value = idLaboratorio;
        document.form_responsables.laboratorio.value = nombre;

        // destruye el cuadro de diálogo
        this.layerResponsables.destroy();

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método llamado por callback que verifica no se encuentre
     * repetido el correo del usuario
     */
    validaMail(mailCorrecto){

        // inicializamos las variables
        this.Correcto = false;

        // pasamos el mail y verificamos de forma sincrónica
        $.ajax({
            url: 'responsables/verificamail.php?mail='+this.Email+'&id='+this.IdResponsable,
            type: "GET",
            cahe: false,
            contentType: false,
            processData: false,
            dataType: 'json',
            async: false,
            success: function(data) {

                // si retornó verdadero
                if (data.Registros == 0){
                    responsables.Correcto = true;
                } else {
                    responsables.Correcto = false;
                }

            }});

        // retornamos
        mailCorrecto(this.Correcto);

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método llamado por callback que verifica no este ingresando
     * un nombre de usuario repetido
     */
    validaUsuario(usuarioCorrecto){

        // inicializamos las variables
        this.Correcto = false;

        // pasamos el usuario sincrónico
        $.ajax({
            url: 'responsables/verificausuario.php?usuario='+this.Usuario,
            type: "GET",
            cahe: false,
            contentType: false,
            processData: false,
            dataType: 'json',
            async: false,
            success: function(data) {

                // si no encontró
                if (data.Registros == 0){
                    responsables.Correcto = true;
                } else {
                    responsables.Correcto = false;
                }

            }});

        // retornamos
        usuarioCorrecto(this.Correcto);

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que verifica los datos del formulario antes de enviarlos
     * al servidor
     */
    verificaResponsable(){

        // declaración de variables
        var mensaje;

        // si está editando
        if (document.form_responsables.idresponsable.value != ""){
            this.IdResponsable = document.form_responsables.idresponsable.value;
        }

        // si no cargó el nombre
        if (document.form_responsables.nombre.value == ""){

            // presenta el mensaje y retorna
            mensaje = "Debe ingresar el nombre completo del responsable";
            new jBox('Notice', {content: mensaje, color: 'red'});
            document.form_responsables.nombre.focus();
            return false;

        // si declaró
        } else {

            // lo asigna a la variable de clase
            this.Nombre = document.form_responsables.nombre.value;

        }

        // si está dando altas
        if (this.IdResponsable == 0){

            // verifica que halla ingresado el usuario
            if (document.form_responsables.usuario.value === ""){

                // presenta el mensaje y retorna
                mensaje = "Ingrese el nombre de usuario del responsable";
                new jBox('Notice', {content: mensaje, color: 'red'});
                document.form_responsables.usuario.focus();
                return false;

            // si está todo bien
            } else {

                // asigna en la variable de clase
                this.Usuario = document.form_responsables.usuario.value;

            }

        }

        // si no cargó la institución
        if (document.form_responsables.institucion.value == ""){

            // presenta el mensaje y retorna
            mensaje = "Ingrese la Institución a la que pertenece";
            new jBox('Notice', {content: mensaje, color: 'red'});
            document.form_responsables.institucion.focus();
            return false;

        // si declaró
        } else {

            // asigna en la variable de clase
            this.Institucion = document.form_responsables.institucion.value;

        }

        // si no indicó el cargo
        if (document.form_responsables.cargo === ""){

            // presenta el mensaje y retorna
            mensaje = "Ingrese el cargo que ocupa el usuario";
            new jBox('Notice', {content: mensaje, color: 'red'});
            document.form_responsables.cargo.focus();
            return false;

        // si indicó
        } else {

            // asigna en la variable de clase
            this.Cargo = document.form_responsables.cargo.value;
        }

        // si no cargó el mail
        if (document.form_responsables.e_mail.value === ""){

            // presenta el mensaje y retorna
            mensaje = "Ingrese el mail del usuario, recuerde que <br>";
            mensaje += "será utilizado para recuperar la contraseña";
            new jBox('Notice', {content: mensaje, color: 'red'});
            document.form_responsables.e_mail.focus();
            return false;

        // si cargó verifica el formato
        } else if (!echeck(document.form_responsables.e_mail.value)) {

            // presenta el mensaje y retorna
            mensaje = "El formato del mail parece incorrecto, <br>verifique por favor";
            new jBox('Notice', {content: mensaje, color: 'red'});
            document.form_responsables.e_mail.focus();
            return false;

        }

        // asigna a la variable de clase
        this.Email = document.form_responsables.e_mail.value;

        // si no cargó el teléfono
        if (document.form_responsables.telefono.value == ""){

            // presenta el mensaje y retorna
            mensaje = "Indique el teléfono del usuario por favor";
            new jBox('Notice', {content: mensaje, color: 'red'});
            document.form_responsables.telefono.focus();
            return false;

        // si cargó
        } else {

            // asigna en la variable de clase
            this.Telefono = document.form_responsables.telefono.value;

        }

        // si no indicó el país
        if (document.form_responsables.pais.value == 0){

            // presenta el mensaje y retorna
            mensaje = "Seleccione de la lista el País donde<br>";
            mensaje += "reside el usuario";
            new jBox('Notice', {content: mensaje, color: 'red'});
            document.form_responsables.pais.focus();
            return false;

        // si indicó
        } else {

            // asigna en la variable de clase
            this.IdPais = document.form_responsables.pais.value;

        }

        // si no cargó la localidad
        if (document.form_responsables.localidad.value == 0){

            // presenta el mensaje y retorna
            mensaje = "Seleccione de la lista la Localidad<br>";
            mensaje += "donde reside el usuario";
            new jBox('Notice', {content: mensaje, color: 'red'});
            document.form_responsables.localidad.focus();
            return false;

        // si cargó
        } else {

            // asigna en la variable de clase
            this.CodLoc = document.form_responsables.localidad.value;

        }

        // si no cargó la dirección
        if (document.form_responsables.direccion.value == ""){

            // presenta el mensaje y retorna
            mensaje = "Ingrese la dirección postal del usuario, recuerde<br>";
            mensaje += "que es necesaria para enviar las muestras";
            new jBox('Notice', {content: mensaje, color: 'red'});
            document.form_responsables.direccion.focus();
            return false;

        // si la declaró
        } else {

            // asigna en la variable de clase
            this.Direccion = document.form_responsables.direccion.value;

        }

        // si no cargó el código postal
        if (document.form_responsables.codigopostal.value == ""){

            // presenta el mensaje y retorna
            mensaje = "Indique el Código Postal correspondiente";
            new jBox('Notice', {content: mensaje, color: 'red'});
            document.form_responsables.codigopostal.focus();
            return false;

        // si declaró
        } else {

            // asigna en la variable de clase
            this.CodigoPostal = document.form_responsables.codigopostal.value;

        }

        // si no es responsable, verifica que indique laboratorio
        if (!document.form_responsables.responsable.checked && document.form_responsables.idlaboratorioresponsable.value == ""){

            // presenta el mensaje y retorna
            mensaje = "Si el usuario no es responsable Jurisdiccional, debe<br>";
            mensaje += "indicar cual es el laboratorio que administra";
            new jBox('Notice', {content: mensaje, color: 'red'});
            document.form_responsables.laboratorio.focus();
            return false;

        }

        // si es responsable
        if (document.form_responsables.responsable.checked){
            this.Responsable = "Si";
        } else {
            this.Responsable = "No";
            this.IdLaboratorio = document.form_responsables.idlaboratorioresponsable.value;
        }

        // si está activo
        if (document.form_responsables.activo.checked){
            this.Activo = "Si";
        } else {
            this.Activo = "No";
        }

        // si es de nivel central
        if (document.form_responsables.nivelcentral.checked){
            this.NivelCentral = "Si";
        } else {
            this.NivelCentral = "No";
        }

        // si está dando altas
        if (this.IdResponsable == 0){

            // verifica que no esté repetido el usuario
            this.validaUsuario(function(usuarioCorrecto){

                // si está repetido
                if (!responsables.Correcto){

                    // presenta el mensaje
                    mensaje = "Ese nombre de usuario está en uso !!!";
                    new jBox('Notice', {content: mensaje, color: 'red'});

                }

            });

            // verifica que no esté repetido el mail
            this.validaMail(function(mailCorrecto){

                // si está repetido
                if (!responsables.Correcto){

                    // presenta el mensaje
                    mensaje = "Mail ya se encuentra en uso !!!";
                    new jBox('Notice', {content: mensaje, color: 'red'});

                }

            });

        }

        // asigna los comentarios
        this.Observaciones = CKEDITOR.instances['observaciones'].getData();

        // grabamos el registro
        this.grabaUsuario();

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que envía por post los datos del formulario
    */
    grabaUsuario(){

        // si hubo un registro repetido
        if (!this.Correcto){
            return false;
        }

        // usamos un objeto html5
        var datosResponsable = new FormData();

        // si está editando
        if (this.IdResponsable != 0){
            datosResponsable.append("Id", this.IdResponsable);
        }

        // agregamos al formdata
        datosResponsable.append("Nombre", this.Nombre);
        datosResponsable.append("Usuario", this.Usuario);
        datosResponsable.append("Institucion", this.Institucion);
        datosResponsable.append("Cargo", this.Cargo);
        datosResponsable.append("Mail", this.Email);
        datosResponsable.append("Telefono", this.Telefono);
        datosResponsable.append("Pais", this.IdPais);
        datosResponsable.append("Localidad", this.CodLoc);
        datosResponsable.append("Direccion", this.Direccion);
        datosResponsable.append("CodigoPostal", this.CodigoPostal);
        datosResponsable.append("Responsable", this.Responsable);

        // si no es responsable
        if (this.Responsable == "No"){
            datosResponsable.append("IdLaboratorio", this.IdLaboratorio);
        }

        // sigue agregando
        datosResponsable.append("Activo", this.Activo);
        datosResponsable.append("NivelCentral", this.NivelCentral);
        datosResponsable.append("Comentarios",this.Observaciones);

        // envía el formulario por ajax en forma asincrónica
        $.ajax({
            type: "POST",
            url: "responsables/graba_responsable.php",
            data: datosResponsable,
            cahe: false,
            contentType: false,
            processData: false,
            dataType: 'json',
            success: function(data) {

                // si ocurrió un error
                if (data.Id == 0){

                    // presenta el mensaje
                    new jBox('Notice', {content: "Ha ocurrido un error", color: 'red'});

                } else {

                    // almacenamos la id en el formulario para poder
                    // agregar unidades inmediatamente
                    document.form_responsables.idresponsable.value = data.Id;

                    // presenta el mensaje
                    new jBox('Notice', {content: "Registro grabado ... ", color: 'green'});

                    // setea el foco
                    document.form_responsables.nombre.focus();

                }

            },

        });

        // reiniciamos el contador
        cce.contador();

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que einicia el formulario de responsables y setea el foco en
     * el primer campo
     */
    cancelaUsuario(){

        // reiniciamos el contador
        contador();

        // reinicia y setea el foco
        document.form_responsables.reset();
        document.form_responsables.nombre.focus();
        return false;

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que lee las coordenadas del formulario y llama
     * la librerìa de google maps para presentarla
     */
    mapaUsuario(){

        // reiniciamos el contador
        cce.contador();

        // obtenemos las coordenadas
        var coordenadas = document.getElementById("coordenadas").value;

        // si no están declaradas
        if (coordenadas == ""){

            // presenta el mensaje
            mensaje = "Lo siento, no tenemos coordenadas GPS<br>";
            mensaje += "para este usuario.";
            new jBox('Notice', {content: mensaje, color: 'red'});
            return false;

        }

        // obtenemos las coordenadas recortando el paréntesis y
        // los signos de puntuación
        var latitud = parseFloat(coordenadas.substring(1,coordenadas.indexOf(",")));
        var longitud = parseFloat(coordenadas.substring(coordenadas.indexOf(",") + 1, coordenadas.length -1));

        // obtenemos la leyenda a presentar
        var leyenda = document.getElementById("nombre").value;
        leyenda += "<br>";
        leyenda += document.getElementById("institucion").value;
        leyenda += "<br>";
        leyenda += document.getElementById("cargo").value;
        leyenda += "<br>";
        leyenda += document.getElementById("direccion").value;

        // llamamos la rutina de presentación
        mapas.initMap(leyenda, latitud, longitud);

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que carga en un layer emergente los responsables sin georreferenciar
     */
    geoResponsables(){

        // reiniciamos el contador
        cce.contador();

        // abrimos el formulario
        this.layerResponsables = new jBox('Modal', {
                                animation: 'flip',
                                closeOnEsc: true,
                                closeOnClick: false,
                                closeOnMouseleave: false,
                                closeButton: true,
                                overlay: false,
                                title: 'Georreferenciar Responsables',
                                draggable: 'title',
                                repositionOnContent: true,
                                theme: 'TooltipBorder',
                                onCloseComplete: function(){
                                    this.destroy();
                                },
                                ajax: {
                                    url: 'responsables/georreferenciar.php',
                                    reload: 'strict'
                                }
                            });
        this.layerResponsables.open();

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param: {int} id - clave del registro (y también de objeto html)
     * @param {string} domicilio - cadena con la dirección a georreferenciar
     * Método que recibe como parámetros la id del registro (que también es la id
     * del texto con las coordenadas) obtiene las coordenadas gps a través del
     * servicio de google maps y luego en caso de éxito, ejecuta la consulta
     * de actualización en el servidor y muestra las coordenadas en la página
     */
    detectaResponsables(id, domicilio){

        // reiniciamos el contador
        cce.contador();

        // declaración de variables
        var coordenadas;

        // instancia el geocoder
        var geocoder = new google.maps.Geocoder();

        // busca la dirección
        geocoder.geocode( { 'address': domicilio}, function(results, status) {

            // si hubo resultados
            if (status == google.maps.GeocoderStatus.OK) {

                // presenta las coordenadas en el formulario
                var coordenadas = results[0].geometry.location;
                document.getElementById(id).value = coordenadas;

                // las graba en la base
                $.post('responsables/graba_coordenadas.php','id='+id+'&coordenadas='+coordenadas);

            // si no encontró
            } else {

                // presenta el mensaje
                mensaje = "No se ha podido detectar una\n";
                mensaje += "serie de coordenadas válidas, el\n";
                mensaje += "error fue: " + status;
                new jBox('Notice', {content: mensaje, color: 'red'});

            }

        });

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param {string} idelemento - objeto html
     * @param {string} codloc - clave indec de la jurisdicción
     * @param {int} idresponsable - clave del registro predeterminado
     * Método que recibe como parámetro un elemento del formulario
     * y la clave de la jurisdicción, completa el select del
     * elemento con la nómina de responsables de esa jurisdicción
     * y si recibe una clave la predetermina
     */
    nominaResponsables(idelemento, codloc, idresponsable){

        // obtenemos la nómina
        $.ajax({
            url: 'responsables/lista_responsables.php?provincia='+codloc,
            type: "GET",
            cahe: false,
            contentType: false,
            processData: false,
            dataType: 'json',
            success: function(data) {

                // agrega el primer elemento en blanco
                $("#" + idelemento).append("<option value='0'>Seleccione</option>");

                // recorre el vector de resultados
                for(var i=0; i < data.length; i++){

                    // si recibió la clave
                    if (typeof(idresponsable) != "undefined"){

                        // si coindicen
                        if (idresponsable == data[i].Id){

                            // lo agrega preseleccionado
                            $("#" + idelemento).append("<option selected value='" + data[i].Id + "'>" + data[i].Nombre + "</option>");

                        // si no coincide
                        } else {

                            // simplemente lo agrega
                            $("#" + idelemento).append("<option value='" + data[i].Id + "'>" + data[i].Nombre + "</option>");

                        }

                    // si no recibió
                    } else {

                        // simplemente lo agrega
                        $("#" + idelemento).append("<option value='" + data[i].Id + "'>" + data[i].Nombre + "</option>");

                    }

                }

            }});

    }

}