<?php

/**
 *
 * responsables/verificamail.php
 *
 * @package     CCE
 * @subpackage  Responsables
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.4.0 (14/08/2018)
 * @copyright   Copyright (c) 2018, INP
 *
 * Procedimiento que recibe por get una dirección de correo y la
 * clave del registro si es una edición, busca los registros
 * coincidentes y retorna la cantidad encontrada, utilizado para
 * evitar el ingreso duplicado
 *
*/

// incluimos e instanciamos la clase
require_once ("responsables.class.php");
$responsable = new Responsables();

// si recibió la id
if (!empty($_GET["id"])){

    // verificamos si no está repitiendo
    $registros = $responsable->verificaMail($_GET["mail"], $_GET["id"]);

} else {

    // verificamos si se encuentra el usuario
    $registros = $responsable->verificaMail($_GET["mail"]);

}

// retornamos el estado
echo json_encode(array("Registros" => $registros));

?>