<?php

/**
 *
 * responsables/buscar.php
 *
 * @package     CCE
 * @subpackage  Responsables
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.4.0 (14/08/2018)
 * @copyright   Copyright (c) 2018, INP
 *
 * Procedimiento que recibe por get el texto a buscar, y presenta
 * la grilla con los registros encontados
 *
*/

// incluimos e instanciamos la clase
require_once ("responsables.class.php");
$responsable = new Responsables();

// busca la cadena
$resultado = $responsable->buscaUsuario($_GET["texto"]);

// si no hubo resultados
if (count($resultado) == 0){

    // presenta el mensaje
    echo "<h2>No se han encontrado registros coincidentes</h2>";

// si hubo resultados
} else {

    // define la tabla
    echo "<table width='90%' align='center' border='0' id='busca_usuarios'>";

    // define los encabezados
    echo "<thead>";
    echo "<tr>";
    echo "<th>Nombre</th>";
    echo "<th>Institución</th>";
    echo "<th>Jurisdicción</th>";
    echo "<th>Localidad</th>";
    echo "<th></th>";
    echo "</tr>";
    echo "</thead>";

    // define el cuerpo
    echo "<tbody>";

    // recorremos el vector
    foreach ($resultado AS $registro){

        // obtenemos el registro
        extract($registro);

        // abrimos la fila
        echo "<tr>";

        // lo agregamos
        echo "<td><small>$nombre_responsable</small></td>";
        echo "<td><small>$institucion_responsable</small></td>";
        echo "<td><small>$provincia_responsable</small></td>";
        echo "<td><small>$localidad_responsable</small></td>";

        // armamos el enlace
        echo "<td>";
        echo "<input type='button'
               name='btnVer'
               class='botoneditar'
               onClick='responsables.getDatosUsuario($id_responsable);'>";
        echo "</td>";

        // cerramos la fila
        echo "</tr>";

    }

    // cierra la tabla
    echo "</tbody>";
    echo "</table>";

    // define el div para el paginador
    echo "<div class='paging'></div>";

}
?>
<SCRIPT>

    // definimos las propiedades de la tabla
     $('#busca_usuarios').datatable({
        pageSize: 15,
        sort: [true, true, true, true, false],
        filters: [true, true, 'select', true, false],
        filterText: 'Buscar ... '
    });

</SCRIPT>