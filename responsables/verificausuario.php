<?php

/**
 *
 * responsables/verificausuario.php
 *
 * @package     CCE
 * @subpackage  Responsables
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.4.0 (13/08/2018)
 * @copyright   Copyright (c) 2018, INP
 *
 * Procedimiento que recibe por get un nombre de usuario y verifica
 * que no se encuentre declarado en la base, retorna el número de
 * registros encontrados
 *
*/

// incluimos e instanciamos la clase
require_once ("responsables.class.php");
$responsable = new Responsables();

// verificamos si se encuentra el usuario
$registros = $responsable->verificaUsuario($_GET["usuario"]);

// retornamos el estado
echo json_encode(array("Registros" => $registros));

?>