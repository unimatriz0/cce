<?php

/**
 *
 * responsables/lista_responsables.php
 *
 * @package     CCE
 * @subpackage  Responsables
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.4.0 (14/08/2018)
 * @copyright   Copyright (c) 2018, INP
 *
 * Procedimiento que recibe por get la clave indec de una provincia
 * y retorna un array con los responsables activos de esa provincia
 *
*/

// incluimos e instanciamos la clase
require_once ("responsables.class.php");
$responsable = new Responsables();

// obtenemos la nómina
$nomina = $responsable->listaResponsables($_GET["provincia"]);

// inicializa las variables
$jsondata = array();

// inicia un bucle recorriendo el vector
foreach ($nomina AS $registro){

    // obtiene el registro
    extract($registro);

    // lo agrega a la matriz
    $jsondata[] = array("Id" => $idresponsable,
                        "Nombre" => $nombreresponsable);

}

// devuelve la cadena
echo json_encode($jsondata);

?>