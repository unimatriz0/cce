<?php

/**
 *
 * responsables/graba_responsable.php
 *
 * @package     CCE
 * @subpackage  Responsables
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.4.0 (14/08/2018)
 * @copyright   Copyright (c) 2018, INP
 *
 * Procedimiento que recibe por post los datos del formulario de
 * responsables, instancia la clase y graba el registro, retorna
 * la id del registro afectado
 *
*/

// inclusión e instanciación de clases
require_once ("responsables.class.php");
$responsable = new Responsables();

// inclusión de las clases de correo
require_once ("../correos/plantillacorreo.class.php");

// si está editando
if (!empty($_POST["Id"])){

    // setea la id
    $responsable->setIdResponsable($_POST["Id"]);

}

// asigna los valores
$responsable->setNombre($_POST["Nombre"]);
$responsable->setInstitucion($_POST["Institucion"]);
$responsable->setCargo($_POST["Cargo"]);
$responsable->setMail($_POST["Mail"]);
$responsable->setTelefono($_POST["Telefono"]);
$responsable->setIdPais($_POST["Pais"]);
$responsable->setIdLocalidad($_POST["Localidad"]);
$responsable->setDireccion($_POST["Direccion"]);
$responsable->setCodigoPostal($_POST["CodigoPostal"]);
$responsable->setUsuario($_POST["Usuario"]);

// si es responsable
if ($_POST["Responsable"] == "Si"){
    $responsable->setResponsableChagas("Si");
} else {
    $responsable->setResponsableChagas("No");
    $responsable->setIdLaboratorio($_POST["IdLaboratorio"]);
}

// si está activo
if ($_POST["Activo"] == "Si"){
    $responsable->setActivo("Si");
} else {
    $responsable->setActivo("No");
}

// si es de nivel central
if ($_POST["NivelCentral"] == "Si"){
    $responsable->setNivelCentral("Si");
} else {
    $responsable->setNivelCentral("No");
}

// termina de asignar
$responsable->setObservaciones($_POST["Comentarios"]);

// graba el registro y obtiene la id
$id = $responsable->grabaResponsable();

// si estaba dando altas
if (empty($_POST["Id"])){

    // si grabó sin problemas
    if ($id != 0){

        // envía un mail al usuario
        // definimos el texto a enviar
        $mensaje = "<h2>Alta de Usuario</h2>";
        $mensaje .= "<p>Estimado/a " . $responsable->getNombre() . "<br>";
        $mensaje .= $responsable->getInstitucion() . "</p>";
        $mensaje .= "<p align='justify'>";
        $mensaje .= "Recientemente usted ha sido incorporado como usuario al ";
        $mensaje .= "Sistema de Control de Calidad de Laboratorios de ";
        $mensaje .= "Chagas, a continuación, le comunicamos que los datos de conexión ";
        $mensaje .= "son los siguientes:</p>";
        $mensaje .= "<p>Usuario: " . $responsable->getUsuario() . "<br>";
        $mensaje .= "Contraseña: " . $responsable->getPassword() . "</p>";
        $mensaje .= "<p align='justify'>";
        $mensaje .= "Al ingresar podrá actualizar la contraseña, por una solo conocida ";
        $mensaje .= "por usted.</p>";
        $mensaje .= "<p align='justify'>";
        $mensaje .= "Recuerde que la web de Control de Calidad Externo se encuentra ";
        $mensaje .= "disponible en <a href://http:fatalachaben.info.tm/calidad/> ";
        $mensaje .= "http://fatalachaben.info.tm/calidad</a>.<br></p>";
        $mensaje .= "<p align='justify'>";
        $mensaje .= "No olvide que ante cualquier inconveniente puede comunicarse ";
        $mensaje .= "a <a mailto:cinvernizzi@gmail.com>cinvernizzi@gmail.com</a> o ";
        $mensaje .= "<a mailto: diagnostico.inp.anlis@gmail.com>diagnostico.inp.anlis@gmail.com</a> ";
        $mensaje .= "donde intentaremos resolver sus dificultades. </p>";

        // instanciamos el objeto
        $mail = new Plantilla();

        // destinatario del mensaje
        $mail->addAddress($_POST["Mail"], $responsable->getNombre());

        // cargamos el contenido del texto
        $mail->setMensaje($mensaje);

        // enviamos el mensaje
        $mail->send();

    }

}

// retornamos la clave del registro
echo json_encode(array("Id" => $id));

?>