<?php

/**
 *
 * Class Responsables | responsables/responsables.class.php
 *
 * @package     CCE
 * @subpackage  Responsables
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.4.0 (14/08/2018)
 * @copyright   Copyright (c) 2018, INP
 *
 * Clase que controla las operaciones sobre la tabla de
 * responsables
 *
*/

// inclusión de archivos
require_once ("../clases/conexion.class.php");

// convención para la nomenclatura de las propiedades, comienzan con una
// letra mayúscula, de tener mas de una palabra no se utilizan separadores
// y la inicial de cada palabra va en mayúscula
// para las variables recibidas como parámetro el criterio es todas en
// minúscula

// convención para la nomenclatura de los metodos, comienzan con set o get
// según asignen un valor o lo lean y luego el nombre del valor a obtener

/**
 * Definición de la clase
 * @author Claudio Invernizzi <cinvernizzi@gmail.com>
 */
class Responsables {

    // declaración de variables de la clase, las declaramos
    // como protected para que puedan ser heredadas pero
    // para asignarle el valor debemos crear los métodos
    protected $Link;                  // puntero a la base de datos

    // definición de variables de la base de datos
    protected $IdResponsable;                // clave del registro
    protected $Nombre;                       // nombre del usuario
    protected $Institucion;                  // institución en la que trabaja
    protected $Cargo;                        // descripción del cargo
    protected $Mail;                         // mail del usuario
    protected $Telefono;                     // teléfono del usuario
    protected $Localidad;                    // nombre de la localidad
    protected $IdLocalidad;                  // clave de la localidad
    protected $Provincia;                    // nombre de la provincia
    protected $Pais;                         // nombre del país de procedencia
    protected $IdPais;                       // clave del país
    protected $Direccion;                    // dirección postal del usuario
    protected $Coordenadas;                  // coordenadas GPS
    protected $CodigoPostal;                 // Código postal
    protected $ResponsableChagas;            // indica si es responsable
    protected $Laboratorio;                  // nombre del laboratorio
    protected $IdLaboratorio;                // clave del laboratorio que administra
    protected $Activo;                       // indica si el usuario está activo
    protected $Observaciones;                // observaciones y comentarios
    protected $Usuario;                      // nombre de usuario
    protected $Password;                     // password sin encriptar del usuario
    protected $FechaAlta;                    // fecha de alta del registro
    protected $NivelCentral;                 // indica si el usuario es de nivel central

    /**
     * Constructor de la clase, establece la conexión con la base
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     */
    function __construct (){

        // nos conectamos a la base de datos
        $this->Link = new Conexion();

        // inicializamos las variables de clase
        $this->IdResponsable = 0;
        $this->Nombre = "";
        $this->Institucion = "";
        $this->Cargo = "";
        $this->Mail = "";
        $this->Telefono = "";
        $this->Localidad = "";
        $this->IdLocalidad = "";
        $this->Provincia = "";
        $this->Pais = "";
        $this->IdPais = 0;
        $this->Direccion = "";
        $this->Coordenadas = "";
        $this->CodigoPostal = "";
        $this->ResponsableChagas = "";
        $this->Laboratorio = "";
        $this->IdLaboratorio = 0;
        $this->Activo = "";
        $this->Observaciones = "";
        $this->Usuario = "";
        $this->Password = "";
        $this->FechaAlta = "";
        $this->NivelCentral = "";

    }

    /**
     * Destructor de la clase, cierra la conexión con la base
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     */
    function __destruct(){

        // elimina el enlace a la base
        $this->Link = null;

    }

    // métodos de asignación de valores
    public function setIdResponsable($idresponsable){

        // verifica que sea un número
        if (!is_numeric($idresponsable)){

            // abandona por error
            echo "La clave del usuario debe ser un número";
            exit;

        // si es correcto
        } else {

            // lo asigna
            $this->IdResponsable = $idresponsable;

        }

    }
    public function setNombre($nombre){
        $this->Nombre = $nombre;
    }
    public function setInstitucion($institucion){
        $this->Institucion = $institucion;
    }
    public function setCargo($cargo){
        $this->Cargo = $cargo;
    }
    public function setMail($mail){
        $this->Mail = $mail;
    }
    public function setTelefono($telefono){
        $this->Telefono = $telefono;
    }
    public function setIdLocalidad($idlocalidad){
        $this->IdLocalidad = $idlocalidad;
    }
    public function setIdPais($idpais){
        $this->IdPais = $idpais;
    }
    public function setDireccion($direccion){
        $this->Direccion = $direccion;
    }
    public function setCoordenadas($coordenadas){
        $this->Coordenadas = $coordenadas;
    }
    public function setCodigoPostal($codigopostal){
        $this->CodigoPostal = $codigopostal;
    }
    public function setResponsableChagas($responsable){
        $this->ResponsableChagas = $responsable;
    }
    public function setIdLaboratorio($idlaboratorio){

        // verifica que sea un número
        if (!is_numeric($idlaboratorio)){

            // abandona por error
            echo "La clave del laboratorio debe ser un número";
            exit;

        // si es un número
        } else {

            // lo asigna
            $this->IdLaboratorio = $idlaboratorio;

        }

    }
    public function setActivo($activo){
        $this->Activo = $activo;
    }
    public function setObservaciones($observaciones){
        $this->Observaciones = $observaciones;
    }
    public function setNivelCentral($nivelcentral){
        $this->NivelCentral = $nivelcentral;
    }
    public function setUsuario($usuario){
        $this->Usuario = $usuario;
    }

    // métodos de obtención de valores
    public function getIdResponsable(){
        return $this->IdResponsable;
    }
    public function getNombre(){
        return $this->Nombre;
    }
    public function getInstitucion(){
        return $this->Institucion;
    }
    public function getCargo(){
        return $this->Cargo;
    }
    public function getMail(){
        return $this->Mail;
    }
    public function getTelefono(){
        return $this->Telefono;
    }
    public function getLocalidad(){
        return $this->Localidad;
    }
    public function getIdLocalidad(){
        return $this->IdLocalidad;
    }
    public function getProvincia(){
        return $this->Provincia;
    }
    public function getIdProvincia(){
        return $this->IdProvincia;
    }
    public function getPais(){
        return $this->Pais;
    }
    public function getIdPais(){
        return $this->IdPais;
    }
    public function getDireccion(){
        return $this->Direccion;
    }
    public function getCodigoPostal(){
        return $this->CodigoPostal;
    }
    public function getCoordenadas(){
        return $this->Coordenadas;
    }
    public function getResponsableChagas(){
        return $this->ResponsableChagas;
    }
    public function getLaboratorio(){
        return $this->Laboratorio;
    }
    public function getIdLaboratorio(){
        return $this->IdLaboratorio;
    }
    public function getActivo(){
        return $this->Activo;
    }
    public function getObservaciones(){
        return $this->Observaciones;
    }
    public function getUsuario(){
        return $this->Usuario;
    }
    public function getPassword(){
        return $this->Password;
    }
    public function getFechaAlta(){
        return $this->FechaAlta;
    }
    public function getNivelCentral(){
        return $this->NivelCentral;
    }

  /**
     * Método utilizado en el formulario de responsables que lista los
     * responsables activos de la jurisdicción que recibe como parámetro
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param {string} $jurisdiccion - código indec ce la jurisdicción
     * @return array
     */
    public function listaResponsables($jurisdiccion){

        // componemos la consulta
        $consulta = "SELECT cce.responsables.NOMBRE AS nombreresponsable,
                            cce.responsables.ID AS idresponsable
                     FROM cce.responsables INNER JOIN diccionarios.localidades ON cce.responsables.LOCALIDAD = diccionarios.localidades.CODLOC
                                           INNER JOIN diccionarios.provincias ON diccionarios.localidades.CODPCIA = diccionarios.provincias.COD_PROV
                     WHERE diccionarios.provincias.COD_PROV = '$jurisdiccion' AND
                           cce.responsables.ACTIVO = 'Si' AND
                           cce.responsables.RESPONSABLE_CHAGAS = 'Si'
                     ORDER BY cce.responsables.NOMBRE;";

        // ejecutamos la consulta
        $resultado = $this->Link->query($consulta);

        // lo pasamos a minúsculas porque según la versión de
        // pdo lo devuelve en mayúsculas o minúsculas
        $nomina = array_change_key_case($resultado->fetchAll(PDO::FETCH_ASSOC), CASE_LOWER);

        // retornamos el vector
        return $nomina;

    }

    /**
     * Método que recibe una dirección de correo y la clave
     * del usuario si es una edición, verifica si ya se encuentra
     * definido el correo en la base y retorna el número de
     * registros encontrados
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param string $mail - cadena con el correo del usuario
     * @param int $id - entero con la clave del usuario
     * @return int
     */
    public function verificaMail($mail, $id = false){

        // inicializamos las variables
        $registros = 0;

        // si no recibió la clave
        if ($id == false){

            // compone la consulta
            $consulta = "SELECT COUNT(*) AS registros
                         FROM cce.responsables
                         WHERE cce.responsables.E_MAIL = '$mail';";

        // si recibió la clave
        } else {

            // verificamos que no la tenga otro usuario
            $consulta = "SELECT COUNT(*) AS registros
                         FROM cce.responsables
                         WHERE cce.responsables.E_MAIL = '$mail' AND
                               cce.responsables.ID != '$id';";

        }

        // obtenemos el registro y retornamos
        $resultado = $this->Link->query($consulta);
        $fila = array_change_key_case($resultado->fetch(PDO::FETCH_ASSOC), CASE_LOWER);
        extract($fila);
        return $registros;

    }

    /**
     * Método que verifica si el nombre de usuario ya se encuentra
     * declarado en la base retorna el número de registros encontados
     * @param string $usuario - nombre de usuario de la tabla responsables
     * @return int
     */
    public function verificaUsuario($usuario){

        // inicializamos las variables
        $registros = 0;

        // definimos la consulta
        $consulta = "SELECT COUNT(*) AS registros
                     FROM cce.responsables
                     WHERE cce.responsables.USUARIO = '$usuario';";

        // obtenemos el registro
        $resultado = $this->Link->query($consulta);
        $fila = array_change_key_case($resultado->fetch(PDO::FETCH_ASSOC), CASE_LOWER);
        extract($fila);
        return $registros;

    }

    /**
     * Método que busca en la tabla de responsables los usuarios que
     * coincidan con el texto recibido
     * @param string $texto - un texto a buscar
     * @return array
     */
    public function buscaUsuario($texto){

        // componemos la consulta
        $consulta = "SELECT cce.responsables.ID AS id_responsable,
                            cce.responsables.NOMBRE AS nombre_responsable,
                            cce.responsables.INSTITUCION AS institucion_responsable,
                            diccionarios.localidades.NOMLOC AS localidad_responsable,
                            diccionarios.provincias.NOM_PROV AS provincia_responsable
                     FROM cce.responsables INNER JOIN diccionarios.localidades ON cce.responsables.LOCALIDAD = diccionarios.localidades.CODLOC
                                           INNER JOIN diccionarios.provincias ON diccionarios.localidades.CODPCIA = diccionarios.provincias.COD_PROV
                     WHERE cce.responsables.NOMBRE LIKE '%$texto%' OR
                           cce.responsables.INSTITUCION LIKE '%$texto%' OR
                           diccionarios.localidades.NOMLOC LIKE '%$texto%' OR
                           diccionarios.provincias.NOM_PROV LIKE '%$texto%';";

        // ejecutamos la consulta
        $resultado = $this->Link->query($consulta);
        $fila = array_change_key_case($resultado->fetchAll(PDO::FETCH_ASSOC), CASE_LOWER);

        // retornamos el vector
        return $fila;

    }

    /**
     * Método que a partir de la id del usuario busca y asigna a
     * las variables de clase los valores del registro
     */
    public function getResponsable(){

        // inicializamos las variables
        $id_responsable = 0;
        $nombre_responsable = "";
        $institucion_responsable = "";
        $cargo_responsable = "";
        $mail_responsable = "";
        $telefono_responsable = "";
        $pais_responsable = "";
        $id_pais = 0;
        $localidad_responsable = "";
        $id_localidad = "";
        $provincia_responsable = "";
        $id_provincia = "";
        $direccion_responsable = "";
        $codigo_postal = "";
        $coordenadas = "";
        $responsable_chagas = "";
        $laboratorio_responsable = 0;
        $activo = "";
        $observaciones = "";
        $usuario = "";
        $fecha_alta = "";
        $nivel_central = "";

        // componemos la consulta
        $consulta = "SELECT cce.responsables.ID AS id_responsable,
                            UCASE(cce.responsables.NOMBRE) AS nombre_responsable,
                            UCASE(cce.responsables.INSTITUCION) AS institucion_responsable,
                            cce.responsables.cargo AS cargo_responsable,
                            cce.responsables.E_MAIL AS mail_responsable,
                            cce.responsables.TELEFONO AS telefono_responsable,
                            diccionarios.paises.NOMBRE AS pais_responsable,
                            diccionarios.paises.ID AS id_pais,
                            diccionarios.localidades.NOMLOC AS localidad_responsable,
                            diccionarios.localidades.CODLOC AS id_localidad,
                            diccionarios.provincias.NOM_PROV AS provincia_responsable,
                            diccionarios.provincias.COD_PROV AS id_provincia,
                            cce.responsables.DIRECCION AS direccion_responsable,
                            cce.responsables.CODIGO_POSTAL AS codigo_postal,
                            cce.responsables.COORDENADAS AS coordenadas,
                            cce.responsables.RESPONSABLE_CHAGAS AS responsable_chagas,
                            cce.laboratorios.NOMBRE AS laboratorio_responsable,
                            cce.responsables.LABORATORIO AS id_laboratorio,
                            cce.responsables.ACTIVO AS activo,
                            cce.responsables.OBSERVACIONES AS observaciones,
                            cce.responsables.USUARIO AS usuario,
                            DATE_FORMAT(cce.responsables.FECHA_ALTA, '%d/%m/%Y') AS fecha_alta,
                            cce.responsables.NIVEL_CENTRAL AS nivel_central
                     FROM cce.responsables INNER JOIN diccionarios.localidades ON cce.responsables.LOCALIDAD = diccionarios.localidades.CODLOC
                                           INNER JOIN diccionarios.provincias ON diccionarios.localidades.CODPCIA = diccionarios.provincias.COD_PROV
                                           INNER JOIN diccionarios.paises ON cce.responsables.PAIS = diccionarios.paises.ID
                                           LEFT JOIN cce.laboratorios ON cce.responsables.laboratorio = cce.laboratorios.ID
                     WHERE cce.responsables.ID = '$this->IdResponsable';";

        // obtenemos el registro
        $resultado = $this->Link->query($consulta);
        $registro = array_change_key_case($resultado->fetch(PDO::FETCH_ASSOC), CASE_LOWER);
        extract($registro);

        // asignamos a las variables de clase
        $this->Nombre = $nombre_responsable;
        $this->Institucion = $institucion_responsable;
        $this->Cargo = $cargo_responsable;
        $this->Mail = $mail_responsable;
        $this->Telefono = $telefono_responsable;
        $this->Pais = $pais_responsable;
        $this->IdPais = $id_pais;
        $this->Localidad = $localidad_responsable;
        $this->IdLocalidad = $id_localidad;
        $this->Provincia = $provincia_responsable;
        $this->IdProvincia = $id_provincia;
        $this->Direccion = $direccion_responsable;
        $this->Coordenadas = $coordenadas;
        $this->CodigoPostal = $codigo_postal;
        $this->ResponsableChagas = $responsable_chagas;
        $this->Laboratorio = $laboratorio_responsable;
        $this->IdLaboratorio = $id_laboratorio;
        $this->Activo = $activo;
        $this->Observaciones = $observaciones;
        $this->Usuario = $usuario;
        $this->FechaAlta = $fecha_alta;
        $this->NivelCentral = $nivel_central;

    }

    /**
     * Método que retorna la id del responsable a partir de su mail
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param string $mail - la dirección de correo ya verificada
     * @return int - clave del responsable
     */
    public function getIdMail($mail){

        // inicializamos las variables
        $id_responsable = 0;

        // componemos y ejecutamos la consulta
        $consulta = "SELECT cce.responsables.ID AS id_responsable
                     FROM cce.responsables
                     WHERE cce.responsables.E_MAIL = '$mail';";

        // obtenemos el registro
        $resultado = $this->Link->query($consulta);
        $registro = array_change_key_case($resultado->fetch(PDO::FETCH_ASSOC), CASE_LOWER);
        extract($registro);

        // retornamos la id
        return $id_responsable;

    }

    /**
     * Método que produce la consulta de actualización o inserción
     * según el caso y retorna la id del registro afectado
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @return int
     */
    public function grabaResponsable(){

        // si está editando
        if ($this->IdResponsable != 0){
            $this->editaResponsable();
        } else {
            $this->nuevoResponsable();
         }

        // retornamos la id
        return $this->IdResponsable;

    }

    /**
     * Método que inserta un nuevo registro en la tabla de responsables
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     */
    protected function nuevoResponsable(){

        // generamos la contraseña al azar
        $this->Password = $this->randomString();

        // compone la consulta de inserción
        $consulta = "INSERT INTO cce.responsables
                                (NOMBRE,
                                 INSTITUCION,
                                 CARGO,
                                 E_MAIL,
                                 TELEFONO,
                                 PAIS,
                                 LOCALIDAD,
                                 DIRECCION,
                                 CODIGO_POSTAL,
                                 RESPONSABLE_CHAGAS,
                                 LABORATORIO,
                                 ACTIVO,
                                 NIVEL_CENTRAL,
                                 OBSERVACIONES,
                                 COORDENADAS,
                                 USUARIO,
                                 PASSWORD)
                                VALUES
                                (:nombre,
                                 :institucion,
                                 :cargo,
                                 :e_mail,
                                 :telefono,
                                 :pais,
                                 :localidad,
                                 :direccion,
                                 :codigo_postal,
                                 :responsable_chagas,
                                 :laboratorio,
                                 :activo,
                                 :nivelcentral,
                                 :observaciones,
                                 :coordenadas,
                                 :usuario,
                                 MD5(:pass));";

        // asignamos la consulta
        $psInsertar = $this->Link->prepare($consulta);

        // asignamos los parámetros de la consulta
        $psInsertar->bindParam(":nombre", $this->Nombre);
        $psInsertar->bindParam(":institucion", $this->Institucion);
        $psInsertar->bindParam(":cargo", $this->Cargo);
        $psInsertar->bindParam(":e_mail", $this->Mail);
        $psInsertar->bindParam(":telefono", $this->Telefono);
        $psInsertar->bindParam(":pais", $this->IdPais);
        $psInsertar->bindParam(":localidad", $this->IdLocalidad);
        $psInsertar->bindParam(":direccion", $this->Direccion);
        $psInsertar->bindParam(":codigo_postal", $this->CodigoPostal);
        $psInsertar->bindParam(":responsable_chagas", $this->ResponsableChagas);
        $psInsertar->bindParam(":laboratorio", $this->IdLaboratorio);
        $psInsertar->bindParam(":activo", $this->Activo);
        $psInsertar->bindParam(":nivelcentral", $this->NivelCentral);
        $psInsertar->bindParam(":observaciones", $this->Observaciones);
        $psInsertar->bindParam(":coordenadas", $this->Coordenadas);
        $psInsertar->bindParam(":usuario", $this->Usuario);
        $psInsertar->bindParam(":pass", $this->Password);

        // ejecutamos la edición
        $resultado = $psInsertar->execute();

        // si salió todo bien
        if ($resultado){

            // obtiene la id del registro insertado
            $this->IdResponsable = $this->Link->lastInsertId();

        // si hubo un error
        } else {

            // inicializa la clave y muestra el error
            $this->IdResponsable = 0;
            echo $resultado;

        }

    }

    /**
     * Método que edita el registro de la tabla de responsables
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     */
    protected function editaResponsable(){

        // compone la consulta de edición
        $consulta = "UPDATE cce.responsables SET
                            NOMBRE = :nombre,
                            INSTITUCION = :institucion,
                            CARGO = :cargo,
                            E_MAIL = :e_mail,
                            TELEFONO = :telefono,
                            PAIS = :pais,
                            LOCALIDAD = :localidad,
                            DIRECCION = :direccion,
                            CODIGO_POSTAL = :codigo_postal,
                            RESPONSABLE_CHAGAS = :responsable_chagas,
                            LABORATORIO = :laboratorio,
                            ACTIVO = :activo,
                            OBSERVACIONES = :observaciones,
                            NIVEL_CENTRAL = :nivel_central
                     WHERE cce.responsables.ID = :id_responsable;";

        // asignamos la consulta
        $psInsertar = $this->Link->prepare($consulta);

        // asignamos los parámetros de la consulta
        $psInsertar->bindParam(":nombre", $this->Nombre);
        $psInsertar->bindParam("institucion", $this->Institucion);
        $psInsertar->bindParam(":cargo", $this->Cargo);
        $psInsertar->bindParam(":e_mail", $this->Mail);
        $psInsertar->bindParam(":telefono", $this->Telefono);
        $psInsertar->bindParam(":pais", $this->IdPais);
        $psInsertar->bindParam(":localidad", $this->IdLocalidad);
        $psInsertar->bindParam(":direccion", $this->Direccion);
        $psInsertar->bindParam(":codigo_postal", $this->CodigoPostal);
        $psInsertar->bindParam(":responsable_chagas", $this->ResponsableChagas);
        $psInsertar->bindParam(":laboratorio", $this->IdLaboratorio);
        $psInsertar->bindParam(":activo", $this->Activo);
        $psInsertar->bindParam(":observaciones", $this->Observaciones);
        $psInsertar->bindParam(":nivel_central", $this->NivelCentral);
        $psInsertar->bindParam(":id_responsable", $this->IdResponsable);

        // ejecutamos la edición
        $resultado = $psInsertar->execute();

        // si hubo un error
        if (!$resultado){

            // inicializa la clave y muestra el error
            $this->IdResponsable = 0;
            echo $resultado;

        }

    }

    /**
     * Método que retorna el array con la nómina de responsables sin georreferenciar
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @return array
     */
     public function sinGeorreferenciar(){

         // componemos la consulta en la vista
         $consulta = "SELECT cce.vw_responsables.id AS id,
                             UPPER(cce.vw_responsables.responsable) AS responsable,
                             cce.vw_responsables.pais AS pais,
                             cce.vw_responsables.provincia AS provincia,
                             cce.vw_responsables.localidad AS localidad,
                             cce.vw_responsables.direccion AS direccion
                       FROM vw_responsables
                       WHERE vw_responsables.coordenadas = '' OR
                             ISNULL(vw_responsables.coordenadas) OR
                             vw_responsables.coordenadas = 'undefined' OR
                             vw_responsables.coordenadas = '(0.0,0.0)'
                       ORDER BY cce.vw_responsables.provincia,
                                cce.vw_responsables.responsable;";

        // obtenemos el array y retornamos
        $resultado = $this->Link->query($consulta);
        $fila = array_change_key_case($resultado->fetchAll(PDO::FETCH_ASSOC), CASE_LOWER);
        return $fila;

     }

    /**
     * Método que recibe como parámetros la id de un responsable y las
     * coordenadas gps, actualiza entonces la base de datos
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param int $id - clave del registro
     * @param string $coordenadas - conjunto de coordenadas gps
     */
    public function grabaCoordenadas($id, $coordenadas){

        // componemos la consulta y ejecutamos la consulta
        $consulta = "UPDATE cce.responsables SET
                            coordenadas = '$coordenadas'
                     WHERE cce.responsables.id = '$id';";
        $this->Link->exec($consulta);

        // retornamos
        return;

    }

    /**
     * Método que recibe como parámetro la clave indec de una
     * jurisdicción y retorna un array con las coordenadas y
     * gps y los datos del responsable de esa jurisdicción
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param string $provincia - clave indec de la jurisdicción
     */
    public function getCoordenadasResponsable($provincia){

        // componemos la consulta
        $consulta = "SELECT cce.vw_responsables.responsable AS responsable,
                            cce.vw_responsables.cargo AS cargo,
                            cce.vw_responsables.direccion AS direccion,
                            cce.vw_responsables.localidad AS localidad,
                            cce.vw_responsables.coordenadas AS coordenadas
                      FROM cce.vw_responsables
                      WHERE cce.vw_responsables.idprovincia = '$provincia' AND
                            cce.vw_responsables.responsable_chagas = 'Si' AND
                            cce.vw_responsables.activo = 'Si' AND
                            cce.vw_responsables.nivelcentral = 'No'
                      LIMIT 1;";

        $resultado = $this->Link->query($consulta);
        $fila = array_change_key_case($resultado->fetch(PDO::FETCH_ASSOC), CASE_LOWER);
        return $fila;

    }

    /**
     * Método protegido que genera una cadena de caracteres al azar
     * utilizado en el alta de responsables y la generación de
     * nueva contraseña
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @return string
     */
    public function randomString(){

        // determinamos la longitud de la cadena e inicializamos
        // la cadena de retorno
        $longitud = 6;
        $randomString = '';

        // asignamos los caracteres
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';

        // obtenemos la longitud de la cadena
        $charactersLength = strlen($characters);

        // recorremos la cadena de caracteres
        for ($i = 0; $i < $longitud; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }

        // lo asignamos también a la variable de clase para que }
        // el sistema de correo pueda obtenerla
        $this->Password = $randomString;

        // retornamos la cadena
        return $randomString;

    }

}

?>