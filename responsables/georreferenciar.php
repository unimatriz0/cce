<?php

/**
 *
 * responsables/georreferenciar.php
 *
 * @package     CCE
 * @subpackage  Responsables
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.4.0 (14/08/2018)
 * @copyright   Copyright (c) 2018, INP
 *
 * Procedimiento que presenta la grilla con los responsables sin
 * georreferenciar
*/

// inclusión de archivos
require_once("responsables.class.php");
$responsables = new Responsables();

// obtenemos la nómina de laboratorios sin georreferenciar
$georreferenciar = $responsables->sinGeorreferenciar();

// si hubo registros
if (count($georreferenciar) != 0){

    // definimos la tabla
    echo "<table width='90%' align='center' border='0' id='geolaboratorios'>";

    // definimos los encabezados
    echo "<thead>";
    echo "<tr>";
    echo "<td>País</td>";
    echo "<td>Jurisdicción</td>";
    echo "<td>Responsable</td>";
    echo "<td>Dirección</td>";
    echo "<td>Coordenadas</td>";
    echo "<td></td>";
    echo "</tr>";
    echo "</thead>";

    // definimos el cuerpo
    echo "<tbody>";

    // recorremos el vector
    foreach($georreferenciar AS $registro){

        // obtenemos el registro
        extract($registro);

        // abrimos la fila
        echo "<tr>";

        // lo presentamos
        echo "<td>$pais</td>";
        echo "<td>$provincia</td>";
        echo "<td>$responsable</td>";
        echo "<td>$direccion</td>";

        // definimos un imput para las coordenadas
        echo "<td>";
        echo "<input type='text'
                     name='nuevacoordresponsable'
                     id='$id'
                     size='15'
                     title='Coordenadas GPS'
                     readonly>";
        echo "</td>";

        // componemos el domicilio
        $domicilio = $direccion . " - " . $localidad . " - " . $provincia . " - " . $pais;

        // el botón georreferenciar
        echo "<td>";
        echo "<input type='button'
                     name='btnCoordenadas'
                     id='btnCoordenadas'
                     class='botonbuscar'
                     title='Busca las coordenadas GPS'
                     onClick='responsables.detectaResponsables(" . chr(34) . $id . chr(34) . ", " . chr(34) . $domicilio . chr(34) . ")'>";
        echo "</td>";

        // cerramos la fila
        echo "</tr>";

    }

    // cerramos la tabla
    echo "</tbody></table>";

    // define el div para el paginador
    echo "<div class='paging'></div>";

// si están todos georreferenciados
} else{

    // presenta el mensaje
    echo "<h2>Todos los responsables están georreferenciados</h2>";

}
?>
<SCRIPT>

    // definimos las propiedades de la tabla
     $('#geolaboratorios').datatable({
        pageSize: 15,
        sort:    [true,     true,     true, true, false, false],
        filters: ['select', 'select', true, true, false, false],
        filterText: 'Buscar ... '
    });

</SCRIPT>
