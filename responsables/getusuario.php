<?php

/**
 *
 * responsables/getusuario.php
 *
 * @package     CCE
 * @subpackage  Responsables
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.4.0 (14/08/2018)
 * @copyright   Copyright (c) 2018, INP
 *
 * Procedimiento que recibe por get la clave de un responsable y
 * obtiene los datos del registro que retorna en un array json
 *
*/

// inclusión de clases
require_once ("responsables.class.php");

// instanciamos la clase
$responsable = new Responsables();

// obtenemos el registro
$responsable->setIdResponsable($_GET["id"]);
$responsable->getResponsable();

// ahora componemos la matriz json y la enviamos
echo json_encode(array("ID" =>                $responsable->getIdResponsable(),
                       "Nombre" =>            $responsable->getNombre(),
                       "Institucion" =>       $responsable->getInstitucion(),
                       "Cargo" =>             $responsable->getCargo(),
                       "Mail" =>              $responsable->getMail(),
                       "Telefono" =>          $responsable->getTelefono(),
                       "IdLocalidad" =>       $responsable->getIdLocalidad(),
                       "IdProvincia" =>       $responsable->getIdProvincia(),
                       "IdPais" =>            $responsable->getIdPais(),
                       "Direccion" =>         $responsable->getDireccion(),
                       "Coordenadas" =>       $responsable->getCoordenadas(),
                       "CodigoPostal" =>      $responsable->getCodigoPostal(),
                       "ResponsableChagas" => $responsable->getResponsableChagas(),
                       "Laboratorio" =>       $responsable->getLaboratorio(),
                       "IdLaboratorio" =>     $responsable->getIdLaboratorio(),
                       "IdLaboratorio" =>     $responsable->getIdLaboratorio(),
                       "Activo" =>            $responsable->getActivo(),
                       "Observaciones" =>     $responsable->getObservaciones(),
                       "Usuario" =>           $responsable->getUsuario(),
                       "FechaAlta" =>         $responsable->getFechaAlta(),
                       "NivelCentral" =>      $responsable->getNivelCentral()));

?>
