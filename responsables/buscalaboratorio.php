<?php

/**
 *
 * responsables/buscalaboratorio.php
 *
 * @package     CCE
 * @subpackage  Responsables
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.4.0 (14/08/2018)
 * @copyright   Copyright (c) 2018, INP
 *
 * Procedimiento utilizada en el abm de usuarios, recibe como parámetros
 * parte del nombre del laboratorio, la jurisdicción y el país, busca los
 * laboratorios que coinciden y arma la grilla con los mismos
 *
*/

// incluimos e instanciamos la clase
require_once ("../laboratorios/laboratorios.class.php");
$laboratorios = new Laboratorios();

// buscamos los laboratorios
$resultado = $laboratorios->listaLaboratorios($_GET["pais"], $_GET["provincia"], $_GET["laboratorio"]);

// si no hubo resultados
if (count($resultado) == 0){

    // presenta el mensaje
    echo "<b>No hay registros coincidentes</b>";

// si encontró
} else {

    // definimos la tabla
    echo "<table width='95%' align='center' border='0'>";

    // definimos los encabezados
    echo "<thead>";
    echo "<tr>";
    echo "<th>Laboratorio</th>";

    // definimos el cuerpo de la tabla
    echo "<tbody>";

    // recorremos el vector
    foreach($resultado AS $registro){

        // obtenemos el registro
        extract($registro);

        // lo presentamos y armamos el enlace
        echo "<tr>";
        echo "<td>";
        echo "<a href='#'
               onClick='responsables.asignaLaboratorio($idlaboratorio, " . chr(34) . $laboratorio . chr(34) . ")'>";
        echo $laboratorio;
        echo "</a>";
        echo "</td>";
        echo "</tr>";

    }

    // cerramos la tabla
    echo "</tbody>";
    echo "</table>";

}

?>