<?php

/**
 *
 * tecnicas/lista_tecnicas.php
 *
 * @package     CCE
 * @subpackage  Tecnicas
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.4.0 (07/01/2018)
 * @copyright   Copyright (c) 2017, INP
 *
 * Método que retorna un array json con la nómina completa
 * de tecnicas declaradas
*/

// incluimos e instanciamos las clases
require_once("tecnicas.class.php");
$tecnicas = new Tecnicas();

// obtenemos la nómina
$nomina = $tecnicas->nominaTecnicas();

// declaramos la matriz
$jsondata = array();

// recorremos el vector
foreach($nomina AS $registro){

    // obtenemos el registro
    extract($registro);

    // lo agregamos a la matriz
    $jsondata[] = array("Id" => $id_tecnica,
                        "Tecnica" => $tecnica);

}

// retornamos el vector
echo json_encode($jsondata);

?>