/*

 Nombre: tecnicas.js
 Fecha: 23/12/2016
 Autor: Lic. Claudio Invernizzi
 E-Mail: cinvernizzi@gmail.com
 Licencia: GPL
 Producido en: INP - Dr. Mario Fatala Chaben
 Buenos Aires - Argentina
 Comentarios: serie de funciones utilizadas para el abm de técnicas diagnósticas

 */

 /**
 *
 * Atención, utilizamos el nuevo estandard definido en ECMAScript 2015 (ES6)
 * este permite definir clases con el mètodo ya utilizado en php y java
 * como creo que es mas claro lo utilizaremos, la compatibilidad está
 * anunciada para firefox y chrome así que no deberìa haber problemas
 *
 */

/* jshint esversion: 6 */

/**
 * @author Claudio Invernizzi <cinvernizzi@gmail.com>
 * @class Clase que controla las operaciones sobre el formulario de
 * técnicas registradas
 */
class Tecnicas {

    /**
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * @constructor
     */
    constructor(){

        // inicializamos las variables
        this.initTecnicas();

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que inicializa las variables de clase
     */
    initTecnicas(){

        // inicializamos las variables
        this.IdTecnica = 0;                // clave del registro
        this.Tecnica = "";                 // nombre de la técnica
        this.Correcto = true;              // switch de técnica repetida

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que carga el formulario de técnicas en el layer
     */
    formTecnicas(){

        // reiniciamos el contador
        cce.contador();

        // carga el formulario
        $("#form_administracion").load("tecnicas/form_tecnicas.php");

    }

    /**
     * Método llamado por callback que verifica no se encuentre
     * repetida la técnica
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     */
    validaTecnica(tecnicaCorrecta){

        // inicializamos las variables
        this.Correcto = false;

        // llamamos la rutina php
        $.ajax({
            url: 'tecnicas/verifica_tecnica.php?tecnica='+tecnicas.Tecnica,
            type: "GET",
            cahe: false,
            contentType: false,
            processData: true,
            dataType: 'json',
            async: false,
            success: function(data) {

                // si no encontró
                if (data.Registros == 0){
                    tecnicas.Correcto = true;
                } else {
                    tecnicas.Correcto = false;
                }

            }});

        // retornamos
        tecnicaCorrecta(this.Correcto);

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param {int} idtecnica - clave del registro
     * Método que verifica los datos del formulario antes de
     * enviarlo al servidor, si no recibe la clave asume que
     * se trada de un alta
     */
    verificaTecnica(idtecnica){

        // declaración de variables
        var mensaje;

        // reiniciamos el contador
        cce.contador();

        // si está danto un alta
        if (typeof(idtecnica) == "undefined"){
            idtecnica = "nueva";
        } else {
            this.IdTecnica = idtecnica;
        }

        // verifica se halla declarado nombre
        if (document.getElementById("tecnica_" + idtecnica).value == ""){

            // presenta el mensaje y retorna
            mensaje = "Debe ingresar el nombre de la técnica";
            new jBox('Notice', {content: mensaje, color: 'red'});
            document.getElementById("tecnica_" + idtecnica).focus();
            return false;

        // si ingresó
        } else {

            // asignamos en la variable de clase
            this.Tecnica = document.getElementById("tecnica_" + idtecnica).value;

        }

        // si está dando un alta
        if (this.IdTecnica == 0){

            // verificamos por callback
            this.validaTecnica(function(tecnicaCorrecta){

                // si está repetido
                if (!tecnicas.Correcto){

                    // presenta el mensaje
                    mensaje = "Esa tecnica ya se encuentra declarada !!!";
                    new jBox('Notice', {content: mensaje, color: 'red'});

                }

            });

        }

        // grabamos
        this.grabaTecnica();

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que envía los datos al servidor para grabarlos
     */
    grabaTecnica(){

        // si el registro está repetido
        if (!this.Correcto){
            return false;
        }

        // implementa el formulario usando objetos html5
        var formulario = new FormData();

        // si está editando
        if (this.IdTecnica != 0){
            formulario.append("Id", this.IdTecnica);
        }

        // agrega la técnica convirtiéndola a mayúsculas
        formulario.append("Tecnica", this.Tecnica.toUpperCase());

        // serializa el formulario y lo envía, recibe
        // como respuesta el resultado de la operación
        $.ajax({
            url: "tecnicas/graba_tecnica.php",
            type: "POST",
            data: formulario,
            cahe: false,
            contentType: false,
            processData: false,
            success: function(data) {

                // si salío todo bien
                if (data.Id != 0){

                    // presenta el mensaje
                    new jBox('Notice', {content: "Registro grabado ... ", color: 'green'});

                    // reinicia las variables de clase
                    tecnicas.initTecnicas();

                    // recarga el formulario para reflejar los cambios
                    tecnicas.formTecnicas();

                // si hubo algún error
                } else {

                    // presenta la alerta
                    new jBox('Notice', {content: "Ha ocurrido un error, intente nuevamente", color: 'red'});

                }

            },

        });

    }

    /**
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param {string} idelemento - id de un objeto html
     * @param {int} idtecnica - clave a predeterminar
     * Método que recibe como parámetro la id de un select de un formulario
     * carga en ese elemento la nómina de tecnicas, si además recibe
     * la clave de un elemento, lo predetermina
     */
    listaTecnicas(idelemento, idtecnica){

        // limpia el combo
        $("#" + idelemento).html('');

        // obtenemos la nómina
        $.ajax({
            url: 'tecnicas/lista_tecnicas.php',
            type: "GET",
            cahe: false,
            contentType: false,
            processData: false,
            dataType: 'json',
            success: function(data) {

                // agrega el primer elemento en blanco
                $("#" + idelemento).append("<option value='0'>Seleccione</option>");

                // recorre el vector de resultados
                for(var i=0; i < data.length; i++){

                    // si recibió la clave
                    if (typeof(idtecnica) != "undefined"){

                        // si coindicen
                        if (idtecnica == data[i].Id){

                            // lo agrega preseleccionado
                            $("#" + idelemento).append("<option selected value='" + data[i].Id + "'>" + data[i].Tecnica + "</option>");

                        // si no coincide
                        } else {

                            // simplemente lo agrega
                            $("#" + idelemento).append("<option value='" + data[i].Id + "'>" + data[i].Tecnica + "</option>");

                        }

                    // si no recibió
                    } else {

                        // simplemente lo agrega
                        $("#" + idelemento).append("<option value='" + data[i].Id + "'>" + data[i].Tecnica + "</option>");

                    }

                }

            }});

    }

}