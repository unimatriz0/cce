<?php

/**
 *
 * tecnicas/form_tecnicas.php
 *
 * @package     CCE
 * @subpackage  Técnicas
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.4.0 (08/08/2018)
 * @copyright   Copyright (c) 2018, INP
 *
 * Método que arma el formulario para el abm de técnicas
 *
*/

// inclusión e implementación de las clases
require_once ("tecnicas.class.php");
$tecnicas = new Tecnicas();

// obtenemos la nómina de las técnicas
$nomina = $tecnicas->nominaTecnicas();

// definimos la tabla
echo "<table width='60%' align='center' border='0'>";

// definimos los encabezados
echo "<thead>";
echo "<tr>";
echo "<th>Técnica</th>";
echo "<th>Alta</th>";
echo "<th>Usuario</th>";
echo "<th></th>";
echo "</tr>";
echo "</thead>";

// definimos el cuerpo
echo "<tbody>";

// iniciamos un bucle recorriendo el vector
foreach ($nomina AS $registro){

    // obtenemos el registro
    extract($registro);

    // si es distinto de otra
    if ($tecnica != "OTRA"){

        // abrimos la fila
        echo "<tr>";

        // presentamos el registro para edición
        echo "<td align='center'>";
        echo "<span class='tooltip'
               title='Nombre de la técnica'>";
        echo "<input type='text' name='tecnica'
               id='tecnica_$id_tecnica'
               size='23'
               value='$tecnica'>";
        echo "</span>";
        echo "</td>";

        // fecha de alta y usuario simplemente lo presentamos
        echo "<td align='center'>$fecha_alta</td>";
        echo "<td align='center'>$usuario</td>";

        // agregamos el enlace para grabar
        echo "<td align='center'>";
        echo "<input type='button'
               name='btnGrabaTecnica'
               id='btnGrabaTecnica'
               onClick='tecnicas.verificaTecnica($id_tecnica)'
               class='botongrabar'>";
        echo "</td>";

        // cerramos la fila
        echo "</tr>";

    }

}

// agregamos una fila para insertar un nuevo registro
echo "<tr>";

    // presentamos el registro
    echo "<td align='center'>";
    echo "<input type='text' name='tecnica'
           id='tecnica_nueva'
           size='23'>";
    echo "</td>";

    // obtenemos la fecha actual
    $fecha_alta = date("d/m/Y");

    // fecha de alta y usuario simplemente lo presentamos
    echo "<td align='center'>";
    echo "<input type='text'
                 name='alta_tecnica'
                 id='alta_tecnica'
                 size='12'
                 readonly>";
    echo "</td>";
    echo "<td align='center'>";
    echo "<input type='text'
                 name='usuario_tecnica'
                 id='usuario_tecnica'
                 size='12'
                 readonly>";
    echo "</td>";

    // agregamos el enlace para grabar
    echo "<td align='center'>";
    echo "<span class='tooltip'
           title='Inserta la técnica en la base'>";
    echo "<input type='button' name='btnNuevaTecnica'
           id='btnNuevaTecnica'
           onClick='tecnicas.verificaTecnica()'
           class='botonagregar'>";
    echo "</span>";
    echo "</td>";
echo "</tr>";

// cerramos la tabla
echo "</tbody>";
echo "</table>";

?>
<SCRIPT>

    // instanciamos los tooltips
    new jBox('Tooltip', {
      attach: '.tooltip',
      theme: 'TooltipBorder'
    });

    // fijamos los valores del nuevo registro
    document.getElementById("alta_tecnica").value = fechaActual();
    document.getElementById("usuario_tecnica").value = sessionStorage.getItem("Usuario");

</SCRIPT>