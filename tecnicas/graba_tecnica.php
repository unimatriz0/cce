<?php

/**
 *
 * tecnicas/graba_tecnica.php
 *
 * @package     CCE
 * @subpackage  Técnicas
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.4.0 (08/08/2018)
 * @copyright   Copyright (c) 2018, INP
 *
 * Método que recibe por post los datos del formulario y
 * ejecuta la consulta en la base
 *
*/

// inclusión e instanciación
require_once ("tecnicas.class.php");
$tecnica = new Tecnicas();

// si recibió la id
if (!empty($_POST["Id"])){
    $tecnica->setIdTecnica($_POST["Id"]);
}

// establecemos el nombre
$tecnica->setTecnica($_POST["Tecnica"]);

// grabamos el registro
$id = $tecnica->grabaTecnica();

// retornamos el estado de la operación
echo json_encode(array("Id" => $id));

?>