<?php

/**
 *
 * tecnicas/verifica_tecnica.php
 *
 * @package     CCE
 * @subpackage  Técnicas
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.4.0 (08/08/2018)
 * @copyright   Copyright (c) 2018, INP
 *
 * Método que recibe el nombre de una técnica por get y verifica
 * que el nombre de una técnica no se encuentre repetido
 *
*/

// incluimos e instanciamos la clase
require_once ("tecnicas.class.php");
$tecnica = new Tecnicas();

// verificamos si existe
$registros = $tecnica->verificaTecnica($_GET["tecnica"]);

// retornamos el estado de la operación
echo json_encode(array("Registros" => $registros));

?>